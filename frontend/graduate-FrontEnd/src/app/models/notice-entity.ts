import { NoticeBrowseModel } from "app/models/NoticeBrowseModel";

export class NoticeEntity {
    id: string;
    title: string;
    noticeContent:  string;
    columnId: string;
    publisherId: string;
    columnName: string;
    remark:  string;
    operator: string;
    operatorId: string;
	/**
	 *
	 * 时间戳
	 */
    timestampTime:Date;
    createTime: string;
    updateTime: string;

    //浏览人信息
    browsePersons:NoticeBrowseModel[];
  }
  