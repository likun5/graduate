export class SerachRegionInfo{
    id:string;
    name:string;//学员姓名
    proviceId:string;//省份id
    proviceName:string;//省份名称
    cityId:string;//城市id
    cityName:string;//城市名称
    countyId:string;//县区id
    countyName:string;//县区名称
    dictId:string;//字典id
    dictName:string;//字典name
    regionId:string;//地域条件的id也是字典的id
    regionName:string;//地域条件的名称也是字典的name
    termId:string;//期数的id也是字典的id
    termName:string;//期数的名称也是字典的name
    sex:string;//性别
    email:string;//邮箱
    
}