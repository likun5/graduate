export class NoticeCategoryEntity {
    id: string;
    columnName: string;
    remark:  string;
    operator: string;
    operatorId: string;
	/**
	 *
	 * 时间戳
	 */
    timestampTime:Date;
    createTime: string;
    updateTime: string;

    
  }
  
