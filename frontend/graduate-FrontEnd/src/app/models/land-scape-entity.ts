export class LandScapeEntity {
	/**
	 * 风景id
	 */
	id: string;
    /**
	 *
	 * 用户id
	 */
	userId: string;
	/**
	 *
	 * 景区名称
	 */
	name: string;
	/**
	 *
	 * 描述
	 */
	description: string;
	/**
	 * 地域id
	 */
	regionId: string;
	/**
	 * 风景地址
	 */
	address: string;
	/**
	 *
	 * 时间戳
	 */
	timestampTime: Date;

	operatorId: string;
	remark: string;
	createTime: string;
	updateTime: string;
	operator: string;
}