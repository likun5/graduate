export class UserModel {
    id: string;
    email:string;
    telNum: string;
    userCode: string;
    password: string;
    userRealName:string;
    schoolNo: string;
    userType: string;
    remark: string;
    createTime:string;
    updateTime:string;
    operator: string;
    idDelete:any;
    registuserId: string;
    rememberMe:boolean;
    token:string;
}
