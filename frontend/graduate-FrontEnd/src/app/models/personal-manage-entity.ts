export class PersonalManageEntity {
    id:String;//毕业生id
    loginId:String;//登录id
    name: String; // 毕业生姓名
    sex: String; // 性别
    grade: String; // 期数
    companyName: String; // 公司
    email: String; // 邮箱
    salaryRange: String; // 月薪范围
    salary:String;//月薪
    education: String; //学历
}
