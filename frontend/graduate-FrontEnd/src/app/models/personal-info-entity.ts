export class PersonalInfoEntity {
	/**
	 * 用户id
	 */
	id: string;
	/**
	 *
	 * 登录ID
	 */
	loginId: string;
	/**
	 *
	 * 用户姓名
	 */
	name: string;
	/**
	 *
	 * 英文名
	 */
	englishName: string;
	/**
	 *
	 * 期数
	 */
	grade: string;
	/**
	 *
	 * 性别
	 */
	sex: string;
	/**
	 *
	 * 出生年月
	 */
	birthday: Date;
	/**
	 *
	 * 邮箱
	 */
	email: string;
	/**
	 *
	 * 常用电话
	 */
	phone: string;
	/**
	 *
	 * 紧急联系人电话
	 */
	emergPhone: string;
	/**
	 *
	 * 紧急联系人姓名
	 */
	emergName: string;
	/**
	 *
	 * 紧急联系人与自己的关系
	 */
	emergRelation: string;
	/**
	 *
	 * 微信号
	 */
	wechat: string;
	/**
	 *
	 * QQ号
	 */
	qq: string;
	/**
	 *
	 * 进入提高班时间
	 */
	enterDmtTime: string;
	/**
	 *
	 * 提高班毕业时间
	 */
	graduateDmtTime: string;
	/**
	 *
	 * 进入大学时间
	 */
	enterCollegeTime: Date;
	/**
	 *
	 * 大学毕业时间
	 */
	graduateCollegeTime: Date;

	provinceId: string;//省份id
	proviceName: string;//省份名称
	cityId: string;//城市id
	cityName: string;//城市名称
	countyOrDistrictId: string;//县区id
	countyOrDistrictName: string;//县区名称

	provinceNativeId: string; // 籍贯省份id
	provinceNativeName: string;//籍贯省份名称
	cityNativeId: string;//籍贯城市id
	cityNativeName: string;//籍贯城市名称
	countyOrDistrictNativeId: string;//籍贯县区id
	countyOrDistrictNativeName: string;//籍贯县区名称
	/**
	 *
	 * 籍贯
	 */
	nativePlace: string;
	/**
	 *
	 * 住所地址
	 */
	address: string;
	/**
	 *
	 * 擅长领域/自我介绍
	 */
	professionalField: string;
	/**
	 *
	 * 操作人名字
	 */
	operatorName: string;
	/**
	 *
	 * 操作人ID
	 */
	operatorId: string;
	/**
	 *
	 * 时间戳
	 */
	timestampTime: Date;
	remark: string;
	createTime: string;
	updateTime: string;
	operator: string;
}