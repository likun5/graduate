export class BrowserInfoEntity {
	id: string;
	operator: string;
	//isDelete: string; // ?
	remark: string;
	createTime: string;
	updateTime: Date;
	loginId: string;
	name: string;
	englishName: string;
    grade: string;
    
	sex: string;
	birthday: string;
	email: string;
	phone: string;
	emergPhone: string;
    emergName: string;
    
	emergRelation: string;
	wechat: string;
	qq: string;//省份id
	enterDmtTime: Date;//省份名称
    graduateDmtTime: Date;//城市id  
    graduateCollegeTime: Date;//城市名称


	//nativeRegionId: string;//县区id  ?
	nativePlace: string;//县区名称
    //regionId: string; // 籍贯省份id    ?
    
	address: string;//籍贯省份名称
	professionalField: string;//籍贯城市id
	operatorId: string;//籍贯城市名称
	timestampTime: Date;//籍贯县区id
}