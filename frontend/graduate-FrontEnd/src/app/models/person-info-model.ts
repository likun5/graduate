import { PersonalInfoEntity } from "app/models/personal-info-entity";

export class PersonInfoModel extends PersonalInfoEntity {
	/**
	 * 个人头像图片
	 */
	pictureUrl: string;
	emergRelationName:string;
	gradeName:string;
}