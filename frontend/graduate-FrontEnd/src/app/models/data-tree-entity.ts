export class DataTreeEntity {
	id: string;
	pId: string;
	name: string;
	url: string;
	open: Boolean;
	checked: Boolean;
	leaf: Boolean;
	// leafq: boolean;
}