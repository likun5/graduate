export class EducationEntity{
	/**
	 *学历id
	 */
	id:string;
    /**
	 *
	 * 用户id
	 */
	    userId:string;
	/**
	 *
	 * 是否进入大米时代（0/1:前/后）
	 */
	    isIndmt:Number;
	/**
	 *
	 * 证书编号
	 */
	    certificateNo:string;
	/**
	 *
	 * 证书名
	 */
	    certificateName:string;
	/**
	 *
	 * 证书类型
	 */
	    certificateType:number;
	/**
	 *
	 * 获取时间
	 */
	    receiveTime:string;
	/**
	 *
	 * 时间戳
	 */
	    timestampTime:Date;
	/**
	* 大学名称
	* 		
	*/
		universityName:string;

    operatorId:string;
    remark:  string;
    createTime: string;
    updateTime: string;
    operator:  string;
	pictureUrls:string[];
}