export class SkillPointEntity{
	/**
	 * 技术点id
	 */
	id:string;
    /**
	 *
	 * 技术名称
	 */
	    techName:string;
	/**
	 *
	 * 父id
	 */
	pid:string;
	// pId:string;
	/**
	 *
	 * 时间戳
	 */
	timestampTime:Date;
    userId:string;
    companyId:string;
    operatorId:string;
    remark: string;
    createTime: string;
    updateTime:string;
    operator: string;
}