export class DictionaryModel{
    id:string;
    dictName:string;
    typeCode:string;
    dictTypeName:string;
    operator:string;
    operatorId:string;
    isDelete:number;
    remark:string;
    updateTime:any;
    createTime:any;
    
}