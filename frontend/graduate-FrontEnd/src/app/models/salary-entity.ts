import { CompanyModel } from "app/models/CompanyModel";
import { CompanyEntity } from "app/models/company-entity";


export class SalaryEntity {

    /**
	 *
	 * 薪资id
	 */
	id: string;
	/**
	 *
	 * 用户公司关联关系id
	 */
	pcRelationId: string;
	/**
	 *
	 * 薪资变化时间
	 */
	salaryChangeTime: string;
	/**
	 *
	 * 薪资
	 */
	salary: number;
	/**
	 *
	 * 每年变化次数
	 */
	timesPerYear: number;
	/**
	 *
	 * 职位
	 */
	possession: string;
	/**
	 *
	 * 年薪
	 */
	annualSalary: string;

	/**
	* 是否删除
	* 		
	*/
	isDelete: number;
	/**
	 * 公司Model
	 * 
	 */
	companyEntity: CompanyEntity;
	/**
	* 公司名称
	* 
	*/
	companyName: String;

	operatorId: string;
	remark: string;
	createTime: string;
	updateTime: string;
	operator: string;

}