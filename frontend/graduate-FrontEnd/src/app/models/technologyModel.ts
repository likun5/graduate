import { SkillPointEntity } from "app/models/skill-point-entity";

export class TechnologyModel {
    techid : string;
    techName : string;
    skillPointEntityList : SkillPointEntity[];
    
}