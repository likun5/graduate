import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { personInfoRoutes } from './person-info.routes';
import { SearchPersonInfoComponent } from './search-person-info/search-person-info.component';
import { DataTableModule } from 'ng-itoo-datatable';
import { FileUploadModule } from 'ng2-file-upload';
import { PersonInfoComponent } from './person-info.component';
import { AddPersonInfoModule } from './add-person-info/add-person-info.module';
import {SearchPersonInfoModule} from './search-person-info/search-person-info.module';
import { FormsModule } from '@angular/forms';

import { ButtonModule,ConfirmDialogModule,DialogModule } from 'primeng/primeng';

@NgModule({
  imports: [
    DataTableModule,
    FileUploadModule,
    CommonModule,
    AddPersonInfoModule,
    SearchPersonInfoModule,
    FormsModule,
    DialogModule,
    ButtonModule,
    ConfirmDialogModule,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(personInfoRoutes) //路由模块

  ],
  declarations: [PersonInfoComponent]
})
export class PersonInfoModule { }
