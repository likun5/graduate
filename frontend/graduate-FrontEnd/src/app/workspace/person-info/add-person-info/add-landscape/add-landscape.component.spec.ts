import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLandscapeComponent } from './add-landscape.component';

describe('AddLandscapeComponent', () => {
  let component: AddLandscapeComponent;
  let fixture: ComponentFixture<AddLandscapeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLandscapeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLandscapeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
