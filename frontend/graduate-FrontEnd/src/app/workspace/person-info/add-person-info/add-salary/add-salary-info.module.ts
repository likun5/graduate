import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPersonInfoComponent } from '../add-person-info.component';
import { AddBasicInfoComponent } from '../add-basic-info/add-basic-info.component';
import { AddCompanyComponent } from '../add-company/add-company.component';
import { AddEducationComponent } from '../add-education/add-education.component';
import { AddSalaryComponent } from '../add-salary/add-salary.component';
import { AddHomeComponent } from '../add-home/add-home.component';

import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { addPersonInfoRoutes } from '../add-person-info.routes';
import { DataTableModule } from 'ng-itoo-datatable';
import { FileUploadModule } from 'ng2-file-upload';
import { ButtonModule, GrowlModule,ConfirmDialogModule,ConfirmationService,DialogModule } from 'primeng/primeng';

@NgModule({
  imports: [   

    CommonModule,
    NgZorroAntdModule,    
    ButtonModule,
    GrowlModule,
    ConfirmDialogModule,
    DialogModule,
    FileUploadModule,
    DataTableModule,
    FileUploadModule,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(addPersonInfoRoutes) //路由模块
  ],
  declarations: [AddPersonInfoComponent, AddBasicInfoComponent, AddCompanyComponent, AddEducationComponent, AddSalaryComponent, AddHomeComponent]
})
export class AddPersonInfoModule { }
