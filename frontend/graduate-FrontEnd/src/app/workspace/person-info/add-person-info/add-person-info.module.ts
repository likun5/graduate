import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPersonInfoComponent } from './add-person-info.component';
import { AddBasicInfoComponent } from './add-basic-info/add-basic-info.component';
import { AddCompanyComponent } from './add-company/add-company.component';
import { AddEducationComponent } from './add-education/add-education.component';
import { AddSalaryComponent } from './add-salary/add-salary.component';
import { AddHomeComponent } from './add-home/add-home.component';

import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { addPersonInfoRoutes } from './add-person-info.routes';
import { DataTableModule } from 'ng-itoo-datatable';
import { FileUploadModule } from 'ng2-file-upload';

import { FormsModule } from '@angular/forms';
import { ButtonModule, GrowlModule,ConfirmDialogModule,ConfirmationService,DialogModule } from 'primeng/primeng';

import { InterceptorService } from '../../../share/interceptor.service'
// 表单验证
import {CustomFormsModule} from "ng2-validation";
import {TabMenuModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';

import {StepsModule,MenuItem} from 'primeng/primeng';
import {CheckboxModule} from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';
import { AddLandscapeComponent } from './add-landscape/add-landscape.component';
import { AddLocalProductComponent } from './add-local-product/add-local-product.component';


@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    FileUploadModule,
    FormsModule,
    CustomFormsModule,
    CalendarModule,

    PanelModule,
    DialogModule,//弹框
    ButtonModule,//弹框
    TabMenuModule,
    ConfirmDialogModule,
    GrowlModule,
    StepsModule,
    CheckboxModule,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(addPersonInfoRoutes) //路由模块
  ],
  declarations: [AddPersonInfoComponent, AddBasicInfoComponent, AddCompanyComponent, AddEducationComponent, AddSalaryComponent, AddHomeComponent, AddLandscapeComponent, AddLocalProductComponent],
providers:[ConfirmationService, MessageService]
})
export class AddPersonInfoModule { }
