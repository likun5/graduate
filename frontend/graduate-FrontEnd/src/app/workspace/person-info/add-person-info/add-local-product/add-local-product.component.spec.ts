import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLocalProductComponent } from './add-local-product.component';

describe('AddLocalProductComponent', () => {
  let component: AddLocalProductComponent;
  let fixture: ComponentFixture<AddLocalProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLocalProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLocalProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
