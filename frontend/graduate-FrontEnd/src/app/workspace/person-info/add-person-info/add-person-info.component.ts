import { Component, OnInit } from '@angular/core';
import { StepsModule, MenuItem } from 'primeng/primeng';
import { Router } from '@angular/router';

@Component({
    selector: 'app-add-person-info',
    templateUrl: './add-person-info.component.html',
    styleUrls: ['./add-person-info.component.css']
})
export class AddPersonInfoComponent implements OnInit {

    items: MenuItem[];
    //   activeIndex: number = 0;
    activeIndexStr: string = localStorage.getItem('activeIndexStr');
    activeIndex: number = Number(this.activeIndexStr);

    constructor(private router: Router) { }

    localurl: string = "workspace/person-info/add-person-info";

      ngOnInit() {
         
        this.items = [
            {
                label: '个人信息',
                command: (event: any) => {
                    // this.activeIndex = 0;
                    localStorage.setItem('activeIndexStr', '0');
                    this.router.navigate([this.localurl + '/add-basic-info']);
                }
            },
            {
                label: '学历信息',
                command: (event: any) => {
                    // this.activeIndex = 1;
                    localStorage.setItem('activeIndexStr', '1');
                    this.router.navigate([this.localurl + "/add-education"]);
                }
            },
            {
                label: '公司信息',
                command: (event: any) => {
                    //   this.activeIndex = 2;
                    localStorage.setItem('activeIndexStr', '2');
                    this.router.navigate([this.localurl + '/add-company']);
                }
            },
            {
                label: '薪资情况',
                command: (event: any) => {
                    //   this.activeIndex = 3;
                    localStorage.setItem('activeIndexStr', '3');
                    this.router.navigate([this.localurl + '/add-salary']);
                }
            },
            {
                label: '家庭信息',
                command: (event: any) => {
                    //   this.activeIndex = 4;
                    localStorage.setItem('activeIndexStr', '4');
                    this.router.navigate([this.localurl + '/add-home']);
                }
            },
            // {
            //     label: '风景信息',
            //     command: (event: any) => {
            //         localStorage.setItem('activeIndexStr', '5');
            //         this.router.navigate([this.localurl + '/add-landscape']);
            //     }
            // },
            // {
            //     label: '特产信息',
            //     command: (event: any) => {
            //         localStorage.setItem('activeIndexStr', '6');
            //         this.router.navigate([this.localurl + '/add-local-product']);
            //     }
            // }

        ];
    }
    pre() {
        switch (this.activeIndex) {
            // case 0:
            //     this.router.navigate([this.localurl + '/add-basic-info']);
            //     break;
            case 1:
                this.router.navigate([this.localurl + "/add-basic-info"]);
                break;
            case 2:
                this.router.navigate([this.localurl + '/add-education']);
                break;
            case 3:
                this.router.navigate([this.localurl + '/add-company']);
                break;
            case 4:
                this.router.navigate([this.localurl + '/add-salary']);
                break;
            case 5:
                this.router.navigate([this.localurl + '/add-home']);
                break;
            // case 6:
            //     this.router.navigate([this.localurl + 'add-local-product']);
            //     break;
        }
        this.activeIndex = this.activeIndex - 1;
        localStorage.setItem('activeIndexStr', this.activeIndex.toString());
    }


    next() {

        switch (this.activeIndex) {
            case 0:
                // this.ls.setObject(
                //     "enrollstudent", student
                // );
                // console.log(this.ls.getObject("enrollstudent"));

                this.router.navigate([this.localurl + "/add-education"]);
                break;

            case 1:
                //获取localstorage存取的true和false
                // if (this.ls.get("convention") == "false") {
                //     alert("请同意公约条款");
                //     return;
                // }
                // this.ls.setObject(
                //     "enrollstudent", student
                // );
                // console.log(this.ls.getObject("enrollstudent"));
                this.router.navigate([this.localurl + '/add-company']);
                break;

            case 2:
                // this.ls.setObject(
                //     "enrollstudent", student
                // );
                // console.log(this.ls.getObject("enrollstudent"));
                this.router.navigate([this.localurl + '/add-salary']);
                break;

            case 3:
                // this.ls.setObject(
                //     "enrollstudent", student
                // );
                // console.log(this.ls.getObject("enrollstudent"));
                this.router.navigate([this.localurl + '/add-home']);
                break;

            // case 4:
            //     this.ls.setObject(
            //         "enrollstudent", student
            //     );
            //     console.log(this.ls.getObject("enrollstudent"));
            //     this.router.navigate([this.localurl + '/freshman-register-successful']);

            case 4:
                this.router.navigate([this.localurl+'/add-landscape']);
                break;
            // case 6:
            //     this.router.navigate([this.localurl+'add-local-product']);
            //     break;
        }
        this.activeIndex = this.activeIndex + 1;
        localStorage.setItem('activeIndexStr', this.activeIndex.toString());
      }

}
