
import { AddBasicInfoComponent } from './add-basic-info/add-basic-info.component';
import { AddEducationComponent } from './add-education/add-education.component';
import { AddCompanyComponent } from './add-company/add-company.component';
import { AddSalaryComponent } from './add-salary/add-salary.component';
import { AddHomeComponent } from './add-home/add-home.component';
import { AddPersonInfoComponent } from './add-person-info.component';
import { AddLandscapeComponent } from './add-landscape/add-landscape.component';
import { AddLocalProductComponent } from './add-local-product/add-local-product.component';
 

export const addPersonInfoRoutes = [
  {
    path: '',
    component: AddPersonInfoComponent,
    children: [
      { path: '', redirectTo: 'add-basic-info', pathMatch: 'full' },
      {
        path: 'add-basic-info',
        component: AddBasicInfoComponent
      },
      {
        path: 'add-education',
        component: AddEducationComponent
      },
      {
        path: 'add-company',
        component: AddCompanyComponent
      },
      {
        path: 'add-salary',
        component: AddSalaryComponent
      },
      {
        path: 'add-home',
        component: AddHomeComponent
      },      
      {
        path: 'add-landscape',
        component: AddLandscapeComponent
      },
      {
        path: 'add-local-product',
        component: AddLocalProductComponent
      }
    ]
  },

];
console.log('添加个人信息路由test2');