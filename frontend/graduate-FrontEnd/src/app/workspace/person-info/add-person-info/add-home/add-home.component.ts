import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeInfoEntity } from '../../../../models/home-info-entity';

//引用拦截器文件
import { InterceptorService } from "../../../../share/interceptor.service";
// 删除确认弹框
import { ConfirmationService } from 'primeng/primeng';
@Component({
  selector: 'app-add-home',
  templateUrl: './add-home.component.html',
  styleUrls: ['./add-home.component.css']
})
export class AddHomeComponent implements OnInit {

  constructor(
    private router: Router,
    private confirmationService: ConfirmationService,
    private http: InterceptorService
  ) { }

  ngOnInit() {
    this.getData();
    this.getRelation();
    this.homeInfo.phone='';
    
  
  }

  doLogin() {
    this.router.navigateByUrl('/workspace/person-info/search-person-info');
  }


  relationOptions: string[];


  //    relationshipId

  // 表格数据
  btnAdd = true;
  btnDelete = true;
  btnEdit = false;
  btnImport = false;
  btnExport = false;
  title = ['姓名', '关系', '工作', '电话', '微信', 'QQ'];
  arrbutes = ["familyMemName", "relationName", 'work', "phone", "wechat", "qq"];
  btnList: string[] = ["编辑"];
  paging = true;
  pageSize = 10;
  page = 1;
  sizeList = [5, 10, 15, 50];
  addInfo: any = new Array();
  arr = new Array();
  btnstyle = [
    { "color": "blue" }]
  total: number;
  data = new Array();
  isCheck = true;


  queryHomeUrl = "graduate-web/homeInfo/selectHomePersonInfo/";
  // queryHomeUrl = 'src/mock-data/search-home-info.json'//假数据
  getData() {
    //let userId = 'UpadWgSLPctmPZHdcBUFCV';
     let userId = localStorage.getItem('userId');
    //  let userId="";
    //  if (localStorage.getItem('studentId') != undefined) {
    //   userId= localStorage.getItem('studentId');

    // } else {
    //   userId= localStorage.getItem('userId');
    // }
    // let url = this.queryHomeUrl;//假数据

    let url = this.queryHomeUrl + userId + '/' + this.page + '/' + this.pageSize;
    this.http.get(url).subscribe(
      res => {
        if (res.json().code == '0000') {
          this.data = res.json().data.list;
          // this.data =res.json().data;//假数据
          console.log("*********************家庭信息data***********************");
          console.log(this.data);
          // console.log(res.json().data.list);
          // console.log(res.json().data.total);
          this.total = res.json().data.total; // 未显示
          console.log("*********************家庭信息Total************************");
          console.log(this.total);

        }
      }
    );
  }



  //获得关系
  getRelation() {
    //方案1-后端数据
    let relationUrl = "graduate-web/dictionary/selectRelation";

    //let relationUrl = 'src/mock-data/seach-dictionary-relation.json';
    this.http.get(relationUrl).subscribe(
      res => {
        if (res.json().code == '0000') {
          // 方案1-后端方法
          this.relationOptions = res.json().data;
          console.log("*********************relationOptions***********************");
          console.log(this.relationOptions);
        } else {
          // 方案2-假数据
          let data: any[] = [
            {
            id: 'JLfyBqa1E7kUpWAiLAAiee',
            dictName: "父子"
          }, {
            id: '57phzcbp5L27k1fhLDWZ1d',
            dictName: "父女"
          }, {
            id: 'Cxmo1fcJsskLWVF1muEJ8o',
            dictName: "母子"
          }, {
            id: 'Y579eZjJ35X8rtM98SSBhP',
            dictName: "母女"
          }, {
            id: 'BYzW9DgWJhhQkAf91DwaKD',
            dictName: "兄弟"
          }, {
            id: '2H3rajKdrLYBEBdARCRNdP',
            dictName: "姐妹"
          }, {
            id: 'SPDhVAraFfHCLrUJwP68Td',
            dictName: "朋友"
          }, {
            id: 'RQepA5sx2uUHLK4192zaFB',
            dictName: "同学"
          }, {
            id: 'VoCATRkLQiYMBzpZi7SypK',
            dictName: "同事"
          }, {
            id: 'N34bUeKGS5JretqhYnewLn',
            dictName: "其他"
          }
          ];
          this.relationOptions = data;
        }
      });
  }

  // 更新页码,更新表格信息
  changepage(data: any) {
    this.page = data.page;
    this.pageSize = data.pageSize;
    this.getData();
  }

  // 删除   一个或多个家庭成员
  deleteDatas(el: any) {
    // console.log(el);
    // console.log(this.data[el[0]].id);
    // console.log(this.data[el[1]].id);
    // console.log(this.data[el[2]].id);
    let operator = '徐玲博-假数据';
    this.confirmationService.confirm({
      message: '你确定删除家庭成员信息吗？',
      accept: () => {
        for (let i = 0; i < el.length; i++) {
          let id = this.data[el[i]].id;
          console.log(this.data[el[i]]);
          let deleteUrl = 'graduate-web/homeInfo/deleteHomeInfo' + '/' + id + '/' + operator;
          let body = JSON.stringify(null);
          this.http.post(deleteUrl, body).subscribe(
            res => {
              if (res.json().code != null && res.json().code == '0000') {
                this.showDialog(res.json().message);
                this.getData();
                this.total--;
              } else {
                this.showDialog(res.json().message);
                this.getData();
              }
            }
          );
        }
      }
    });
  }

  homeInfo: HomeInfoEntity = new HomeInfoEntity();//家庭信息实体
  

  // 提示框
  display: boolean = false;
  messages: '';
  /**
   * 提示框
   */
  showDialog(string) {
    this.messages = string;
    this.display = true;
    let com = this;
    var t = setTimeout(function () { com.display = false }, 1500);
  }

  // 关闭模态框
  close(modal: HTMLElement) {
    modal.style.visibility = 'hidden';
    this.homeInfo = new HomeInfoEntity();
  }

  //打开添加模态框
  addOpen(el: HTMLElement, addModal: HTMLElement) {
    addModal.style.visibility = 'visible';
    document.getElementById('familyMemName').focus();
  }

  //添加信息
  add(addModal: HTMLElement) {
    let addUrl = 'graduate-web/homeInfo/addHomeInfo/';
    //let userId = 'UpadWgSLPctmPZHdcBUFCV';
    let userId = localStorage.getItem('userId');
   //let userId="";
  //  if (localStorage.getItem('studentId') != undefined) {
  //   userId= localStorage.getItem('studentId');

  // } else {
  //   userId= localStorage.getItem('userId');
  // }
    this.homeInfo.userId = userId;
    let body = JSON.stringify(this.homeInfo);
    console.log(this.homeInfo);
    this.http.post(addUrl, body).toPromise().then(
      res => {
        if (res.json().code = "0000") {
          this.showDialog(res.json().message);
          this.getData();//回显
          this.close(addModal);
          console.log(res.json().message + '！！！！！');
        }
      }
    );
  }

  /**
  * 自定义操作列
  */
  operatData(obj: any, editModal: HTMLElement) {
    switch (obj.element.innerText) {
      case '编辑':
        this.editOpen(editModal,obj.data);
        break;
      default:
        break;
    }
  }

  // 打开修改模态框
  editOpen( editModal: HTMLElement,index: any) {
    editModal.style.visibility = 'visible';
    this.homeInfo.familyMemName = this.data[index].familyMemName;
    this.homeInfo.relationshipId = this.data[index].relationshipId;
    this.homeInfo.work = this.data[index].work;
    this.homeInfo.phone = this.data[index].phone;
    this.homeInfo.wechat = this.data[index].wechat;
    this.homeInfo.qq = this.data[index].qq;
    this.homeInfo.email = this.data[index].email;
    this.homeInfo.remark = this.data[index].remark;
    this.homeInfo.userId = this.data[index].userId;
    this.homeInfo.id = this.data[index].id;
    document.getElementById('familyMemName').focus();
    
  }

  relation = new Relation();
  update(editModel: HTMLElement) {
    let editUrl = 'graduate-web/homeInfo/updateHomeInfo/';
     let userId = localStorage.getItem('userId');
  //   let userId="";
  //   if (localStorage.getItem('studentId') != undefined) {
  //    userId= localStorage.getItem('studentId');
 
  //  } else {
  //    userId= localStorage.getItem('userId');
  //  }
    this.homeInfo.userId = userId
    let body = JSON.stringify(this.homeInfo);
    this.http.post(editUrl, body).subscribe(
      res => {
        if(res.json().code=='0000'){
          this.showDialog(res.json().message);
          this.getData();
          this.close(editModel);
        console.log(res.json().message + '！！！！！');
        }
      }
    );
  }



  /**
   *modal框可拖拽
  */
  draggable() {
    $('.modal-dialog').draggable();
  }

}

export class Relation {
  relationName?: string;
  relationId?: string;
}