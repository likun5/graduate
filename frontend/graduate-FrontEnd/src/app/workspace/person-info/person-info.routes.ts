import { PersonInfoComponent } from './person-info.component';
import { SearchPersonInfoComponent } from './search-person-info/search-person-info.component';
import { AddPersonInfoModule } from './add-person-info/add-person-info.module';

console.log('个人信息展示1');
export const personInfoRoutes = [
     { path: '', redirectTo: 'add-person-info', pathMatch: 'full' },
    {
         path: 'add-person-info',
         loadChildren: './add-person-info/add-person-info.module#AddPersonInfoModule'
     },
     {
            path: 'search-person-info',
            loadChildren: './search-person-info/search-person-info.module#SearchPersonInfoModule'
     }
    //  ,
    //  {
    //         path: 'search-all-person',
    //         loadChildren: './search-all-person/search-all-person.module#SearchAllPersonModule'
    //  }

];
      