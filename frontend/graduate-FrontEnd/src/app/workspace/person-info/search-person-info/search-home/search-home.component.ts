import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//引用拦截器文件
import { InterceptorService } from "../../../../share/interceptor.service";
@Component({
  selector: 'app-search-home',
  templateUrl: './search-home.component.html',
  styleUrls: ['./search-home.component.css']
})
export class SearchHomeComponent implements OnInit {

  constructor(private router: Router,
    private http: InterceptorService) { }

  ngOnInit() {
    this.getData();
  }
  // doLogin() {
  //   this.router.navigateByUrl('/workspace/person-info/search-person-info/search-basic-info');
  // }



  // 表格数据
  btnAdd = false;
  btnDelete = false;
  btnEdit = false;
  btnImport = false;
  btnExport = false;
  title = ['姓名', '关系', '工作', '电话', '微信', 'QQ', '邮箱', '备注'];
  // arrbutes = ["familyMemName", "relationshipId", 'work', "phone", "wechat", "qq", 'email', 'remark'];
  arrbutes = ["familyMemName", "relationName", 'work', "phone", "wechat", "qq", 'email', 'remark'];
  btnList: string[] = [];
  paging = true;
  pageSize = 10;
  page = 1;
  sizeList = [5, 10, 15, 50];
  addInfo: any = new Array();
  arr = new Array();
  btnstyle = [
    { "color": "blue" }]
  total: number;
  data = new Array();
  isCheck = true;


  queryHomeUrl = "graduate-web/homeInfo/selectHomePersonInfo/";
  // queryHomeUrl = 'src/mock-data/search-home-info.json'//假数据
  getData() {
    // let userId = 'SdHAYGfnaGYJa68TenWzPm';
    //let userId = 'UpadWgSLPctmPZHdcBUFCV';
    //let userId = localStorage.getItem('userId');
    let userId="";
    if (localStorage.getItem('studentId') != undefined) {
      userId= localStorage.getItem('studentId');
    } else {
      userId= localStorage.getItem('userId');
    }
    // let url = this.queryHomeUrl;//假数据
    let url = this.queryHomeUrl + userId + '/' + this.page + '/' + this.pageSize;
    this.http.get(url).subscribe(
      res => {
        if (res.json().code == '0000') {
          this.data = res.json().data.list;
          console.log(this.data);
          this.total = res.json().data.total; // 未显示
          console.log(this.total);
        }
      }
    );
  }

  // 更新页码,更新表格信息
  changepage(data: any) {
    this.page = data.page;
    this.pageSize = data.pageSize;
    this.getData();
  }
}
