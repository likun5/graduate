import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchLocalProductComponent } from './search-local-product.component';

describe('SearchLocalProductComponent', () => {
  let component: SearchLocalProductComponent;
  let fixture: ComponentFixture<SearchLocalProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchLocalProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchLocalProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
