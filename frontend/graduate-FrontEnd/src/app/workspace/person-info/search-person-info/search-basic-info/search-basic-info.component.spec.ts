import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBasicInfoComponent } from './search-basic-info.component';

describe('SearchBasicInfoComponent', () => {
  let component: SearchBasicInfoComponent;
  let fixture: ComponentFixture<SearchBasicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBasicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
