import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InterceptorService } from '../../../../share/interceptor.service';

// import { PersonInfoModel } from '../../../../models/person-info-model';
import { PersonInfoModel } from 'app/models/person-info-model';
@Component({
  selector: 'app-search-basic-info',
  templateUrl: './search-basic-info.component.html',
  styleUrls: ['./search-basic-info.component.css']
})
export class SearchBasicInfoComponent implements OnInit {

  constructor(
    private router: Router,
    private http: InterceptorService,
  ) { }

  ngOnInit() {
    this.getGrade();
    this.getProvince(); // 所在地省
    this.getData();
  }
  doLogin() {
    this.router.navigateByUrl('/workspace/person-info/search-person-info/search-education');
  }
  doBack() {
    // 返回
    this.router.navigateByUrl('/workspace/search-all-person');
  }
  doEdit() {
    // 修改信息
  }


  gradeOptions: string[]; // 期数集合
  provinceOptions: string[]; // 所在地省集合
  cityOptions: string[]; // 所在地市
  countyOrDistrictOptions: String[]; // 所在地县/区
  nativeProvinceOptions: string[]; // 籍贯省集合
  nativeCityOptions: string[]; // 籍贯市
  nativeCountyOrDistrictOptions: String[]; // 籍贯县/区
  personInfo: PersonInfoModel = new PersonInfoModel(); // 个人信息实体


  getData() {
    // let id = '1234567898'; // 登录人id,假数据      YtH67q6ykRmfH5F6Re2Fb
    // let id = 'UpadWgSLPctmPZHdcBUFCV'; // 登录人id,假数据      YtH67q6ykRmfH5F6Re2Fb

    let id
    if (localStorage.getItem('studentId') != undefined) {
      id = localStorage.getItem('studentId');     

    } else {
      id = localStorage.getItem('userId');
      document.getElementById('back').style.display = 'none';
    }

    // let id=this.personInfo.id; // 管理员查询详情 毕业生id
    let searchUrl = 'graduate-web/personalInfo/selectByPersonId/' + id;


    this.http.get(searchUrl).subscribe(
      res => {
        if (res.json().code == '0000') {
          this.personInfo.name = res.json().data.name;
          this.personInfo.englishName = res.json().data.englishName;
          this.personInfo.grade = res.json().data.grade;
          this.personInfo.gradeName=res.json().data.gradeName;
          this.personInfo.sex = res.json().data.sex;
          this.personInfo.birthday = res.json().data.birthday;
          this.personInfo.email = res.json().data.email;
          this.personInfo.phone = res.json().data.phone;
          this.personInfo.emergPhone = res.json().data.emergPhone;
          this.personInfo.emergName = res.json().data.emergName;
          this.personInfo.emergRelation = res.json().data.emergRelation;
          this.personInfo.emergRelationName = res.json().data.emergRelationName; // 关系名称
          this.personInfo.wechat = res.json().data.wechat;
          this.personInfo.qq = res.json().data.qq;
          this.personInfo.enterDmtTime = res.json().data.enterDmtTime;
          this.personInfo.enterCollegeTime = res.json().data.enterCollegeTime;
          this.personInfo.graduateDmtTime = res.json().data.graduateDmtTime;
          this.personInfo.graduateCollegeTime = res.json().data.graduateCollegeTime;

          this.personInfo.address = res.json().data.address;
          this.personInfo.proviceName = res.json().data.proviceName;
          this.personInfo.provinceId = res.json().data.provinceId;
          this.personInfo.cityName = res.json().data.cityName;
          this.personInfo.cityId = res.json().data.cityId;
          this.personInfo.countyOrDistrictName = res.json().data.countyOrDistrictName;
          this.personInfo.countyOrDistrictId = res.json().data.countyOrDistrictId;

          //从后台传回实体中，不包括籍贯的字段，后面会增加 TODO
          // this.personInfo.nativePlace=res.json().data.nativePlace;
          // this.personInfo.provinceNativeId=res.json().data.provinceNativeId
          // this.personInfo.provinceNativeName=res.json().data.proviceNativeName;
          // this.personInfo.cityNativeName=res.json().data.cityNativeName;
          // this.personInfo.cityNativeId=res.json().data.cityNativeId;
          // this.personInfo.countyOrDistrictNativeName=res.json().data.countyOrDistrictNativeName;
          // this.personInfo.countyOrDistrictNativeId=res.json().data.countyOrDistrictNativeId;

          this.personInfo.remark = res.json().data.remark;
          console.log('====================查询个人详情======================');
          console.log(res.json().data);

          //将默认图片改为学生图片
          if (res.json().data.pictureUrl == null || res.json().data.pictureUrl == "") {
          } else {
            let el: Element = document.getElementById("wizardPicturePreview");
            let pictureURL = res.json().data.pictureUrl;
            el.setAttribute("src", pictureURL);
          }
        }
      }
    );


  }
  //获得期数
  getGrade() {
    // let dictName: string;
    let gradeUrl = "graduate-web/dictionary/selectGrade";
    this.http.get(gradeUrl).subscribe(
      res => {
        if (res.json().code == '0000') {
          // 方案1-后端方法
          this.gradeOptions = res.json().data;
          console.log(this.gradeOptions);
        } else {
          // 方案2-假数据
          let data: any[] = [{
            gradeName: "5期"
          }, {
            gradeName: "6期"
          }, {
            gradeName: "7期"
          }, {
            gradeName: "8期"
          }, {
            gradeName: "9期"
          }, {
            gradeName: "10期"
          }, {
            gradeName: "11期"
          }];
          this.gradeOptions = data;
        }
      }
    );

  }

  /*----------------------所在地三级联动-----------------------------*/

  //获得省
  getProvince() {
    let proviceUrl = "graduate-web/administrativeRegion/selectProvince";
    this.http.get(proviceUrl).subscribe(
      res => {
        if (res.json().code == '0000') {
          // 方案1-后端方法
          this.provinceOptions = res.json().data;
        } else {
          // 方案2-假数据
          let data: any[] = [
            {
              id: "37",
              name: "河北"
            }
          ];
          this.provinceOptions = data;
        }
      }
    );
  }

  // 改变住址省，触发查询市
  changeProvince(provinceId: string) {
    this.getCity(provinceId);
    console.log(provinceId);
  }

  // 获得市
  getCity(provinceId) {
    // provinceId = "37";
    // 方案1-后端方法
    let cityUrl = "graduate-web/administrativeRegion/selectSubRegionById/" + provinceId;

    this.http.get(cityUrl).subscribe(
      res => {
        if (res.json().code = "0000") {
          this.cityOptions = res.json().data;
          console.log(this.cityOptions);
        } else {
          // 方案2-假数据
          let data: any[] = [
            {
              id: "38",
              pid: "37",
              name: "石家庄"
            }, {
              id: "124",
              pid: "37",
              name: "保定"
            }, {
              id: "197",
              pid: "37",
              name: "廊坊"
            }, {
              id: "150",
              pid: "37",
              name: "张家口"
            }, {
              id: "84",
              pid: "37",
              name: "邯郸"
            }
          ];
          this.cityOptions = data;
        }
      }
    );
  }

  // 改变住址市，触发改变县区
  changeCity(cityId: string) {
    this.getCountyOrDistrict(cityId);
  }

  // 获得县/区
  getCountyOrDistrict(cityId) {
    // cityId = "38";
    // 方案1-后端方法
    let countyOrDistrictUrl = "graduate-web/administrativeRegion/selectSubRegionById/" + cityId;
    this.http.get(countyOrDistrictUrl).subscribe(
      res => {
        if (res.json().code == "0000") {
          this.countyOrDistrictOptions = res.json().data;
        } else {
          // 方案2-假数据
          let data: any[] = [
            {
              id: "48",
              pid: "38",
              name: "正定县"
            }, {
              id: "203",
              pid: "197",
              name: "大城县"
            }, {
              id: "55",
              pid: "38",
              name: "平山县"
            }, {
              id: "199",
              pid: "197",
              name: "广阳区"
            }, {
              id: "198",
              pid: "197",
              name: "安次区"
            }, {
              id: "43",
              pid: "38",
              name: "裕华区"
            }, {
              id: "39",
              pid: "38",
              name: "长安区"
            }
          ];
          this.countyOrDistrictOptions = data;
        }
      }
    );
  }

  /*----------------------籍贯三级联动-----------------------------*/

  // 改变籍贯省，触发查询市
  changeNativeProvince(provinceId: string) {
    this.getNativeCity(provinceId);
    // this.getNativeProvince(); // 籍贯省
    console.log(provinceId);
  }

  // 获得市
  getNativeCity(provinceId) {
    // provinceId = "37";
    // 方案1-后端方法
    let cityUrl = "graduate-web/administrativeRegion/selectSubRegionById/" + provinceId;

    this.http.get(cityUrl).subscribe(
      res => {
        if (res.json().code = "0000") {
          this.nativeCityOptions = res.json().data;
          console.log(this.nativeCityOptions);
        } else {
          // 方案2-假数据
          let data: any[] = [
            {
              id: "38",
              pid: "37",
              name: "石家庄"
            }, {
              id: "124",
              pid: "37",
              name: "保定"
            }, {
              id: "197",
              pid: "37",
              name: "廊坊"
            }, {
              id: "150",
              pid: "37",
              name: "张家口"
            }, {
              id: "84",
              pid: "37",
              name: "邯郸"
            }
          ];
          this.nativeCityOptions = data;
        }
      }
    );
  }

  // 改变籍贯市，触发改变县区
  changeNativeCity(cityId: string) {
    this.getNativeCountyOrDistrict(cityId);
  }

  // 获得县/区
  getNativeCountyOrDistrict(cityId) {
    console.log(cityId + "zhixingdaozhelile");
    // 方案1-后端方法
    let countyOrDistrictUrl = "graduate-web/administrativeRegion/selectSubRegionById/" + cityId;
    this.http.get(countyOrDistrictUrl).subscribe(
      res => {
        if (res.json().code == "0000") {
          this.nativeCountyOrDistrictOptions = res.json().data;
        } else {
          // 方案2-假数据
          let data: any[] = [
            {
              id: "48",
              pid: "38",
              name: "正定县"
            }, {
              id: "203",
              pid: "197",
              name: "大城县"
            }, {
              id: "55",
              pid: "38",
              name: "平山县"
            }, {
              id: "199",
              pid: "197",
              name: "广阳区"
            }, {
              id: "198",
              pid: "197",
              name: "安次区"
            }, {
              id: "43",
              pid: "38",
              name: "裕华区"
            }, {
              id: "39",
              pid: "38",
              name: "长安区"
            }
          ];
          this.nativeCountyOrDistrictOptions = data;
        }
      }
    );
  }

}
