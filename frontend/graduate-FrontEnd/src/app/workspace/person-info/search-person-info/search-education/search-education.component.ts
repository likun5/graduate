import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/primeng';
import { InterceptorService } from '../../../../share/interceptor.service';
import { EducationEntity } from '../../../../models/education-entity';
@Component({
  selector: 'app-search-education',
  templateUrl: './search-education.component.html',
  styleUrls: ['./search-education.component.css']
})
export class SearchEducationComponent implements OnInit {

  studentId: string;
  private msgs: any; //提示信息
  private typeData = new Array();
  private educationData = new Array();  //学历实体集合

  //查询条件
  private educationId: string = "";  //课程id
  private searchContent = ""; //查询条件
  // 页面表格数据
  private tableUrl: string = "graduate-web/education/findPersonAll" + '/';
  private deleteUrl: string = "graduate-web/education/delete";
  educationInfo: EducationEntity = new EducationEntity();

  btnDelete: boolean = false;
  btnAdd: boolean = false;
  btnEdit: boolean = false;
  btnList: string[] = [];
  // '图片',
  title = ['证书编号', '证书名', '获取时间', '学校名称', '备注'];
  arrbutes = ["certificateNo", "certificateName", "receiveTime", "universityName", "remark"];
  isLink = [false, false, false, false];
  paging: boolean = true;
  pageSize = 5;
  page = 1;
  total: number;

  sizeList = [5, 10, 20, 50];
  data = new Array();
  btnstyle = [
    { "color": "green" }]

  constructor(private router: Router,
    private confirmationService: ConfirmationService,
    private http: InterceptorService) { }
  ngOnInit() {
     this.getData(true);
  }



  //查询路径
  //queryEducationUrl = tableUrl;//查询信息
  //queryEducationUrl = "src/assets/mock-data/addEducation.json";//使用假数据
  //查询表格显示的数据-于云丽-2017年12月29日
  getData(isQuery?: boolean) {


    if (localStorage.getItem('studentId') != undefined) {
      this.studentId = localStorage.getItem('studentId');

    } else {
      this.studentId = localStorage.getItem('userId');
    }

    let url = this.tableUrl + this.page + '/' + this.pageSize + '/' + this.studentId;
    this.http.get(url).subscribe(
      res => {
        if (res.json().code === "0000") {
         this.data = res.json().data.list;
          this.total=res.json().data.total;

          for(var i=0;i<res.json().data.list.length;i++)
          {  
            let receiveDate=this.translateDate(new Date(res.json().data.list[i].receiveTime));
            this.data[i].receiveTime=receiveDate;
            console.info(receiveDate);
          }
        }
      }
    )
  }

changepage(data:any){
  this.page=data.page;
  this.pageSize=data.pageSize;
  this.getData();
}

  operatData(obj: any, editModal: HTMLElement) {
    switch (obj.element.innerText) {
      case '编辑':
        this.editOpen(obj.data, editModal);
        break;
      default:
        break;
    }
  }

  // 打开修改模态框
  editOpen(index: any, editModal: HTMLElement) {
    editModal.style.visibility = 'visible';
    this.educationInfo.id = this.data[index].id;
    this.educationInfo.userId = this.data[index].userId;
    this.educationInfo.isIndmt = this.data[index].isIndmt;
    this.educationInfo.certificateNo = this.data[index].certificateNo;
    this.educationInfo.certificateName = this.data[index].certificateName;
    this.educationInfo.certificateType = this.data[index].certificateType;
    this.educationInfo.receiveTime = this.data[index].receiveTime;
    this.educationInfo.timestampTime = this.data[index].timestampTime;
    this.educationInfo.universityName = this.data[index].universityName;
    this.educationInfo.remark = this.data[index].remark;
    this.educationInfo.operator = this.data[index].operator;
    this.educationInfo.createTime = this.data[index].createTime;
    document.getElementById('certificateNo').focus();
  }
  // 修改信息
  update(editModel: HTMLElement) {
    let editUrl = 'graduate-web/education/update/';

    let body = JSON.stringify(this.educationInfo);
    this.http.post(editUrl, body).toPromise().then(
      res => {
        if (res.json().code = '0000') {
          this.showDialog(res.json().message);
          this.getData();
          this.close(editModel);
          console.log(res.json().message + '！！！！！');
        }

      }
    );
  }

  // 提示框
  display: boolean = false;
  messages: '';
  /**
   * 提示框
   */
  showDialog(string) {
    this.messages = string;
    this.display = true;
  }
  // 关闭模态框
  close(modal: HTMLElement) {
    modal.style.visibility = 'hidden';
    this.educationInfo = new EducationEntity();
  }
  /**
   *modal框可拖拽
  */
  draggable() {
    $('.modal-dialog').draggable();
  }




  //打开添加模态框
  addOpen(el: HTMLElement, addModal: HTMLElement) {

    addModal.style.visibility = 'visible';
    document.getElementById('userId').focus();
  }

  //添加信息
  add(addModal: HTMLElement) {
    let addUrl = 'graduate-web/education/insert/';

    let body = JSON.stringify(this.educationInfo);
    this.http.post(addUrl, body).toPromise().then(
      res => {
        this.showDialog(res.json().message);
        this.getData();//回显
        this.close(addModal);
        console.log(res.json().message + '！！！！！');
      }
    );
  }




  //表格添加-于云丽-2017年12月29日
  addInscription(obj: any) {
    localStorage.setItem("isEdit", JSON.stringify(false));
    this.router.navigateByUrl('/workspace/person-info/add-person-info/add-salary');
  }





  deleteDatas(el: any) {
    // console.log(el);
      console.log(this.data[el[0]].id);
    // console.log(this.data[el[1]].id);
    // console.log(this.data[el[2]].id);
    //、、 let operator = '徐玲博-假数据';

    this.confirmationService.confirm({
      message: '你确定删除学历信息吗？',
      accept: () => {
        for (let i = 0; i < el.length; i++) {
          let operator = 'yyl';
          let id = this.data[el[i]].id;
          console.log(this.data[el[i]]);
          let deleteUrl = 'graduate-web/education/deleteById' + '/' + id + '/' + operator;
          console.log(deleteUrl + '执行到这了');

          let body = JSON.stringify(null);
          console.log(this.http.post(deleteUrl, body) + '执行到这了2');
          this.http.post(deleteUrl, body).subscribe(
            res => {

              if (res.json().code != null && res.json().code == '0000') {
                this.showDialog(res.json().message);
                console.log(res.json().data + '执行到这了3');

                this.getData();
                this.total--;
              } else {
                this.showDialog(res.json().message);
                this.getData();
              }
            }
            );
        }
      }
    });
  }
  /**
   * 将有格式的替换成没有格式的
   */
  onConvert(obj: any) {
    let showText;
    showText = obj.replace(/&nbsp;/g, ' ');
    showText = showText.replace(/<br>/g, '\n');
    return showText;
  }


 //将毫秒数转换成日期
 translateDate(date) {
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? '0' + m : m;
  var d = date.getDate();
  d = d < 10 ? ('0' + d) : d;
  var a = date.getM
  return y + '-' + m + '-' + d;
}

  doLogin() {
    this.router.navigateByUrl('/workspace/person-info/search-person-info/search-company');
  }
}
