import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';

//?import { searchPersonInfoRoutes } from '../search-person-info.routes';
import { DataTableModule } from 'ng-itoo-datatable';
import { FileUploadModule } from 'ng2-file-upload';
import { ButtonModule, GrowlModule,ConfirmDialogModule,ConfirmationService,DialogModule } from 'primeng/primeng';

@NgModule({
  imports: [   

    CommonModule,
    NgZorroAntdModule,    
    ButtonModule,
    GrowlModule,
    ConfirmDialogModule,
    DialogModule,
    FileUploadModule,
    DataTableModule,
    FileUploadModule,
    NgZorroAntdModule.forRoot(),
    //?RouterModule.forChild(searchPersonInfoRoutes) //路由模块
  ],
 //? declarations: [SearchEducationComponent]
})
export class AddPersonInfoModule { }
