import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyModel } from 'app/models/CompanyModel';
import {FieldsetModule} from 'primeng/primeng';
import { InterceptorService } from '../../../../share/interceptor.service'

@Component({
  selector: 'app-search-company',
  templateUrl: './search-company.component.html',
  styleUrls: ['./search-company.component.css']
})
export class SearchCompanyComponent implements OnInit {

  constructor( private router:Router, private http : InterceptorService) { }

  private companyName : string;
 
  private companyTel : string;
  private userId : string;
 
  private companyModel : CompanyModel = new CompanyModel();
 	 
   ngOnInit() {
 
    //加载时，就查询
 
    this.searchCompany();
 
  }
  // addCompany(companyModel : CompanyModel) {
 
  //   this.companyName = companyModel.companyName;
     
  //   this.companyTel = companyModel.companyTel;
  //   this.userId = companyModel.userId;
     
  //   alert(this.companyName);
     
  //   alert(this.companyTel);
     
  //   //this.url = "src/mock-data/addstudent.json";
     
  // }
     
  searchCompany() {
     
    // let url = "src/assets/mock-data/searchCompany.json";
    // let url = "graduate-web/company/selectCompanyByUserId/"+this.userId;
    let userId= localStorage.getItem('userId');
    let url = "graduate-web/company/selectCompanyByUserId/" + userId;
    
    this.http.get(url).subscribe(
     
      res => {
     
        this.companyModel = res.json().data;
     
      }
     
    )
        
       }
  doLogin() {
    this.router.navigateByUrl('/workspace/person-info/search-person-info/search-salary');
  }
}
