import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchLandscapeComponent } from './search-landscape.component';

describe('SearchLandscapeComponent', () => {
  let component: SearchLandscapeComponent;
  let fixture: ComponentFixture<SearchLandscapeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchLandscapeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchLandscapeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
