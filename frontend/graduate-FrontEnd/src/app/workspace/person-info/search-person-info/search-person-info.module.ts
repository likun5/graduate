import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchPersonInfoComponent } from './search-person-info.component';
import { DataTableModule } from 'ng-itoo-datatable';
import { FileUploadModule } from 'ng2-file-upload';
import { SearchBasicInfoComponent } from './search-basic-info/search-basic-info.component';
import { SearchCompanyComponent } from './search-company/search-company.component';
import { SearchEducationComponent } from './search-education/search-education.component';
import { SearchHomeComponent } from './search-home/search-home.component';
import { SearchSalaryComponent } from './search-salary/search-salary.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterModule } from '@angular/router';
import { searchinfoRoutes } from './search-person-info.routes';
// import { InterceptorService } from "../../../share/interceptor.service";
// import { SearchAllPersonComponent } from './search-all-person/search-all-person.component';

import { FormsModule } from '@angular/forms';
import { ButtonModule,ConfirmDialogModule,ConfirmationService,DialogModule } from 'primeng/primeng';
// 表单验证
import {CustomFormsModule} from "ng2-validation";
import {TabMenuModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';

import {StepsModule,MenuItem} from 'primeng/primeng';

import {PanelModule} from 'primeng/primeng';
import {FieldsetModule} from 'primeng/primeng';
import { SearchLandscapeComponent } from './search-landscape/search-landscape.component';
import { SearchLocalProductComponent } from './search-local-product/search-local-product.component';
@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    FileUploadModule,
    StepsModule,
    FormsModule,
    DialogModule,
    ButtonModule,
    ConfirmDialogModule,
    CustomFormsModule,
    TabMenuModule,
    PanelModule,
    FieldsetModule,
    CalendarModule,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(searchinfoRoutes)
  ],
  declarations: [
    SearchBasicInfoComponent,
    SearchCompanyComponent,
    SearchEducationComponent,
    SearchHomeComponent,
    SearchSalaryComponent,
    SearchPersonInfoComponent,
    SearchLandscapeComponent,
    SearchLocalProductComponent,
    // SearchAllPersonComponent // 毕业生信息查询
  ]
})
export class SearchPersonInfoModule { }
