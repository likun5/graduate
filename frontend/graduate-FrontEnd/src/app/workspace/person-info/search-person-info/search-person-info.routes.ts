import { SearchPersonInfoComponent } from './search-person-info.component'
import { SearchBasicInfoComponent } from './search-basic-info/search-basic-info.component';
import { SearchCompanyComponent } from './search-company/search-company.component';
import { SearchEducationComponent } from './search-education/search-education.component';
import { SearchHomeComponent } from './search-home/search-home.component';

import { SearchSalaryComponent } from './search-salary/search-salary.component';
import { SearchLandscapeComponent } from './search-landscape/search-landscape.component';
import { SearchLocalProductComponent } from './search-local-product/search-local-product.component';
 
export const searchinfoRoutes = [
    {
        path: '',
        component: SearchPersonInfoComponent,
        children: [
            { path: '', redirectTo: 'search-basic-info', pathMatch: 'full' },

            {
                path: 'search-basic-info',
                component: SearchBasicInfoComponent
            }, {
                path: 'search-company',
                component: SearchCompanyComponent
            }, {
                path: 'search-education',
                component: SearchEducationComponent
            }, {
                path: 'search-home',
                component: SearchHomeComponent
            }, {
                path: 'search-salary',
                component: SearchSalaryComponent
            }, {
                path: 'search-landscape',
                component: SearchLandscapeComponent
            }, {
                path: 'search-local-product',
                component: SearchLocalProductComponent
            }
        ]
    },
]

 