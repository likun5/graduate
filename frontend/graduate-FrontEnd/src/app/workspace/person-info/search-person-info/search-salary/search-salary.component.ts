import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/primeng';
import { InterceptorService } from '../../../../share/interceptor.service';
import { SalaryEntity } from '../../../../models/salary-entity' ;
@Component({
  selector: 'app-search-salary',
  templateUrl: './search-salary.component.html',
  styleUrls: ['./search-salary.component.css']
})
export class SearchSalaryComponent implements OnInit {

 private userId:any;
  private msgs: any; //提示信息 
  private typeData = new Array();
  private salaryData = new Array();  //学历实体集合

 

   salaryInfo :SalaryEntity = new SalaryEntity();
  btnDelete: boolean = false;
  btnAdd: boolean = false;
  btnEdit: boolean = false; 
 btnList: string[] = [];
  title = ['月薪 (元)', '福利待遇', '所属公司', '职位', '变动时间', '一年几薪'];
  arrbutes = ["salary", "wellfare", "companyName","possession", "salaryChangeTime","timesPerYear"];
  isLink = [false, false, false, false];
  paging: boolean = true;
  pageSize = 5;
  page = 1;
  total: number;

  sizeList = [5, 10, 20, 50];
  data = new Array();
  btnstyle = [
    { "color": "green" }]
// 页面表格数据
  private tableUrl: string = "graduate-web/salary/findAll"+ '/' ;
  private deleteUrl: string = "graduate-web/salary/delete";
  private getTableDateUrl="graduate-web/salary/PageSelectSalaryByUserId"  //拼接url-hgt-2018年3月8日
  operatorId:any;   //定义操作人变量-haoguiting-2018年3月7日

  constructor(private router:Router,
  private confirmationService: ConfirmationService,
  private http: InterceptorService) { }

  //将毫秒数转换成日期
  translateDate(date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? '0' + m : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var a = date.getM
    return y + '-' + m + '-' + d;
  }

  ngOnInit() {
      this.getData(true);
  }
  getData(isQuery?: boolean) {
    // let url = this.tableUrl+ this.page + '/' + this.pageSize;   //原有URL无法实现根据登录用户ID查询薪资-hgt-2018年3月8日
    //this.operatorId = localStorage.getItem('userId');  //获得登录用户的id作为操作人的id-haoguiting-2018年3月7日
    console.log("开始执行查询薪资");
    let operatorId="";
    let userId ="";
    if (localStorage.getItem('studentId') != undefined) {
      operatorId= localStorage.getItem('studentId');

      userId= localStorage.getItem('studentId');
      console.log("开始打印studentId");
      console.log(userId);
    } else {
      operatorId= localStorage.getItem('userId');

      userId = localStorage.getItem('userId');
      console.log("开始打印userId");
      console.log(userId);
    }
    console.log(operatorId);
    console.log("studentId"+localStorage.getItem('studentId'));
    console.log("userid"+localStorage.getItem('userId'));
    console.log("txb");
    console.log("查询前打印userId");
    console.log(userId);
    //let url=this.getTableDateUrl+"/"+this.operatorId+ '/'+ this.page + '/' + this.pageSize;;  //分页根据登录人ID查询薪资信息-hgt-2018年3月7日
    let url=this.getTableDateUrl+"/"+userId+ '/'+ this.page + '/' + this.pageSize;  //分页根据登录人ID查询薪资信息-yyl-2018年3月15日
    console.log(url);
    this.http.get(url).subscribe(
      res => {
        if (res.json().code === "0000") {
          this.data = res.json().data.list;
          this.total=res.json().data.total;
          for(var i=0;i<res.json().data.list.length;i++)
          {  
            let receiveDate=this.translateDate(new Date(res.json().data.list[i].salaryChangeTime));
            this.data[i].salaryChangeTime=receiveDate;
            console.info(receiveDate);
          }
        }
      }
    )
    console.log("结束执行查询薪资");
  }
changepage(data:any){
  this.page=data.page;
  this.pageSize=data.pageSize;
  this.getData();
}
 
  // 打开修改模态框
  editOpen(index: any, editModal: HTMLElement) {
    editModal.style.visibility = 'visible';
    this.salaryInfo.id = this.data[index].id;
    // this.salaryInfo.pcRalationId = this.data[index].pcRalationId;
    // this.salaryInfo.salaryChangeTime = this.data[index].salaryChangeTime;
    // this.salaryInfo.salary = this.data[index].salary;
    // this.salaryInfo.timesPerYear = this.data[index].timesPerYear;
    // this.salaryInfo.possesion = this.data[index].possesion;
    // this.salaryInfo.wellfare = this.data[index].wellfare;
    // this.salaryInfo.isDelete = this.data[index].isDelete;
    this.salaryInfo.remark = this.data[index].remark;
    this.salaryInfo.operator = this.data[index].operator;
    this.salaryInfo.createTime = this.data[index].createTime;
    //document.getElementById('salary').focus();
  }
  // 修改信息
  update(editModel: HTMLElement) {
    let editUrl = 'graduate-web/salary/update/';

    let body = JSON.stringify(this.salaryInfo);
    this.http.post(editUrl, body).toPromise().then(
      res => {
        if (res.json().code = '0000') {
          this.showDialog(res.json().message);
          this.getData();
          this.close(editModel);
          console.log(res.json().message + '！！！！！');
        }

      }
    );
  }

  // 提示框
  display: boolean = false;
  messages: '';
  /**
   * 提示框
   */
  showDialog(string) {
    this.messages = string;
    this.display = true;
  }
  // 关闭模态框
  close(modal: HTMLElement) {
    modal.style.visibility = 'hidden';
    this.salaryInfo = new SalaryEntity();
  }
  /**
   *modal框可拖拽
  */
  // draggable() {
  //   $('.modal-dialog').draggable();
  // }




  //打开添加模态框
  addOpen(el: HTMLElement, addModal: HTMLElement) {

    addModal.style.visibility = 'visible';
    //document.getElementById('userId').focus();
  }

  //添加信息
  add(addModal: HTMLElement) {
    let addUrl = 'graduate-web/salary/insert/';

    let body = JSON.stringify(this.salaryInfo);
    this.http.post(addUrl, body).toPromise().then(
      res => {
        this.showDialog(res.json().message);
        this.getData();//回显
        this.close(addModal);
        console.log(res.json().message + '！！！！！');
      }
    );
  }

 
  deleteDatas(el: any) {
    // console.log(el);
      console.log(this.data[el[0]].id);
    // console.log(this.data[el[1]].id);
    // console.log(this.data[el[2]].id);
    //、、 let operator = '徐玲博-假数据';

    this.confirmationService.confirm({
      message: '你确定删除学历信息吗？',
      accept: () => {
        for (let i = 0; i < el.length; i++) {
          let operator = this.data[el[i]].operator;
          let id = this.data[el[i]].id;
          console.log(this.data[el[i]]);
          let deleteUrl = 'graduate-web/salary/delete' + '/' + id + '/' + operator;
          console.log(deleteUrl + '执行到这了');

          let body = JSON.stringify(null);
          console.log(this.http.post(deleteUrl, body) + '执行到这了2');
          this.http.post(deleteUrl, body).subscribe(
            res => {
              
              if (res.json().code != null && res.json().code == '0000') {
                this.showDialog(res.json().message);
                console.log(res.json().data + '执行到这了3');

                this.getData();
                this.total--;
              } else {
                this.showDialog(res.json().message);
                this.getData();
              }
            }
            );
        }
      }
    });
  }
 
  
  //表格操作-于云丽-2017年12月29日
  operatData(obj: any) {
  
    switch (obj.element.innerText) {
      case "编辑":
        localStorage.setItem("isEdit", JSON.stringify(true));      
        this.router.navigateByUrl('/workspace/person-info/add-person-info/add-salary');
        break;
    }
  }
 //表格添加-于云丽-2017年12月29日
  addInscription(obj: any) {
    localStorage.setItem("isEdit", JSON.stringify(false));
    this.router.navigateByUrl('/workspace/person-info/add-person-info/add-salary');
  }

 
 /**
  * 将有格式的替换成没有格式的
  */
  onConvert(obj: any) {
    let showText;
    showText = obj.replace(/&nbsp;/g, ' ');
    showText = showText.replace(/<br>/g, '\n');
    return showText;
  }
  
  doLogin() {
    this.router.navigateByUrl('/workspace/person-info/search-person-info/search-home');
  }
}
