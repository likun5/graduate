import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//引用拦截器文件
import { InterceptorService } from "../../../share/interceptor.service";
import { Observable } from "rxjs/Observable";

import { StepsModule, MenuItem } from 'primeng/primeng';

@Component({
    selector: 'app-search-person-info',
    templateUrl: './search-person-info.component.html',
    styleUrls: ['./search-person-info.component.css']
})
export class SearchPersonInfoComponent implements OnInit {

    items: MenuItem[];
    activeIndex: number = 0;

    localurl: string = "workspace/person-info/search-person-info";

    //实例化拦截器服务
    constructor(private router: Router) { }

    ngOnInit() {
        this.items = [
            {
                label: '个人信息',
                command: (event: any) => {
                    this.activeIndex = 0;
                    this.router.navigate([this.localurl + '/search-basic-info']);
                }
            },
            {
                label: '学历信息',
                command: (event: any) => {
                    this.activeIndex = 1;
                    this.router.navigate([this.localurl + "/search-education"]);
                }
            },
            // {
            //     label: '公司信息',
            //     command: (event: any) => {
            //         this.activeIndex = 2;
            //         this.router.navigate([this.localurl + '/search-company']);
            //     }
            // },
            {
                label: '薪资情况',
                command: (event: any) => {
                    this.activeIndex = 2;
                    this.router.navigate([this.localurl + '/search-salary']);
                }
            },
            {
                label: '家庭信息',
                command: (event: any) => {
                    this.activeIndex = 3;
                    this.router.navigate([this.localurl + '/search-home']);
                }
            },
            // {
            //     label: '风景信息',
            //     command: (event: any) => {
            //         this.activeIndex = 5;
            //         this.router.navigate([this.localurl + '/search-landscape']);
            //     }
            // },
            // {
            //     label: '特产信息',
            //     command: (event: any) => {
            //         this.activeIndex = 6;
            //         this.router.navigate([this.localurl + '/search-local-product']);
            //     }
            // }

        ];
    }

    pre() {
        switch (this.activeIndex) {
            // case 0:
            //     this.router.navigate([this.localurl + '/add-basic-info']);
            //     break;
            case 1:
                this.router.navigate([this.localurl + "/search-basic-info"]);
                break;
            case 2:
                this.router.navigate([this.localurl + '/search-education']);
                break;
            // case 3:
            //     this.router.navigate([this.localurl + '/search-company']);
            //     break;
            case 3:
                this.router.navigate([this.localurl + '/search-salary']);
                break;
            // case 5:
            //     this.router.navigate([this.localurl + '/search-landscape']);
            //     break;
            // case 6:
            //     this.router.navigate([this.localurl + '/search-local-product']);
            //     break;

        }
        this.activeIndex = this.activeIndex - 1;
    }


    next() {

        switch (this.activeIndex) {
            case 0:
                // this.ls.setObject(
                //     "enrollstudent", student
                // );
                // console.log(this.ls.getObject("enrollstudent"));

                this.router.navigate([this.localurl + "/search-education"]);
                break;

            // case 1:
            //     //获取localstorage存取的true和false
            //     // if (this.ls.get("convention") == "false") {
            //     //     alert("请同意公约条款");
            //     //     return;
            //     // }
            //     // this.ls.setObject(
            //     //     "enrollstudent", student
            //     // );
            //     // console.log(this.ls.getObject("enrollstudent"));
            //     this.router.navigate([this.localurl + '/search-company']);
            //     break;

            case 1:
                // this.ls.setObject(
                //     "enrollstudent", student
                // );
                // console.log(this.ls.getObject("enrollstudent"));
                this.router.navigate([this.localurl + '/search-salary']);
                break;

            case 2:
                // this.ls.setObject(
                //     "enrollstudent", student
                // );
                // console.log(this.ls.getObject("enrollstudent"));
                this.router.navigate([this.localurl + '/search-home']);
                break;

            // case 4:
            //     this.ls.setObject(
            //         "enrollstudent", student
            //     );
            //     console.log(this.ls.getObject("enrollstudent"));
            //     this.router.navigate([this.localurl + '/freshman-register-successful']);

            // case 5:
            //     this.router.navigate([this.localurl + '/search-landscape']);
            //     break;
            // case 6:
            //     this.router.navigate([this.localurl + '/search-local-product']);
            //     break;
        }
        this.activeIndex = this.activeIndex + 1;
    }
}