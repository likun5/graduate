import { BusinessComponent } from './business.component';

export const businessRoutes = [
    {
        path: '',
        redirectTo: 'business',
        pathMatch: 'full'
    },
    {
        path: 'business',
        component: BusinessComponent
    },
    

]