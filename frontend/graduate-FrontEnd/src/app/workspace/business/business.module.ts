import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { businessRoutes } from './business.routes';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { BusinessComponent } from './business.component';

@NgModule({
  imports: [
    CommonModule,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(businessRoutes) //路由模块
  ],
  declarations: [BusinessComponent]
})
export class BusinessModule { }
