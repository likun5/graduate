import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnologyManageComponent } from './technology-manage.component';

describe('TechnologyManageComponent', () => {
  let component: TechnologyManageComponent;
  let fixture: ComponentFixture<TechnologyManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnologyManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnologyManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
