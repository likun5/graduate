// declare var $: any;
import { InterceptorService } from '../../share/interceptor.service';
import { SkillPointEntity } from 'app/models/skill-point-entity';
import { technologyManageRoutes } from 'app/workspace/technology-manage/technology-manage.routes';
// 删除确认弹框
import { ConfirmationService } from 'primeng/primeng';
//路由跳转
import { Router } from '@angular/router';
import { Component, OnInit,ViewChild } from '@angular/core';
//引入表格组件
import { DataTableModule } from 'ng-itoo-datatable';
import{FileUploader} from 'ng2-file-upload';
import { Headers, Http, Response,RequestOptions } from '@angular/http';
import { locale } from 'moment';
import { Body } from '@angular/http/src/body';

@Component({
  selector: 'app-technology-manage',
  templateUrl: './technology-manage.component.html',
  styleUrls: ['./technology-manage.component.css'],
})

export class TechnologyManageComponent implements OnInit {
  filename="";
  display=false;

  //使用ViewChild实例化 表格组件。
  @ViewChild(DataTableModule)
  private dataCom: DataTableModule;

info="";
fileUrl="";
/**
 * 选中文件-2017-09-28 21:48:13
 * @param event 
 */
selectedFileOnChanged(event:any) {
  // 打印文件选择名称
  console.log(event);
  console.log(event.target.value);
  this.filename=event.target.value;
 // this.uploadFile();
}
/**
 * 显示选择文件的框
 */
show(el:HTMLElement){
 el.click();
}

dictionnaryName="技术点管理"
dictionnaryId="MBzBRLVCkQ4fSCrYr1AmfT";
PointPid="MBzBRLVCkQ4fSCrYr1AmfT";
//树形结构
// treeURL = "graduate-web/skillPoint/selectSkillDirection";//后端-查询全部
// treeURL = 'src/Data-Test/select-skill-derection.json' 
treeURL = 'http://192.168.22.101:8080/graduate-web/skillPoint/selectSkillDirection' 
// treeURL = 'http://localhost:8080/graduate-web/skillPoint/selectSkillDirection' 

url="graduate-web/skillPoint/PageSelectSkillByDirectionId/";

// url='src/Data-Test/select-skill-point.json';
  click(obj:any){
    this.dictionnaryName=obj.name;  //右边上方名字联动显示
    // this.dictionnaryName=obj.tech_name;
    //字典类型id：
    this.dictionnaryId=obj.id;   //获取pid
    this.PointPid=obj.pid;
    this.getData(this.url,this.dictionnaryId);
    this.info="";
  }

 //modal框的名字
  property:string;
//表格的标题
 /*************表格所需要初始化的变量******************************** */
 constructor(    
  private router: Router,
  private confirmationService: ConfirmationService,
  private http: InterceptorService) {  }
  //初始化查询数据。把data赋给表格
  
  ngOnInit() {
    this.dictionnaryId="MBzBRLVCkQ4fSCrYr1AmfT"; //自定义一个pid，默认查询的数据
    this.getData(this.url,this.dictionnaryId);
  }

  /**
 * 字典实体
 */
// dicmodel=new DictionaryModel();
technologyOptions: string[];//技术方向
skillPointInfo: SkillPointEntity = new SkillPointEntity();

arr = new Array();
  btnAdd:boolean  = true;
  btnDelete:boolean  = true;
  btnImport:boolean  = false;
  btnExport:boolean = false;
  btnEdit:boolean=false;
  btnList: string[] = ["编辑"];
 title=['技术点名称','技术点描述'];
  arrbutes = ["techName", "remark"];
  paging:boolean = false;
  data = new Array();
  total: number;
  pageSize = 10;
  page = 1;
  sizeList = [5, 10, 20, 50];
  isCheck = true;
  btnstyle = [
    { "color": "green" },
    { "color": "red" },
    { "color": "" }]
  
    /****************************************************************** */
  /**
   * 查询表结构
   */
  getData(url:string,dictionnaryId:string) {
    // let urlsearch=`${url}/${id}/${dic}`;
    // let url = this.selectCategoryUrl  + this.page + '/' + this.pageSize; // 后端-分页查询
    let urlsearch=this.url+ this.dictionnaryId + '/'+ this.page + '/' + this.pageSize;
    // alert("我在这里"+this.dictionnaryId);  //测试程序用
    this.http.get(urlsearch).subscribe(
      res=>{
        if (res.json().code == '0000') {
          this.data = res.json().data.list;
          console.log(this.data);
          this.total = res.json().data.total; // 显示条数
        }
        // console.log(res);
        // this.data=res.json().data;
        // this.total=res.json().data.length;
      }
    )
   
  }

 /**
  * 编辑
  * @param obj
  * @param editmodal 
  */
  operatData(obj: any, modal: HTMLElement) {
  
      this.skillPointInfo=this.data[obj.data];
      console.log(this.skillPointInfo);
      this.property="编辑";
      modal.style.visibility = 'visible';
    
  }

  /**
   * 编辑更新方法
   */
  save(modal:HTMLElement) {
      
    this.skillPointInfo.operator="haoguiting";
    this.skillPointInfo.operatorId = localStorage.getItem('userId');  //获得登录用户的id作为操作人的id
    if(this.skillPointInfo.id!=""&&this.skillPointInfo.id!=null){
      console.log(this.skillPointInfo);
    let urlUpdate="graduate-web/skillPoint/updateSkillPoint/";
    let body=JSON.stringify(this.skillPointInfo);
    this.http.post(urlUpdate,body).subscribe(
      res=>{
        if(res.json().code=="0000"){
          this.msgs=[{
            severity:'success',
            summary:'提示',
            detail:"恭喜您,更新成功!"      
         }]
          this.getData(this.treeURL,this.dictionnaryId);
          this.close(modal);
        }else{
          this.msgs=[{
            severity:'error',
            summary:'警告',
            detail:"亲,更新失败!"      
         }]
        }
      }
    )
  }
  }
 /**
  * 编辑弹框出现-常银玲-2017-09-18 11:39:25
  */
  edit(obj,modal){
    console.log(obj);
    if(obj.length==0||obj==""){
      alert("不好意思，请选中之后进行编辑");
      return;
    }
    if(obj.length>1){
      alert("只能选择一条记录!");
      return;
    }else{
      this.skillPointInfo=this.data[obj];
      this.property="编辑";
      modal.style.visibility = 'visible';
    }
    
  }

/**
 * 添加技术点-郝贵婷-2018-2-21 15:49:57
 */
add(modal:HTMLElement){
  /**
   * 编辑接口
   */
  // console.log(this.property);
  console.log(this.skillPointInfo);
  
  if(this.skillPointInfo.techName ==undefined){
    console.log("技术点名称不能为空！");
    // this.showDialog("技术点名称不能为空！");
    alert("技术点名称不能为空！");
    return;
  }

  if (this.property=="编辑"){

    this.save(modal);
    return ;
  }
  this.skillPointInfo.pid=this.dictionnaryId;
  // this.skillPointInfo.operator="haoguiting2.27";
  this.skillPointInfo.operator=localStorage.getItem('userName');
  this.skillPointInfo.operatorId = localStorage.getItem('userId');  //获得登录用户的id作为操作人的id
  // this.skillPointInfo.pId=this.PointPid;  //获得pid

  console.log(this.skillPointInfo);
  

  if(this.skillPointInfo.techName!="" && this.skillPointInfo.techName!=null){
    console.log(this.skillPointInfo);
  

  let urlAdd="graduate-web/skillPoint/addSkillPoint/";
  console.log("----remark");
  console.log(this.skillPointInfo);
  let body=JSON.stringify(this.skillPointInfo);
  console.log("---remark01-----");
  console.log(body);
  this.http.post(urlAdd,body).subscribe(
    res=>{
      if(res.json().code=="0000"){
        console.log('body');
        console.log(body);
        this.msgs=[{
          // severity:'success', /绿色框提示信息
          severity:'success',   //蓝色框提示信息
          summary:'提示',
          detail:"恭喜您,添加成功!"      
       }]

        this.getData(this.treeURL,this.dictionnaryId)
        this.close(modal);
      }else{
      
        this.msgs=[{
          severity:'error',
          summary:'警告',
          detail:"亲,添加失败!"      
       }]
       this.getData(this.treeURL,this.dictionnaryId)
       this.close(modal);
      }
    }
  )
}}
    
    /**
   * 添加弹框出来，然后令dicmodel为空
   */
  open(el: any, modal: HTMLElement) {
    this.skillPointInfo=new SkillPointEntity();
    this.property="添加";
    console.log(this.skillPointInfo);
    modal.style.visibility = 'visible';
  }
 /**
  * 关闭弹框
  * @param el 
  */
  close(modal: HTMLElement) {
    modal.style.visibility = 'hidden';
  }

  //右上角提示信息
  msgs:any;
  //提示框
  messages: "";
  
  /**
   * 提示框
   */
  showDialog(string) {
    this.messages = string;
    this.display = true;
  }
  // 删除：一个或多个技术点
  deleteDatas(el: any) {
    // console.log(this.data[el[0]].id);
    let operator = 'hgt-测试误删';
    this.confirmationService.confirm({
      message: '你确定删除通知类别吗？',
      accept: () => {
        for (let i = 0; i < el.length; i++) {
          let id = this.data[el[i]].id;
          console.log(this.data[el[i]]);
          let deleteUrl = 'graduate-web/skillPoint/deleteSkillPoint' + '/' + id + '/' + operator;
          let body = JSON.stringify(null);
          this.http.post(deleteUrl, body).subscribe(
            res => {
              if (res.json().code != null && res.json().code == '0000') {
                this.msgs = [{severity:'success', summary:'提示：', detail:'删除成功！'}];
                this.getData(this.url,this.dictionnaryId);
                this.total--;
              } else {
                this.showDialog(res.json().message);
                this.getData(this.url,this.dictionnaryId);
              }
            }
          );
        }
      }
    });
  }

  /**
   * 搜索-郝贵婷-2018-2-26 16:15:38
   */
  query(info:any) {
 
    //需要添加模糊查询方法
    let url="";
    if(info.trim == null || info.trim == "/%20"){
      url='graduate-web/skillPoint/fuzzySkillInfoByName/' + this.dictionnaryId
    }else{
      url='graduate-web/skillPoint/fuzzySkillInfoByName/' + this.dictionnaryId + "?strLike=" + this.info 
    }
    this.http.get(url).subscribe(
     res => {
       if (res.json().code == '0000') {
         this.data=res.json().data;
         this.total=res.json().data.length;
       }
     })
}
  
}


