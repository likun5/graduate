import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { technologyManageRoutes } from './technology-manage.routes';
import { TechnologyManageComponent } from './technology-manage.component';
import { DataTableModule } from 'ng-itoo-datatable';
import { FileUploadModule } from 'ng2-file-upload';
import { FormsModule } from '@angular/forms';
import { ButtonModule, GrowlModule,ConfirmDialogModule,DialogModule } from 'primeng/primeng';
import { TreeModule } from 'ng-itoo-tree';
import { ConfirmationService } from 'primeng/primeng';
import { MessagesModule } from 'primeng/primeng';
import { TechnologyDirectionComponent } from './technology-direction/technology-direction.component'; //提示框

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    FileUploadModule,
    FormsModule, // ngModel
    DialogModule,
    ButtonModule,
    MessagesModule,//提示框
    GrowlModule,
    TreeModule,
    ConfirmDialogModule,
    // ConfirmationService,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(technologyManageRoutes) //路由模块
  ],
  declarations: [TechnologyManageComponent, TechnologyDirectionComponent]
})
export class TechnologyManageModule { }
