
import { TechnologyManageComponent } from './technology-manage.component';
import { TechnologyDirectionComponent } from './technology-direction/technology-direction.component'; 

export const technologyManageRoutes = [
    {
        path: '',
        redirectTo: 'technology-manage',
        pathMatch: 'full'
    },
    {
        path: 'technology-manage',
        component: TechnologyManageComponent
    },
    {
        path: 'technology-direction',
        component: TechnologyDirectionComponent
    }
];