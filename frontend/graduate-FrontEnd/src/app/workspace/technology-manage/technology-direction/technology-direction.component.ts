import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NoticeCategoryEntity } from '../../../models/notice-category-entity';
import { InterceptorService } from "../../../share/interceptor.service"; //引用拦截器文件
import { ConfirmationService } from 'primeng/primeng';  //删除确认弹框
import { SkillPointEntity } from 'app/models/skill-point-entity';

@Component({
  selector: 'app-technology-direction',
  templateUrl: './technology-direction.component.html',
  styleUrls: ['./technology-direction.component.css']
})
export class TechnologyDirectionComponent implements OnInit {

  constructor( 
    private router: Router,
    private confirmationService: ConfirmationService,
    private http: InterceptorService) { }

  ngOnInit() {
    this.getData();
  }
   
  
  skillPointInfo: SkillPointEntity = new SkillPointEntity();  //获取技术方向实体
  noticeCategory: NoticeCategoryEntity = new NoticeCategoryEntity();//通知类别实体
  selectSkillDirectionUrl = "graduate-web/skillPoint/PageSelectSkillDirection/";//后端-分页查询技术方向-hgt-2018年3月10日
  //获取表格数据
  getData() {
    let url = this.selectSkillDirectionUrl  + this.page + '/' + this.pageSize; // 后端-分页查询
    this.http.get(url).subscribe(   
      res => {
        if (res.json().code == '0000') {
          this.data = res.json().data.list;
          console.log(this.data);
          this.total = res.json().data.total; // 未显示
        }
      }
    );
  }



  //无查询条件时传递 空格|%20
  noContent = "%20";

  // 表格数据
  btnAdd = true;
  btnDelete = true;
  btnEdit = false;
  btnImport = false;
  btnExport = false;
  title = ['技术方向', '内容描述'];
  arrbutes = ["techName", 'remark'];
  btnList: string[] = ["编辑"];
  //true为真分页
  paging = true;
  pageSize = 10;
  page = 1;
  sizeList = [5, 10, 15, 50];
  addInfo: any = new Array();
  arr = new Array();
  btnstyle = [
    { "color": "blue" }]
  total: number;
  data = new Array();
  isCheck = true;

  //自定义编辑列
  operatData(obj: any, editModal: HTMLElement) {
    switch (obj.element.innerText) {
      case '编辑':
        this.editOpen(obj.data, editModal);
        break;
      default:
        break;
    }
  }

  // 删除   一个或多个通知类别
  deleteDatas(el: any) {
    let operator = 'hgt-测试误删';
    this.confirmationService.confirm({
      message: '你确定删除通知类别吗？',
      accept: () => {
        for (let i = 0; i < el.length; i++) {
          let id = this.data[el[i]].id;
          console.log(this.data[el[i]]);
          this.skillPointInfo.operator=localStorage.getItem('userName');     //获得登录用户的name作为操作人的name
          this.skillPointInfo.operatorId = localStorage.getItem('userId');  //获得登录用户的id作为操作人的id
          let deleteUrl = 'graduate-web/skillPoint/deleteSkillPoint' + '/' + id + '/' + operator;
          let body = JSON.stringify(null);
          this.http.post(deleteUrl, body).subscribe(
            res => {
              if (res.json().code != null && res.json().code == '0000') {
                this.msgs = [{severity:'success', summary:'提示：', detail:'删除成功！'}];
                this.getData();
                this.total--;
              } else {
                this.showDialog(res.json().message);
                this.getData();
              }
            }
          );
        }
      }
    });
  }

    /**
  * 判断字符串是否为 null或为 "" 值，是则返回true，否返回false 郑晓东 2017年11月20日17点43分
  * @param obj 字符串
  */
  isStrEmpty(obj: string) {
    if (obj == undefined || obj == null || obj.trim() == "") {
      return true;
    }
    return false;
  }

  //打开添加模态框
  addOpen(el: HTMLElement, addModal: HTMLElement) {
    addModal.style.visibility = 'visible';
    // document.getElementById('name').focus();
  }

  add(addModal: HTMLElement) {

    if(this.skillPointInfo.techName ==undefined){
      console.log("技术点名称不能为空！");
      // this.showDialog("技术点名称不能为空！");
      alert("技术点名称不能为空！");
      return;
    }
    let addUrl = "graduate-web/skillPoint/addSkillPoint/";
    console.log(this.skillPointInfo);
    let body = JSON.stringify(this.skillPointInfo);
    this.http.post(addUrl,body).subscribe(
      res => {
        if (res.json().code = "0000") {
          console.log(res.json().code);
          this.msgs = [{severity:'info', summary:'提示：', detail:'添加成功！'}];
          this.close(addModal);
          this.getData();
        }else{      
          this.msgs=[{
            severity:'error',
            summary:'警告',
            detail:"亲,添加失败!"      
         }]         
         this.close(addModal);
         this.getData();
        }
      }
    );
  }

  //提示框
  display: boolean = false;
  messages: "";
  /**
   * 提示框
   */
  showDialog(string) {
    this.messages = string;
    this.display = true;
  }
  //右上角提示信息
  msgs:any;

    // 打开修改模态框
    editOpen(index: any, editModal: HTMLElement) {
      editModal.style.visibility = 'visible';
      this.skillPointInfo.techName = this.data[index].techName;
      this.skillPointInfo.remark = this.data[index].remark;
      this.skillPointInfo.id = this.data[index].id;
      // document.getElementById('name').focus();
    }

  // 修改信息
  update(editModel: HTMLElement) {
    let editUrl = "graduate-web/skillPoint/updateSkillPoint/";
    console.log(this.skillPointInfo.id + "test-id");
    let body = JSON.stringify(this.skillPointInfo);
    // this.http.post(editUrl, body).toPromise().then(   //另一种写法，区别是……
    this.http.post(editUrl,body).subscribe(
      res => {
        if (res.json().code = '0000') {
          this.msgs = [{severity:'success', summary:'提示：', detail:'修改成功！'}];
          this.getData();
          this.close(editModel);
        }

      }
    );
  }

  // 更新页码,更新表格信息
  changepage(data: any) {
    this.page = data.page;
    this.pageSize = data.pageSize;
    this.getData();
  }

  /*********************添加修改模态框End************************/

  // 关闭模态框
  close(modal: HTMLElement) {
    modal.style.visibility = 'hidden';
    this.skillPointInfo = new SkillPointEntity();  //new一个新类
  }
  /**
   *modal框可拖拽
  */
  draggable() {
    $('.modal-dialog').draggable();
  }


}
