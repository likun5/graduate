import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnologyDirectionComponent } from './technology-direction.component';

describe('TechnologyDirectionComponent', () => {
  let component: TechnologyDirectionComponent;
  let fixture: ComponentFixture<TechnologyDirectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnologyDirectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnologyDirectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
