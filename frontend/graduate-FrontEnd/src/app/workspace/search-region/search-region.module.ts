import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';//引入http的服务
import { FormsModule } from '@angular/forms';

import { RadioButtonModule } from 'primeng/primeng';
import { DataTableModule } from 'ng-itoo-datatable';
import { FileUploadModule } from 'ng2-file-upload';

import { InterceptorService } from "../../share/interceptor.service";
import { searchRegionRoutes } from './search-region.routes';
import { SearchRegionComponent } from './search-region.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';




// import {BaiduMapModule} from "angular2-baidu-map";//引入百度地图的组件
@NgModule({
  imports: [
    CommonModule,
    RadioButtonModule,
    // BaiduMapModule,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(searchRegionRoutes),
    DataTableModule,
    HttpModule,
    FormsModule,
    FileUploadModule
  ],
  declarations: [SearchRegionComponent]
})
export class SearchRegionModule { }
