import{SearchRegionComponent} from'./search-region.component';

export const searchRegionRoutes=[
    {
        path: '',
        redirectTo: 'search-region',
        pathMatch: 'full'
      },
      {
        path: 'search-region',
        component: SearchRegionComponent
      },
]