// import{SearchRegionComponent} from'./search-region.component';

// export const searchRegionRoutes=[
//     {
//         path: '',
//         redirectTo: 'search-region',
//         pathMatch: 'full'
//       },
//       {
//         path: 'search-region',
//         component: SearchRegionComponent
//       },
// ]

import {CommercialComponent}  from './commercial.component';

export const commercialRoutes =[
    {
        path:'',
        redirectTo:'commercial',
        pathMatch:'full',

    },
    {
        path:'commercial',
        component:CommercialComponent
    }

]