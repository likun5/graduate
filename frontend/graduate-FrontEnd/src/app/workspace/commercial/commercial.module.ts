import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import {commercialRoutes} from './commercial.routes';
//import { personInfoRoutes } from './person-info.routes';
//import { SearchPersonInfoComponent } from './search-person-info/search-person-info.component';
import { DataTableModule } from 'ng-itoo-datatable';
import { FileUploadModule } from 'ng2-file-upload';
import{CommercialComponent} from './commercial.component';
// import { PersonInfoComponent } from './person-info.component';
// import { AddPersonInfoModule } from './add-person-info/add-person-info.module';
// import {SearchPersonInfoModule} from './search-person-info/search-person-info.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    DataTableModule,
    FileUploadModule,
    CommonModule,
    
    //AddPersonInfoModule,
   // SearchPersonInfoModule,
    FormsModule,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(commercialRoutes),
   // RouterModule.forChild(personInfoRoutes) //路由模块

  ],
  declarations: [CommercialComponent]
})
export class CommercialModule { }