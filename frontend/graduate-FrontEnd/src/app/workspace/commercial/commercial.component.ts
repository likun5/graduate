import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InterceptorService } from '../../share/interceptor.service';
import { SerachRegionInfo } from 'app/models/search-region-info';
import { CommercialInfo } from 'app/models/commercialInfo';
import { Console } from '@angular/core/src/console';
import { concat } from 'rxjs/operators/concat';
declare var AMap: any;

@Component({
    moduleId: module.id,
    selector: 'app-commercial',
    templateUrl: './commercial.component.html',
    styleUrls: ['./commercial.component.css']
})

export class CommercialComponent implements OnInit {
    constructor(private http: InterceptorService,
        private router: Router, ) {
    }

    ngOnInit() {
        console.log("aaa");
        this.getOptions();

    }

    // /****************************变量声明******************************************* */
    regionInfo:SerachRegionInfo=new SerachRegionInfo();//查询所需要的变量
    // getData(){
    // this.http.get(this.url).subscribe(
    // res=>{
    // if(res.json().code=="0000"){
    // this.data=res.json().data;
    // console.log(this.data);
    // this.total=res.json().total;
    // console.log(this.total);
    // }
    // }
    // );
    // }



    changepage(data: any) {
        let url = "";

    }

    /*************************************************省市县的三级联动************************ */

    getOptions() {

        this.getProvinceOptions();
        this.getAllCommercialInfo();
    }

    provinceOptions: string[] = [];
    getProvinceOptions() {
        let url = "graduate-web/administrativeRegion/selectProvince";
        this.http.get(url).subscribe(
            res => {
                console.log("123");
                if (res.json().code == "0000") {
                    this.provinceOptions = res.json().data;
                }
            }
        );
    }

    changeProvice(proviceId: string) {
        this.getCityOptions(proviceId)
    }

    cityOptions: string[] = [];
    getCityOptions(proviceId: string) {
        let url = "graduate-web/administrativeRegion/selectSubRegionById/" + proviceId;
        this.http.get(url, "GET").subscribe(
            res => {
                if (res.json().code == "0000") {
                    this.cityOptions = res.json().data;                  
                }
            }
        );
    }
 
    changeCity(cityId: string) {
        this.getCountyOptions(cityId);
    }

    countyOptions: string[] = [];
    getCountyOptions(cityId: string) {
        let url = "graduate-web/administrativeRegion/selectSubRegionById/" + cityId;
        this.http.get(url, "GET").subscribe(
            res => {
                if (res.json().code == "0000") {
                    this.countyOptions = res.json().data;
                }
            }
        );
    }


    lanlatList: CommercialInfo[]=[]
    getAllCommercialInfo(){
        
      let  url="graduate-web/commercialOppotunity/queryAllCommercialInfo";
      var map = new AMap.Map("map", { resizeEnable: true });
      var infoWindow = new AMap.InfoWindow({ offset: new AMap.Pixel(0, -30) });
      this.http.get(url).subscribe(
        res =>{
            if(res.json().code=="0000"){
                this.lanlatList=res.json().data;        
                  for (var i = 0, marker; i < this.lanlatList.length; i++) {
                      var lngY=this.lanlatList[i].commercialLng;
                      var latX=this.lanlatList[i].commercialLat;         
                      var marker = new AMap.Marker({
                      icon:"https://webapi.amap.com/theme/v1.3/markers/n/mark_b"+(i+1)+".png",    
                          position: [lngY ,latX],    
                          map: map
                      });
                      marker.content = "商机名称："+this.lanlatList[i].commercialName+"<br>"+"商机描述: "+this.lanlatList[i].commercialDes+"<br>"
                      +"联系人: "+this.lanlatList[i].persionName+"<br>"+"号码："+this.lanlatList[i].persionPhone;
                      marker.on('click', markerClick);
                      marker.emit('click', { target: marker });
                  }       
            }
        }
      )

        function markerClick(e) {
            infoWindow.setContent(e.target.content);
            infoWindow.open(map, e.target.getPosition());
        }
        map.setFitView();
     
    }

 
    pageSize = 10;
    page = 1;

    queryRegionCommercialById(){

        let  url="graduate-web/commercialOppotunity/queryRegionCommercialById/"+this.regionInfo.countyId+"/"+this.page + "/" + this.pageSize
        var map = new AMap.Map("map", { resizeEnable: true });
        var infoWindow = new AMap.InfoWindow({ offset: new AMap.Pixel(0, -30) });
        this.http.get(url).subscribe(
          res =>{
              if(res.json().code=="0000"){
                  this.lanlatList=res.json().data.list;        
                    for (var i = 0, marker; i < this.lanlatList.length; i++) {

                        var lngY=this.lanlatList[i].commercialLng;
                        var latX=this.lanlatList[i].commercialLat;         
                        var marker = new AMap.Marker({
                        icon:"https://webapi.amap.com/theme/v1.3/markers/n/mark_b"+(i+1)+".png",    
                            position: [lngY ,latX],    
                            map: map
                        });
                        marker.content = "商机名称："+this.lanlatList[i].commercialName+"<br>"+"商机描述: "+this.lanlatList[i].commercialDes+"<br>"
                        +"联系人: "+this.lanlatList[i].persionName+"<br>"+"号码："+this.lanlatList[i].persionPhone;
                        marker.on('click', markerClick);
                        marker.emit('click', { target: marker });
                    }       
              }
          }
        )
  
          function markerClick(e) {
              infoWindow.setContent(e.target.content);
              infoWindow.open(map, e.target.getPosition());
          }
          map.setFitView();

    }


    /************************************搜索按钮事件********************************* */
    search() {

        var map = new AMap.Map("map", { resizeEnable: true });
        //自定义Marker
        var customMarker = new AMap.Marker({
            offset: new AMap.Pixel(-14, -34),//相对于基点的位置
            icon: new AMap.Icon({  //复杂图标
                size: new AMap.Size(27, 36),//图标大小
                image: "http://webapi.amap.com/images/custom_a_j.png", //大图地址
                imageOffset: new AMap.Pixel(-28, 0)//相对于大图的取图位置
            })
        });
        //加载地图工具条
        map.plugin(["AMap.ToolBar"], function () {
            var toolBar = new AMap.ToolBar({ locationMarker: customMarker });
            map.addControl(toolBar);
        });

        map.plugin(["AMap.Scale"], function () {
            var scale = new AMap.Scale();
            map.addControl(scale);
        });

        map.plugin(["AMap.MapType"], function () {
            var type = new AMap.MapType();
            map.addControl(type);
        });

        map.plugin(["AMap.OverView"], function () {
            var view = new AMap.OverView();
            view.open();
            map.addControl(view);
        });


        this.queryRegionCommercialById();
      
        
    }
}