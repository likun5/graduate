
import { DictionaryComponent } from './dictionary.component';

export const dictionaryRoutes = [
    {
        path: '',
        redirectTo: 'dictionary',
        pathMatch: 'full'
    },
    {
        path: 'dictionary',
        component: DictionaryComponent
    }
];