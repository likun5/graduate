/**
 * 杜娟新建字典组件-2018-2-21 15:40:53
 */
import { Component, OnInit ,ViewChild} from '@angular/core';
//引入表格组件
import { DataTableModule } from 'ng-itoo-datatable';
import{FileUploader} from 'ng2-file-upload';
import { Headers, Http, Response,RequestOptions } from '@angular/http';
import { InterceptorService } from '../../share/interceptor.service';
import { DictionaryModel} from '../../models/dictionary.model';
import { ConfirmationService } from 'primeng/primeng';

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ['./dictionary.component.css'],

  
})
export class DictionaryComponent implements OnInit {

  constructor(
     private http: InterceptorService,
     private confirmService: ConfirmationService
   
    ) { }

  ngOnInit() {
    this.dictionnaryId="GRADE";
    this.dictionnaryName="期数";
    this.getData(this.url,this.dictionnaryId,"%20");
  }
 //定义message框
  message="";
  display=false; //刚开始赋值为false，让弹框不显示。
  //定义grow框
  msgs:any;
  //定义字典
  dictionnaryName="字典表管理"
  dictionnaryId:string="%20";
  dictionnaryTypeId:string ="%20"
  id:string;
  info="";
  
  
  // treeURL ="graduate-web/dictionary/selectAllDictionaryTypeByTree";

  treeURL = 'http://192.168.22.101:8080/graduate-web/dictionary/selectAllDictionaryTypeByTree';

  url ="graduate-web/dictionary/selectDictionaryByTypeCode";
  click(obj:any){
  console.log(obj)
    this.dictionnaryName=obj.name;
    //字典类型code：
    this.dictionnaryId=obj.id;
    this.id = obj.id;
    this.dicmodel.typeCode = obj.id;
    this.dicmodel.dictTypeName=obj.name;

    
    this.getData(this.url,this.dictionnaryId,"%20");
    this.info="";
  }

  /**
 * 字典实体
 */
dicmodel=new DictionaryModel();
arr = new Array();
  btnAdd:boolean  = true;
  btnDelete:boolean  = true;
  btnImport:boolean  = false;
  btnExport:boolean = false;
  btnEdit:boolean=false;
  btnList: string[] = ["编辑"];
  title=['管理内容','备注'];
  arrbutes = ["dictName", "remark"];
  paging:boolean = false;
  data = new Array();
  total: number;
  pageSize = 10;
  page = 1;
  sizeList = [5, 10, 20, 50];
  isCheck = true;
  btnstyle = [
    { "color": "green" },
    { "color": "red" },
    { "color": "" }]
   /**
   * 根据type查询表格数据-杜娟-2018-2-25 15:02:29
   */
  getData(url:string,id:string,dic:string) {
    let urlsearch=`${url}/${id}`;
    this.http.get(urlsearch).subscribe(
      res=>{
        console.log(res);
        this.data=res.json().data;
        this.total=res.json().data.length;
      }
    )

}
/**
 * 添加字典-杜娟-2018-2-21 15:49:57
 */
  add(modal:HTMLElement){
    /**
     * 编辑接口
     */
    // console.log(this.property);
    console.log(this.dicmodel);
    
    if (this.property=="编辑"){

      this.save(modal);
      return ;
    }
    
    //获取typeCode和typeName,操作人id和操作人name
    this.dicmodel.typeCode=this.dictionnaryId;
    this.dicmodel.dictTypeName=this.dictionnaryName;
    this.dicmodel.operator=localStorage.getItem('userName');
    this.dicmodel.operatorId = localStorage.getItem('userId');

    

    if(this.dicmodel.dictName!="" && this.dicmodel.dictName!=null){
      console.log(this.dicmodel);
    

    let urlAdd="graduate-web/dictionary/addDictionary";
    console.log(this.dicmodel);
    let body=JSON.stringify(this.dicmodel);
    this.http.post(urlAdd,body).subscribe(
      res=>{
        if(res.json().code=="0000"){
          this.msgs=[{
            severity:'success',
            summary:'提示',
            detail:"恭喜您,添加成功!"      
         }]
          this.getData(this.url,this.dictionnaryId,"%20");
          this.close(modal);
        }else{
        
          this.msgs=[{
            severity:'error',
            summary:'警告',
            detail:"亲,添加失败!"      
         }]
          this.getData(this.url,this.dictionnaryId,"%20");
          this.close(modal);
        }
      }
    )
  }}
  /**
   * 显示添加弹框-杜娟-2018-2-21 15:55:57
   * @param el 
   * @param modal 
   */
  open(el: any, modal: HTMLElement) {
     this.dicmodel=new DictionaryModel();
    console.log(this.dicmodel);
    modal.style.visibility = 'visible';
    this.property="添加";
  }
  /**
   * 关闭弹框-杜娟-2018-2-21 15:56:31
   */
  close(el: HTMLElement) {
    el.style.visibility = 'hidden';
  }
  /**
  * 编辑弹框出现-杜娟-2018-2-21 16:10:59
  */
  property:string;
 
  /**
  * 编辑-杜娟-2018-2-25 15:02:39
  * @param obj
  * @param editmodal 
  */
  operatData(obj: any, modal: HTMLElement) {
    
        this.dicmodel=this.data[obj.data];
        this.property="编辑";
        modal.style.visibility = 'visible';
      
    }
  /**
   * 搜索-杜娟-2018-2-21 16:15:38
   */
  query(info:any) {
 
     //需要添加模糊查询方法
     let url="";
     if(info.trim == null || info.trim == "/%20"){
       url='graduate-web/dictionary/fuzzyDictionaryInfoByName/' + this.dictionnaryName
     }else{
       url='graduate-web/dictionary/fuzzyDictionaryInfoByName/' + this.dictionnaryName + "?strLike=" + this.info 
     }
     this.http.get(url).subscribe(
      res => {
        if (res.json().code == '0000') {
          this.data=res.json().data;
          this.total=res.json().data.length;
        }
      })
}
  /**
   * 编辑更新方法
   */
  save(modal:HTMLElement) {
      
     //获取typeCode和typeName,操作人id和操作人name
    this.dicmodel.typeCode=this.dictionnaryId;
    this.dicmodel.dictTypeName=this.dictionnaryName;
    this.dicmodel.operator=localStorage.getItem('userName');
    this.dicmodel.operatorId = localStorage.getItem('userId');

      if(this.dicmodel.id!=""&&this.dicmodel.id!=null){
        console.log(this.dicmodel);
      let urlUpdate="graduate-web/dictionary/updateDictionary";
      let body=JSON.stringify(this.dicmodel);
      this.http.post(urlUpdate,body).subscribe(
        res=>{
          if(res.json().code=="0000"){
            this.msgs=[{
              severity:'success',
              summary:'提示',
              detail:"恭喜您,更新成功!"      
           }]
            this.getData(this.treeURL,this.dictionnaryId,"%20")
            this.close(modal);
          }else{
            this.msgs=[{
              severity:'error',
              summary:'警告',
              detail:"亲,更新失败!"      
           }]
           this.getData(this.url,this.dictionnaryId,"%20");
           this.close(modal);
          }
        }
      )
    }
   }

  /**
   * 批量删除(勾选任意即可删除)-杜娟-2017-10-26 14:26:47
   */
  deleteDatas(obj:any){
    let ids=new Array(); //id的集合
     obj.forEach(element => {
       ids.push(this.data[element].id) ; 
   });
   let urldels="graduate-web/dictionary/deleteDictionary";
   let body=JSON.stringify(ids);
   this.confirmService.confirm({
    message: '你确定要删除吗?',
    accept: () => {
      this.http.delete(urldels,body).subscribe(
        res=>{
          if(res.json().code=="0000"){
            console.log(this.dictionnaryId);
            this.getData(this.url,this.dictionnaryId,"%20");
            this.message="恭喜您,删除成功!";
            this.display=true;
          }else{
            this.message="恭喜您,删除失败!";
            this.display=true;
          }
        }
      )
    } })
  }

   /**
   *modal框可拖拽-袁甜梦-2018年3月1日17:26:11
  */
  draggable() {
    $('.modal-dialog').draggable();
  }
  
}