import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { dictionaryRoutes } from './dictionary.routes';
import { DictionaryComponent } from './dictionary.component';
import { DataTableModule } from 'ng-itoo-datatable';
import { FileUploadModule } from 'ng2-file-upload';
import { FormsModule } from '@angular/forms';
import { ButtonModule, GrowlModule,ConfirmDialogModule,DialogModule } from 'primeng/primeng';
import { TreeModule} from 'ng-itoo-tree';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    FileUploadModule,
    FormsModule, // ngModel
    DialogModule,
    ButtonModule,
    GrowlModule,
    ConfirmDialogModule,
    // ConfirmationService,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(dictionaryRoutes), //路由模块
    TreeModule
  ],
  declarations: [DictionaryComponent]
})
export class DictionaryModule { }
