import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkspaceComponent } from './workspace.component';
// import { SearchSalaryEducationInfoComponent } from './search-salary-education-info/search-salary-education-info.component';

import { RouterModule } from '@angular/router';
import { workspaceRoutes } from './workspace.routes';

import { NgZorroAntdModule } from 'ng-zorro-antd';

import {MenubarModule,MenuItem} from 'primeng/primeng'; //priemng
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { DataTableModule } from 'ng-itoo-datatable';
import { FileUploadModule } from 'ng2-file-upload';

import {CalendarModule} from 'primeng/primeng';


@NgModule({
  imports: [
    DataTableModule,
    FileUploadModule,
    CommonModule,
    CalendarModule,
    MenubarModule,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(workspaceRoutes) //路由模块
  ],
  declarations: [WorkspaceComponent ,
    // SearchSalaryEducationInfoComponent
  ]
})
export class WorkspaceModule { }
