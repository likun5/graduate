
import { SearchAllPersonComponent } from './search-all-person.component';

export const searchAllPersonRoutes = [
    {
        path: '',
        redirectTo: 'search-all-person',
        pathMatch: 'full'
    },
    {
        path: 'search-all-person',
        component: SearchAllPersonComponent
    }
];