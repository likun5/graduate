import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyInformComponent } from './my-inform.component';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { myInformRoutes } from './my-inform.routes';

@NgModule({
  imports: [
    CommonModule,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(myInformRoutes) //路由模块
  ],
  declarations: [MyInformComponent]
})
export class MyInformModule { }
