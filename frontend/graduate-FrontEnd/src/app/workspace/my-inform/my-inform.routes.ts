import { MyInformComponent } from './my-inform.component';

export const myInformRoutes = [
    {
        path: '',
        redirectTo: 'mu-inform',
        pathMatch: 'full'
    },
    {
        path: 'mu-inform',
        component: MyInformComponent
    },
    

]