import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoticeComponent } from './notice.component';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { noticeRoutes } from './notice.routes';
import { SetNoticeCategoryComponent } from './set-notice-category/set-notice-category.component';
//import { AddNoticeComponent } from './add-notice/add-notice.component';
import { FileUploadModule } from 'ng2-file-upload';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'ng-itoo-datatable';
import { DialogModule,ButtonModule} from 'primeng/primeng';
import { ConfirmationService,ConfirmDialogModule } from 'primeng/primeng';
import { MessagesModule } from 'primeng/primeng'; //提示框
import {GrowlModule} from 'primeng/primeng';  //右上角提示信息

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    FileUploadModule,
    FormsModule,
    DialogModule,
    MessagesModule,//提示框
    GrowlModule,   //右上角提示框
    ButtonModule,
    ConfirmDialogModule,
    NgZorroAntdModule.forRoot(),
    RouterModule.forChild(noticeRoutes) //路由模块
  ],
  declarations: [NoticeComponent,SetNoticeCategoryComponent]

})
export class NoticeModule { }