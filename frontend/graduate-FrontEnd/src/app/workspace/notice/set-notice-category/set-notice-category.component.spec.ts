import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetNoticeCategoryComponent } from './set-notice-category.component';

describe('SetNoticeCategoryComponent', () => {
  let component: SetNoticeCategoryComponent;
  let fixture: ComponentFixture<SetNoticeCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetNoticeCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetNoticeCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
