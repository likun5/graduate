import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NoticeCategoryEntity } from '../../../models/notice-category-entity';

//引用拦截器文件
import { InterceptorService } from "../../../share/interceptor.service";
//删除确认弹框
import { ConfirmationService } from 'primeng/primeng';
@Component({
  selector: 'app-set-notice-category',
  templateUrl: './set-notice-category.component.html',
  styleUrls: ['./set-notice-category.component.css']
})
export class SetNoticeCategoryComponent implements OnInit {

  constructor(
    private router: Router,
    private confirmationService: ConfirmationService,
    private http: InterceptorService
  ) { }

  ngOnInit() {
    this.getData();
  }

  doLogin() {
    this.router.navigateByUrl('/workspace/set-notice-category');
  }

  //selectCategoryUrl = 'src/Data-Test/select-category-info.json'//模拟数据 PageSelectNoticeNoticeCategory
  //selectCategoryUrl = "graduate-web/noticeCategory/selectNoticeCategoryInfo/";//后端-查询全部
  
  selectCategoryUrl = "graduate-web/noticeCategory/PageSelectNoticeCategoryInfo/";//后端-分页查询
  

  getData() {
    //let userId = '123SdHAYGfnaGYJa68TenWzPm';
    let url = this.selectCategoryUrl  + this.page + '/' + this.pageSize; // 后端-分页查询
    //let url = this.selectCategoryUrl  //模拟数据
    //let url = this.selectCategoryUrl + userId;
    this.http.get(url).subscribe(
      
      res => {
        if (res.json().code == '0000') {
          this.data = res.json().data.list;
          console.log(this.data);
          this.total = res.json().data.total; // 未显示
        }
      }
    );
  }
  noticeCategory: NoticeCategoryEntity = new NoticeCategoryEntity();//通知类别实体


  //无查询条件时传递 空格|%20
  noContent = "%20";

  // 表格数据
  btnAdd = true;
  btnDelete = true;
  btnEdit = false;
  btnImport = false;
  btnExport = false;
  title = ['通知类别', '更新时间', '公告数量', '操作'];
  arrbutes = ["columnName", "updateTime", 'remark', "operator"];
  btnList: string[] = ["编辑"];
  //true为真分页
  paging = true;
  pageSize = 10;
  page = 1;
  sizeList = [5, 10, 15, 50];
  addInfo: any = new Array();
  arr = new Array();
  btnstyle = [
    { "color": "blue" }]
  total: number;
  data = new Array();
  isCheck = true;

  //自定义编辑列
  operatData(obj: any, editModal: HTMLElement) {
    switch (obj.element.innerText) {
      case "详情":
        this.router.navigateByUrl('/workspace/notice/notice-info');
        break;
      case '编辑':
        this.editOpen(obj.data, editModal);
        break;
      default:
        break;
    }
  }

  // 删除   一个或多个通知类别
  deleteDatas(el: any) {
    // console.log(el);
    // console.log(this.data[el[0]].id);
    // console.log(this.data[el[1]].id);
    // console.log(this.data[el[2]].id);
    let operator = 'hgt-测试误删';
    this.confirmationService.confirm({
      message: '你确定删除通知类别吗？',
      accept: () => {
        for (let i = 0; i < el.length; i++) {
          let id = this.data[el[i]].id;
          console.log(this.data[el[i]]);
          let deleteUrl = 'graduate-web/noticeCategory/deleteNoticeCategory' + '/' + id + '/' + operator;
          let body = JSON.stringify(null);
          this.http.post(deleteUrl, body).subscribe(
            res => {
              if (res.json().code != null && res.json().code == '0000') {
                // this.showDialog(res.json().message);
                this.msgs = [{severity:'info', summary:'提示：', detail:'删除成功！'}];
                this.getData();
                this.total--;
              } else {
                this.showDialog(res.json().message);
                this.getData();
              }
            }
          );
        }
      }
    });
  }

    /**
  * 判断字符串是否为 null或为 "" 值，是则返回true，否返回false 郑晓东 2017年11月20日17点43分
  * @param obj 字符串
  */
  isStrEmpty(obj: string) {
    if (obj == undefined || obj == null || obj.trim() == "") {
      return true;
    }
    return false;
  }
  //打开添加模态框
  addOpen(el: HTMLElement, addModal: HTMLElement) {

    addModal.style.visibility = 'visible';
    document.getElementById('name').focus();

  }

  add(addModal: HTMLElement) {

    if (this.noticeCategory == null) {
      this.showDialog("内容为空，请填写数据！");
    }
    let addUrl = "graduate-web/noticeCategory/addNoticeCategory/";
    let body = JSON.stringify(this.noticeCategory);
    console.log(body + "执行我!执行我!");
    this.http.post(addUrl, body).toPromise().then(
      res => {
        if (res.json().code = "0000") {
          console.log(res.json().code);
          // this.showDialog("通知类别添加成功！");
          this.msgs = [{severity:'info', summary:'提示：', detail:'添加成功！'}];
          this.close(addModal);
          this.getData();
        }
      }
    );
  }

  //提示框
  display: boolean = false;
  messages: "";
  /**
   * 提示框
   */
  showDialog(string) {
    this.messages = string;
    this.display = true;
  }


    // 打开修改模态框
    editOpen(index: any, editModal: HTMLElement) {
      editModal.style.visibility = 'visible';
      this.noticeCategory.columnName = this.data[index].columnName;
      this.noticeCategory.remark = this.data[index].remark;
      this.noticeCategory.id = this.data[index].id;
      document.getElementById('name').focus();
    }

      // 修改信息
  update(editModel: HTMLElement) {
    let editUrl = 'graduate-web/noticeCategory/updateNoticeCategory/';
    // let userId = 'SdHAYGfnaGYJa68TenWzPm'; // 假数据，从登陆信息获取
    // this.personalInfo.id = userId;
    console.log(this.noticeCategory.id + "woshiid");
    let body = JSON.stringify(this.noticeCategory);
    this.http.post(editUrl, body).toPromise().then(
      res => {
        if (res.json().code = '0000') {
          // this.showDialog(res.json().message);
          this.msgs = [{severity:'info', summary:'提示：', detail:'修改成功！'}];
          this.getData();
          this.close(editModel);
          console.log(res.json() + '！！！！！');
        }

      }
    );
  }

  // 更新页码,更新表格信息
  changepage(data: any) {
    this.page = data.page;
    this.pageSize = data.pageSize;
    this.getData();
  }

  /*********************添加修改模态框End************************/

  // 关闭模态框
  close(modal: HTMLElement) {
    modal.style.visibility = 'hidden';
    this.noticeCategory = new NoticeCategoryEntity();
  }
  /**
   *modal框可拖拽
  */
  draggable() {
    $('.modal-dialog').draggable();
  }



//打开添加模态框
// addOpen(el: HTMLElement, addModal: HTMLElement){
//   addModal.style.visibility = 'visible';
//   // document.getElementById('studentCode').focus();
// }


//右上角提示信息
msgs:any;
  
}
