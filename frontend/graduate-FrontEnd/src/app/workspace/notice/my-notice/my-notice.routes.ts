import { NoticeListComponent } from './notice-list/notice-list.component';
//import { AddNoticeComponent } from './add-notice/add-notice.component';
// import { NoticeInfoComponent } from './notice-info/notice-info.component';
import { MyNoticeComponent } from './my-notice.component';
import { NoticeAllComponent } from './notice-all/notice-all.component';
import { NoticeInfoComponent } from './notice-info/notice-info.component';
import { AddNoticeComponent } from './add-notice/add-notice.component';
import { EditNoticeInfoComponent } from './edit-notice-info/edit-notice-info.component'; //编辑通知

export const myNoticeRoutes = [
    // {
    //     // path: '',
    //     // redirectTo: 'mu-notice',
    //     // pathMatch: 'full'
    //     path: '',
    //     redirectTo: 'notice-all',
    //     pathMatch: 'full'
    // },
    // {
    //     path: 'notice-info',
    //     component: NoticeInfoComponent
    // },
    {
        path: '',
        component: MyNoticeComponent,
        children: [
          // { path: '', redirectTo: 'notice-all', pathMatch: 'full' },
          {
            path: 'notice-all',
            component: NoticeAllComponent
          },   
          {
            path: 'notice-list/:id',
            component: NoticeListComponent
          },
          {
            path: 'notice-info/:id',
            component: NoticeInfoComponent
        },  
         {
              path: 'add-notice',
              component: AddNoticeComponent
          }
          ,  
         {
              path: 'edit-notice-info/:id',
              component: EditNoticeInfoComponent
          }
        ]
      }
  
    
      
]