import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { noticeListRoutes } from './notice-list.routes';   //我的通知路由
import { FileUploadModule } from 'ng2-file-upload';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'ng-itoo-datatable';
import { NoticeListComponent } from './notice-list.component';   //我的组件
import { DialogModule } from 'primeng/primeng';
import { ConfirmationService,ConfirmDialogModule, } from 'primeng/primeng';
import { NoticeInfoComponent } from '../notice-info/notice-info.component';
import { SafeHtmlPipe } from '../keep-html.pipe'; //页面HTML转换
import { TabViewModule} from 'primeng/primeng';
import { MessagesModule } from 'primeng/primeng'; //提示框
import {GrowlModule} from 'primeng/primeng';  //右上角提示信息
// import {EditorModule} from 'primeng/editor';
import {EditorModule,DropdownModule} from 'primeng/primeng';
import { NoticeDetailComponent } from './notice-detail/notice-detail.component';   //子组件-通知详情

import { PersonalInfoEntity } from '../../../../models/personal-info-entity';
import { BrowserInfoEntity } from '../../../../models/browser-info-entity';
import { SimpleChanges} from '@angular/core';//查看是否浏览记录添加的引用
import { MenuItem } from 'primeng/primeng';//查看是否浏览记录添加的引用
import { EditNoticeInfoComponent } from '../edit-notice-info/edit-notice-info.component'; //编辑通知

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    FileUploadModule,
    FormsModule,
    DialogModule,
    EditorModule,
    DropdownModule,
    ConfirmDialogModule,
    MessagesModule,//提示框
    GrowlModule,
    ConfirmDialogModule,
    ConfirmationService,
    DialogModule,
    NgZorroAntdModule.forRoot(),
    TabViewModule,
    RouterModule.forChild(noticeListRoutes) //路由模块
  ],
  declarations: [NoticeListComponent,NoticeInfoComponent,SafeHtmlPipe,NoticeDetailComponent,EditNoticeInfoComponent]
  //providers:[ConfirmationService]
})
export class NoticeListModule { }