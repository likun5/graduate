import { NoticeListComponent } from './notice-list.component';
import { NoticeInfoComponent } from '../notice-info/notice-info.component';
import { NoticeDetailComponent } from './notice-detail/notice-detail.component';
import { EditNoticeInfoComponent } from '../edit-notice-info/edit-notice-info.component'; //编辑通知

export const noticeListRoutes = [
    {
        path: '',
        component: NoticeListComponent,
        children: [
          { path: '', redirectTo: 'notice-list', pathMatch: 'full' },
          {
            path: 'notice-list',
            component: NoticeListComponent
          },   
          {
            path: 'notice-info/:id',
            component: NoticeInfoComponent
        }
        ,   
          {
            path: 'notice-detail/:id',
            component: NoticeDetailComponent
        }
        ,  
        {
             path: 'edit-notice-info/:id',
             component: EditNoticeInfoComponent
         }
        ]
      }
  
    
      
]