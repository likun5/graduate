import { Component, OnInit,Input,OnChanges } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { NoticeCategoryEntity } from '../../../../models/notice-category-entity';
import { NoticeEntity } from '../../../../models/notice-entity';
import { InterceptorService } from "../../../../share/interceptor.service"; //引用拦截器文件
// 来自notice-info的引用
import { PersonalInfoEntity } from '../../../../models/personal-info-entity';
import { BrowserInfoEntity } from '../../../../models/browser-info-entity';
import { SimpleChanges} from '@angular/core';//查看是否浏览记录添加的引用
import { MessagesModule } from 'primeng/primeng'; //提示框
import {GrowlModule} from 'primeng/primeng';
import { MenuItem } from 'primeng/primeng';//查看是否浏览记录添加的引用
import { ConfirmDialogModule, ConfirmationService,DialogModule,TabViewModule} from 'primeng/primeng'; //弹出框



@Component({
  selector: 'app-notice-list',
  templateUrl: './notice-list.component.html',
  // templateUrl: './test.html',
  styleUrls: ['./notice-list.component.css']
})
export class NoticeListComponent implements OnInit {

  @Input() data: string[][] = new Array();  //保存根据category查询到的notice数据，全部的date
  @Input() pageNum: number;      //总页数

  constructor(    
    private router: Router,
    private confirmationService: ConfirmationService,
    private http: InterceptorService,
    private activeRoute:ActivatedRoute) { }

  doLogin() {
      this.router.navigateByUrl('/workspace/notice-list');

  }
  id:any;
  ngOnInit() {
  this.id=this.activeRoute.snapshot.paramMap.get("id");
    //this.getCategories();
    // this.getNoticesByCategory(this.id);
    // let id="99887711fbc254673bd666";  
    // this.getNoticesByCategory(id);
    
    //alert(id);
  }
  ngOnChanges(){
    this.notiecInfoFlag=true;
    this.detailNoticeFlag=false;
    this.editNoticePageFlag=false;
    this.deleteNoticePageFlag=false;
  }


   //提示框
   display: boolean = false;
  selectNoticeByCategoryUrl = "graduate-web/notice/PageSelectNoticeNoticeCategoryByColumnId/"; //后端联调



  // findNoticeDataList(index:number){
    findNoticeDataList(id:String){
      this.notiecInfoFlag=false;
      this.detailNoticeFlag=true;
      this.editNoticePageFlag=false;
      this.deleteNoticePageFlag=false;
      this.getNotices(id);
    // this.router.navigateByUrl("workspace/notice/my-notice/notice-info/" + this.notices[index].id);
    // noticeInfo.style.display = "none";
    // detailNotice.style.display = "block";
    // toolPage.style.display = "none";
   
  }

  //noticeCategory: NoticeCategoryEntity = new NoticeCategoryEntity();//通知类别实体
  //categories = new Array();
  notice: NoticeEntity=new NoticeEntity();//通知实体
  notices = new Array();

    // 更新页码,更新表格信息
    changepage(data: any) {
      this.page = data.page;
      this.pageSize = data.pageSize;
      // this.getNoticesByCategory(this.id);
    }
    //true为真分页
    paging = true;
    pageSize = 5;
    page = 1;
    sizeList = [5, 10, 15, 50];
    // pageNum:number;
    msgs:any;

  /**-------------------------------------------翻页-------------------------------------------- */
  naxtPage(){
    this.page=this.page+1;
    if(this.page>this.pageNum){
       this.msgs=[{
          severity:'info',
          summary:'提示',
          detail:"已经是最后一页"    
       }]
       this.page-=1;
      return;
    }
    // this.getNoticesByCategory(this.id);
  }
  previousPage(){
    this.page=this.page-1;
    if(this.page<1){
      this.msgs=[{
          severity:'info',
          summary:'提示',
          detail:"已经是第一页"    
       }]
      this.page+=1;
      return;
    }
    // this.getNoticesByCategory(this.id);
  }


 notiecInfoFlag=true;
 detailNoticeFlag=false;
 editNoticePageFlag=false;
 deleteNoticePageFlag=false;


test(){
  alert(this.detailNoticeFlag)
  this.notiecInfoFlag=true;
  this.detailNoticeFlag=false;
  this.editNoticePageFlag=false;
  this.deleteNoticePageFlag=false;
}


  //根据通知Id获取通知信息
  selectNoticeDetailUrl = "graduate-web/notice/findNoticeNoticeCategoryById/";
  categoryIdForDelete:any;   //全局变量供删除通知后跳转页面使用
  getNotices(id:String) {
    this.categoryIdForDelete=id; //全局变量供删除通知后跳转页面使用
    // alert("--------------------------------kfsodjfajo-------------");
    let url = this.selectNoticeDetailUrl + id; //与后端联调
    this.http.get(url).subscribe(
      res => {
        if (res.json().code == '0000') {
          this.notices = res.json().data;
          console.log(this.notices);
        }
      }
    );
  }

  browser: BrowserInfoEntity=new BrowserInfoEntity();//浏览人实体
  allBrowser = new Array();
  selectAllBrowserUrl = "graduate-web/browse/selectAllBrowserByNoticeId/";
  showDialog(id:String) {
      this.display = true;
      let url = this.selectAllBrowserUrl+ id;//与后端联调
      this.http.get(url).subscribe(
        res => {
          if (res.json().code == '0000') {
            this.allBrowser = res.json().data;
            console.log(this.allBrowser);
          }
        }
      );
  }

  //定义一个接受noticeId的变量
  noticeId:any;
  isBrowse:String;
  isNoBrowse:String;
  warm: boolean = false;
  showBrowseMarkDate(id:String) {
      this.warm = true;
      this.noticeId=id;
      this.isBrowse='1';
      this.isNoBrowse='0';
      let url = this.selectBrowseMarkUrl+id+'/' +this.isBrowse;//查询已浏览名单---与后端联调
      this.http.get(url).subscribe(
      res => {
          if (res.json().code == '0000') {
            this.allBrowseMark = res.json().data;
            console.log(this.allBrowseMark);
          }
        }
      );

      let urll = this.selectNoBrowsMarkUrl+ id+'/' +this.isNoBrowse;//查询未浏览名单---与后端联调
      this.http.get(urll).subscribe(
        res => {
          if (res.json().code == '0000') {
            this.noBrowseMark = res.json().data;
            console.log(this.noBrowseMark);
          }
        }
      );
  }
  

  browseMark: BrowserInfoEntity=new BrowserInfoEntity();//浏览人实体
  allBrowseMark = new Array();
  noBrowseMark = new Array();
  //查询已浏览名单---模拟数据测试
  selectBrowseMarkUrl = "graduate-web/browse/selectBrowseByNoticeId/"; //后端联调
  //查询未浏览名单---模拟数据测试
  selectNoBrowsMarkUrl = "graduate-web/browse/selectBrowseByNoticeId/"; //后端联调



    // 定义操作人
    opertor:any;

    //删除成功后跳转
    deleteNoticeUrl = "graduate-web/notice/deleteNotice/";
    // deleteNotice(id:String){
    //   alert("进入deleteNotice方法1======="+id);
    //   this.Confirm(id);
    //   alert("进入deleteNotice方法3======="+this.categoryIdForDelete);
    //   this.getNotices(this.categoryIdForDelete);
    //   alert("进入deleteNotice方法4======="+this.categoryIdForDelete);
      // this.notiecInfoFlag=true;
      // this.detailNoticeFlag=false;
      // this.editNoticePageFlag=false;
      // this.deleteNoticePageFlag=false;
    // }

    Confirm(id:String) {
      // alert("进入deleteNotice方法2======="+id);
      this.confirmationService.confirm({
          message: '确定删除该通知吗？',
          accept: () => {           
            this.opertor='haoguiting';
            let url = this.deleteNoticeUrl+id+'/' +this.opertor;//删除通知---与后端联调
            this.http.post(url,this.opertor).subscribe(
            res => {
                if (res.json().code == '0000') {
                  alert("删除成功！");
                  // this.msgs = [{severity:'info', summary:'提示：', detail:'删除成功！'}];
                  //确认删除后跳转路由
                  this.router.navigateByUrl("workspace/notice/my-notice/notice-all" );
                  this.notiecInfoFlag=false;
                  this.detailNoticeFlag=false;
                  this.editNoticePageFlag=false;
                  this.deleteNoticePageFlag=true;
                                   
                }else{
                  this.msgs = [{severity:'info', summary:'提示：', detail:'删除失败！'}];
                }
              }
            );
           
          },
          reject: () => {
              this.msgs = [{severity:'info', summary:'提示：', detail:'取消删除'}];
              // alert("取消删除");
          }
      });

  }

  

  //点击修改，调用修改方法
  editNoticeData(id:String){
    this.notiecInfoFlag=false;
    this.detailNoticeFlag=false;
    this.editNoticePageFlag=true;
    this.deleteNoticePageFlag=false;
    this.getNoticeDetail(id);
    // this.router.navigateByUrl("workspace/notice/my-notice/edit-notice-info/" + id);     //路由跳转到修改组件
  }

  //点击修改按钮，获取想要修改的通知详情noticeDetail
    //获取通知信息
    noticeDetail: NoticeEntity=new NoticeEntity();//通知实体
    selectNoticeInfoUrl = "graduate-web/notice/findNoticeNoticeCategoryById/";
    getNoticeDetail(id:String) {
      let url = this.selectNoticeInfoUrl + id; //与后端联调
      this.http.get(url).subscribe(
        res => {
          if (res.json().code == '0000') {
            this.noticeDetail = res.json().data;
            console.log(res.json().data);
            console.log(this.noticeDetail);
          }
        }
      );
      this.queryPage(id);  //查询树资源
    }

      /**填充页面资源树---全部接收人，并标记已选择的接收人
   * 
   */
  queryPage(id:String){
    //设置zTree属性
    let setting = {
      check: {
        enable: true,
        chkStyle: "checkbox",
        chkboxType: { "Y" : "ps", "N" : "ps" }
      },
      data: {
        simpleData: {
          enable: true,
          idKey: "id",
          pIdKey: "pId",
          rootPId: "0"
        }
      }
    };
    let zNodesData;
    let url = "graduate-web/browse/selectAllBrowserByNoticeIdAsTree/"+id ;  //后端联调
    this.http.get(url).subscribe(
      res =>{
        if(res.json().code == "0000"){
          zNodesData = res.json().data;
        }
      }
    )
  }
}
