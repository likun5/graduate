import { NoticeListComponent } from '../../notice-list/notice-list.component';
import { NoticeDetailComponent } from './notice-detail.component';
import { NoticeAllComponent } from '../../notice-all/notice-all.component';

export const noticeDetailRoutes = [
    {
        path: '',
        component: NoticeListComponent,
        children: [
         {
            path: 'notice-all',
            component: NoticeAllComponent
            },
          {
            path: 'notice-list/:id',
            component: NoticeListComponent
          },    
          {
            path: 'notice-list',
            component: NoticeListComponent
          },  
          {
            path: 'notice-detail',
            component: NoticeDetailComponent
        }
        ]
      }
  
    
      
]