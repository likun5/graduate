import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditNoticeInfoComponent } from './edit-notice-info.component';

describe('EditNoticeInfoComponent', () => {
  let component: EditNoticeInfoComponent;
  let fixture: ComponentFixture<EditNoticeInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditNoticeInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditNoticeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
