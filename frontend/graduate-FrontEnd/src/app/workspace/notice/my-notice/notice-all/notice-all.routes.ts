import { NoticeAllComponent } from './notice-all.component';
import { NoticeInfoComponent } from '../notice-info/notice-info.component';



export const noticeAllRoutes = [
    {
        path: '',
        component: NoticeAllComponent,
        children: [
          { path: '', redirectTo: 'notice-all', pathMatch: 'full' },
          {
            path: 'notice-all',
            component: NoticeAllComponent
          },   
          {
            path: 'notice-info/:id',
            component: NoticeInfoComponent
        }
        ]
      }
  
    
      
]