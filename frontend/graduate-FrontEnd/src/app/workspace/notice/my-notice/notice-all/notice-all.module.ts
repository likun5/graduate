import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { noticeAllRoutes } from './notice-all.routes';   //我的通知路由
import { FileUploadModule } from 'ng2-file-upload';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'ng-itoo-datatable';
import { NoticeAllComponent } from './notice-all.component';   //我的组件
import { DialogModule } from 'primeng/primeng';
import { ConfirmationService,ConfirmDialogModule, } from 'primeng/primeng';
import { NoticeInfoComponent } from '../notice-info/notice-info.component';
import { SafeHtmlPipe } from '../keep-html.pipe'; //页面HTML转换
import { TabViewModule} from 'primeng/primeng';
import { MessagesModule } from 'primeng/primeng'; //提示框
import {GrowlModule} from 'primeng/primeng';  //右上角提示信息
// import {EditorModule} from 'primeng/editor';
import {EditorModule,DropdownModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    FileUploadModule,
    FormsModule,
    DialogModule,
    EditorModule,
    DropdownModule,
    ConfirmDialogModule,
    MessagesModule,//提示框
    GrowlModule,
    NgZorroAntdModule.forRoot(),
    TabViewModule,
    RouterModule.forChild(noticeAllRoutes) //路由模块
  ],
  declarations: [NoticeAllComponent,NoticeInfoComponent,SafeHtmlPipe]
  //providers:[ConfirmationService]
})
export class NoticeListModule { }