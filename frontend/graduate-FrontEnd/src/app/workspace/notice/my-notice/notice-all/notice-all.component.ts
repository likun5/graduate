import { Component, OnInit } from '@angular/core';
import { NoticeEntity } from '../../../../models/notice-entity';
import { Router } from '@angular/router';
//引用拦截器文件
import { InterceptorService } from "../../../../share/interceptor.service";

@Component({
  selector: 'app-notice-all',
  templateUrl: './notice-all.component.html',
  styleUrls: ['./notice-all.component.css']
})
export class NoticeAllComponent implements OnInit {

  constructor(    private router: Router,
    //private confirmationService: ConfirmationService,
    private http: InterceptorService) { }

  doLogin() {
    this.router.navigateByUrl('/workspace/notice/my-notice/notice-all');
  }

  ngOnInit() {
    this.getNotices();
  }
  selectNoticeInfoUrl = "graduate-web/notice/PageSelectNoticeNoticeCategory/";// 后端-分页查询
  

  getNotices() {
    let url = this.selectNoticeInfoUrl  + this.page + '/' + this.pageSize; // 后端-分页查询
    this.http.get(url).subscribe(
      res => {
        console.log(res.json().data)
        if (res.json().code == '0000') {
          this.notices = res.json().data.list;
          this.pageNum= res.json().data.pages;  //获得总页数
          for(var i =0;i< this.notices.length; i++){
            if(this.notices[i].noticeContent.length>100){
              this.notices[i].preview =this.notices[i].noticeContent.substr(0,100)+'...';
            }else{
              this.notices[i].preview =this.notices[i].noticeContent;
            }
          }
          console.log(this.notices);
        }
      }
    );
  }
  findNoticeDataList(id:String, noticeInfo1: HTMLElement, detailNotice1: HTMLElement, toolPage1: HTMLElement){
    //this.router.navigateByUrl("workspace/notice/my-notice/notice-list/" + this.categories[index].id);
    // alert(id);
    // this.router.navigateByUrl("workspace/notice/my-notice/notice-info/" + id);
    noticeInfo1.style.display = "none";
    detailNotice1.style.display = "block";
    toolPage1.style.display = "none";
    this.router.navigateByUrl("workspace/notice/my-notice/notice-info/" + id);

   
  }

  notice: NoticeEntity=new NoticeEntity();//通知实体
  notices = new Array();

    // 更新页码,更新表格信息
    changepage(data: any) {
      this.page = data.page;
      this.pageSize = data.pageSize;
      this.getNotices();
    }

  //true为真分页
  paging = true;
  pageSize = 5;
  page = 1;
  sizeList = [5, 10, 15, 50];
  pageNum:number;
  msgs:any;

  /**-------------------------------------------翻页-------------------------------------------- */
  naxtPage(){
    this.page=this.page+1;
    if(this.page>this.pageNum){
       this.msgs=[{
          severity:'info',
          summary:'提示',
          detail:"已经是最后一页"    
       }]
       this.page-=1;
      return;
    }
    this.getNotices();
  }
  previousPage(){
    this.page=this.page-1;
    if(this.page<1){
      this.msgs=[{
          severity:'info',
          summary:'提示',
          detail:"已经是第一页"    
       }]
      this.page+=1;
      return;
    }
    this.getNotices();
  }
}