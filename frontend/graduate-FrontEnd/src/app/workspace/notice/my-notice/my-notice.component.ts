import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NoticeCategoryEntity } from '../../../models/notice-category-entity';
import { NoticeEntity } from '../../../models/notice-entity';


//引用拦截器文件
import { InterceptorService } from "../../../share/interceptor.service";

@Component({
  selector: 'app-my-notice',
  templateUrl: './my-notice.component.html',
  styleUrls: ['./my-notice.component.css']
})
export class MyNoticeComponent implements OnInit {

  constructor(
    private router: Router,
    //private confirmationService: ConfirmationService,
    private http: InterceptorService
  ) { }

  doLogin() {
    //this.router.navigateByUrl('/workspace/notice/my-notice');
  }

  ngOnInit() {
    
    this.getCategories();
    //this.getNotices();
    
  }

  //selectNoticeInfoUrl = 'src/Data-Test/select-notice-info.json'//模拟数据 
 // selectCategoryUrl = 'src/Data-Test/select-category-info.json'//模拟数据
  //selectNoticeInfoUrl = "graduate-web/notice/selectNoticeNoticeCategory/";
  selectCategoryUrl = "graduate-web/noticeCategory/selectNoticeCategoryInfo/";
  getCategories() {
    //let userId = '123SdHAYGfnaGYJa68TenWzPm';
    let url = this.selectCategoryUrl  //模拟数据
    //let url = this.selectCategoryUrl + userId;
    this.http.get(url).subscribe(
      res => {
        if (res.json().code == '0000') {
          this.categories = res.json().data;
          console.log(this.categories);
        }
      }
    );
  }

  //根据类别查询通知信息
  findDataList(index:number){
    // this.router.navigateByUrl("workspace/notice/my-notice/notice-list/" + this.categories[index].id);
    this.getNoticesByCategory(this.categories[index].id);
  }

  doNewNotice(){
    this.allNoticeFlag=false;
    this.listNoticeFlag = false;
    this.addNoticeFlag = true;
    //this.router.navigateByUrl("workspace/notice/my-notice/notice-tolist/" + this.categories[index].id);
    // this.router.navigateByUrl("workspace/notice/my-notice/add-notice" );
    // listNotice.style.display = "none";
    // allNotice.style.display = "none";
    // addNotice.style.display = "block";
  }

  // getNotices() {
  //   //let userId = '123SdHAYGfnaGYJa68TenWzPm';
  //   let url = this.selectNoticeInfoUrl  //模拟数据
  //   //let url = this.selectCategoryUrl + userId;
  //   this.http.get(url).subscribe(
  //     res => {
  //       if (res.json().code == '0000') {
  //         this.notices = res.json().data;
  //         for(var i =0;i< this.notices.length; i++){
  //           if(this.notices[i].noticeContent.length>100){
  //             this.notices[i].preview =this.notices[i].noticeContent.substr(0,100)+'...';
  //           }else{
  //             this.notices[i].preview =this.notices[i].noticeContent;
  //           }
  //         }
  //         console.log(this.notices);
  //       }
  //     }
  //   );
  // }
  noticeCategory: NoticeCategoryEntity = new NoticeCategoryEntity();//通知类别实体
  // notice: NoticeEntity=new NoticeEntity();//通知实体


  categories = new Array();
  // notices = new Array();
  
  notices = new Array();

  //true为真分页
  paging = true;
  pageSize = 5;
  page = 1;
  sizeList = [5, 10, 15, 50];
  pageNum:number;
  msgs:any;

  selectNoticeByCategoryUrl = "graduate-web/notice/PageSelectNoticeNoticeCategoryByColumnId/"; //后端联调-根据类别id查询通知
  /**
   * 根据类别id查询通知
   * @param id 
   */
  getNoticesByCategory(id:String) {
    // alert(id);
    this.allNoticeFlag=false;
    this.listNoticeFlag=true;
    this.addNoticeFlag=false;
    // this.allNoticeFlag=false;
    // this.listNoticeFlag= true;
    // // this.f=true;
    // this.addNoticeFlag = false;
    // alert(this.listNoticeFlag);
    // alert(this.allNoticeFlag);
    // alert("走到这里啦");

    this.notices=new Array();
    let url = this.selectNoticeByCategoryUrl + id+'/' + this.page + '/' + this.pageSize;  //后端-分页查询
    this.http.get(url).subscribe(
      res => {
        if (res.json().code == '0000') {
          // this.m = "true";
          // this.n = false;
          this.notices = res.json().data.list;
          this.pageNum= res.json().data.pages;  //获得总页数
          for(var i =0;i< this.notices.length; i++){
            if(this.notices[i].noticeContent.length>100){
              this.notices[i].preview =this.notices[i].noticeContent.substr(0,100)+'...';
            }else{
              this.notices[i].preview =this.notices[i].noticeContent;
            }
          }
          console.log(this.notices);
        }
      }
    );
    // listNotice.style.display = "block";
    // allNotice.style.display = "none";
    // addNotice.style.display = "none";
  }


  // componentType=1;
  // a=true;
  // b=false;
  // c=false;
  // f=false;

  cancleAddNotice(str:String){
    this.allNoticeFlag=false;
    this.listNoticeFlag =true ;
    this.addNoticeFlag = false;
  }

  // m="true";
  // n=false;

  //设置标识，控制div控件显示问题
  allNoticeFlag=true;
  listNoticeFlag=false;
  addNoticeFlag=false;

}

