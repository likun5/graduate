import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { NoticeEntity } from '../../../../models/notice-entity';
import { PersonalInfoEntity } from '../../../../models/personal-info-entity';
import { BrowserInfoEntity } from '../../../../models/browser-info-entity';

import { Input,OnChanges,SimpleChanges} from '@angular/core';//查看是否浏览记录添加的引用
//import { DatePipe } from '@angular/common';//查看是否浏览记录添加的引用
// import { Message } from 'primeng/components/common/api';//查看是否浏览记录添加的引用
import { MessagesModule } from 'primeng/primeng'; //提示框
import {GrowlModule} from 'primeng/primeng';

import { MenuItem } from 'primeng/primeng';//查看是否浏览记录添加的引用

// import { MessageService } from 'primeng/components/common/messageservice'; //提示信息

//引用拦截器文件
import { InterceptorService } from "../../../../share/interceptor.service";

//弹出框
import { ConfirmDialogModule, ConfirmationService,DialogModule,TabViewModule} from 'primeng/primeng';
// import { Body } from '../../../../../../node_modules/_@angular_http@4.4.6@@angular/http/src/body';


@Component({
  selector: 'app-notice-info',
  // templateUrl: './test.html',
  templateUrl: './notice-info.component.html',
  styleUrls: ['./notice-info.component.css']
})
export class NoticeInfoComponent implements OnInit {

  constructor(    private router: Router,
    private confirmationService: ConfirmationService,
    private http: InterceptorService,
    // private messageService: MessageService,    //提示信息
    private activeRoute:ActivatedRoute) { }
    
  id:any;
  ngOnInit() {
    //this.getData();
    this.id=this.activeRoute.snapshot.paramMap.get("id");
    // alert("notice-info的ngOnInit方法里："+this.id);   //测试alert
    this.getNotices(this.id);
    //this.showBrowseMark();
    //this.showNoBrowseMark();
    
  }

    doLogin() {
    //this.router.navigateByUrl('/workspace/notice/my-notice/notice-info');
  }

    //selectNoticeInfoUrl = 'src/Data-Test/select-one-notice.json'//模拟数据 
    selectNoticeInfoUrl = "graduate-web/notice/findNoticeNoticeCategoryById/";


    getNotices(id:String) {
      //let userId = '123SdHAYGfnaGYJa68TenWzPm';
      // alert("到getNotices方法啦");  //测试alert
      let url = this.selectNoticeInfoUrl + id; //与后端联调
      //let url = this.selectNoticeInfoUrl ;  //模拟数据
      //let url = this.selectCategoryUrl + userId;
      this.http.get(url).subscribe(
        res => {
          if (res.json().code == '0000') {
            this.notices = res.json().data;
            console.log(this.notices);
          }
        }
      );
    }
  
    notice: NoticeEntity=new NoticeEntity();//通知实体
    notices = new Array();

    // person: PersonalInfoEntity=new PersonalInfoEntity();//毕业生实体
    // allBrowser = new Array();
    
    // selectAllBrowserDate(){
    //   this.http.get(this.selectAllBrowserUrl).subscribe(
    //     res => {
    //       if (res.json().code == '0000') {
    //         this.allBrowser = res.json().data;
    //         console.log(this.allBrowser);
    //       }
    //     }
    //   );
    // }


  browser: BrowserInfoEntity=new BrowserInfoEntity();//浏览人实体
  allBrowser = new Array();
  //获得全部接受人信息 graduate-web/notice/selectAllBrowserByNoticeId/
  //selectAllBrowserUrl = 'src/Data-Test/select-all-browser.json' //模拟数据  id:String
  //selectAllBrowserUrl = "graduate-web/notice/selectAllBrowserByNoticeId/";
  selectAllBrowserUrl = "graduate-web/browse/selectAllBrowserByNoticeId/";
  display: boolean = false;
  showDialog(id:String) {
      this.display = true;
      let url = this.selectAllBrowserUrl+ id;//与后端联调
      // let url = this.selectAllBrowserUrl //模拟数据 
      this.http.get(url).subscribe(
        res => {
          if (res.json().code == '0000') {
            this.allBrowser = res.json().data;
            console.log(this.allBrowser);
          }
        }
      );
  }

  //定义一个接受noticeId的变量
  noticeId:any;
  isBrowse:String;
  isNoBrowse:String;
  warm: boolean = false;
  showBrowseMarkDate(id:String) {
      this.warm = true;
      this.noticeId=id;
      this.isBrowse='1';
      this.isNoBrowse='0';
      let url = this.selectBrowseMarkUrl+id+'/' +this.isBrowse;//查询已浏览名单---与后端联调
      this.http.get(url).subscribe(
      res => {
          if (res.json().code == '0000') {
            this.allBrowseMark = res.json().data;
            console.log(this.allBrowseMark);
          }
        }
      );

      let urll = this.selectNoBrowsMarkUrl+ id+'/' +this.isNoBrowse;//查询未浏览名单---与后端联调
      this.http.get(urll).subscribe(
        res => {
          if (res.json().code == '0000') {
            this.noBrowseMark = res.json().data;
            console.log(this.noBrowseMark);
          }
        }
      );
  }
  

  browseMark: BrowserInfoEntity=new BrowserInfoEntity();//浏览人实体
  allBrowseMark = new Array();
  noBrowseMark = new Array();
  //查询已浏览名单---模拟数据测试
  selectBrowseMarkUrl = "graduate-web/browse/selectBrowseByNoticeId/"; //后端联调
  //查询未浏览名单---模拟数据测试
  selectNoBrowsMarkUrl = "graduate-web/browse/selectBrowseByNoticeId/"; //后端联调


  //查询已浏览名单---模拟数据测试
  // selectBrowseMarkUrl = 'src/Data-Test/select-all-browser.json' //模拟数据  
  // showBrowseMark() {
  //     //this.display = true;
      //let url = this.selectBrowseMarkUrl+ id+isBrowse;//与后端联调
  //     let url = this.selectBrowseMarkUrl //模拟数据 
  //     this.http.get(url).subscribe(
  //       res => {
  //         if (res.json().code == '0000') {
  //           this.allBrowseMark = res.json().data;
  //           console.log(this.allBrowseMark);
  //         }
  //       }
  //     );
  // }

    //查询未浏览名单---模拟数据测试
    // selectNoBrowsMarkUrl = 'src/Data-Test/select-no-browse-mark.json' //模拟数据 
    // showNoBrowseMark() {
    //     //this.display = true;
    //     //let url = this.selectBrowseMarkUrl+ id+isNoBrowse;//与后端联调
    //     let url = this.selectNoBrowsMarkUrl //模拟数据 
    //     this.http.get(url).subscribe(
    //       res => {
    //         if (res.json().code == '0000') {
    //           this.noBrowseMark = res.json().data;
    //           console.log(this.noBrowseMark);
    //         }
    //       }
    //     );
    // }

    // 提示信息
    //msgs: Message[] = [];
    msgs:any;
    opertor:any;
    //删除框
    deleteNoticeUrl = "graduate-web/notice/deleteNotice/";
    Confirm(id:String) {
      this.confirmationService.confirm({
          message: '确定删除该通知吗？',
          accept: () => {           
            this.opertor='haoguiting';
            let url = this.deleteNoticeUrl+id+'/' +this.opertor;//删除通知---与后端联调
            this.http.post(url,this.opertor).subscribe(
            res => {
                if (res.json().code == '0000') {
                  this.msgs = [{severity:'info', summary:'提示：', detail:'删除成功！'}];
                  //确认删除后跳转路由
                  this.router.navigateByUrl("workspace/notice/my-notice/notice-all" );
                     
                }else{
                  this.msgs = [{severity:'info', summary:'提示：', detail:'删除失败！'}];
                }
              }
            );
            // graduate-web/notice/deleteNotice
            alert("删除成功");
          },
          reject: () => {
              this.msgs = [{severity:'info', summary:'提示：', detail:'取消删除'}];
              // alert("取消删除");
          }
      });
  }


  editNoticeData(id:String){
    this.router.navigateByUrl("workspace/notice/my-notice/edit-notice-info/" + id);   
  }
  

}
