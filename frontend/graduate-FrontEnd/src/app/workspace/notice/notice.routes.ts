import { NoticeComponent } from './notice.component';
import { SetNoticeCategoryComponent } from './set-notice-category/set-notice-category.component';
//import { AddNoticeComponent } from './add-notice/add-notice.component';

export const noticeRoutes = [
    {
        // path: '',
        // redirectTo: 'mu-notice',
        // pathMatch: 'full'
        path: '',
        redirectTo: 'notice-info',
        pathMatch: 'full'
    },
    // {
    //     // path: 'mu-notice',
    //     // component: NoticeComponent
    //     path: 'add-notice',
    //     component: AddNoticeComponent
    // },
    {
        path: 'set-notice-category',
        component: SetNoticeCategoryComponent
      },    
      {
        path: 'my-notice',
        loadChildren:'./my-notice/my-notice.module#MyNoticeModule'
      }

]