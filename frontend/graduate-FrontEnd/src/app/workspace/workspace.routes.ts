
import { WorkspaceComponent } from './workspace.component';
import { SearchSalaryEducationInfoComponent } from './search-salary-education-info/search-salary-education-info.component';
 
export const workspaceRoutes = [
  {
    path: '',
    component: WorkspaceComponent,
    children: [
     { path: '', redirectTo: 'person-info', pathMatch: 'full' },
    //  { path: '', redirectTo: 'person-info'},
      { path: 'person-info', loadChildren: './person-info/person-info.module#PersonInfoModule' },
      { path: 'commercial', loadChildren: './commercial/commercial.module#CommercialModule' },      
     // { path: '', redirectTo: 'serach-info', pathMatch: 'full'  }, 
      // { path: '', redirectTo: 'serach-info'},
      { path: 'search-region', loadChildren: './search-region/search-region.module#SearchRegionModule' },
      { path: 'forum', loadChildren: './forum/forum.module#ForumModule' },
      { path: 'notice', loadChildren: './notice/notice.module#NoticeModule' },
      { path: 'my-inform', loadChildren: './my-inform/my-inform.module#MyInformModule' },
      { path: 'business', loadChildren: './business/business.module#BusinessModule' },
      { path: 'technology-manage', loadChildren: './technology-manage/technology-manage.module#TechnologyManageModule' },
      {path:'salary-education',loadChildren:'./search-salary-education-info/search-salary-education-info.module#SearchSalaryEducationInfoModule'},
      {path:'search-all-person',loadChildren:'./search-all-person/search-all-person.module#SearchAllPersonModule'},
      {path:'dictionary',loadChildren:'./dictionary/dictionary.module#DictionaryModule'}      
    ] 
   },
  
];
 