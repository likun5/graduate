import { Component, OnInit ,Input,SimpleChanges} from '@angular/core';
import { DatePipe } from '@angular/common';
import { InterceptorService } from '../../../share/interceptor.service';
@Component({
  selector: 'app-each-info',
  templateUrl: './each-info.component.html',
  styleUrls: ['./each-info.component.css'],
  providers: [DatePipe],
})
export class EachInfoComponent implements OnInit {

  @Input() inputId:string;
  @Input() inputName:string;

  constructor(private http:InterceptorService,private datePipe:DatePipe) { }

  ngOnInit() {
    this.getData();
  }

  
  //！！！检测输入参数的改变，不可删除：关联上面的实现 OnChanges
  ngOnChanges(changes:SimpleChanges){

    this.getData();
  }

  // eachname:string="0期";
  titleLast='最高薪资对比图';
  title:string;
  // xAxis_data=['2013','2014', '2015', '2016', '2017', '2018', '2019'];
  // series_data=[820, 932, 901, 934, 1290, 1330, 1320];
  
  avedata:any;//平均薪资
  mindata:any;//最低薪资
  maxdata:any;//最高薪资

  //主url地址
  url:string='graduate-web/salary/';

  //根据ID获取薪资数据
  getData(){
    console.log("123");
    this.title=this.inputName+this.titleLast;

    let url:any;
    let data:any;

    //获取平均薪资
    url = this.url+'selectAverageSalaryByGradeId/'+this.inputId;
    data = this.getSalaryDataByUrl(url);
    this.avedata=this.setSalaryData(data);

    //获取最高薪资
    url=this.url+'selectHighSalaryByGradeId/'+this.inputId;
    data = this.getSalaryDataByUrl(url);
    this.maxdata = this.setSalaryData(data);

    //获取最低薪资
    url=this.url+'selectLowestSalaryByGradeId/'+this.inputId;
    data = this.getSalaryDataByUrl(url);
    this.mindata = this.setSalaryData(data);
  }

  //查询薪资数据
  getSalaryDataByUrl(url:string):any{
    this.http.get(url).subscribe(res=>{
      if(res ==undefined){
        console.log("查询"+this.inputName+"平均薪资无返回结果");
      }else{
        if(res.json().code=='0000'){
          console.log(res.json().data);
          return res.json().data;
        }else{
          console.log(res.json().message);
        }
      }
    })
    return null;
  }
   
  //设置薪资数据格式
  setSalaryData(data:any){
    let xAxis_data:any[];
    let series_data:any[];
    if(data==null || data.length==0){
      return;
    }
    for(let i=0;i<data.length;i++){
      xAxis_data.push(this.getFormatDateToYear(data[i].createTime));
      series_data.push(data[i].salary);
    }
    data = {
      labels: xAxis_data,
      datasets: [
          {
              label: this.title,
              data: series_data,
              fill: false,
              borderColor: '#4bc0c0'
          },
      ]
    }
    return data;
  }
  
  //格式化日期，获取year
  getFormatDateToYear(strDate:string){
    let date =new Date(strDate);
    // console.log(this.datePipe.transform(date,"yyyy-MM"));
    // console.log(date.getFullYear().toString());
    return this.datePipe.transform(date,"yyyy");
  }

  /** 通过期数查询对应薪资信息 */
  information:'请输入期数：*期';
  query(event:any){
    //返回结构  undefined/value  value是输入值
    let value = event.substring(event.lastIndexOf('/')+1);
    console.log(value);
    this.inputName=value;

    this.title=this.inputName+this.titleLast;

    let url:any;
    let data:any;

    //获取平均薪资
    url = this.url+''+value;
    data = this.getSalaryDataByUrl(url);
    this.avedata=this.setSalaryData(data);

    //获取最高薪资
    url=this.url+'selectHighSalaryByGradeId'+value;
    data = this.getSalaryDataByUrl(url);
    this.maxdata = this.setSalaryData(data);

    //获取最低薪资
    url=this.url+''+value;
    data = this.getSalaryDataByUrl(url);
    this.mindata = this.setSalaryData(data);
  }
  
}

// aveSalaryData:any[];
  // //查询平均工资信息
  // getAveSalaryData(url:string){
  //   this.http.get(url).subscribe(res=>{
  //     if(res ==undefined){
  //       console.log("查询"+this.inputName+"平均薪资无返回结果");
  //     }else{
  //       if(res.json().code=='0000'){
  //         console.log(res.json().data);
  //         this.aveSalaryData=res.json().data;
  //         this.title=this.inputName+this.titleLast;
  //         this.setAveSalaryData();
  //       }else{
  //         console.log(res.json().message);
  //       }
  //     }
  //   })
  // }

  // setAveSalaryData(){

  // }

  // //查询最高工资信息
  // maxSalaryData:any[];
  // getMaxSalaryData(url:string){
  //   this.http.get(url).subscribe(res=>{
  //     if(res ==undefined){
  //       console.log("查询"+this.inputName+"平均薪资无返回结果");
  //     }else{
  //       if(res.json().code=='0000'){
  //         console.log(res.json().data);
  //         this.aveSalaryData=res.json().data;
  //         this.title=this.inputName+this.titleLast;
  //         this.setMaxSalaryData();
  //       }else{
  //         console.log(res.json().message);
  //       }
  //     }
  //   })
  // }
  // setMaxSalaryData(){

  // }

  // //查询最低工资
  // minSalaryData:any[];
  // getMinSalaryData(url:string):any{
  //   this.http.get(url).subscribe(res=>{
  //     if(res ==undefined){
  //       console.log("查询"+this.inputName+"平均薪资无返回结果");
  //     }else{
  //       if(res.json().code=='0000'){
  //         console.log(res.json().data);
  //         this.aveSalaryData=res.json().data;
  //         this.title=this.inputName+this.titleLast;
  //         return res.json().data;
  //       }else{
  //         console.log(res.json().message);
  //       }
  //     }
  //   })
  //   return null;
  // }
  // setMinSalaryData(data:any[]){
  //   let minSalaryData=this.getMinSalaryData('');
  //   if(minSalaryData!=null && minSalaryData.length>0){
  //     this.minSalaryData=this.setSalaryData(minSalaryData);
  //   }
  //   return data;
  // }