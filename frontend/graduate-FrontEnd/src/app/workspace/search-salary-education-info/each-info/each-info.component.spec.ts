import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EachInfoComponent } from './each-info.component';

describe('EachInfoComponent', () => {
  let component: EachInfoComponent;
  let fixture: ComponentFixture<EachInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EachInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EachInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
