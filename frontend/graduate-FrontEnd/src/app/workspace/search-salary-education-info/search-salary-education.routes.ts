import { SearchSalaryEducationInfoComponent } from './search-salary-education-info.component';
import { OneInfoComponent } from './one-info/one-info.component';
import { EachInfoComponent } from './each-info/each-info.component';
import { EduCertInfoComponent } from './all-info/edu-cert-info/edu-cert-info.component';
import { GraduateYearInfoComponent } from './all-info/graduate-year-info/graduate-year-info.component';
import { ThisYearInfoComponent } from './all-info/this-year-info/this-year-info.component';

export const SearchSalaryEducationInfoRoutes=[
    {
        path:'',
        component:SearchSalaryEducationInfoComponent,
        children:[
            {path:'',redirectTo:'',pathMatch:'full'},
            {path:'one-info/:id/:name',component:OneInfoComponent},
            // {path:'each-info',component:EachInfoComponent},
            // {path:'edu-cert',component:EduCertInfoComponent},
            // {path:'graduate-year',component:GraduateYearInfoComponent},
            // {path:'this-year',component:ThisYearInfoComponent}
        ]
    }
]