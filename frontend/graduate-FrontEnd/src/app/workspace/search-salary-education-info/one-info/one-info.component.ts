import { Component,OnInit, Input,OnChanges,SimpleChanges} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import {Message} from 'primeng/components/common/api';
import { MenuItem } from 'primeng/primeng';

import { InterceptorService } from '../../../share/interceptor.service';

@Component({
  selector: 'app-one-info',
  templateUrl: './one-info.component.html',
  styleUrls: ['./one-info.component.css'],
  providers: [DatePipe],
})
export class OneInfoComponent implements OnInit,OnChanges {

  @Input() inputId:string;
  @Input() inputName:string;

  constructor(private http:InterceptorService,private datePipe:DatePipe,private activate:ActivatedRoute) { }

  ngOnInit() {
    this.getDate();
  }

  //！！！检测输入参数的改变，不可删除：关联上面的实现 OnChanges
  ngOnChanges(changes:SimpleChanges){

    this.getDate();
  }

  msgs: Message[];

  eachname:string="0期";
  titleLast='薪资变化图';

  title:string;
  xAxis_data:string[]=[];
  series_data:string[]=[];
  
  data:any;

  information:'请输入姓名';
  salaryInfo:any[]=[];
  userName:string='';
  salaryUrl="graduate-web/salary/";
  eduUrl="graduate-web/education/";
  getDate(){
    this.userName = this.inputName;
    let url=this.salaryUrl+'findSalaryByPersonId/'+ this.inputId;
    this.getSalaryData(url);
    url=this.eduUrl+'findEducationByPersonId/'+ this.inputId;
    this.getEduInfoDate(url);
  }

  //设置薪资数据
  getSalaryData(url:string){
    this.data=null;
     //获取个人薪资信息
     this.http.get(url).subscribe(res=>{
      if(res ==undefined){
        // this.showDialog("查询"+this.userName+"个人薪资水平无返回结果");
      }else{
        if(res.json().code=='0000'){
          this.salaryInfo=res.json().data;
          this.title=this.userName+this.titleLast;
          if(res.json().data!=null){
            this.setSalaryData();
          }
        }else{
          // this.showDialog("查询"+this.userName+"个人薪资水平无返回结果");
        }
      }
    })
  }
  //设置薪资格式
  setSalaryData(){
    //初始化数据项
    this.xAxis_data=[];
    this.series_data=[];

    //设置数据项
    for(let i=0;i<this.salaryInfo.length;i++){
      this.xAxis_data.push(this.getFormatDateToYear(this.salaryInfo[i].createTime));
      this.series_data.push(this.salaryInfo[i].salary);
    }

    this.data = {
      labels: this.xAxis_data,
      datasets: [
          {
            label: this.title,
            data: this.series_data,
            fill: false,
            borderColor: '#4bc0c0'
          }
      ]
    }
  }

  eduInfo:any[];
  //设置学历数据
  getEduInfoDate(url:string){
    // let data={
    //   id	:	"T3u9HK7jLMhKPS3auHvE8F"	
    //   operator	:	"大米时代"
    //   isDelete	:	0
    //   remark	:	1
    //   createTime	:	"2018-01-07 08:37:10"
    //   updateTime	:	"2018-01-07 08:37:10"
    //   userId	:	1
    //   isInDmt	:	1
    //   certificateNo	:	1
    //   certificateName	:	"北京师范大学学士学位"
    //   certificateType	:	1
    //   receiveTime	:	-28800000	
    //   universityName	:	1
    //   operatorId	:	"操作员id"
    //   timestampTime	:	1515844638000
    // }
    //获取个人薪资信息
    this.eduInfo=[];
    this.http.get(url).subscribe(res=>{
      if(res ==undefined){
        // this.showDialog("查询"+this.userName+"学历信息无返回结果");
      }else{
        if(res.json().code=='0000'){
          // this.showDialog(res.json().data);
          this.eduInfo=res.json().data;
          if(res.json().data!=null){
            this.setEduInfoDate();
          }
        }else{
          // this.showDialog("查询"+this.userName+"学历信息无返回结果");
        }
      }
    })

  }
  //设置学历信息
  setEduInfoDate(){
    
  }

  //格式化日期，获取year
  getFormatDateToYear(strDate:string){
    let date =new Date(strDate);
    console.log(this.datePipe.transform(date,"yyyy-MM"));
    return this.datePipe.transform(date,"yyyy-MM");
  }

  //通过名称查询个人薪资学历信息
  query(event:string){
    //返回结构  undefined/value  value是输入值
    let value = event.substring(event.lastIndexOf('/')+1);
    if(value.trim()=='%20' || value.trim()==''){
      this.showDialog("亲，没有输入查询条件，无法查询哦！");
      return;
    }
    this.userName=value;
    //调用通过名称查询薪资数据的方法
    let url=this.salaryUrl+'findSalaryByName/'+ value;
    this.getSalaryData(url);
    //调用通过名称查询学历信息的方法
    url=this.eduUrl+'findEducationByName/'+ value;
    this.getEduInfoDate(url);
  }

  
  //**********************************************提示框Start**************************************88 */
  display:boolean=false;
  messages:"";
  /**
   * 提示框
   */
  showDialog(string){
    this.messages=string;
    this.display=true;
  }
  //**********************************************提示框END**************************************88 */
}
