import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SearchSalaryEducationInfoComponent } from './search-salary-education-info.component';
import { OneInfoComponent } from './one-info/one-info.component';
import { EachInfoComponent } from './each-info/each-info.component';
import { AllInfoComponent } from './all-info/all-info.component';
import { EduCertInfoComponent } from './all-info/edu-cert-info/edu-cert-info.component';
import { GraduateYearInfoComponent } from './all-info/graduate-year-info/graduate-year-info.component';
import { ThisYearInfoComponent } from './all-info/this-year-info/this-year-info.component';

import { SearchSalaryEducationInfoRoutes } from './search-salary-education.routes';

import {SlideMenuModule,TabViewModule,ChartModule,DataTableModule,PaginatorModule,GrowlModule,MenuModule} from 'primeng/primeng';
import { SearchModule } from 'ng-itoo-search';

import {NgZorroAntdModule} from 'ng-zorro-antd';

import { DialogModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,

    SearchModule,
    DialogModule,
    RouterModule.forChild(SearchSalaryEducationInfoRoutes),
    SlideMenuModule,TabViewModule,ChartModule,DataTableModule,PaginatorModule,GrowlModule,MenuModule,
    NgZorroAntdModule
  ],
  declarations: [SearchSalaryEducationInfoComponent, OneInfoComponent, EachInfoComponent, AllInfoComponent, EduCertInfoComponent, ThisYearInfoComponent, GraduateYearInfoComponent]
})
export class SearchSalaryEducationInfoModule { }
