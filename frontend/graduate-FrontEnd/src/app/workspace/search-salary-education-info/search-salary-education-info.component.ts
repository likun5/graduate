import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/primeng';
import { Router } from '@angular/router';
import { InterceptorService } from '../../share/interceptor.service';
import { PeriodPersonModel } from 'app/models/PeriodPersonModel';
@Component({
  selector: 'app-search-salary-education-info',
  templateUrl: './search-salary-education-info.component.html',
  styleUrls: ['./search-salary-education-info.component.css']
})
export class SearchSalaryEducationInfoComponent implements OnInit {

  constructor(private http:InterceptorService,public router: Router) { }

  items: MenuItem[];

  
  ngOnInit() {
    this.getSlideMenu();
    this.getTermOptions();
  }

  //每个人菜单数据
  periodPersonModels:PeriodPersonModel[]=[];
  getSlideMenu(){
    let url = 'graduate-web/browse/findPeriodPersonModels/';
    this.http.get(url).subscribe(res=>{
      if(res ==undefined){
        console.log("查询薪资学历统计菜单无返回结果");
      }else{
        if(res.json().code=='0000'){
          this.periodPersonModels=res.json().data;
        }else{
          console.log(res.json().message);
        }
      }
    })
  }

   /**
   * 获取期数
   */

  eachs: string[][];

  getTermOptions() {
    let url = "graduate-web/dictionary/selectGrade";
    this.http.get(url).subscribe(
      res => {
        if (res.json().code == "0000") {
          this.eachs = res.json().data;
        }else{
          console.log(res.json().message);
        }
      }
    );
  }


  personId:string=null;
  personName:string;
  eachId:string=null;
  eachName:string;

  thisYear:String;
  graduateYear:String;
  eduCert:string;

  //清除数据
  clearParams(){
    this.personId=null;
    this.eachId=null;
    this.thisYear=null;
    this.graduateYear=null;
    this.eduCert=null;
  }

  //查询每个人的数据
  oneInfoClick(personId:any,personName:any){
    console.log(personId);
    this.clearParams();
    this.personId = personId;
    this.personName = personName;
    // this.router.navigateByUrl('workspace/salary-education/one-info/'+personId+'/'+personName);
  }
  //查询每期的数据
  eachInfoClick(eachId:any,eachName:any){
    console.log(eachId);
    this.clearParams();
    this.eachId= eachId;
    this.eachName=eachName;
  }
  //查询全体今年的数据
  thisYearClick(event:any){
    this.clearParams();
    this.thisYear = event.target.value;
  }
  //查询全体毕业时的数据
  graduateYearClick(event:any){
    this.clearParams();
    this.graduateYear = event.target.value;
  }
  //查询全体证书的数据
  eduCertClick(event:any){
    this.clearParams();
    this.eduCert = event.target.value;
  }

}
