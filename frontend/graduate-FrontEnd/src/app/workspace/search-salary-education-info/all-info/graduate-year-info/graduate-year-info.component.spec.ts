import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduateYearInfoComponent } from './graduate-year-info.component';

describe('GraduateYearInfoComponent', () => {
  let component: GraduateYearInfoComponent;
  let fixture: ComponentFixture<GraduateYearInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduateYearInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduateYearInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
