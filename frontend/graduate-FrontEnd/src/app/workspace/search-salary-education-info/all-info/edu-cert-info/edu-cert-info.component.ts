import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-edu-cert-info',
  templateUrl: './edu-cert-info.component.html',
  styleUrls: ['./edu-cert-info.component.css']
})
export class EduCertInfoComponent implements OnInit {

  @Input() inputId:string;

  constructor() { }

  ngOnInit() {
    console.log(this.inputId)
  }

  rows=10;
  totals=120;
  rowsPerPageOptions=[10,20,30,40];

  paginate(event:any){
    console.log(event);
    //event.first = Index of the first record
    //event.rows = Number of rows to display in new page
    //event.page = Index of the new page
    //event.pageCount = Total number of pages

  }

  cars:any[]=[
    {
      vin:'1',
      year:'郑晓东',
      color:'河北大学学士学位'
    },
    {
      vin:'2',
      year:'韩丽萍',
      color:'河北大学学士学位'
    }
  ];

}
