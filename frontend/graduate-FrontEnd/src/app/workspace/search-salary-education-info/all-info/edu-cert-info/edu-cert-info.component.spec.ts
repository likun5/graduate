import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EduCertInfoComponent } from './edu-cert-info.component';

describe('EduCertInfoComponent', () => {
  let component: EduCertInfoComponent;
  let fixture: ComponentFixture<EduCertInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EduCertInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EduCertInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
