import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-this-year-info',
  templateUrl: './this-year-info.component.html',
  styleUrls: ['./this-year-info.component.css']
})
export class ThisYearInfoComponent implements OnInit {

  @Input() inputId:string;

  constructor() { }

  ngOnInit() {
    this.getData();
  }

  eachname:string="0期";
  titel='最高薪资对比图';
  xAxis_data=['2013','2014', '2015', '2016', '2017', '2018', '2019'];
  series_data=[820, 932, 901, 934, 1290, 1330, 1320];
  
  data:any;
  mindata:any;
  maxdata:any;

  getData(){
    this.data = {
      labels: this.xAxis_data,
      datasets: [
          {
              label: this.titel,
              data: this.series_data,
              fill: false,
              borderColor: '#4bc0c0'
          },
      ]
    };
    this.mindata = {
      labels: this.xAxis_data,
      datasets: [
          {
              label: this.titel,
              data: this.series_data,
              fill: false,
              borderColor: '#4bc0c0'
          },
      ]
    };
    this.maxdata = {
      labels: this.xAxis_data,
      datasets: [
          {
              label: this.titel,
              data: this.series_data,
              fill: false,
              borderColor: '#4bc0c0'
          },
      ]
    }
  }
}
