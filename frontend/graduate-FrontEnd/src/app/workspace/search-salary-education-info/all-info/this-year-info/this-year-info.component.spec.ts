import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThisYearInfoComponent } from './this-year-info.component';

describe('ThisYearInfoComponent', () => {
  let component: ThisYearInfoComponent;
  let fixture: ComponentFixture<ThisYearInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThisYearInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThisYearInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
