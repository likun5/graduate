import { ForumComponent } from './forum.component';

export const forumRoutes = [
    {
        path: '',
        redirectTo: 'forum',
        pathMatch: 'full'
    },
    {
        path: 'forum',
        component: ForumComponent
    },


]