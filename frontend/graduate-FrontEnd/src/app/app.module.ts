
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
import { ConfirmationService,  ButtonModule, Message } from 'primeng/primeng';
import { InterceptorService } from './share/interceptor.service';

import { XHRBackend, RequestOptions } from '@angular/http';
import { LoginComponent } from './login/login.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { AuthGuardService } from './share/auth-guard.service';

import {CustomFormsModule} from "ng2-validation";
import { SignComponent } from './sign/sign.component';
import { ConfirmDialogModule ,DialogModule} from 'primeng/primeng';
import { ResetPasswordComponent } from './reset-password/reset-password.component';


export function interceptorFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions) {
  let service = new InterceptorService(xhrBackend, requestOptions);
  return service;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignComponent,
    ResetPasswordComponent,



  

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ButtonModule,
    ReactiveFormsModule,
    CustomFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    ConfirmDialogModule,
    DialogModule,
    RouterModule.forRoot(appRoutes)
  ],
  // 以下设置，访问angular站点会自动在根节点后面加一个#锚点，解决刷新报404错误
   providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy },

    //  InterceptorService
  {
    provide: InterceptorService,
    useFactory: interceptorFactory,
    deps: [XHRBackend, RequestOptions]
  },
  AuthGuardService,
  ConfirmationService
],
  bootstrap: [AppComponent]
})
export class AppModule { }
