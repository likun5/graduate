import { Component, OnInit } from '@angular/core';
import { UserModel } from '../models/user-model';
import { Router } from '@angular/router';
import { InterceptorService } from "../share/interceptor.service";
import { PersonalInfoEntity } from '../models/personal-info-entity';
@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.css']
})
export class SignComponent implements OnInit {

  constructor(
    private router: Router,
    private http: InterceptorService) {}

    logoRUL = 'assets/images/login/default-avatar.png';
    
  ngOnInit() {

  }
  user: UserModel = new UserModel();
  messages: string;
  message:string;
  private timer;
  private identificateTime: number = 5;

  //定时跳转
  // timing() {
  //   this.timer = setInterval(() => {
  //     this.identificateTime = this.identificateTime - 1;
  //     if (this.identificateTime <= 0) {
  //       this.router.navigateByUrl('login');
  //       return;
  //     }
  //   }, 1000);
  //   this.identificateTime = 5;
  // }

  // display: boolean = false;
  /**
   * 获取用户账号，判断注册是否重复
   * @param input 用户账号
   */
  getInput(input:any){
    let url = 'graduate-web/personalInfo/selectPersonByLoginId/'+input;
    this.http.get(url).toPromise().then(
      res => {
        if (res.json().code == '0000') {         
          this.messages = '该账号已存在！请重新填写！';
        }
        else{
          this.messages = '';
          
        }
      }
    )
  }
  /**
   * 注册 
   */
  doSign() {
    if (this.isStrEmpty(this.user.userRealName)) {
      this.showDialog('姓名不能为空，请填写姓名');
      //this.timing();

      return;
    }
    if (this.isStrEmpty(this.user.userCode)) {
      this.showDialog('账号不能为空，请填写账号');
      //this.timing();

      return;
    }
    if (this.isStrEmpty(this.user.password)) {
      this.showDialog('密码不能为空，请填写密码');
      //this.timing();

      return;
    }
    if (this.isStrEmpty(this.user.re_password)) {
      this.showDialog('确认密码不能为空，请填写确认密码');
      //this.timing();

      return;
    }
    if (this.user.password.trim() == this.user.re_password.trim()) {
      let url = 'graduate-web/personalInfo/signIn';
      let body = JSON.stringify(this.user);
      this.http.post(url, body).toPromise().then(
        res => {
          if (res.json().code == '0000') {          
            this.messages = res.json().message;
          }
          else{
            this.messages = res.json().message;
          }
        }
      )
    }
    else {
        this.showDialog('请保持密码和确认密码一致！');
      //this.timing();

      return;
    }
  }
  

  doLogin() {
    this.router.navigateByUrl('login');
  }

  display: boolean = false;
  DiaMessage: "";

  /**
     * 提示框
     */
  showDialog(string) {
    this.DiaMessage = string;
    this.display = true;
    let com = this;
    var t = setTimeout(function () { com.display = false }, 1500);
  }

  closeDialog() {
    this.display = false;
  }

  // 定时关闭弹出框
  timing() {
    this.timer = setInterval(() => {
      this.identificateTime = this.identificateTime - 1;
      if (this.identificateTime <= 0) {
        // clearInterval(this.timer);
        this.closeDialog();
      }
    }, 1000);
    this.identificateTime = 5;
  }

  /**
  * 判断字符串是否为 null或为 "" 值，是则返回true，否返回false
  * @param obj 字符串
  */
 isStrEmpty(obj: string) {
  if (obj == undefined || obj == null || obj.trim() == "") {
    return true;
  }
  return false;
}
}
