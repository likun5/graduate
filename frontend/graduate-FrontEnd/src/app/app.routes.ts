/*this is example-十二期杨晓风*/
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignComponent } from './sign/sign.component';
import{ResetPasswordComponent} from './reset-password/reset-password.component'
export const appRoutes = [
	{
		path: '',
		redirectTo: 'login',
		pathMatch: 'full'
	},
	{
		path: 'login',
		component: LoginComponent
	}
	, {
		path: 'sign',
		component: SignComponent
	},
	{
		path: 'reset-password',
		component: ResetPasswordComponent
	},
	{
		path: 'workspace',
		loadChildren: './workspace/workspace.module#WorkspaceModule'
	},
	// {
	// 	path: '**', // fallback router must in the last
	// 	component: LoginComponent
	// }
];