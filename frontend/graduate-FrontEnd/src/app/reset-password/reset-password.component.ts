import { Component, OnInit } from '@angular/core';
import { UserModel } from '../models/user-model';
import { Router } from '@angular/router';
import { InterceptorService } from "../share/interceptor.service";
import { PersonalInfoEntity } from '../models/personal-info-entity';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  constructor(
    private router: Router,
    private http: InterceptorService) { }

    logoRUL = 'assets/images/login/default-avatar.png';

  ngOnInit() {
  }
  user: UserModel = new UserModel();
  messages: string;
  message:string;

  /**
   * 重置
   */
  doReset() {
    if (this.isStrEmpty(this.user.userCode)) {
      this.showDialog('账号不能为空，请填写账号');
      this.messages='';
      return;
    }
    let input=this.user.userCode;
    let url1 = 'graduate-web/personalInfo/selectPersonByLoginId/'+input;
    this.http.get(url1).toPromise().then(
      res => {
        if (res.json().code == '0000') {         
          this.messages = '';
          let url = 'graduate-web/personalInfo/resetPassword';
      let body = JSON.stringify(this.user);
      this.http.post(url, body).toPromise().then(
        res => {
          if (res.json().code == '0000') {          
            this.showDialog(res.json().message);
            this.messages = '已将密码重置为账号（手机号），请登录！';
          }
          else{
            this.messages = res.json().message;
          }
        }
      )
        }
        else{
          // this.messages = '该账号不存在！请重新填写！';
          this.showDialog('该账号不存在！请重新填写!');
          // return;
        }
      }
    )
      
    
    
  }
  

  doLogin() {
    this.router.navigateByUrl('login');
  }

  display: boolean = false;
  DiaMessage: "";

  /**
     * 提示框
     */
  showDialog(string) {
    this.DiaMessage = string;
    this.display = true;
    let com = this;
    var t = setTimeout(function () { com.display = false }, 1500);
  }

  closeDialog() {
    this.display = false;
  }

  

  /**
  * 判断字符串是否为 null或为 "" 值，是则返回true，否返回false
  * @param obj 字符串
  */
 isStrEmpty(obj: string) {
  if (obj == undefined || obj == null || obj.trim() == "") {
    return true;
  }
  return false;
}
}


