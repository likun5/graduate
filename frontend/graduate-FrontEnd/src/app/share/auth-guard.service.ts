import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {
  Http,
  RequestOptionsArgs,
  RequestOptions,
  Response,
  Headers
} from '@angular/http';

@Injectable()
export class AuthGuardService {

    constructor(private http: Http) { }

    //调用远程地址，获取数据 get方法
    public getGetData(url: string): Promise<any> {
        return this.http
        .get(url)
        .toPromise()
        .then(response => response.json() as any)
        .catch(this.handleError);

    }
    //错误处理
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    /**用户按钮权限的处理 */
    private headers = new Headers({ 'Content-Type': 'application/json;' });
 
    public QueryModuleButtonApplication(url: string): Promise<any> {
        return this.http
        .post(url,{headers:this.headers})
        .toPromise()
        .then(response => response.json() as any)
        }
    
    logout():void{
        localStorage.removeItem("userId");
        // this.subject.next(Object.assign({}));
    }
    
    /*权限验证获取身份票据 */
    getTicket(): string{
        let id = localStorage.getItem("userId") || sessionStorage.getItem("userId");
        let token = localStorage.getItem("Authorization") || sessionStorage.getItem("Authorization");
        return "?id=" + id.trim() + "&token=" + token;
    }
    /*获取登录用户的userId */
    getUserId(): string{
        console.log("localuserId具体值获取"+localStorage.getItem("userId"));
        console.log("sessionuserId具体值获取"+sessionStorage.getItem("userId"));
        let userId = localStorage.getItem("userId") || sessionStorage.getItem("userId");
        return userId;
    }
}