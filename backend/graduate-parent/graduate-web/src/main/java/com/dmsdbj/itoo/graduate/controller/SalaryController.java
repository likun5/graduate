package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.SalaryEntity;
import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import com.dmsdbj.itoo.graduate.facade.SalaryFacade;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;



/**
 * @author 徐玲博
 * DESCRIPTION: demo展示
 * create: 2017-11-26 16:14:54.
 */
@Api(value = "薪资信息接口",tags="薪资信息接口")
@RequestMapping("/salary")
@Controller
public class SalaryController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(SalaryController.class);

    @Reference
    SalaryFacade salaryFacade;


    @RequestMapping(value = {"/index"},method = RequestMethod.GET)
    public String index() {
        return "salary";
    }

    /**
     * 根据ID查询
     * @param id
     */
    @ApiOperation(value="根据id查询薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
        try {
            SalaryEntity salaryEntity = salaryFacade.findById(id);
            if (salaryEntity != null) {
                return ItooResult.build("0000", "查询薪资成功",salaryEntity);
            }else{
                return ItooResult.build("1111", "查询薪资为空");
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "查询薪资失败");
        }
    }

    /**
     *根据年级查询薪资列表
     *返回对应的用户id和薪资信息列表组成的map
     */
    @ApiOperation(value="根据期数id查询最高薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/selectHighSalaryByGradeId/{gradeId}"}, method = RequestMethod.GET)
    public ItooResult selectHighSalaryByGradeId(@PathVariable String gradeId ){
        try {
            Double highSalary = salaryFacade.selectHighSalaryByGradeId(gradeId);
            //List<SalaryEntity> list = salaryFacade.selectByName(name);

            if (highSalary !=0) {
                return ItooResult.build("0000", "根据年级id查询最高薪资成功",highSalary);
            }else{
                return ItooResult.build("1111", "根据年级id查询最高薪资为空",highSalary);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据年级id查询薪资失败");
        }
    }


    /**
     *根据年级查询薪资列表
     *返回对应的用户id和薪资信息列表组成的map
     */
    @ApiOperation(value="根据期数id查询最低薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/selectLowestSalaryByGradeId/{gradeId}"}, method = RequestMethod.GET)
    public ItooResult selectLowestSalaryByGradeId(@PathVariable String gradeId ){
        try {
            Double lowestSalary = salaryFacade.selectLowestSalaryByGradeId(gradeId);
            //List<SalaryEntity> list = salaryFacade.selectByName(name);

            if (lowestSalary !=0) {
                return ItooResult.build("0000", "根据年级id查询最低薪资成功",lowestSalary);
            }else{
                return ItooResult.build("1111", "根据年级id查询最低薪资为空",lowestSalary);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据年级id查询最低薪资失败");
        }
    }
    /**
     *根据年级查询薪资列表
     *返回对应的用户id和薪资信息列表组成的map
     */
    @ApiOperation(value="根据期数id查询平均薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/selectAverageSalaryByGradeId/{gradeId}"}, method = RequestMethod.GET)
    public ItooResult selectAverageSalaryByGradeId(@PathVariable String gradeId ){
        try {
            Double averageSalary = salaryFacade.selectAverageSalaryByGradeId(gradeId);
           if (averageSalary !=0.0) {
                return ItooResult.build("0000", "根据年级id查询平均薪资成功",averageSalary);
            }else{
                return ItooResult.build("1111", "根据年级id查询平均薪资为空",averageSalary);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据年级id查询平均薪资失败");
        }
    }


    /**
     *根据人员id查询薪资列表
     *返回对应的用户id和薪资信息列表组成的map
     */
    @ApiOperation(value="根据人员id查询薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/findSalaryByPersonId/{personId}"}, method = RequestMethod.GET)
    public ItooResult findSalaryByPersonId(@PathVariable String personId){
        try {
            List<SalaryEntity>  list = salaryFacade.selectByPersonId(personId);
            //List<SalaryEntity> list = salaryFacade.selectByName(name);
            //根据姓名信息查询

            if (list.size() !=0) {
                return ItooResult.build("0000", "根据人员id查询薪资成功",list);
            }else{
                return ItooResult.build("1111", "根据人员id查询薪资为空",list);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据姓名查询薪资失败");
        }
    }







    /**
     *根据姓名查询薪资列表
     *返回对应的用户id和薪资信息列表组成的map
     */
    @ApiOperation(value="根据姓名查询本人薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/findSalaryByName/{name}"}, method = RequestMethod.GET)
    public ItooResult findSalaryByName(@PathVariable String name){
        try {
             List<SalaryEntity> list  = salaryFacade.selectByName(name);
            //List<SalaryEntity> list = salaryFacade.selectByName(name);
           //根据姓名信息查询

            if (list.size() !=0) {
                return ItooResult.build("0000", "根据姓名查询薪资成功",list);
            }else{
                return ItooResult.build("1111", "根据姓名查询薪资为空",list);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据姓名查询薪资失败");
        }
    }

    /**
     *根据年级查询薪资列表
     *返回对应的用户id和薪资信息列表组成的map
     */
    @ApiOperation(value="根据年级查询本人薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/findSalaryByGrade/{grade}"}, method = RequestMethod.GET)
    public ItooResult findSalaryByGrade(@PathVariable String grade){
        try {
            Map<String, List<SalaryEntity>> map = salaryFacade.selectByGrade(grade);
            //List<SalaryEntity> list = salaryFacade.selectByName(name);
            //根据姓名信息查询

            if (map.size() !=0) {
                return ItooResult.build("0000", "根据年级查询薪资成功",map);
            }else{
                return ItooResult.build("1111", "根据年级查询薪资为空",map);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据年级查询薪资失败");
        }
    }

    /**
     *根据年级查询薪资列表
     *返回对应的用户id和薪资信息列表组成的map
     */
    @ApiOperation(value="根据年级查询最低薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/findLowestSalaryByGrade/{grade}"}, method = RequestMethod.GET)
    public ItooResult findLowestSalaryByGrade(@PathVariable String grade){
        try {
              List<SalaryEntity> list = salaryFacade.selectLowestSalaryByGrade(grade);
             if (list.size() !=0) {
                return ItooResult.build("0000", "根据年级查询最低薪资成功",list);
            }else{
                return ItooResult.build("1111", "根据年级查询最低薪资为空",list);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据年级查询最低薪资失败");
        }
    }

    /**
     *根据年级查询最高薪资列表
     *返回对应的用户最高薪资信息
     */
    @ApiOperation(value="根据年级查询最低薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/findHighestSalaryByGrade/{grade}"}, method = RequestMethod.GET)
    public ItooResult findHighestSalaryByGrade(@PathVariable String grade){
        try {
            List<SalaryEntity> list = salaryFacade.selectHighestSalaryByGrade(grade);
            if (list.size() !=0) {
                return ItooResult.build("0000", "根据年级查询最高薪资成功",list);
            }else{
                return ItooResult.build("1111", "根据年级查询最高薪资为空",list);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据年级查询最高薪资失败");
        }
    }
    /**
     *根据年级查询最高薪资列表
     *返回对应的用户最高薪资信息
     */
    @ApiOperation(value="根据年级查询平均薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/findAverageSalaryByGrade/{grade}"}, method = RequestMethod.GET)
    public ItooResult findAverageSalaryByGrade(@PathVariable String grade){
        try {
            Double avarageSalary = salaryFacade.selectAverageSalaryByGrade(grade);
            if (avarageSalary  !=0) {
                return ItooResult.build("0000", "根据年级查询平均薪资成功",avarageSalary);
            }else{
                return ItooResult.build("1111", "根据年级查询平均薪资为空",avarageSalary);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据年级查询平均薪资失败");
        }
    }

    /**
     * 根据ID查询
     *
     */
    @ApiOperation(value="查询所有薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/findAll/{page}/{pageSize}"}, method = RequestMethod.GET)
    public ItooResult findAll(@PathVariable int page,@PathVariable int pageSize) {
//    @RequestMapping(value = {"/findAll"}, method = RequestMethod.GET)
//     public ItooResult findAll() {
    try {
//             int page =2;
           // int pageSize=6;
            PageInfo<SalaryEntity> list = salaryFacade.findAll( page,pageSize);
        //List<SalaryEntity> list = salaryFacade.findAll( );
            if (list != null) {
                return ItooResult.build("0000", "查询薪资成功",list);
            }else{
            return ItooResult.build("1111", "查询薪资为空");
        }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "查询薪资失败");
        }
    }



    /*
    * 添加薪资信息
    * @author yyl
    * */
    @ApiOperation(value="添加薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    public ItooResult addSalary( @RequestBody SalaryEntity salaryEntity) {
        try {
            int result = salaryFacade.addSalaryEntity(salaryEntity);
            if (result > 0) {
                return ItooResult.build("0000", "添加成功",result);
            } else {
                return ItooResult.build("1111", "薪资信息为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "添加失败");
        }
    }

    /*
     * 删除薪资信息
     * @author yyl
     * */
    @ApiOperation(value="删除所有薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/delete/{id}/{operator}"}, method = RequestMethod.POST)
    public ItooResult deleteSalary(@PathVariable String id, @PathVariable String operator) {
        try {
            int result = salaryFacade.deleteSalaryEntity(id, operator);
            if (result > 0) {
                return ItooResult.build("0000", "删除成功",result);
            } else {
                return ItooResult.build("1111", "薪资信息为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "删除失败");
        }
    }

    /*
     * 修改薪资信息
     * @author yyl
     * */
    @ApiOperation(value="修改薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    public ItooResult updateEntity(@RequestBody SalaryEntity salaryEntity) {
        try {
            int result = salaryFacade.updateSalaryEntity(salaryEntity);
            if (result > 0) {
                return ItooResult.build("0000", "更新薪资信息成功",result);
            } else {
                return ItooResult.build("1111", "薪资信息为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "更新失败");
        }
    }



    /*根据关系Id、年限查询薪资-时间列表---测试通过
    * @author yyl
    * */
    @ApiOperation(value="根据个人公司关系和日期查询薪资信息")
    @ResponseBody
    @RequestMapping(value = {"/selectByPcRelation/{pcRelation}/{date}"}, method = RequestMethod.GET)
    public ItooResult selectByPcRelation(@PathVariable String pcRelation, @PathVariable java.sql.Date date) {
        try {
            List<SalaryEntity> resultlist = salaryFacade.selectByPcRelation(pcRelation,date);
            if (resultlist.size() > 0) {
                return ItooResult.build("0000", "查询薪资成功",resultlist);
            } else {
                return ItooResult.build("1111", "查询结果为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "查询失败");
        }
    }


    /**
     * 根据人员公司信息列表和时间列表查询最高工资----测试，排序字段失败
     * @param :relationIds
     * @param date
     * @return 最高工资的记录
     */
    @ApiOperation(value="根据人员公司信息列表和时间列表查询最高工资")
    @ResponseBody
    @RequestMapping(value = {"/selecthighestSalaryByRelatoinId/{relationIds}/{date}"}, method = RequestMethod.GET)
    public ItooResult selecthighestSalaryByRelatoinId(@PathVariable List<String> relationIds, @PathVariable java.sql.Date date){

        try {
            List<SalaryEntity> resultlist = salaryFacade.selecthighestSalaryByRelatoinId(relationIds, date);
            if (resultlist.size() > 0) {
                return ItooResult.build("0000", "查询最高工资成功",resultlist);
            } else {
                return ItooResult.build("1111", "查询最高工资结果为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "查询最高工资失败");
        }
    }

    /**
     * 查询人员公司信息列表和时间列表查询最低工资----排序字段失败
     * @param relationIds
     * @param date
     * @return 平均最低的记录
     */
    @ApiOperation(value="查询人员公司信息列表和时间列表查询最低工资")
    @ResponseBody
    @RequestMapping(value = {"/selectlowestSalaryByRelatoinId/{relationIds}/{date}"}, method = RequestMethod.GET)
    public ItooResult selectlowestSalaryByRelatoinId(@PathVariable List<String> relationIds, @PathVariable java.sql.Date date) {
        try {
            List<SalaryEntity> resultlist = salaryFacade.selectlowestSalaryByRelatoinId(relationIds, date);
            if (resultlist.size() > 0) {
                return ItooResult.build("0000", "查询最低工资成功",resultlist);
            } else {
                return ItooResult.build("1111", "查询最低工资结果为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "查询最低工资失败");
        }
    }

    /**
     * 根据期数查询本期平均工资-----测试有问题，去掉排序字段
     *
     * @param relationIds
     * @param date
     * @return 平均工资的记录
     */
    @ApiOperation(value="查询人员公司信息列表和时间查询平均工资")
    @ResponseBody
    @RequestMapping(value = {"/selectAverageSalaryByRelatoinId/{relationIds}/{date}"}, method = RequestMethod.GET)
    public ItooResult selectAverageSalaryByRelatoinId(@PathVariable List<String> relationIds, @PathVariable java.sql.Date date){
        try {
            double result = salaryFacade.selectAverageSalaryByRelatoinId(relationIds, date);
            if (result > 0) {
                return ItooResult.build("0000", "查询平均工资成功",result);
            } else {
                return ItooResult.build("1111", "查询平均工资结果为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "查询平均工资失败");
        }
    }

    /**
     * 整体本年度最高薪资统计-----测试有问题，去掉排序字段
     * @param date
     * @return 本年度全体中的最高工资和期数List
     */
    @ApiOperation(value="根据时间查询本年度所有人的最高薪资")
    @ResponseBody
    @RequestMapping(value = {"/selecthighestSalaryofAll/{date}"}, method = RequestMethod.GET)
    public ItooResult selecthighestSalaryofAll(@PathVariable java.sql.Date date){
        try {
            List<SalaryEntity> resultlist  = salaryFacade.selecthighestSalaryofAll(date);
            if (resultlist.size() > 0) {
                return ItooResult.build("0000", "查询本年度最高工资成功",resultlist);
            } else {
                return ItooResult.build("1111", "查询本年度最高工资结果为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "查询本年度最高工资失败");
        }
    }

    /**
     * 整体本年度最低薪资统计-----测试有问题，去掉排序字段
     * @param date
     * @return 本年度最低工资和期数List
     */
    @ApiOperation(value="根据时间查询本年度所有人的最低薪资")
    @ResponseBody
    @RequestMapping(value = {"/selectlowestSalaryofAll/{date}"}, method = RequestMethod.GET)
    public ItooResult selectlowestSalaryofAll(@PathVariable java.sql.Date date) {
        try {
            List<SalaryEntity> resultlist  = salaryFacade.selectlowestSalaryofAll(date);
            if (resultlist.size() > 0) {
                return ItooResult.build("0000", "查询本年度最低工资成功",resultlist);
            } else {
                return ItooResult.build("1111", "查询本年度最低工资结果为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "查询本年度最低工资失败");
        }
    }


    /**
     * 查本年度全体员工全部工资
     * @param date
     */
    @ApiOperation(value="根据时间查询本年度所有人的平均薪资")
    @ResponseBody
    @RequestMapping(value = {"/findAverageOfALL/{date}"}, method = RequestMethod.GET)
    public ItooResult findAverageOfALL(@PathVariable java.sql.Date date) {
        try {
            Double salary = salaryFacade.findAverageSalary(date);
            if (salary != null) {
                return ItooResult.build("0000", "查询平均薪资成功",salary);
            }else{
                return ItooResult.build("1111", "查询平均薪资为空");
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "查询平均薪资失败");
        }
    }

    /**
     *根据毕业生id查询薪资公司相关信息-haoguiting--2018年3月7日 selectSalaryByUserId
     * @param userId
     * @return
     */
    @ApiOperation(value="hgt-根据毕业生id查询薪资公司相关信息")
    @ResponseBody
    @RequestMapping(value = {"/PageSelectSalaryByUserId/{userId}/{page}/{pageSize}"}, method = RequestMethod.GET)
    public ItooResult PageSelectSalaryByUserId(@PathVariable String userId,@PathVariable int page,@PathVariable int pageSize){
        PageInfo<CompanySalaryModel> SalaryModelList;
        try {
            SalaryModelList = salaryFacade.PageSelectSalaryByUserId(userId,page,pageSize);
            if (CollectionUtils.isEmpty(SalaryModelList.getList())) {
                return ItooResult.build("1111", "根据毕业生id查询薪资公司相关信息为空",SalaryModelList);
            }else{
                return ItooResult.build("0000", "根根据毕业生id查询薪资公司相关信息成功！",SalaryModelList);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据毕业生id查询薪资公司相关信息失败！");
        }
    }




}
