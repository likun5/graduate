package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.SkillPointEntity;
import com.dmsdbj.itoo.graduate.entity.ext.TechnologyModel;
import com.dmsdbj.itoo.graduate.facade.SkillPointFacade;
import com.dmsdbj.itoo.tool.base.entity.TreeEntity;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import com.dmsdbj.itoo.graduate.tool.ResultCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author : 徐玲博
 * DESCRIPTION : 技术点类
 * create : 2017-11-16 14:31:07.
 */
@Api(value="技术管理API",tags="技术管理API")
@RequestMapping("/skillPoint")
@Controller
public class SkillPointController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(SkillPointController.class);

    @Reference
    SkillPointFacade skillPointFacade;

    /**
     * @return : skillPoint
     * @author : 徐玲博
     * DESCRIPTION : 首页导向页
     */
    @RequestMapping(value = {"/index"}, method = RequestMethod.GET)
    public String index() {
        return "skillPoint";
    }

    /**
     * 根据id查询技术-徐玲博-2017-12-5 08:52:40
     *
     * @param id 技术点id
     * @return skillPointEntity 技术实体
     */
    @ApiOperation(value = "根据id查询技术",notes = "根据id查询技术")
    @ResponseBody
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
        try {
            SkillPointEntity skillPointEntity = skillPointFacade.findById(id);
            if (skillPointEntity != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询该课程成功", skillPointEntity);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询该课程为空");
            }

        } catch (Exception e) {
            logger.error("查询技术异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询技术异常");
        }
    }

    /**
     * 添加技术点或技术方向-徐玲博-2017-12-3 16:45:33
     *
     * @param skillPointEntity 技术实体
     * @return  result 受影响行
     */
    @ApiOperation(value = "添加技术点或技术方向",notes = "添加技术点或技术方向")
    @ResponseBody
    @RequestMapping(value = "addSkillPoint/", method = RequestMethod.POST)
    public ItooResult addSkillPoint(@RequestBody SkillPointEntity skillPointEntity) {

        int result;
        try {
            result = skillPointFacade.addSkillPoint(skillPointEntity);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "添加技术成功！", result);
            } else {
                return ItooResult.build(ResultCode.FAIL, "添加技术0条！");
            }
        } catch (Exception e) {
            logger.error("addSkillPoint 添加技术异常", e);
            return ItooResult.build(ResultCode.FAIL, "添加技术异常！");
        }

    }

    /**
     * 修改技术点或技术方向-徐玲博-2017-12-3 17:12:42
     *
     * @param skillPointEntity 技术实体
     * @return result 受影响行
     */
    @ApiOperation(value = "修改技术点或技术方向",notes = "修改技术点或技术方向")
    @ResponseBody
    @RequestMapping(value = "updateSkillPoint/", method = RequestMethod.POST)
    public ItooResult updateSkillPoint(@RequestBody SkillPointEntity skillPointEntity) {

        int result;
        try {
            result = skillPointFacade.updateSkillPoint(skillPointEntity);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "修改技术成功", result);
            } else {
                return ItooResult.build(ResultCode.FAIL, "修改技术0条");
            }
        } catch (Exception e) {
            logger.error("addSkillPoint 修改技术异常", e);
            return ItooResult.build(ResultCode.FAIL, "修改技术异常！",e);
        }

    }
    /**
     * 删除技术点或技术方向-徐玲博-2017-12-3 17:14:13
     *
     * @param id       技术点ID
     * @param operator 操作人
     * @return result  受影响行
     */
    @ApiOperation(value = "删除技术点或技术方向",notes = "删除技术点或技术方向")
    @ResponseBody
    @RequestMapping(value = "deleteSkillPoint/{id}/{operator}", method = RequestMethod.POST)
    public ItooResult deleteSkillPoint(@PathVariable String id, @PathVariable String operator) {

        int result;
        try {
            result = skillPointFacade.deleteSkillPoint(id, operator);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "删除技术成功", result);
            } else {
                return ItooResult.build(ResultCode.FAIL, "删除技术0条！");
            }
        } catch (Exception e) {
            logger.error("addSkillPoint 删除技术异常", e);
            return ItooResult.build(ResultCode.FAIL, "删除技术异常！");
        }

    }

    /**
     * 查询技术点-或技术方向-徐玲博-2017-11-20 16:54:36
     *
     * @param id 用户Id
     * @return 技术列表
     */
    @ApiOperation(value = "查询技术点-或技术方向",notes = "查询技术点-或技术方向")
    @ResponseBody
    @RequestMapping(value = "selectSkillPointById/{id}", method = RequestMethod.GET)
    public ItooResult selectSkillPointById(@PathVariable String id) {

        List<SkillPointEntity> skillPointEntity;
        try {
            skillPointEntity = skillPointFacade.selectSkillPointById(id);
            if (skillPointEntity != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询技术成功", skillPointEntity);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询技术为空！");
            }
        } catch (Exception e) {
            logger.error("addSkillPoint 查询技术异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询技术异常！");
        }

    }

    /**
     * 根据pid查询技术-徐玲博-2017-11-21 14:42:51
     *
     * @param pid 技术方向
     * @return 技术方向列表
     */
    @ApiOperation(value = "根据pid查询技术",notes = "根据pid查询技术")
    @ResponseBody
    @RequestMapping(value = "selectParentSkill/{pid}", method = RequestMethod.GET)
    public ItooResult selectParentSkillByPid(@PathVariable String pid) {

        List<SkillPointEntity> skillPointEntity;
        try {
            skillPointEntity = skillPointFacade.selectParentSkillByPid(pid);
            if (skillPointEntity != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询技术方向成功！", skillPointEntity);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询技术方向为空！");
            }
        } catch (Exception e) {
            logger.error("addSkillPoint 查询技术方向异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询技术方向异常！");
        }

    }

    /**
     * 公司接口-查询所有的技术-并根据技术方向分类-徐玲博-2017-12-26 15:55:23
     *
     * @return 列表
     */
    @ApiOperation(value = "查询所有的技术",notes = "查询所有的技术")
    @ResponseBody
    @RequestMapping(value = "selectSkillPoint", method = RequestMethod.GET)
    public ItooResult selectSkillPoint() {
        try {
            List<Map<String, List<SkillPointEntity>>> mapList = skillPointFacade.selectSkillPoint();
            if (CollectionUtils.isEmpty(mapList)) {
                return ItooResult.build(ResultCode.FAIL, "查询信息为空！");
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "查询技术点信息成功！", mapList);
            }
        } catch (Exception e) {
            logger.error("查询技术点异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询技术点信息失败！");
        }
    }

    /**
     * 查询所有的技术方向实体-徐玲博-2018-1-13 19:15:10
     *
     * @return 技术方向list
     */
    @ResponseBody
    @RequestMapping(value = "/selectTechnicalDirection", method = RequestMethod.GET)
    public ItooResult selectTechnicalDirection() {
        try {
            List<SkillPointEntity> skillPointEntityList = skillPointFacade.selectTechnicalDirection();
            if (!CollectionUtils.isEmpty(skillPointEntityList)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询技术方向成功！", skillPointEntityList);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询技术方向为空！");
            }
        } catch (Exception e) {
            logger.error("查询技术方向异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询技术方向异常");
        }
    }

    /**
     * 分页查询所有技术-徐玲博-2018-1-13 23:34:56
     * @param page 页 码
     * @param pageSize 页大小
     * @return 技术list
     */
    @ResponseBody
    @RequestMapping(value = "pageAllTechnology/{page}/{pageSize}", method = RequestMethod.GET)
    public ItooResult pageAllTechnology(@PathVariable int page, @PathVariable int pageSize) {
        try {
            PageInfo<TechnologyModel>technologyModelPageInfo=skillPointFacade.pageAllTechnology(page,pageSize);
            if(technologyModelPageInfo!=null){
                return ItooResult.build(ResultCode.SUCCESS,"分页查询技术点成功！",technologyModelPageInfo);
            }else {
                return ItooResult.build(ResultCode.FAIL,"分页查询技术点为空！");
            }

        } catch (Exception e) {
            logger.error("分页查询技术点异常",e);
            return  ItooResult.build(ResultCode.FAIL,"分页查询技术点异常");
        }
    }

    /**
     * 查询所有技术（包括技术方向）-徐玲博-2018-1-13 23:17:42
     *
     * @return 技术list
     */
    @ResponseBody
    @RequestMapping(value = "/selectAllTechnology", method = RequestMethod.GET)
    public ItooResult selectAllTechnology() {
        try {
            List<TechnologyModel> technologyModelList = skillPointFacade.selectAllTechnology();
            if (!CollectionUtils.isEmpty(technologyModelList)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询所有技术成功！", technologyModelList);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询所有技术为空！");
            }
        } catch (Exception e) {
            logger.error("查询所有技术异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询所有技术异常");
        }
    }


    /**
     * 查询全部技术方向（树形结构）-haoguiting-2018年2月22日
     *
     * @return 技术方向list
     */
    @ApiOperation(value = "查询全部技术方向（树形结构）",notes = "查询全部技术方向（树形结构）")
    @ResponseBody
    @RequestMapping(value = "/selectSkillDirection", method = RequestMethod.GET)
    public ItooResult selectSkillDirection() {
        List<TreeEntity> listTree = new ArrayList<>();   // Tree实体集合
        try {
            List<SkillPointEntity> skillPointEntityList = skillPointFacade.selectTechnicalDirection();
            for(SkillPointEntity skillPointEntity:skillPointEntityList){
                TreeEntity tree = new TreeEntity();
                tree.setId(skillPointEntity.getId());// 树节点Id   peopleId
                tree.setName(skillPointEntity.getTechName());// 树的节点名称 peopleName
                tree.setpId(skillPointEntity.getPId());
                listTree.add(tree);
            }
            if (!CollectionUtils.isEmpty(listTree)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询技术方向成功！", listTree);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询技术方向为空！");
            }
        } catch (Exception e) {
            logger.error("查询技术方向异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询技术方向异常");
        }
    }


    /**
     * 分页查询技术方向-haoguiting-2018年3月10日
     *
     * @param page 页码
     * @param pageSize 页大小
     * @return 技术方向list
     */
    @ApiOperation(value = "分页查询技术方向",notes = "分页查询技术方向")
    @ResponseBody
    @RequestMapping(value = "PageSelectSkillDirection/{page}/{pageSize}", method = RequestMethod.GET)
    public ItooResult PageSelectSkillDirection(@PathVariable int page, @PathVariable int pageSize) {
        try {
            PageInfo<SkillPointEntity> skillPointEntity=skillPointFacade.PageSelectSkillDirection(page,pageSize);
            if(skillPointEntity!=null){
                return ItooResult.build(ResultCode.SUCCESS,"分页查询技术方向成功！",skillPointEntity);
            }else {
                return ItooResult.build(ResultCode.FAIL,"分页查询技术方向为空！");
            }

        } catch (Exception e) {
            logger.error("分页查询技术方向异常",e);
            return  ItooResult.build(ResultCode.FAIL,"分页查询技术方向异常");
        }
    }

    /**
     * 分页查询根据技术方向查询技术点-haoguiting-2018年2月22日
     *
     * @param pid 技术方向
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 技术点list
     */
    @ApiOperation(value = "分页查询根据技术方向查询技术点",notes = "分页查询根据技术方向查询技术点")
    @ResponseBody
    @RequestMapping(value = "PageSelectSkillByDirectionId/{pid}/{page}/{pageSize}", method = RequestMethod.GET)
    public ItooResult PageSelectSkillByDirectionId(@PathVariable String pid,@PathVariable int page, @PathVariable int pageSize) {
        try {
            PageInfo<SkillPointEntity> skillPointEntity=skillPointFacade.PageSelectSkillByDirectionId(pid,page,pageSize);
            if(skillPointEntity!=null){
                return ItooResult.build(ResultCode.SUCCESS,"分页查询技术点成功！",skillPointEntity);
            }else {
                return ItooResult.build(ResultCode.FAIL,"分页查询技术点为空！");
            }

        } catch (Exception e) {
            logger.error("分页查询技术点异常",e);
            return  ItooResult.build(ResultCode.FAIL,"分页查询技术点异常");
        }
    }

    /**
     * 模糊查询技术点-haoguiting-2018年2月27日
     *
     * @param pid 技术方向Pid
     * @return 技术点list
     */
    @ApiOperation(value = "模糊查询技术点",notes = "模糊查询技术点")
    @ResponseBody
    @RequestMapping(value = {"/fuzzySkillInfoByName/{pid}"},method = RequestMethod.GET)
    public ItooResult fuzzySkillInfoByName(@RequestParam(required = false, defaultValue = "") String strLike,@PathVariable String pid) {
        try {
            List<SkillPointEntity>  skillPointEntities = skillPointFacade.fuzzySkillInfoByName(strLike,pid);
            if (skillPointEntities !=null ) {
                return ItooResult.build(ResultCode.SUCCESS, "模糊查询技术点成功", skillPointEntities);
            } else {
                return ItooResult.build(ResultCode.FAIL, "模糊查询技术点成功为空");
            }
        } catch (Exception e) {
            logger.error("模糊查询技术点成功异常", e);
            return ItooResult.build(ResultCode.FAIL, "模糊查询技术点成功失败");
        }
    }
}
