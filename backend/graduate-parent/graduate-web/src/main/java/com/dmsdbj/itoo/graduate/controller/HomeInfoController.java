package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.HomeInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.HomeInfoModel;
import com.dmsdbj.itoo.graduate.facade.HomeInfoFacade;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;



/**
 * @author : 徐玲博
 * DESCRIPTION : 家庭信息
 * create : 2017-11-16 14:31:07.
 */
@Api(value = "家庭信息管理API",tags = "家庭信息管理API")
@RequestMapping("/homeInfo")
@Controller
public class HomeInfoController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(HomeInfoController.class);

    @Reference
    private HomeInfoFacade homeInfoFacade;

    /**
     * 首页导向页
     * @author : 徐玲博
     * @return homeInfo
     */
    @RequestMapping(value = {"/index"},method = RequestMethod.GET)
    public String index() {
        return "homeInfo";
    }

    /**
     * 查找家庭信息-xlb-2017-12-4 15:43:33
     *
     * @param id 家庭成员id
     * @return 家庭信息
     */
    @ApiOperation(value = "查找家庭信息",notes = "查找家庭信息")
    @ResponseBody
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
        try {
            HomeInfoEntity homeInfoEntity = homeInfoFacade.findById(id);
            if (homeInfoEntity != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询该课程成功", homeInfoEntity);
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "查询该课程为空");
            }
        } catch (Exception e) {
            logger.error("", e);
            return ItooResult.build(ResultCode.FAIL, "查询该课程失败");
        }
    }

    /**
     * 添加家庭成员-徐玲博-2017-11-20 11:03:07
     *
     * @param homeInfoEntity 家庭成员信息实体
     * @return 受影响行
     */
    @ApiOperation(value = "添加家庭成员",notes = "添加家庭成员")
    @ResponseBody
    @RequestMapping(value = "addHomeInfo/", method = RequestMethod.POST)
    public ItooResult addHomeInfo(@RequestBody HomeInfoEntity homeInfoEntity) {

        int result;
        try {
            result = homeInfoFacade.addHomeInfo(homeInfoEntity);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "添加家庭成员成功", result);
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "添加家庭成员失败",result);
            }
        } catch (Exception e) {
            logger.error("addHomeInfo添加家庭成员异常", e);
            return ItooResult.build(ResultCode.FAIL, "添加家庭成员异常");
        }
    }

    /**
     * 删除家庭成员信息-徐玲博-2017-12-4 09:57:17
     *
     * @param id       家庭成员id
     * @param operator 操作人
     * @return 受影响行数
     */
    @ApiOperation(value = "删除家庭成员信息",notes = "删除家庭成员信息")
    @ResponseBody
    @RequestMapping(value = "deleteHomeInfo/{id}/{operator}", method = RequestMethod.POST)
    public ItooResult deleteHomeInfo(@PathVariable String id, @PathVariable String operator) {

        int result;
        try {
            result = homeInfoFacade.deleteHomeInfo(id, operator);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "删除家庭信息成功", result);
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "删除家庭信息失败");
            }
        } catch (Exception e) {
            logger.error("deleteHomeInfo 删除家庭信息异常", e);
            return ItooResult.build(ResultCode.FAIL, "删除家庭信息异常");
        }
    }

    /**
     * 分页查询家庭信息列表-徐玲博-2017-12-4 10:20:12
     * @param userId  当前登录用户ID
     * @param page 页数
     * @param pageSize 每页大小
     * @return 家庭信息列表
     */
    @ApiOperation(value = "分页查询家庭信息列表",notes = "分页查询家庭信息列表")
    @ResponseBody
    @RequestMapping(value = "selectHomePersonInfo/{userId}/{page}/{pageSize}", method = RequestMethod.GET)
    public ItooResult selectHomePersonInfo(@PathVariable String userId,@PathVariable int page,@PathVariable int pageSize) {

        PageInfo<HomeInfoModel> homeInfoModelPageInfo;
        try {
            homeInfoModelPageInfo = homeInfoFacade.selectHomePersonInfo(userId,page,pageSize);
            if (homeInfoModelPageInfo != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询家庭信息成功", homeInfoModelPageInfo);
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "查询家庭信息失败");
            }
        } catch (Exception e) {
            logger.error("selectHomePersonInfo查询家庭信息异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询家庭信息异常");
        }
    }
    /**
     * 修改家庭成员-徐玲博-2017-12-4 10:24:39
     * @param homeInfoEntity 家庭成员实体
     * @return 受影响行
     */
    @ApiOperation(value = "修改家庭成员",notes = "修改家庭成员")
    @ResponseBody
    @RequestMapping(value = "updateHomeInfo/",method = RequestMethod.POST)
    public ItooResult updateHomeInfo(@RequestBody HomeInfoEntity homeInfoEntity){

        int result;
        try {
            result=homeInfoFacade.updateHomeInfo(homeInfoEntity);
            if(result>0){
                return ItooResult.build(ResultCode.SUCCESS,"修改家庭信息成功",result);
            }else {
                return ItooResult.build(ResultCode.SUCCESS,"修改家庭信息失败");
            }
        } catch (Exception e) {
            logger.error("updateHomeInfo修改家庭信息列表异常");
            return ItooResult.build(ResultCode.FAIL,"修改家庭信息异常");
        }
    }
}
