package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;

import com.dmsdbj.itoo.graduate.entity.NoticeCategoryEntity;
import com.dmsdbj.itoo.graduate.facade.NoticeCategoryFacade;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;


/**
 * @author 徐玲博
 * DESCRIPTION: demo展示
 * create: 2017-11-26 16:14:54.
 */
@Api(value = "设置通知类别",tags="设置通知类别")
@RequestMapping("/noticeCategory")
@Controller
public class NoticeCategoryController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(NoticeCategoryController.class);

    @Reference
    NoticeCategoryFacade noticeCategoryFacade;



    /**
     * 查找测试-徐玲博-{date}
     *
     * @param id 类别id
     * @return 通知类别实体
     */
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
		try {
			NoticeCategoryEntity noticeCategoryEntity = noticeCategoryFacade.findById(id);
			if (noticeCategoryEntity != null) {
				return ItooResult.build("0000", "查询该课程为空", noticeCategoryEntity);
			}else{
				return ItooResult.build("0000", "查询该课程成功", noticeCategoryEntity);
			}
        
        } catch (Exception e) {
            logger.error("",e);
			return ItooResult.build("1111", "查询该课程失败");
        }
    }

    /**
     * 添加通知分类信息-郝贵婷-2017-12-5 14:15:45
     * @param  noticeCategoryEntity 通知类别实体
     * @return 受影响行
     */
    @ApiOperation(value="添加通知类别信息",notes="添加通知类别信息")
    @ResponseBody
    @RequestMapping(value = "addNoticeCategory/", method = RequestMethod.POST)
    public ItooResult addNoticeCategory(@RequestBody NoticeCategoryEntity noticeCategoryEntity) {
        int resultCount;
        try {
            resultCount = noticeCategoryFacade.addNoticeCategory(noticeCategoryEntity);
            if (resultCount > 0) {
                return ItooResult.build("0000", "添加通知分类成功！",resultCount);
            } else {
                return ItooResult.build("1111", "添加通知分类失败！");
            }
        } catch (Exception e) {

            logger.error("addNoticeCategory 添加通知分类异常", e);
            return ItooResult.build("1111", "添加通知分类异常！");
        }

    }

    /**
     * 根据Id删除通知分类信息-郝贵婷-2017-12-5 14:15:45
     * @param id  通知分类ID
     * @param operator 操作人
     * @return 影响行数
     */
    @ApiOperation(value="删除通知类别信息",notes="删除通知类别信息")
    @ResponseBody
    @RequestMapping(value = "deleteNoticeCategory/{id}/{operator}", method = RequestMethod.POST)
    public ItooResult deleteNoticeCategory(@PathVariable String id, @PathVariable String operator) {
        int resultCount;
        try {
            resultCount = noticeCategoryFacade.deleteNoticeCategory(id, operator);
            if (resultCount > 0) {
                return ItooResult.build("0000", "删除通知分类成功",resultCount);
            } else {
                return ItooResult.build("1111", "删除通知分类失败！");
            }
        } catch (Exception e) {

            logger.error("deleteNoticeCategory 删除通知分类异常", e);
            return ItooResult.build("1111", "删除通知分类异常！");
        }

    }
    /**
     * 根据Id修改通知分类信息-郝贵婷-2017-12-5 14:15:45
     * @param noticeCategoryEntity 通知分类实体
     * @return 受影响行
     */
    @ApiOperation(value="修改通知类别信息",notes="修改通知类别信息")
    @ResponseBody
    @RequestMapping(value = "updateNoticeCategory/", method = RequestMethod.POST)
    public ItooResult updateNoticeCategory(@RequestBody NoticeCategoryEntity noticeCategoryEntity) {
        int resultCount;
        try {
            resultCount = noticeCategoryFacade.updateNoticeCategory(noticeCategoryEntity);
            if (resultCount > 0) {
                return ItooResult.build("0000", "修改通知分类成功！",resultCount);
            } else {
                return ItooResult.build("1111", "修改通知分类失败！");
            }
        } catch (Exception e) {
            logger.error("updateNoticeCategory 通知分类异常", e);
            return ItooResult.build("1111", "修改通知分类异常！");
        }

    }

    /**
     * 根据Id查询通知分类信息-郝贵婷-2017-12-5 14:15:45
     * @param categoryId 通知类别id
     * @return 通知类别实体
     */
    @ApiOperation(value="查询通知类别",notes = "根据ID查询通知类别")
    @ResponseBody
    @RequestMapping(value = "findNoticeCategoryById/{categoryId}", method = RequestMethod.GET)
    public ItooResult findNoticeCategoryById(@PathVariable String categoryId) {
        NoticeCategoryEntity noticeCategoryEntity;
        try {
            noticeCategoryEntity =  noticeCategoryFacade.findById(categoryId);
            if (noticeCategoryEntity != null) {
                return ItooResult.build("0000", "查询通知分类成功", noticeCategoryEntity);
            } else {
                return ItooResult.build("1111", "查询通知分类失败");
            }
        } catch (Exception e) {
            logger.error("findNoticeCategoryById 查询通知分类异常", e);
            return ItooResult.build("1111", "查询通知分类失败");
        }

    }

    /**
     * 假分页-查询全部通知分类-郝贵婷-2018-01-11 14:15:45
     * @param page 页码
     * @param pageSize 每页显示条数
     * @return 类别list
     */
    @ApiOperation(value="分页查询全部通知分类",notes="分页查询全部通知分类")
    @ResponseBody
    @RequestMapping(value = "PageSelectNoticeCategoryInfo/{page}/{pageSize}", method = RequestMethod.GET)
    public ItooResult PageSelectNoticeCategoryInfo(@PathVariable int page,@PathVariable int pageSize ) {

        try {
            PageInfo<NoticeCategoryEntity> manageModelPageInfo = noticeCategoryFacade.PageSelectNoticeCategoryInfo(page, pageSize);
            if (CollectionUtils.isEmpty(manageModelPageInfo.getList())) {
                return ItooResult.build(ResultCode.SUCCESS, "无相关条件的查询结果");
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "分页查询全部通知分类成功", manageModelPageInfo);
            }
        } catch (Exception e) {
            logger.error("PageSelectNoticeCategoryInfo 分页查询全部通知分类异常", e);
            return ItooResult.build(ResultCode.FAIL, "分页查询全部通知分类失败");
        }

    }


    /**
     * 查询全部通知分类-郝贵婷-2017-12-5 14:15:45
     * @return 通知分类list列表
     */
    @ApiOperation(value="查询全部通知分类",notes="查询全部通知分类")
    @ResponseBody
    @RequestMapping(value = "selectNoticeCategoryInfo/", method = RequestMethod.GET)
    public ItooResult selectNoticeCategoryInfo() {
        List<NoticeCategoryEntity> noticeCategoryEntityList;
        try {
            noticeCategoryEntityList = noticeCategoryFacade.selectNoticeCategoryInfo();
            if (noticeCategoryEntityList != null) {
                return ItooResult.build("0000", "查询通知分类成功", noticeCategoryEntityList);
            } else {
                return ItooResult.build("1111", "查询通知分类失败");
            }
        } catch (Exception e) {
            logger.error("selectNoticeCategoryInfo 查询通知分类异常", e);
            return ItooResult.build("1111", "查询通知分类失败");
        }

    }

    /**
     * 根据主键批量删除类别-郝贵婷-2017-12-5 14:15:45
     * @param id  通知分类ID
     * @param operator 操作人
     * @return 影响行数
     */
    @ApiOperation(value="根据主键批量删除类别",notes="根据主键批量删除类别")
    @ResponseBody
    @RequestMapping(value = "deleteNoticeCategorys/{id}/{operator}", method = RequestMethod.POST)
    public ItooResult deleteNoticeCategorys(@PathVariable String id, @PathVariable String operator) {
        int resultCount;
        try {
            resultCount = noticeCategoryFacade.deleteNoticeCategorys(id, operator);
            if (resultCount > 0) {
                return ItooResult.build("0000", "根据主键批量删除类别成功",resultCount);
            } else {
                return ItooResult.build("1111", "根据主键批量删除类别失败！");
            }
        } catch (Exception e) {

            logger.error("deleteNoticeCategorys 根据主键批量删除类别异常", e);
            return ItooResult.build("1111", "根据主键批量删除类别异常！");
        }

    }
}
