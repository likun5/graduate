package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;

import com.dmsdbj.itoo.graduate.entity.ForumCommentEntity;
import com.dmsdbj.itoo.graduate.facade.ForumCommentFacade;
import com.dmsdbj.itoo.tool.business.ItooResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * @author ls
 * DESCRIPTION: demo展示
 * create: 2017-12-27 09:54:26.
 */

@RequestMapping("/forumComment")
@Controller
public class ForumCommentController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(ForumCommentController.class);

    @Reference
    ForumCommentFacade forumCommentFacade;

    /**
     * @author ls
     * DESCRIPTION:首页导向页
	 * @params:
     * @return:
	 * @Date:${DATE}
	 * @Modified By:  
     */
    @RequestMapping(value = {"/index"}, method = RequestMethod.GET )
    public String index() {
        return "forumComment";
    }

    /**
     * DESCRIPTION:查找测试
     * @author ls
	 * @Date:${DATE}
     * @param id
     */
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
		try {
			ForumCommentEntity forumCommentEntity = forumCommentFacade.findById(id);
			if (forumCommentEntity != null) {
				return ItooResult.build("0000", "查询该课程成功", forumCommentEntity);
			}else{
				return ItooResult.build("0000", "查询该课程为空", null);
			}
        
        } catch (Exception e) {
            logger.error("",e);
			return ItooResult.build("1111", "查询该课程失败");
        }
    }
}    
