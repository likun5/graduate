package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.LandScapeEntity;
import com.dmsdbj.itoo.graduate.facade.LandScapeFacade;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;

import java.util.List;

import org.springframework.web.bind.annotation.*;

/**
 * @author 徐玲博
 * DESCRIPTION: graduate展示
 * create: 2017-11-16 14:31:07.
 */
@Api(value = "风景API", tags = "风景API")
@RequestMapping("/landScape")
@Controller
public class LandScapeController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(LandScapeController.class);

    @Reference
    LandScapeFacade landScapeFacade;

    /**
     * @author sxm
     * DESCRIPTION:首页导向页
     * @params:
     * @return:
     * @Date:
     * @Modified By:
     */
    @RequestMapping(value = {"/index"}, method = RequestMethod.GET)
    public String index() {
        return "landScape";
    }

    /**
     * 根据风景id查询风景-徐玲博-2018-2-1 15:00:12
     *
     * @param id 风景id
     * @return LandScapeEntity 风景实体
     */
    @ResponseBody
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
        try {
            LandScapeEntity landScapeEntity = landScapeFacade.findById(id);
            if (landScapeEntity != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询景点成功", landScapeEntity);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询景点为空");
            }
        } catch (Exception e) {
            logger.error("查询景点异常{}", e);
            return ItooResult.build(ResultCode.FAIL, "查询景点失败");
        }
    }


    /**
     * 查询风景-徐玲博-2018-2-1 15:01:00
     *
     * @return 风景列表
     */
    @RequestMapping(value = "/queryLandScape/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ItooResult queryLandScape(@PathVariable String id) {
        try {

            List<LandScapeEntity> landScapeEntityList = landScapeFacade.queryLandScape(id);

            if (!CollectionUtils.isEmpty(landScapeEntityList)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询所有景点信息成功！", landScapeEntityList);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询所有景点信息为空！");
            }
        } catch (Exception e) {
            logger.error("查询所有景区信息异常！", e);
            return ItooResult.build(ResultCode.FAIL, "查询所有景点信息失败！");
        }
    }

    /**
     * 分页查询-查询风景-徐玲博-2018-2-1 15:01:00
     *
     * @return 风景列表
     */
    @RequestMapping(value = "/selectLandScape/{id}/{page}/{pageSize}", method = RequestMethod.GET)
    @ResponseBody
    public ItooResult selectLandScape(@PathVariable String id,@PathVariable int page, @PathVariable int pageSize) {
        try {

            PageInfo<LandScapeEntity> landScapeEntityList = landScapeFacade.selectLandScape(id,page, pageSize);

            if (!CollectionUtils.isEmpty(landScapeEntityList.getList())) {
                return ItooResult.build(ResultCode.SUCCESS, "查询所有景点信息成功！", landScapeEntityList);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询所有景点信息为空！");
            }
        } catch (Exception e) {
            logger.error("查询所有景点信息异常！{}", e);
            return ItooResult.build(ResultCode.FAIL, "查询所有景点信息失败！");
        }
    }

    /**
     * 添加景区-徐玲博-2018-2-1 15:04:40
     *
     * @param landScapeEntity 风景实体
     * @return 受影响行
     */
    @RequestMapping(value = "/addLandScape", method = RequestMethod.POST)
    @ResponseBody
    public ItooResult addLandScape(@RequestBody LandScapeEntity landScapeEntity) {

        int flag;
        try {
            flag = landScapeFacade.addLandScape(landScapeEntity);
            if (flag > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "添加景点成功！", flag);
            } else {
                return ItooResult.build(ResultCode.FAIL, "景点添加0条！");
            }
        } catch (Exception e) {
            logger.error("基础信息服务 景区管理 向景区表中插入数据 异常{}", e);
            return ItooResult.build(ResultCode.FAIL, "景点添加失败！");
        }
    }


    /**
     * 更新风景-徐玲博-2018-2-1 15:06:43
     *
     * @param landScapeEntity 风景实体
     * @return 受影响行
     */
    @RequestMapping(value = {"/updateLandSpace"}, method = RequestMethod.POST)
    @ResponseBody
    public ItooResult updateLandSpace(@RequestBody LandScapeEntity landScapeEntity) {
        int flag ;
        try {
            flag = landScapeFacade.updateLandSpace(landScapeEntity);
            if(flag>0){
                return ItooResult.build(ResultCode.SUCCESS, "修改景点信息成功！", flag);
            }else{
                return ItooResult.build(ResultCode.FAIL, "景点信息修改未成功！");
            }

        } catch (Exception e) {
            logger.error("基础信息服务 景区管理 json转换出错 错误：{}", e);
            return ItooResult.build(ResultCode.FAIL, "景点信息修改数据失败！");
        }
    }

    /**
     * 删除风景-徐玲博-2018-2-1 14:58:45
     *
     * @param landScapeEntityList 景区实体
     * @return 受影响行
     */
    @RequestMapping(value = {"/deleteLandSpace/{operator}"}, method = RequestMethod.POST)
    @ResponseBody
    public ItooResult deleteLandSpace(@PathVariable List<LandScapeEntity> landScapeEntityList) {

//        if (StringUtils.isEmpty(landIds) || "null".equals(landIds)) {
//            return ItooResult.build(ResultCode.FAIL, "参数为空，或是空字符串");
//        }
        try {
            int flag = landScapeFacade.deleteLandSpace(landScapeEntityList);
            if (flag > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "根据景区id删除景区信息成功！", flag);
            } else {
                return ItooResult.build(ResultCode.FAIL, "根据景区id删除景区信息不存在！");
            }

        } catch (Exception e) {
            logger.error("基础信息服务 景区管理 根据景区id删除景区信息异常{}", e);
            return ItooResult.build(ResultCode.FAIL, "根据景区id删除景区信息失败！");
        }
    }

    /**
     * 根据景点ids-删除景点-徐玲博-2018-2-8 11:09:54
     * @param ids 景点id
     * @param operator 操作人
     * @return 受影响行
     */
    @ResponseBody
    @RequestMapping(value = "deleteLand/{operator}",method = RequestMethod.POST)
    public ItooResult deleteLand(@RequestBody List<String> ids,@PathVariable String operator){
        try {
            int flag=landScapeFacade.deleteLand(ids, operator);
            if(flag>0){
                return ItooResult.build(ResultCode.SUCCESS,"已成功删除所选景点",flag);
            }else {
                return ItooResult.build(ResultCode.FAIL,"删除景点0条");
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build(ResultCode.FAIL,"批量删除景点失败");
        }
    }
}    
