package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.CommercialOppotunityEntity;
import com.dmsdbj.itoo.graduate.entity.ext.RegionCommercialModel;
import com.dmsdbj.itoo.graduate.facade.CommercialOppotunityFacade;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * @author sxm
 * DESCRIPTION: graduate展示
 * create: 2017-11-16 14:31:07.
 */

@RequestMapping("/commercialOppotunity")
@Controller
public class CommercialOppotunityController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(CommercialOppotunityController.class);

    @Reference
    CommercialOppotunityFacade commercialOppotunityFacade;

    /**
     * @author sxm
     * DESCRIPTION:首页导向页
	 * @params:
     * @return:
	 * @Date:
	 * @Modified By:  
     */
    @RequestMapping(value = {"/index"},method = RequestMethod.GET)
    public String index() {
        return "commercialOppotunity";
    }

    /**
     * 查找测试-sxm-{date}
     *
     * @param id
     */
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
		try {
			CommercialOppotunityEntity commercialOppotunityEntity = commercialOppotunityFacade.findById(id);
			if (commercialOppotunityEntity != null) {
				return ItooResult.build("0000", "查询该课程为空", commercialOppotunityEntity);
			}else{
				return ItooResult.build("0000", "查询该课程成功", commercialOppotunityEntity);
			}
        
        } catch (Exception e) {
            logger.error("",e);
			return ItooResult.build("1111", "查询该课程失败");
        }
    }

    /**
     * 通过区域id 查询地域商机
     * @param regionId  区域ID
     * @param pageNum   页码
     * @param pageSize  页量
     * @return  ItooResult
     */
    @RequestMapping(value = {"/queryRegionCommercialById/{regionId}/{pageNum}/{pageSize}"},method = RequestMethod.GET)
    @ResponseBody
    public ItooResult queryRegionCommercialById(@PathVariable String regionId,@PathVariable int pageNum,@PathVariable int pageSize){

        PageInfo<RegionCommercialModel> regionCommercialModelPageInfo =null;
        try{
            regionCommercialModelPageInfo=commercialOppotunityFacade.queryRegionCommercialById(regionId,pageNum,pageSize);
            if(regionCommercialModelPageInfo !=null){
                return ItooResult.build("0000","查询成功！",regionCommercialModelPageInfo);
            }else {
                return ItooResult.build("1111","查询失败！");
            }
        }catch (Exception e){
            logger.error("查询失败！",e);
            return  ItooResult.build("1111","查询失败！");
        }
    }

    /**
     * 方法重载-查询提高班所有毕业生的商机信息-唐凌峰
     * @return ItooResult
     */
    @RequestMapping(value = {"/queryAllCommercialInfo"},method = RequestMethod.GET)
    @ResponseBody
    public ItooResult queryAllCommercialInfo(){
        try{
            List<RegionCommercialModel> regionCommercialModelList= commercialOppotunityFacade.queryRegionCommercialById();
            if( regionCommercialModelList !=null&& regionCommercialModelList.size()>0){
                return ItooResult.build("0000","查询成功",regionCommercialModelList);
            }else {
                return  ItooResult.build("1111","查询失败！");
            }
        }
        catch (Exception e){
            logger.error("查询失败！");
            return ItooResult.build("1111","查询失败");
        }
    }
}    
