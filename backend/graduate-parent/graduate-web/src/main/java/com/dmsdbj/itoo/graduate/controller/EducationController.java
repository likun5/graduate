package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.EducationEntity;
import com.dmsdbj.itoo.graduate.entity.ext.EducationModel;
import com.dmsdbj.itoo.graduate.entity.ext.PersonalEducationEntity;
import com.dmsdbj.itoo.graduate.facade.EducationFacade;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.dmsdbj.itoo.tool.fastdfs.FastDfsUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


/**
 * @author: sxm 
 * @DESCRIPTION: graduate展示
 * @create: 2017-11-16 14:31:07.
 */
@Api(value = "学历信息接口",tags="学历信息接口")
@RequestMapping("/education")
@Controller
public class EducationController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(EducationController.class);

    @Reference
    EducationFacade educationFacade;
    private static final String url = "classpath:fast_client.conf";
    @Value("${IMAGE_SERVER_BASE_URL}")
    private String IMAGE_SERVER_BASE_URL;

    /**
     *
     * @return
     */
    @RequestMapping(value = {"/index"},method = RequestMethod.GET)
    public String index() {
        return "education";
    }

    /**
     * 根据id查询课程
     * @param id 学历ID
     * @return 查询结果和结果集合
     */
    @ApiOperation(value="根据id查询学历")

    @ResponseBody
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)

    public ItooResult findById(@PathVariable String id) {
        try {
            EducationEntity educationEntity = educationFacade.findById(id);
            if (educationEntity != null) {
                return ItooResult.build("0000", "查询该学历成功", educationEntity);
            } else {
                return ItooResult.build("1111", "查询该学历为空");
            }
        } catch (Exception e) {
            logger.error("", e);
            return ItooResult.build("1111", "查询该学历失败");
        }
    }


    /**
     * 上传图片-于云丽-2018年1月22日
     * @param file 上传文件
     * @return 返回ItooResult封装类
     */
    @ResponseBody
    @RequestMapping(value = {"/importPicture"}, method = RequestMethod.POST)
    public ItooResult importPicture(@RequestParam(value = "file") MultipartFile file) {
        String realUrl = "yyl";
        try {
            //取图片扩展名
            String originalFilename = file.getOriginalFilename();
            //取扩展名不要“.”
            String extName = originalFilename.substring(originalFilename.lastIndexOf('.') + 1);
            FastDfsUtil client = new FastDfsUtil(url);
            String urlString = client.uploadFile(file.getBytes(), extName);
            String keyId = "4m5D199Msg8aeK38YJ1gfo";  //学历信息
            int count = educationFacade.addPictureUrl(urlString, keyId);
            //服务器路径
            realUrl = IMAGE_SERVER_BASE_URL + urlString;
            if (count > 0) {
//            if (!StringUtils.isEmpty(urlString)) {
                return ItooResult.build(ResultCode.SUCCESS,"上传图片成功", realUrl);
            } else {
                return ItooResult.build(ResultCode.FAIL,"上传图片失败", realUrl);
            }
        } catch (Exception e) {
            logger.error("上传图片异常！", e);
            return ItooResult.build(ResultCode.FAIL,"上传图片出现错误", realUrl);
        }

    }


    /**
     * 根据人员id查询学历信息
     * @param personId 人员ID
     * @return 查询结果和结果的合集
     */
    @ApiOperation(value="根据人员id查询学历信息")
    @ResponseBody
    @RequestMapping(value = {"/findEducationByPersonId/{personId}"}, method = RequestMethod.GET)
    public ItooResult findEducationByPersonId(@PathVariable String personId){
        try {
            List<EducationEntity>  list = educationFacade.findByPersonId(personId);
            //List<SalaryEntity> list = salaryFacade.selectByName(name);
            //根据姓名信息查询

            if (list.size() !=0) {
                return ItooResult.build("0000", "根据人员id查询薪资成功",list);
            }else{
                return ItooResult.build("1111", "根据人员id查询薪资为空",list);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据姓名查询薪资失败");
        }
    }


    /**
     * 根据人员名词查询学历信息
     * @param name 姓名
     * @return 查询结果与结果集合
     */
    @ApiOperation(value="根据人员名词查询学历信息")
    @ResponseBody
    @RequestMapping(value = {"/findEducationByName/{name}"}, method = RequestMethod.GET)
    public ItooResult findEducationByName(@PathVariable String name){
        try {
            List<EducationEntity>  list = educationFacade.findByName(name);
            //List<SalaryEntity> list = salaryFacade.selectByName(name);
            //根据姓名信息查询

            if (list.size() !=0) {
                return ItooResult.build("0000", "根据人员id查询薪资成功",list);
            }else{
                return ItooResult.build("1111", "根据人员id查询薪资为空",list);
            }
        } catch (Exception e) {
            logger.error("",e);
            return ItooResult.build("1111", "根据姓名查询薪资失败");
        }
    }

    /**
     * 根据证书类型查询学历
     * @param certificateType 证书类型
     * @return 查询结果与结果集合
     */
    @ApiOperation(value="根据证书类型查询学历")
    @ResponseBody
    @RequestMapping(value = {"/findByCertificateType/{certificateType}"}, method = RequestMethod.GET)
    public ItooResult findByCertificateType(@PathVariable String certificateType) {
        try {
            List<EducationEntity> list = educationFacade.findByCertificateType(certificateType);
            if (list.size() > 0) {
                return ItooResult.build("0000", "查询学历成功", list);
            } else {
                return ItooResult.build("1111", "查询学历为空", list);
            }
        } catch (Exception e) {
            logger.error("", e);
            return ItooResult.build("1111", "查询学历失败");
        }
    }


    /**
     * 查询所有学历信息列表
     * @param page 页码
     * @param pageSize 每页显示数量
     * @return 查询结果与结果集合
     */
    @ApiOperation(value="查询所有学历信息列表")
    @ResponseBody
    @RequestMapping(value = {"/findAll/{page}/{pageSize}"}, method = RequestMethod.GET)
    public ItooResult findAll( @PathVariable int page,@PathVariable int pageSize) {
        try {
            PageInfo<EducationEntity> result = educationFacade.findAll( page, pageSize);
            if (result != null) {
                return ItooResult.build("0000", "查询全部学历信息成功",result);
            } else {
                return ItooResult.build("1111", "查询全部学历信息为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "查询学历信息失败");
        }
    }

    /**
     * 查询所有学历信息列表
     * @param page 页码
     * @param pageSize 每页显示数量
     * @param UserId 用户ID
     * @return 查询结果与实体集合
     */
    @ApiOperation(value="查询所有学历信息列表")
    @ResponseBody
    @RequestMapping(value = {"/findPersonAll/{page}/{pageSize}/{UserId}"}, method = RequestMethod.GET)
    public ItooResult findPersonAll( @PathVariable int page,@PathVariable int pageSize,@PathVariable String UserId) {
        try {
            PageInfo<EducationEntity> result = educationFacade.findPersonAll( page, pageSize,UserId);
            if (result != null) {
                return ItooResult.build("0000", "查询全部学历信息成功",result);
            } else {
                return ItooResult.build("1111", "查询全部学历信息为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "查询学历信息失败");
        }
    }

    /**
     * 添加学历信息
     * @param educationEntity 添加实体
     * @return 添加结果
     */
    @ApiOperation(value="添加学历信息")
    @ResponseBody
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    public ItooResult insert(@RequestBody  EducationEntity educationEntity) {
        try {
            int result = educationFacade.addEducationEntity(educationEntity);
            if (result > 0) {

                return ItooResult.build("0000", "添加成功",result);
            } else {
                return ItooResult.build("1111", "学历信息为空",result);
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "添加失败");
        }
    }

    /**
     * 根据id和iperator删除学历信息
     * @param id 用户ID
     * @param operator  操作员ID
     * @return 删除结果
     */
    @ApiOperation(value="根据id和iperator删除学历信息")
    @ResponseBody
    @RequestMapping(value = {"/deleteById/{id}/{operator}"}, method = RequestMethod.POST)
    public ItooResult deleteEducation(@PathVariable String id, @PathVariable String operator) {
        try {
            int result = educationFacade.deleteEducationEntity(id, operator);
            if (result > 0) {
                return ItooResult.build("0000", "删除成功");
            } else {
                return ItooResult.build("1111", "学历信息为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "删除失败");
        }
    }

    /**
     * 根据用户userid删除学历信息
     * @param userId 用户ID
     * @param operator 操作员ID
     * @return 删除结果
     */
    @ApiOperation(value="根据用户userid删除学历信息")
    @ResponseBody
    @RequestMapping(value = {"/deleteByUserId/{userId}/{operator}"}, method = RequestMethod.POST)
    public ItooResult deleteByUserId(@PathVariable String userId, @PathVariable String operator) {
        try {
            int result = educationFacade.deleteByPersonId(userId, operator);
            if (result > 0) {
                return ItooResult.build("0000", "删除学历信息成功");
            } else {
                return ItooResult.build("1111", "删除学历信息条数为0");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "删除学历信息失败");
        }
    }

    /**
     * 修改学历信息
     * @param educationModel 学历实体
     * @return 修改结果
     */
    @ApiOperation(value="修改学历信息")
    @ResponseBody
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    public ItooResult updateEducation(@RequestBody  EducationModel educationModel) {
        try {
            int result = educationFacade.updateEducationEntity(educationModel);
            if (result > 0) {
                return ItooResult.build("0000", "更新学历信息成功",result);
            } else {
                return ItooResult.build("1111", "更新学历信息失败");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "更新异常");
        }
    }

    /**
     * 查询所有提高班毕业生学历信息-唐凌峰-2018年2月7日17:28:43
     * @param
     * @return ItooResult 查询结果与实体集合
     */
    @ApiOperation(value="询所有提高班毕业生学历信息")
    @ResponseBody
    @RequestMapping(value = {"/queryEducationInfo"},method = RequestMethod.GET)
    public ItooResult queryEducationInfo(){
        try{
            List<PersonalEducationEntity> personalEducationEntityList=educationFacade.queryPersonalEducation();
             if(personalEducationEntityList !=null && personalEducationEntityList.size()>0){
                 //return  ItooResult.build("0000","查询成功",personalEducationEntityList);
                 return   ItooResult.build("0000","查询成功！",personalEducationEntityList);
             }else {
                 return  ItooResult.build("1111","查询失败！");
             }
        }
        catch (Exception e){
            return  ItooResult.build("1111","查询失败！");
        }
        //return null;
    }

    /**
     * 通过个人ID 查询个人学历信息-唐凌峰-2018年2月8日15:06:08
     * @param persionId 个人ID
     * @return ItooResult 查询结果与实体集合
     */
    @ApiOperation(value="通过个人ID 查询毕业生学历信息！")
    @RequestMapping(value = {"/findEducationInfoByPersonId/{persionId}"},method = RequestMethod.GET)
    @ResponseBody
    public ItooResult findEducationInfoByPersonId( @PathVariable String persionId){
        List<PersonalEducationEntity> personalEducationEntityList;
        try{
            personalEducationEntityList=educationFacade.findEducationByPersonId(persionId);
            if(personalEducationEntityList !=null && personalEducationEntityList.size()>0){
                return ItooResult.build("0000","查询成功！",personalEducationEntityList);
            }else {
                return  ItooResult.build("1111","查询失败！");

            }
        }catch (Exception e){
            return  ItooResult.build("1111","查询失败！");
        }
        //return null;
    }

    /**
     * 保存实体和图片信息 - 2018年3月14日11:15:10-薛宇
     * @param EducationModelList 学历实体
     * @return 保存结果成功或者失败
     */
    @ApiOperation(value="保存实体和图片信息")
    @ResponseBody
    @RequestMapping(value = {"/addEducationList"}, method = RequestMethod.POST)
  public  ItooResult addEducationList(@RequestBody  List<EducationModel> EducationModelList){
        int count;
        try {
            count = educationFacade.addEducationModel(EducationModelList);
        } catch (Exception e) {
            logger.error("添加学历信息异常",e);
            return ItooResult.build(ResultCode.FAIL, "添加学历信息异常");
        }
        if (count > 0) {
            return ItooResult.build(ResultCode.SUCCESS, "添加学历信息成功");
        } else {
            return ItooResult.build(ResultCode.FAIL, "添加学历信息失败");
        }
  }

    @ApiOperation(value="查询实体和图片信息")
    @RequestMapping(value = {"/queryEducationModel/{id}"},method = RequestMethod.GET)
    @ResponseBody
  public ItooResult queryEducationModel(@PathVariable String id){
       EducationModel educationModel =new EducationModel();
        try {
            educationModel = educationFacade.queryEducationModel(id);
            if (educationModel != null) {
                return ItooResult.build("0000", "查询全部学历信息成功",educationModel);
            } else {
                return ItooResult.build("1111", "查询全部学历信息为空");
            }
        } catch (Exception e) {
            return ItooResult.build("1111", "查询学历信息失败");
        }
    }
  }
