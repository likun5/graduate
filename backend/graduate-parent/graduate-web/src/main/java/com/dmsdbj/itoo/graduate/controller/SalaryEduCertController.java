package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.ext.PeriodSalaryModel;
import com.dmsdbj.itoo.graduate.facade.SalaryEduCertFacade;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Api(value = "薪资学历统计信息接口",tags="薪资学历统计信息接口")
@Controller
@RequestMapping("salaryEduCert")
public class SalaryEduCertController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(SalaryEduCertController.class);

    //定义返回成功Code的常量
    private static final String SUCCESSCODE="0000";
    //定义返回失败Code的常量
    private static final String ERRORCODE="1111";

    @Reference
    SalaryEduCertFacade salaryEduCertFacade;

    /**
     * 根据期数ID 查询本期每年的薪资情况（平均、最高、最低） 郑晓东 2018年1月27日
     * @param periodId 期数ID
     * @return ItooResult
     */
    @ApiOperation(value="根据期数ID查询本期每年的薪资统计信息")
    @RequestMapping(value = {"/selectAMMSalaryByPeriodId/{periodId}"}, method = RequestMethod.GET)
    @ResponseBody
    public ItooResult selectAMMSalaryByPeriodId(@PathVariable String periodId){
        try {
            List<PeriodSalaryModel> list = salaryEduCertFacade.selectAMMSalaryByPeriodId(periodId);
            if (CollectionUtils.isEmpty(list)) {
                return ItooResult.build(ERRORCODE,"查询本期每年的薪资信息无数据！");
            }
            return ItooResult.build(SUCCESSCODE,"查询本期每年的薪资信息成功!",list);
        }catch (ItooRuntimeException e){
            logger.error("SalaryEduCertController.selectAMMSalaryByPeriodId is error!");
            return ItooResult.build(ERRORCODE,"查询本期每年的薪资信息异常！");
        }
    }

    /**
     * 查询全体今年的薪资情况（平均、最高、最低） 郑晓东 2018年1月27日
     * @return ItooResult
     */
    @ApiOperation(value="查询全体今年的薪资统计信息")
    @RequestMapping(value = {"/selectAMMSalaryInThisYear"}, method = RequestMethod.GET)
    @ResponseBody
    public ItooResult selectAMMSalaryInThisYear(){
        try {
            List<PeriodSalaryModel> list = salaryEduCertFacade.selectAMMSalaryInThisYear();
            if (CollectionUtils.isEmpty(list)) {
                return ItooResult.build(ERRORCODE,"查询全体今年的薪资信息无数据！");
            }
            return ItooResult.build(SUCCESSCODE,"查询全体今年的薪资信息成功!",list);
        }catch (ItooRuntimeException e){
            logger.error("SalaryEduCertController.selectAMMSalaryInThisYear is error!");
            return ItooResult.build(ERRORCODE,"查询全体今年的薪资信息异常！");
        }
    }

    /**
     * 查询全体毕业时的薪资情况（平均、最高、最低） 郑晓东 2018年1月27日
     * @return ItooResult
     */
    @ApiOperation(value="根据全体毕业时的薪资统计信息")
    @RequestMapping(value = {"/selectAMMSalaryInGradeYear"}, method = RequestMethod.GET)
    @ResponseBody
    public ItooResult selectAMMSalaryInGradeYear(){
        try {
            List<PeriodSalaryModel> list = salaryEduCertFacade.selectAMMSalaryInGradeYear();
            if (CollectionUtils.isEmpty(list)) {
                return ItooResult.build(ERRORCODE,"查询全体毕业时的薪资信息无数据！");
            }
            return ItooResult.build(SUCCESSCODE,"查询全体毕业时的薪资信息成功!",list);
        }catch (ItooRuntimeException e){
            logger.error("SalaryEduCertController.selectAMMSalaryInGradeYear is error!");
            return ItooResult.build(ERRORCODE,"查询全体毕业时的薪资信息异常！");
        }
    }
}
