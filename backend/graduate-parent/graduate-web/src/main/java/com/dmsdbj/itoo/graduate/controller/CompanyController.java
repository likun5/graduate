package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.ext.CompanyModel;
import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import com.dmsdbj.itoo.graduate.entity.ext.TechnologyModel;
import com.dmsdbj.itoo.graduate.facade.CompanyFacade;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.dmsdbj.itoo.tool.fastdfs.FastDfsUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


/**
 * @author sxm
 * DESCRIPTION 展示
 * create: 2017-11-16 14:31:07.
 *
 */

@RequestMapping("/company")
@Controller
public class CompanyController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(CompanyController.class);

    private static final String URL = "classpath:fast_client.conf";
    @Value("${IMAGE_SERVER_BASE_URL}")
    private String IMAGE_SERVER_BASE_URL;


    @Reference
    CompanyFacade companyFacade;


    /**
     * 添加公司信息-宋学孟-2017年11月23日
     *
     * @param companyModelList 公司及关系表实体
     * @return 封装类
     */
    @ResponseBody
    @RequestMapping(value = {"/addCompany"}, method = RequestMethod.POST)
    public ItooResult addCompany(@RequestBody List<CompanyModel> companyModelList) {
        int count;
        try {
            count = companyFacade.addCompany(companyModelList);
        } catch (Exception e) {
            logger.error("添加公司信息异常",e);
            return ItooResult.build(ResultCode.FAIL, "添加/修改公司信息异常");
        }
        if (count > 0) {
            return ItooResult.build(ResultCode.SUCCESS, "添加/修改公司信息成功");
        } else {
            return ItooResult.build(ResultCode.FAIL, "添加/修改公司信息失败");
        }
    }

    /**
     * 删除公司-宋学孟-2017年11月23日
     *
     * @param companyId 公司id
     * @param userId    学员id
     * @return 封装类
     */
    @ResponseBody
    @RequestMapping(value = {"/deleteCompany/{companyId}/{userId}/{operatorId}"}, method = RequestMethod.GET)
    public ItooResult deleteCompany(@PathVariable String companyId, @PathVariable String userId, @PathVariable String operatorId) {
        int count;
        try {
            count = companyFacade.deleteCompany(companyId, userId, operatorId);
        } catch (Exception e) {
            logger.error("删除公司异常",e);
            return ItooResult.build(ResultCode.FAIL, "删除公司异常");
        }
        if (count > 0) {
            return ItooResult.build(ResultCode.SUCCESS, "删除公司成功");
        } else {
            return ItooResult.build(ResultCode.FAIL, "删除公司失败");
        }
    }

    /**
     * 修改公司相关信息-2017年11月25日
     *
     * @param companyModel 公司及关系表实体
     * @return 封装类
     */
    @Deprecated
    @ResponseBody
    @RequestMapping(value = {"/updateCompany"}, method = RequestMethod.POST)
    public ItooResult updateCompany(@RequestBody CompanyModel companyModel) {
        int count;
        try {
            count = companyFacade.updateCompany(companyModel);
        } catch (Exception e) {
            logger.error("修改公司相关信息异常",e);
            return ItooResult.build(ResultCode.FAIL, "修改公司相关信息异常");
        }
        if (count > 0) {
            return ItooResult.build(ResultCode.SUCCESS, "修改公司相关信息成功");
        } else {
            return ItooResult.build(ResultCode.FAIL, "修改公司相关信息失败");
        }
    }

    /**
     * 查询公司相关信息-宋学孟-2017年11月25日
     *
     * @param userId 学员Id
     * @return 封装类
     */
    @ResponseBody
    @RequestMapping(value = {"/selectCompanyByUserId/{userId}"}, method = RequestMethod.GET)
    public ItooResult selectCompanyByUserId(@PathVariable String userId) {
        CompanyModel companyModel;
        try {
            companyModel = companyFacade.selectCompanyByUserId(userId);
        } catch (Exception e) {
            logger.error("查询公司相关信息异常",e);
            return ItooResult.build("1111", "查询公司相关信息异常");
        }
        if (companyModel != null) {
            return ItooResult.build("0000", "查询公司相关信息成功", companyModel);
        } else {
            return ItooResult.build("1111", "查询公司相关信息失败");
        }
    }

    /**
     * 查询公司相关信息-所有公司（添加流程）-2018年1月18日
     *
     * @param userId 学员Id
     * @return 封装类
     */
    @ResponseBody
    @RequestMapping(value = {"/selectCompanysByUserId/{userId}"}, method = RequestMethod.GET)
    public ItooResult selectCompanysByUserId(@PathVariable String userId) {
        List<CompanyModel> companyModelList;
        try {
            companyModelList = companyFacade.selectCompanysByUserId(userId);
        } catch (Exception e) {
            logger.error("查询公司相关信息异常", e);
            return ItooResult.build("1111", "查询公司相关信息失败");
        }
        if (!CollectionUtils.isEmpty(companyModelList)) {
            return ItooResult.build("0000", "查询公司相关信息成功", companyModelList);
        } else {
            return ItooResult.build("1111", "未查到公司相关信息");
        }
    }

    /**
     * 查询学员工作经历和薪资-宋学孟-2017年12月4日
     *
     * @param userId 用户id
     * @return 封装类
     */
    @Deprecated
    @ResponseBody
    @RequestMapping(value = {"/selectCompanySalaryByUserId/{userId}"}, method = RequestMethod.GET)
    public ItooResult selectCompanySalaryByUserId(@PathVariable String userId) {
        List<CompanySalaryModel> companySalaryModelList;
        try {
            companySalaryModelList = companyFacade.selectCompanySalaryByUserId(userId);
        } catch (Exception e) {
            logger.error("查询学员工作经历和薪资异常",e);
            return ItooResult.build(ResultCode.FAIL, "查询学员工作经历和薪资失败");
        }
        if (!CollectionUtils.isEmpty(companySalaryModelList)) {
            return ItooResult.build("0000", "查询学员工作经历和薪资成功", companySalaryModelList);
        } else {
            return ItooResult.build("1111", "查询学员工作经历和薪资失败");
        }
    }

    /**
     * 查询所有技术点的类别和详细技术点-宋学孟-2018年1月13日
     *
     * @return 封装类
     */
    @ResponseBody
    @RequestMapping(value = "selectSkillPoint", method = RequestMethod.GET)
    public ItooResult selectSkillPoint() {
        try {
            List<TechnologyModel> list = companyFacade.selectSkillPoint();
            if (CollectionUtils.isEmpty(list)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询信息为空！");
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "查询技术点信息成功！", list);
            }
        } catch (Exception e) {
            logger.error("查询技术点异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询技术点信息异常！");
        }
    }

    /**
     * 上传图片-宋学孟-2018年1月14日
     *
     * @param file 上传文件
     * @return 返回ItooResult封装类
     */
    @ResponseBody
    @RequestMapping(value = {"/importPicture"}, method = RequestMethod.POST)
    public ItooResult importPicture(@RequestParam(value = "file") MultipartFile file) {
        String realUrl;
        try {
            //取图片扩展名
            String originalFilename = file.getOriginalFilename();
            //取扩展名不要“.”
            String extName = originalFilename.substring(originalFilename.lastIndexOf('.') + 1);
            FastDfsUtil client = new FastDfsUtil(URL);
            String urlString = client.uploadFile(file.getBytes(), extName);
            //String keyId = "D6FicyEQ4c7QgemkBUvFPV";  //个人和公司关系表的id
            //int count = companyFacade.addPictureUrl(urlString, keyId);  //如果是和个人和公司的关系绑定，则现在不能插入到数据库中
            //服务器路径
           // realUrl = IMAGE_SERVER_BASE_URL + urlString;
            realUrl = urlString;

            if (!StringUtils.isEmpty(urlString)) {
                return ItooResult.build(ResultCode.SUCCESS, "上传图片成功", realUrl);
            } else {
                return ItooResult.build(ResultCode.FAIL, "上传图片失败");
            }
        } catch (Exception e) {
            logger.error("上传图片异常！", e);
            return ItooResult.build(ResultCode.FAIL, "上传图片出现错误");
        }

    }

    /**
     * 根据相应的id，查询对应的实体（包含url）-宋学孟-2018年1月14日
     *
     * @param keyId 关联外键id
     * @return 封装类
     */
    @Deprecated
    @ResponseBody
    @RequestMapping(value = "selectPictureById/{keyId}", method = RequestMethod.GET)
    public ItooResult selectPictureById(@PathVariable String keyId) {
        List<String> pictureUrlList;
        try {

            pictureUrlList = companyFacade.selectPictureById(keyId);
            if (CollectionUtils.isEmpty(pictureUrlList)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询图片为空！");
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "查询图片成功！", pictureUrlList);
            }
        } catch (Exception e) {
            logger.error("查询图片异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询图片失败！");
        }
    }
}    
