package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
import com.dmsdbj.itoo.graduate.entity.ext.DictionaryTreeModel;
import com.dmsdbj.itoo.graduate.facade.DictionaryFacade;
import com.dmsdbj.itoo.graduate.tool.DictionaryTypeCode;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * graduate展示
 *
 * @author : sxm
 * create : 2017-11-16 14:31:07.
 */

@RequestMapping("/dictionary")
@Controller
public class DictionaryController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(DictionaryController.class);

    @Reference
    DictionaryFacade dictionaryFacade;

    /**
     * 首页导向页
     *
     * @return : 首页
     * @author : sxm
     * @return string
     */
    @RequestMapping(value = {"/index"}, method = RequestMethod.GET)
    public String index() {
        return "dictionary";
    }

    /**
     * 查找测试-sxm-{date}
     * @param id id
     * @return 字典信息
     */
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public ItooResult findById(@PathVariable String id) {
        try {
            DictionaryEntity dictionaryEntity = dictionaryFacade.findById(id);
            if (dictionaryEntity != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询该课程成功", dictionaryEntity);
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "查询该课程为空");
            }

        } catch (Exception e) {
            logger.error("查询该课程异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询该课程失败");
        }
    }

    /**
     * 根据字典id查询字典名词 - 徐玲博-2018-1-29 18:31:59
     *
     * @param id 字典id
     * @return String
     */
    @ResponseBody
    @RequestMapping(value = {"findDictionaryNameById/{id}"},method = RequestMethod.GET)
    public ItooResult findDictionaryNameById(@PathVariable String id) {
        try {
            String dictionaryName;
            dictionaryName = dictionaryFacade.findDictionaryNameById(id);
            if (!"".equals(dictionaryName)) {
                return ItooResult.build(ResultCode.SUCCESS, "根据id查询字典名称成功", dictionaryName);
            } else {
                return ItooResult.build(ResultCode.FAIL, "根据id查询字典名称为空");
            }
        } catch (Exception e) {
            logger.error("根据id查询字典名称异常", e);
            return ItooResult.build(ResultCode.FAIL, "根据id查询字典名称失败");
        }
    }

    /**
     * 根据字典名称查询字典id - 徐玲博-2018-1-29 18:35:58
     *
     * @param dictionaryName 字典名称
     * @return String
     */
    @ResponseBody
    @RequestMapping(value = {"/findDictionaryIdByName/{dictionaryName}"},method = RequestMethod.GET)
    public ItooResult findDictionaryIdByName(@PathVariable String dictionaryName) {
        try {
            String dictionaryId = dictionaryFacade.findDictionaryIdByName(dictionaryName);
            if (!"".equals(dictionaryId)) {
                return ItooResult.build(ResultCode.SUCCESS, "根据字典名称查询id成功", dictionaryId);
            } else {
                return ItooResult.build(ResultCode.FAIL, "根据字典名称查询id失败");
            }
        } catch (Exception e) {
            logger.error("根据字典名称查id异常", e);
            return ItooResult.build(ResultCode.FAIL, "根据字典名称查询id失败");
        }
    }

    /**
     * 模糊查询
     * @param dictionarytyName
     * @return 模糊查询字典信息
     */
    @ResponseBody
    @RequestMapping(value = {"/fuzzyDictionaryInfoByName/{dictionarytyName}"},method = RequestMethod.GET)
    public ItooResult fuzzyDictionaryInfoByName(@RequestParam(required = false, defaultValue = "") String strLike,@PathVariable String dictionarytyName) {
        try {
            List<DictionaryEntity>  dictionaryEntities = dictionaryFacade.fuzzyDictionaryInfoByName(strLike,dictionarytyName);
            if (dictionaryEntities !=null ) {
                return ItooResult.build(ResultCode.SUCCESS, "根据字典名称查询信息成功", dictionaryEntities);
            } else {
                return ItooResult.build(ResultCode.FAIL, "根据字典名称查询字典信息为空");
            }
        } catch (Exception e) {
            logger.error("根据字典名称查id异常", e);
            return ItooResult.build(ResultCode.FAIL, "根据字典名称查询信息失败");
        }
    }

    /**
     * 添加字典 -李爽-2017-12-2 19:33:50
     *
     * @param dictionaryEntity 字典实体
     * @return 添加成功与否
     */
    @RequestMapping(value = {"/addDictionary"}, method = RequestMethod.POST)
    @ResponseBody
    public ItooResult addDictionary(@RequestBody DictionaryEntity dictionaryEntity) {
        try {
            int b = dictionaryFacade.addDictionary(dictionaryEntity);
            if (b > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "添加字典成功！");
            } else if (b == -10) {
                return ItooResult.build(ResultCode.FAIL, "不能重复添加字典!");
            }
            return ItooResult.build(ResultCode.FAIL, "不可以重复添加字典!");
        } catch (Exception e) {
            logger.error("添加字典 失败", e);
            return ItooResult.build(ResultCode.FAIL, "添加字典失败！", e);
        }
    }

    /**
     * 批量添加字典 -李爽-2017-12-2 19:32:23
     *
     * @param dictionaryEntityList 字典实体集合
     * @return 添加结果
     */
    @RequestMapping(value = {"/addDictionarys"}, method = RequestMethod.POST)
    @ResponseBody
    public ItooResult addDictionarys(@RequestBody List<DictionaryEntity> dictionaryEntityList) {
        try {
            int b = dictionaryFacade.addDictionary(dictionaryEntityList);
            if (b > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "添加字典成功！");
            } else if (b == -10) {
                return ItooResult.build(ResultCode.FAIL, "不能重复添加字典!");
            }
            return ItooResult.build(ResultCode.FAIL, "不可以重复添加字典!");
        } catch (Exception e) {
            logger.error("批量添加字典 失败", e);
            return ItooResult.build(ResultCode.FAIL, "批量添加字典失败！", e);
        }
    }

    /**
     * 修改字典信息 -李爽-2017-12-2 19:31:20
     *
     * @param dictionaryEntity 字典表实体
     * @return 修改结果
     */
    @ResponseBody
    @RequestMapping(value = {"/updateDictionary"}, method = RequestMethod.POST)
    public ItooResult updateDictionary(@RequestBody DictionaryEntity dictionaryEntity) {

        try {
            boolean b = dictionaryFacade.updateDictionary(dictionaryEntity);
            if (b) {
                return ItooResult.build(ResultCode.SUCCESS, "修改字典信息成功！");
            }
            return ItooResult.build(ResultCode.FAIL, "修改字典信息失败！");
        } catch (Exception e) {
            logger.error("修改字典 失败", e);
            return ItooResult.build(ResultCode.FAIL, "修改字典信息失败！", e);
        }
    }


    /**
     * 根据主键删除字典信息  李爽 -2017-12-2 19:27:35
     *
     * @param id 主键id
     * @return 删除结果
     */
    @RequestMapping(value = {"/deleteDictionaryById"}, method = RequestMethod.DELETE)
    @ResponseBody
    public ItooResult deleteDictionaryById(@RequestBody String id) {
        try {
            boolean b = dictionaryFacade.deleteDictionary(id);
            if (b) {
                return ItooResult.build(ResultCode.SUCCESS, "删除字典信息成功！");
            }
            return ItooResult.build(ResultCode.FAIL, "删除字典信息失败！");
        } catch (Exception e) {
            logger.error("根据主键删除字典 失败", e);
            return ItooResult.build(ResultCode.FAIL, "删除字典信息失败！");
        }
    }

    /**
     * 批量删除字典  李爽 -2017-12-2 19:24:25
     *
     * @param ids 主键集合
     * @return 删除结果
     */
    @RequestMapping(value = {"/deleteDictionary"}, method = RequestMethod.DELETE)
    @ResponseBody
    public ItooResult deleteDictionary(@RequestBody List<String> ids) {
        try {
            boolean b = dictionaryFacade.deleteDictionary(ids);
            if (b) {
                return ItooResult.build(ResultCode.SUCCESS, "批量删除字典信息成功！");
            }
            return ItooResult.build(ResultCode.FAIL, "批量删除字典信息失败！");
        } catch (Exception e) {
            logger.error("批量删除字典 失败", e);
            return ItooResult.build(ResultCode.FAIL, "批量删除字典信息失败！");
        }
    }


    /**
     * 查询所有字典类型
     *
     * @return 字典实体list，字典类型+类型code
     */
    @ResponseBody
    @RequestMapping(value = {"/selectAllDictionaryType"}, method = RequestMethod.GET)
    public ItooResult selectAllDictionaryType() {
        try {
            List<DictionaryEntity> dictionaryEntityList = dictionaryFacade.selectAllDictionaryType();
            if (CollectionUtils.isNotEmpty(dictionaryEntityList)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询所有字典类型成功！", dictionaryEntityList);
            }
            return ItooResult.build(ResultCode.FAIL, "查询所有字典类型为空！");
        } catch (Exception e) {
            logger.error("查询所有字典 失败", e);
            return ItooResult.build(ResultCode.FAIL, "查询所有字典类型失败！");
        }
    }

    /**
     * 分页查询字典类型-hgt-2018年3月10日 PageSelectDictionaryType
     * @param page 页数
     * @param pageSize 每页大小
     * @return 字典类型list
     */
    @ApiOperation(value = "分页查询字典类型",notes = "分页查询字典类型")
    @ResponseBody
    @RequestMapping(value = "PageSelectDictionaryType/{page}/{pageSize}", method = RequestMethod.GET)
    public ItooResult PageSelectDictionaryType(@PathVariable int page, @PathVariable int pageSize) {
        try {
            PageInfo<DictionaryEntity> dictionaryEntityList = dictionaryFacade.PageSelectDictionaryType(page,pageSize);
            if (CollectionUtils.isNotEmpty(dictionaryEntityList.getList())) {
                return ItooResult.build(ResultCode.SUCCESS, "查询字典类型成功！", dictionaryEntityList);
            }
            return ItooResult.build(ResultCode.FAIL, "查询所有字典类型为空！");
        } catch (Exception e) {
            logger.error("查询所有字典 失败", e);
            return ItooResult.build(ResultCode.FAIL, "查询所有字典类型失败！");
        }
    }

    /**
     * 查询所有字典类型以树的形式返回
     * @return 所有字典类型
     */
    @ResponseBody
    @RequestMapping(value = {"/selectAllDictionaryTypeByTree"}, method = RequestMethod.GET)
    public ItooResult selectAllDictionaryTypeByTree(){
        try {
            List<DictionaryTreeModel> dictionaryTreeModel = dictionaryFacade.selectAllDictionaryTypeByTree();
            if (CollectionUtils.isNotEmpty(dictionaryTreeModel)) {
                return ItooResult.build(ResultCode.SUCCESS, "以树的形式查询字典类型成功！", dictionaryTreeModel);
            }
            return ItooResult.build(ResultCode.FAIL, "查询所有字典类型为空！");
        } catch (Exception e) {
            logger.error("查询所有字典 失败", e);
            return ItooResult.build(ResultCode.FAIL, "查询所有字典类型失败！");
        }
    }

    /**
     * 查询地址类型 -李爽-2017-12-6 14:54:04
     *
     * @return 字典实体list
     */
    @ResponseBody
    @RequestMapping(value = {"/selectAddressType"}, method = RequestMethod.GET)
    public ItooResult selectAddressType() {
        try {
            String aa = DictionaryTypeCode.ADDRESS_TYPE;
            List<DictionaryEntity> dictionaryEntityList = dictionaryFacade.selectDictionaryByTypeCode(aa);
            if (CollectionUtils.isNotEmpty(dictionaryEntityList)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询地域种类成功", dictionaryEntityList);
            }
            return ItooResult.build(ResultCode.FAIL, "查询地域种类为空");
        } catch (Exception e) {
            logger.error("查询地域种类 失败", e);
            return ItooResult.build(ResultCode.FAIL, "查询地域种类失败");
        }
    }

    /**
     * 查询所有期数 -李爽-2017-12-6 14:53:51
     *
     * @return 字典实体list
     */
    @ResponseBody
    @RequestMapping(value = {"/selectGrade"}, method = RequestMethod.GET)
    public ItooResult selectGrade() {
        try {
            List<DictionaryEntity> dictionaryEntityList = dictionaryFacade.selectDictionaryByTypeCode(DictionaryTypeCode.GRADE);
            if (CollectionUtils.isNotEmpty(dictionaryEntityList)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询期数成功", dictionaryEntityList);
            }
            return ItooResult.build(ResultCode.FAIL, "查询期数为空");
        } catch (Exception e) {
            logger.error("查询期数 失败", e);
            return ItooResult.build(ResultCode.FAIL, "查询期数失败");
        }
    }

    /**
     * 查询所有关系-李爽-2018-1-24
     *
     * @return 关系list
     */
    @ResponseBody
    @RequestMapping(value = {"/selectRelation"}, method = RequestMethod.GET)
    public ItooResult selectRelation() {
        try {
            List<DictionaryEntity> dictionaryEntityList = dictionaryFacade.selectDictionaryByTypeCode(DictionaryTypeCode.RELEATION);
            if (CollectionUtils.isNotEmpty(dictionaryEntityList)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询关系成功", dictionaryEntityList);
            }
            return ItooResult.build(ResultCode.FAIL, "查询关系为空");
        } catch (Exception e) {
            logger.error("查询关系 失败", e);
            return ItooResult.build(ResultCode.FAIL, "查询关系失败");
        }
    }

    /**
     * 根据类型查询字典数据-杜娟-2018-2-24 19:35:57
     * @param typeCode
     * @return 字典数据
     */
    @ResponseBody
    @RequestMapping(value ={"/selectDictionaryByTypeCode/{typeCode}"},method = RequestMethod.GET)
    public ItooResult selectDictionaryByTypeCode(@PathVariable String typeCode){
        try {
            List<DictionaryEntity > dictionaryEntities = dictionaryFacade.selectDictionaryByTypeCode(typeCode);
            if ( CollectionUtils.isNotEmpty(dictionaryEntities) ){

                return ItooResult.build(ResultCode.SUCCESS,"根据type查询表格数据成功",dictionaryEntities);
            }
            return ItooResult.build(ResultCode.FAIL,"根据type查询表格数据为空");
        } catch( Exception e){
            logger.error("查询失败", e);
            return ItooResult.build(ResultCode.FAIL,"根据type查询表格数据失败");
        }
    }
}    
