package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.LocalProductEntity;
import com.dmsdbj.itoo.graduate.facade.LocalProductFacade;
import com.dmsdbj.itoo.tool.business.ItooResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;

import java.util.List;
import org.springframework.web.bind.annotation.*;
import com.dmsdbj.itoo.tool.uuid.BaseUuidUtils;
/**
 * @author sxm
 * DESCRIPTION: graduate展示
 * create: 2017-11-16 14:31:07.
 */

@RequestMapping("/localProduct")
@Controller
public class LocalProductController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(LocalProductController.class);

    @Reference
    LocalProductFacade localProductFacade;

    /**
     * @author sxm
     * DESCRIPTION:首页导向页
	 * @params:
     * @return:
	 * @Date:
	 * @Modified By:  
     */
    @RequestMapping(value = {"/index"},method = RequestMethod.GET)
    public String index() {
        return "localProduct";
    }

    /**
     * 查找测试-sxm-{date}
     *
     * @param id
     */
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
		try {
			LocalProductEntity localProductEntity = localProductFacade.findById(id);
			if (localProductEntity != null) {
				return ItooResult.build("0000", "查询该课程为空", localProductEntity);
			}else{
				return ItooResult.build("0000", "查询该课程成功", localProductEntity);
			}
        
        } catch (Exception e) {
            logger.error("",e);
			return ItooResult.build("1111", "查询该课程失败");
        }
    }

    /**
     *查询所有特产信息
     *@param
     *@return
     */
    @RequestMapping(value = "/queryLocalProduct", method = RequestMethod.GET)
    @ResponseBody
    public ItooResult queryLocalProduct(){

        try {
            List<LocalProductEntity> localProductEntity = localProductFacade.queryLocalProduct();

            if (localProductEntity == null || localProductEntity.isEmpty()) {
                return ItooResult.build("1111", "查询所有特产信息失败！");
            }
            return ItooResult.build("0000", "查询所有特产信息成功！", localProductEntity);
        } catch (Exception e) {
            logger.error("controller转换json出错！", e);
            return ItooResult.build("1111", "查询所有特产信息失败！");
        }
    }

    /**
     *添加特产信息
     *@param
     *@return List<LocalProductEntity> 特产类型实体集合
     */
    @CrossOrigin
    @RequestMapping(value = "/addLocalProduct", method = RequestMethod.POST)
    public ItooResult addLocalProduct(@RequestBody LocalProductEntity localProductEntity) {
        boolean flag;
        try {
            localProductEntity.setId(BaseUuidUtils.base58Uuid());
            flag = localProductFacade.addLocalProduct(localProductEntity);
            if (flag) {
                return ItooResult.build("1111", "向特产表中插入数据失败！");
            }
            return ItooResult.build("0000", "向特产表中插入数据成功！");
        } catch (Exception e) {
            logger.error("基础信息服务 特产管理 向特产表中插入数据 错误", e);
            return ItooResult.build("1111", "向特产表中插入数据失败！");
        }
    }


    /**
     * 修改特产信息
     *
     * @param localProductEntity 特产实体
     * @return ItooResult
     */
    @RequestMapping(value = {"/updateLocalProduct"}, method = RequestMethod.POST)

    @ResponseBody
    public ItooResult updateLocalProduct(@RequestBody LocalProductEntity localProductEntity) {
        int flag ;
        try {
            flag = localProductFacade.updateLocalProduct(localProductEntity);
            if (flag == 0) {
                return ItooResult.build("1111", "向特产表中更新数据失败！");
            }
            if (flag == -10) {
                return ItooResult.build("1111", "传入的必要参数为空，无法更新！");
            }
            if (flag == -100) {
                return ItooResult.build("1111", "查询没有该数据，无法更新！");
            }
            return ItooResult.build("0000", "向特产表中插入数据成功！", flag);
        } catch (Exception e) {
            logger.error("基础信息服务 特产管理 json转换出错 错误", e);
            return ItooResult.build("1111", "向特产表中更新数据失败！");
        }
    }


    /**
     * 根据景区id删除景区
     *
     * @param productIds 特产id
     * @return ItooResult
     * @throws Exception
     */
    @RequestMapping(value = {"/deleteLocalProduct/{productIds}"}, method = RequestMethod.POST)

    @ResponseBody
    public ItooResult deleteLocalProduct(@PathVariable List<String> productIds) {

        if (CollectionUtils.isEmpty(productIds)) {
            return ItooResult.build("1111", "参数为空，或是空字符串");
        }
        try {
            boolean flag = localProductFacade.deleteLocalProduct(productIds);
            if (!flag) {
                return ItooResult.build("1111", "根据景区id删除景区信息不存在！");
            }
            return ItooResult.build("0000", "根据景区id删除景区信息成功！");
        } catch (Exception e) {
            logger.error("基础信息服务 景区管理 根据景区id删除景区信息失败", e);
            return ItooResult.build("1111", "根据景区id删除景区信息失败！");
        }
    }

}    
