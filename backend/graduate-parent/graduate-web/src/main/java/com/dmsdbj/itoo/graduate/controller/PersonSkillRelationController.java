package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;


import com.dmsdbj.itoo.graduate.entity.PersonSkillRelationEntity;
import com.dmsdbj.itoo.graduate.facade.PersonSkillRelationFacade;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;
import io.swagger.annotations.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * @author : 徐玲博
 * DESCRIPTION : 个人技术关系controller
 * create : 2017-11-26 16:14:54.
 */
@Api(value = "个人技术关系API", tags = "个人技术关系API")
@RequestMapping("/personSkillRelation")
@Controller
public class PersonSkillRelationController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(PersonSkillRelationController.class);

    @Reference
    private PersonSkillRelationFacade personSkillRelationFacade;

    /**
     * @return :personSkillRelation
     * @author : 徐玲博
     * DESCRIPTION :首页导向页
     */
    @RequestMapping(value = {"/index"}, method = RequestMethod.GET)
    public String index() {
        return "personSkillRelation";
    }

    /**
     * 查找关系-徐玲博-2017-12-4 11:47:13
     *
     * @param id 关系id
     */
    @ApiOperation(value = "根据id查找个人技术关系", notes = "根据id查找个人技术关系")
    @ResponseBody
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
        try {
            PersonSkillRelationEntity personSkillRelationEntity = personSkillRelationFacade.findById(id);
            if (personSkillRelationEntity != null) {
                return ItooResult.build(ResultCode.FAIL, "查询该课程为空", personSkillRelationEntity);
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "查询该课程成功");
            }

        } catch (Exception e) {
            logger.error("", e);
            return ItooResult.build(ResultCode.FAIL, "查询该课程失败");
        }
    }

    /**
     * 根据id查询所有技术-徐玲博-2017-12-4 10:58:00
     *
     * @param userId 人员id
     * @return ItooResult及技术列表
     */
    @ApiOperation(value = "根据id查询所有技术", notes = "根据id查询所有技术")
    @ResponseBody
    @RequestMapping(value = "selectSkillByPerson/{userId}", method = RequestMethod.GET)
    public ItooResult selectSkillByPerson(@PathVariable String userId) {

        List<PersonSkillRelationEntity> personSkillRelationEntityList;
        try {
            personSkillRelationEntityList = personSkillRelationFacade.selectSkillByPerson(userId);
            if (personSkillRelationEntityList != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询某人熟悉的所有技术成功", personSkillRelationEntityList);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询某人熟悉的所有技术失败");
            }
        } catch (Exception e) {
            logger.error("selectSkillByPerson查询某人熟悉的所有技术异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询某人熟悉的所有技术异常");
        }

    }

    /**
     * 查询会某个技术的所有人-徐玲博-2017-12-4 10:58:19
     *
     * @param skillId 技术id
     * @return ItooResult及人员列表
     */
    @ApiOperation(value = "查询会某个技术的所有人", notes = "查询会某个技术的所有人")
    @ResponseBody
    @RequestMapping(value = "selectPersonBySkill/{skillId}", method = RequestMethod.GET)
    public ItooResult selectPersonBySkill(@PathVariable String skillId) {

        List<PersonSkillRelationEntity> personSkillRelationEntityList;
        try {
            personSkillRelationEntityList = personSkillRelationFacade.selectPersonBySkill(skillId);
            if (personSkillRelationEntityList != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询会某个技术的所有人成功", personSkillRelationEntityList);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询会某个技术的所有人失败");
            }
        } catch (Exception e) {
            logger.error("selectPersonBySkill查询会某个技术的所有人异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询会某个技术的所有人异常");
        }

    }

    /**
     * 添加个人和技术的关系-徐玲博-2017-12-4 11:03:39
     *
     * @param personSkillRelationEntity 个人技术关系实体
     * @return ItooResult及受影响行
     */
    @ApiOperation(value = "添加个人和技术的关系", notes = "添加个人和技术的关系")
    @ResponseBody
    @RequestMapping(value = "addPersonSkillRelation/", method = RequestMethod.POST)
    public ItooResult addPersonSkillRelation(@RequestBody PersonSkillRelationEntity personSkillRelationEntity) {

        int result;
        try {
            result = personSkillRelationFacade.addPersonSkillRelation(personSkillRelationEntity);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "添加会某个技术的人成功", result);
            } else {
                return ItooResult.build(ResultCode.FAIL, "添加会某个技术的人失败");
            }
        } catch (Exception e) {
            logger.error("addPersonSkillRelation添加会某个技术的人异常", e);
            return ItooResult.build(ResultCode.FAIL, "添加会某个技术的人失败");
        }

    }

    /**
     * 修改个人和技术的关系-徐玲博-2017-12-4 11:03:39
     *
     * @param personSkillRelationEntity 个人技术关系实体
     * @return ItooResult及受影响行
     */
    @ApiOperation(value = "修改个人和技术的关系", notes = "修改个人和技术的关系")
    @ResponseBody
    @RequestMapping(value = "updatePersonSkillRelation/", method = RequestMethod.POST)
    public ItooResult updatePersonSkillRelation(@RequestBody PersonSkillRelationEntity personSkillRelationEntity) {

        int result;
        try {
            result = personSkillRelationFacade.updatePersonSkillRelation(personSkillRelationEntity);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "修改会某个技术的人成功", result);
            } else {
                return ItooResult.build(ResultCode.FAIL, "修改会某个技术的人失败");
            }
        } catch (Exception e) {
            logger.error("updatePersonSkillRelation修改会某个技术的人异常", e);
            return ItooResult.build(ResultCode.FAIL, "修改会某个技术的人失败");
        }

    }

    /**
     * 删除个人和技术的关系-徐玲博-2017-12-4 11:03:39
     *
     * @param id       用户id
     * @param operator 操作人
     * @return ItooResult及受影响行
     */
    @ApiOperation(value = "删除个人和技术的关系", notes = "删除个人和技术的关系")
    @ResponseBody
    @RequestMapping(value = "deletePersonSkillRelation/{id}/{operator}", method = RequestMethod.POST)
    public ItooResult deletePersonSkillRelation(@PathVariable String id, @PathVariable String operator) {

        int result;
        try {
            result = personSkillRelationFacade.deletePersonSkillRelation(id, operator);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "删除会某个技术的人成功", result);
            } else {
                return ItooResult.build(ResultCode.FAIL, "删除会某个技术的人失败");
            }
        } catch (Exception e) {
            logger.error("deletePersonSkillRelation删除会某个技术的人异常", e);
            return ItooResult.build(ResultCode.FAIL, "删除会某个技术的人失败");
        }

    }
}
