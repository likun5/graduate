package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dmsdbj.itoo.graduate.entity.AdministrativeRegionEntity;
import com.dmsdbj.itoo.graduate.facade.AdministrativeRegionFacade;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * @author 李爽
 * DESCRIPTION: demo展示
 * create: 2017-12-05 10:55:09.
 */

@RequestMapping("/administrativeRegion")
@Controller
@Api(value = "行政区")
public class AdministrativeRegionController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(AdministrativeRegionController.class);

    @Reference
    AdministrativeRegionFacade administrativeRegionFacade;

    /**
     * @author 李爽
     * DESCRIPTION:首页导向页
	 * @params:
     * @return:
	 * @Date:
	 * @Modified By:  
     */
    @RequestMapping(value = {"/index"},method = RequestMethod.GET)
    public String index() {
        return "administrativeRegion";
    }

    /**
     * 查找测试-李爽- 2017-12-05 10:55:09
     *
     * @param id
     */
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public ItooResult findById(@PathVariable String id) {
		if(StringUtils.isEmpty(id)){
            return ItooResult.build(ResultCode.FAIL, "id为空");
        }
        try {
			AdministrativeRegionEntity administrativeRegionEntity = administrativeRegionFacade.findById(id);
			if (administrativeRegionEntity != null) {
				return ItooResult.build(ResultCode.SUCCESS, "查询该课程成功", administrativeRegionEntity);
			}else{
				return ItooResult.build(ResultCode.SUCCESS, "查询该课程为空", administrativeRegionEntity);
			}
        
        } catch (Exception e) {
            logger.debug("Processing trade with id: {}", id);
            logger.error("",e);
			return ItooResult.build(ResultCode.FAIL, "查询该课程失败");
        }
    }

    /**
     * 查询所有的省份 -李爽-2017-12-5 15:00:26
     * @return
     */
    @RequestMapping(value = {"/selectProvince"}, method = RequestMethod.GET)
    @ResponseBody
    public ItooResult selectProvince(){

        try {
            List<AdministrativeRegionEntity>  administrativeRegionEntityList = administrativeRegionFacade.selectProvince();
            if (CollectionUtils.isNotEmpty(administrativeRegionEntityList)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询所有省份成功", administrativeRegionEntityList);
            }else{
                return ItooResult.build(ResultCode.SUCCESS, "查询所有省份为空", administrativeRegionEntityList);
            }
        } catch (Exception e) {
            logger.error("selectProvince()",e);
            return ItooResult.build(ResultCode.FAIL, "查询所有省份失败");
        }

    }

    /**
     * 根据id查询子行政区，比如根据某一省/市 id，查询其下是市/区县 -李爽-2017-12-5 15:02:36
     * @param id
     * @return
     */
    @RequestMapping(value = {"/selectSubRegionById/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据用户名获取用户对象1", httpMethod = "GET", notes = "根据用户名获取用户对象2")
    public ItooResult selectSubRegionById(@PathVariable String id){

        if(StringUtils.isEmpty(id)){
            return ItooResult.build(ResultCode.FAIL, "id为空");
        }
        try {
            List<AdministrativeRegionEntity>  administrativeRegionEntityList = administrativeRegionFacade.selectSubRegionById(id);
            if (CollectionUtils.isNotEmpty(administrativeRegionEntityList)) {
                return ItooResult.build(ResultCode.SUCCESS, "查询子行政区成功", administrativeRegionEntityList);
            }else{
                return ItooResult.build(ResultCode.SUCCESS, "查询子行政区为空", administrativeRegionEntityList);
            }
        } catch (Exception e) {
            logger.error("selectSubRegionById()",e);
            return ItooResult.build(ResultCode.FAIL, "查询子行政区失败");
        }
    }



}    
