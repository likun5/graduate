package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.dmsdbj.itoo.graduate.entity.ApplicationEntity;
import com.dmsdbj.itoo.graduate.entity.ext.RoleApplicationModel;
import com.dmsdbj.itoo.graduate.entity.ext.UserRoleModel;
import com.dmsdbj.itoo.graduate.facade.PortalManagementFacade;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;


/**
 * @author : xulingbo 
 * DESCRIPTION : 查询资源
 * create : 2018-01-04 15:54:54.
 */
@Api(value = "权限信息接口",tags="权限信息接口")
@RequestMapping("/portalManagement")
@Controller
public class PortalManagementController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(PortalManagementController.class);

    @Reference
    PortalManagementFacade portalManagementFacade;



    /**
     * 查找测试-宋学孟
     *
     * @param id
     */
    @ApiOperation(value="根据id查询权限信息接口")
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
		try {
			ApplicationEntity applicationEntity = portalManagementFacade.findById(id);
			if (applicationEntity != null) {
				return ItooResult.build(ResultCode.SUCCESS, "查询权限信息成功", applicationEntity);
			}else{
				return ItooResult.build(ResultCode.FAIL, "查询权限信息为空");
			}
        
        } catch (Exception e) {
            logger.error("",e);
			return ItooResult.build(ResultCode.FAIL, "查询该课程失败");
        }
    }

    /**
     * 用于顶栏显示，根据userId查找服务-宋学孟-2018年1月18日
     * @param userId 用户名
     * @return 顶栏信息
     */
    @ApiOperation(value="查询用户顶栏权限信息")
    @ResponseBody
    @RequestMapping(value = {"/queryUserServiceApplication/{userId}"}, method = RequestMethod.POST)

    // @RequiresPermissions({"query"})
    public ItooResult queryUserServiceApplication(@PathVariable String userId) {
        List<RoleApplicationModel> list;
        try {
            list = portalManagementFacade.queryUserServiceApplication(userId);
        } catch (Exception e) {
            logger.error("PortalManagementController.queryUserServiceApplication 查询服务信息失败 {}", e);
            return ItooResult.build(ResultCode.FAIL, "权限服务 根据userId查询资源 queryUserServiceApplication 返回值为null");
        }
        return ItooResult.build(ResultCode.SUCCESS, "权限服务 根据userId查询资源成功", list);
    }

    /**
     * 用portal的左侧栏显示，根据userId查找所有的子模块和页面-宋学孟-2018年1月18日
     * @param userId 用户ID
     * @param applicationParentId 资源父ID
     * @return 侧栏信息
     */
    @ApiOperation(value="查询用户侧栏权限信息")
    @ResponseBody
    @RequestMapping(value = {"queryServiceModuleApplication/{userId}/{applicationParentId}"}, method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
    public String queryServiceModuleApplication(@PathVariable String userId, @PathVariable String applicationParentId) {
        List<Map<String, Object>> list;
        String jsonString = "";
        try {
            list = portalManagementFacade.queryServiceModuleApplication(userId, applicationParentId);
            jsonString = JSON.toJSONString(list);
        } catch (Exception e) {
            logger.error("PortalManagementController.queryServiceModuleApplication 查询模块和页面失败 {}", e);
        }
        return jsonString;
    }

    /**
     * 根据userId，applicationId查找页面中的按钮-宋学孟-2018年1月18日
     * @param userId 用户ID
     * @param applicationId 资源ID
     * @return 按钮信息
     */
    @ApiOperation(value="查询用户按钮权限信息")
    @ResponseBody
    @RequestMapping(value = {"/queryModuleButtonApplication/userId/{userId}/applicationId/{applicationId}"}, method = RequestMethod.GET)

    public ItooResult queryModuleButtonApplication(@PathVariable String userId, @PathVariable String applicationId) {
        List<RoleApplicationModel> list;
        try {
            list = portalManagementFacade.queryModuleButtonApplication(userId, applicationId);
        } catch (Exception e) {
            logger.error("PortalManagementController.queryModuleButtonApplication 根据userId和applicationId查询页面按钮失败 {}", e);
            return ItooResult.build("1111", "权限服务 根据用户id和资源id查询按钮信息 queryModuleButtonApplication 参数为null");
        }
        return ItooResult.build("0000", "根据用户id和资源id查询按钮信息成功", list);
    }

    /**
     * 登录根据userId查询UserEntity-宋学孟-2018年1月18日
     * @param userCode
     * @return
     */
    @ApiOperation(value="根据用户名和密码查询用户信息接口")
    @ResponseBody
    @RequestMapping(value = {"/selectUserInfoByCode/{userCode}/{password}"}, method = RequestMethod.POST)
    public ItooResult selectUserInfoByCode(@PathVariable String userCode, @PathVariable String password) {
        UserRoleModel userRoleModel;

        try {
            userRoleModel = portalManagementFacade.selectUserInfoByCode(userCode, password);
            if (userRoleModel != null) {
                return ItooResult.build("0000", "登录根据userId查询UserEntity成功",userRoleModel);
            }else{
                return ItooResult.build("1111", "登录根据userId查询UserEntity异常，結果是空");
            }
        } catch (Exception e) {
            logger.error("登录根据userId查询UserEntity失败:{}", e);
            return ItooResult.build("1111", "查询失败");
        }

    }





}
