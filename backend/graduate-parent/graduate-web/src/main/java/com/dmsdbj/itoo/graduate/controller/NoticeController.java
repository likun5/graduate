package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;

import com.dmsdbj.itoo.graduate.entity.NoticeEntity;
import com.dmsdbj.itoo.graduate.entity.ext.NoticeBrowseModel;
import com.dmsdbj.itoo.graduate.entity.ext.NoticeNoticeCategoryModel;
import com.dmsdbj.itoo.graduate.facade.NoticeFacade;
import com.dmsdbj.itoo.graduate.facade.PersonalInfoFacade;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

/**
 * @author 徐玲博
 * DESCRIPTION: demo展示
 * create: 2017-11-26 16:14:54
 */
@Api(value = "通知信息API",tags="通知信息API")
@RequestMapping("/notice")
@Controller
public class NoticeController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(NoticeController.class);

    @Reference
    NoticeFacade noticeFacade;

    @Reference
    PersonalInfoFacade personalInfoFacade;



    /**
     * 查找测试-徐玲博-{date}
     *
     * @param id 通知id
     * @return 通知实体
     */
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
		try {
			NoticeEntity noticeEntity = noticeFacade.findNoticeNoticeCategoryModelById(id);
			if (noticeEntity != null) {
				return ItooResult.build("0000", "查询该课程为空", noticeEntity);
			}else{
				return ItooResult.build("0000", "查询该课程成功", noticeEntity);
			}
        
        } catch (Exception e) {
            logger.error("",e);
			return ItooResult.build("1111", "查询该课程失败");
        }
    }

    /**
     * 添加通知信息-haoguiting-2017年12月6日
     * @param noticeBrowseModel 通知浏览Model
     * @return  受影响行数
     */
    @ApiOperation(value="添加通知信息",notes="添加通知信息")
    @ResponseBody
    @RequestMapping(value = "addNotice/", method = RequestMethod.POST)
    public ItooResult addNotice(@RequestBody NoticeBrowseModel noticeBrowseModel) {
        int resultCount;
        try {
            resultCount = noticeFacade.addNotice(noticeBrowseModel);
            if (resultCount > 0) {
                return ItooResult.build("0000", "添加浏览记录成功！",resultCount);
            } else {
                return ItooResult.build("1111", "添加浏览记录失败！");
            }
        } catch (Exception e) {
            logger.error("addNotice 添加浏览记录异常", e);
            return ItooResult.build("1111", "添加浏览记录异常！");
        }

    }

    /**
     * 修改通知信息-haoguiting-2017-12-5 14:15:45
     * @param noticeEntity 通知实体
     * @return 受影响行
     */
    @ApiOperation(value="修改通知信息",notes="修改通知信息")
    @ResponseBody
    @RequestMapping(value = "updateNotice/", method = RequestMethod.POST)
    public ItooResult updateNotice(@RequestBody NoticeEntity noticeEntity) {
        int resultCount;
        try {
            resultCount = noticeFacade.updateNotice(noticeEntity);
            if (resultCount > 0) {
                return ItooResult.build("0000", "修改浏览记录成功！",resultCount);
            } else {
                return ItooResult.build("1111", "修改浏览记录失败！");
            }
        } catch (Exception e) {
            logger.error("updateNotice 浏览记录异常", e);
            return ItooResult.build("1111", "修改浏览记录异常！");
        }

    }

    /**
     * 删除通知信息-haoguiting-2017年11月27
     * @param noticeID 通知id
     * @param operator 浏览人
     * @return 受影响行数
     */
    @ApiOperation(value="删除通知信息",notes="删除通知信息")
    @ResponseBody
    @RequestMapping(value = "deleteNotice/{noticeID}/{operator}", method = RequestMethod.POST)
    public ItooResult deleteNotice(@PathVariable String noticeID, @PathVariable String operator) {
        int resultCount;
        try {
            resultCount = noticeFacade.deleteNotice(noticeID, operator);
            if (resultCount > 0) {
                return ItooResult.build("0000", "删除浏览记录成功",resultCount);
            } else {
                return ItooResult.build("1111", "删除浏览记录失败！");
            }
        } catch (Exception e) {
            logger.error("deleteNotice 删除浏览记录异常", e);
            return ItooResult.build("1111", "删除浏览记录异常！");
        }

    }

    /**
     *  根据ID查询通知-haoguiting-2017年11月27日
     * @param id 通知id
     * @return noticeNoticeCategoryModel 通知实体
     */
    @ApiOperation(value="根据ID查询通知信息",notes = "根据ID查询通知信息")
    @ResponseBody
    @RequestMapping(value = "findNoticeNoticeCategoryById/{id}", method = RequestMethod.GET)
    public ItooResult findNoticeNoticeCategoryById(@PathVariable String id) {
        NoticeNoticeCategoryModel noticeNoticeCategoryModel;
        try {
            noticeNoticeCategoryModel = noticeFacade.findNoticeNoticeCategoryModelById(id);
            if (noticeNoticeCategoryModel != null) {
                return ItooResult.build("0000", "根据ID查询通知信息成功", noticeNoticeCategoryModel);
            } else {
                return ItooResult.build("1111", "根据ID查询通知信息失败");
            }
        } catch (Exception e) {
            logger.error("findNoticeNoticeCategoryById 根据ID查询通知信息异常", e);
            return ItooResult.build("1111", "根据ID查询通知信息失败");
        }

    }

    /**
     * 查询全部通知信息-haoguiting-2017年11月27日
     * @return noticeNoticeCategoryModel通知list
     */
    @ApiOperation(value="查询全部通知信息",notes="查询全部通知信息")
    @ResponseBody
    @RequestMapping(value = "selectNoticeNoticeCategory/", method = RequestMethod.GET)
    public ItooResult selectNoticeNoticeCategory() {
        List<NoticeNoticeCategoryModel> noticeNoticeCategoryModel;
        try {
            noticeNoticeCategoryModel = noticeFacade.selectNoticeNoticeCategory();
            if (noticeNoticeCategoryModel != null) {
                return ItooResult.build("0000", "查询通知分类成功", noticeNoticeCategoryModel);
            } else {
                return ItooResult.build("1111", "查询通知分类失败");
            }
        } catch (Exception e) {
            logger.error("selectNoticeNoticeCategory 查询通知分类异常", e);
            return ItooResult.build("1111", "查询通知分类失败");
        }

    }


    /**
     * 根据通知分类统计通知数量-haoguiting-2018年1月9日
     * @param columnId 通知类别
     * @return  受影响行
     */
    @ApiOperation(value="根据通知分类统计通知数量",notes="根据通知分类统计通知数量")
    @ResponseBody
    @RequestMapping(value = "selectNoticeCountByColumnId/{columnId}", method = RequestMethod.POST)
    public ItooResult selectNoticeCountByColumnId(@PathVariable String columnId) {
        int resultCount;
        try {
            resultCount = noticeFacade.selectNoticeCountByColumnId(columnId);
            if (resultCount > 0) {
                return ItooResult.build("0000", "根据通知分类统计通知数量查询成功",resultCount);
            } else {
                return ItooResult.build("1111", "根据通知分类统计通知数量查询失败！");
            }
        } catch (Exception e) {
            logger.error("selectNoticeCountByColumnId 根据通知分类统计通知数量查询异常", e);
            return ItooResult.build("1111", "根据通知分类统计通知数量查询异常！");
        }

    }

    /**
     * 根据分类ID查询通知-haoguiting-2017年12月2日
     * @param columnId 通知类别
     * @return NoticeNoticeCategoryModel通知list
     */
    @ApiOperation(value="根据分类ID查询通知",notes="根据分类ID查询通知")
    @ResponseBody
    @RequestMapping(value = "selectNoticeNoticeCategoryByColumnId/{columnId}", method = RequestMethod.GET)
    public ItooResult selectNoticeNoticeCategoryByColumnId(@PathVariable String columnId) {
        List<NoticeNoticeCategoryModel> noticeNoticeCategoryModel;
        try {
            noticeNoticeCategoryModel = noticeFacade.selectNoticeNoticeCategoryByColumnId(columnId);
            if (noticeNoticeCategoryModel != null) {
                return ItooResult.build("0000", "根据分类ID查询通知信息成功", noticeNoticeCategoryModel);
            } else {
                return ItooResult.build("1111", "根据分类ID查询通知信息失败");
            }
        } catch (Exception e) {
            logger.error("selectNoticeNoticeCategoryByColumnId 根据分类ID查询通知信息异常", e);
            return ItooResult.build("1111", "根据分类ID查询通知信息失败");
        }

    }

    /**
     * 真分页-查询全部通知信息-haoguiting-2018-01-11 14:15:45
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 通知list
     */
    @ApiOperation(value="分页查询全部通知信息",notes="分页查询全部通知信息")
    @ResponseBody
    @RequestMapping(value = "PageSelectNoticeNoticeCategory/{page}/{pageSize}", method = RequestMethod.GET)
    public ItooResult PageSelectNoticeNoticeCategory(@PathVariable int page,@PathVariable int pageSize ) {

        try {
            PageInfo<NoticeNoticeCategoryModel> manageModelPageInfo = noticeFacade.PageSelectNoticeNoticeCategory(page, pageSize);
            if (CollectionUtils.isEmpty(manageModelPageInfo.getList())) {
                return ItooResult.build(ResultCode.SUCCESS, "无相关条件的查询结果");
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "分页查询全部通知信息成功", manageModelPageInfo);
            }
        } catch (Exception e) {
            logger.error("PageSelectNoticeNoticeCategory 分页查询全部通知信息异常", e);
            return ItooResult.build(ResultCode.FAIL, "分页查询全部通知信息失败");
        }

    }
    /**
     * 真分页-根据分类ID查询通知-haoguiting-2018年01月10日
     * @param columnId  分类Id
     * @param page 页码
     * @param  pageSize 页大小
     * @return 通知list
     */
    @ApiOperation(value="分页查询根据分类ID查询通知",notes="分页查询根据分类ID查询通知")
    @ResponseBody
    @RequestMapping(value = "PageSelectNoticeNoticeCategoryByColumnId/{columnId}/{page}/{pageSize}", method = RequestMethod.GET)
    public ItooResult PageSelectNoticeNoticeCategoryByColumnId(@PathVariable String columnId,@PathVariable int page,@PathVariable int pageSize ) {

        try {
            PageInfo<NoticeNoticeCategoryModel> manageModelPageInfo = noticeFacade.PageSelectNoticeNoticeCategoryByColumnId(columnId,page, pageSize);
            if (CollectionUtils.isEmpty(manageModelPageInfo.getList())) {
                return ItooResult.build(ResultCode.SUCCESS, "无相关条件的查询结果");
            } else {
                return ItooResult.build(ResultCode.SUCCESS, "分页查询根据分类ID查询通知成功", manageModelPageInfo);
            }
        } catch (Exception e) {
            logger.error("PageSelectNoticeNoticeCategoryByColumnId 分页查询根据分类ID查询通知异常", e);
            return ItooResult.build(ResultCode.FAIL, "分页查询根据分类ID查询通知失败");
        }

    }

    /**
     * 更新通知信息（包括接受人）-haoguiting-2017-12-5 14:15:45
     * @param noticeBrowseModel 浏览记录实体
     * @return 受影响行
     */
    @ApiOperation(value="更新通知信息（包括接受人）",notes="更新通知信息（包括接受人）")
    @ResponseBody
    @RequestMapping(value = "updateNoticeBrowse/", method = RequestMethod.POST)
    public ItooResult updateNoticeBrowse(@RequestBody NoticeBrowseModel noticeBrowseModel) {
        int resultCount;
        try {
            resultCount = noticeFacade.updateNoticeBrowse(noticeBrowseModel);
            if (resultCount > 0) {
                return ItooResult.build("0000", "更新通知信息（包括接受人）成功！",resultCount);
            } else {
                return ItooResult.build("1111", "更新通知信息（包括接受人）失败！");
            }
        } catch (Exception e) {
            logger.error("updateNoticeBrowse 更新通知信息（包括接受人）异常", e);
            return ItooResult.build("1111", "更新通知信息（包括接受人）异常！");
        }

    }
}    
