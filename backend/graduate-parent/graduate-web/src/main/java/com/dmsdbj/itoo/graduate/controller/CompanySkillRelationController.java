package com.dmsdbj.itoo.graduate.controller;

import com.alibaba.dubbo.config.annotation.Reference;


import com.dmsdbj.itoo.graduate.entity.CompanySkillRelationEntity;
import com.dmsdbj.itoo.graduate.facade.CompanySkillRelationFacade;
import com.dmsdbj.itoo.graduate.tool.ResultCode;
import com.dmsdbj.itoo.tool.business.ItooResult;
import io.swagger.annotations.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * @author : 徐玲博
 * DESCRIPTION : demo展示
 * create : 2017-11-26 16:14:54.
 */
@Api(value = "公司技术关系API",tags = "公司技术关系API")
@RequestMapping("/companySkillRelation")
@Controller
public class CompanySkillRelationController {

    //定义打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(CompanySkillRelationController.class);

    @Reference
    private CompanySkillRelationFacade companySkillRelationFacade;

    /**
     * @return : companySkillRelation
     * @author : 徐玲博
     * DESCRIPTION :首页导向页
     */
    @RequestMapping(value = {"/index"},method = RequestMethod.GET)
    public String index() {
        return "companySkillRelation";
    }

    /**
     * 根据id查询公司技术关系-徐玲博-2017-12-4 11:29:10
     *
     * @param id 关系id
     */
    @ApiOperation(value = "根据id查询公司技术关系",notes = "根据id查询公司技术关系")
    @ResponseBody
    @RequestMapping(value = {"/findById/{id}"}, method = RequestMethod.GET)
    public ItooResult findById(@PathVariable String id) {
        try {
            CompanySkillRelationEntity companySkillRelationEntity = companySkillRelationFacade.findById(id);
            if (companySkillRelationEntity != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询该课程为空", companySkillRelationEntity);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询该课程成功");
            }

        } catch (Exception e) {
            logger.error("", e);
            return ItooResult.build(ResultCode.FAIL, "查询该课程失败");
        }
    }

    /**
     * 根据技术id查询所有使用的公司-徐玲博-2017-12-4 10:34:54
     *
     * @param skillId 技术id
     * @return ItooResult 及人员id -关系列表
     */
    @ApiOperation(value = "根据技术id查询所有使用的公司",notes = "根据技术id查询所有使用的公司")
    @ResponseBody
    @RequestMapping(value = "selectCompanyBySkill/{skillId}", method = RequestMethod.GET)
    public ItooResult selectCompanyBySkill(@PathVariable String skillId) {

        List<CompanySkillRelationEntity> companySkillRelationEntityList;
        try {
            companySkillRelationEntityList = companySkillRelationFacade.selectCompanyBySkill(skillId);
            if (companySkillRelationEntityList != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询包括某技术的公司成功", companySkillRelationEntityList);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询包括某技术的公司失败");
            }
        } catch (Exception e) {
            logger.error("selectCompanyBySkill查询包括某技术的公司异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询包括某技术的公司异常");
        }

    }

    /**
     * 根据公司id查询，包括所有的技术-徐玲博-2017-12-4 10:37:25
     *
     * @param companyId 公司id
     * @return ItooResult 及技术id -关系列表
     */
    @ApiOperation(value = "根据公司id查询所有的技术",notes = "根据公司id查询所有的技术")
    @ResponseBody
    @RequestMapping(value = "selectSkillByCompany/{companyId}", method = RequestMethod.GET)
    public ItooResult selectSkillByCompany(@PathVariable String companyId) {

        List<CompanySkillRelationEntity> companySkillRelationEntityList;
        try {
            companySkillRelationEntityList = companySkillRelationFacade.selectSkillByCompany(companyId);
            if (companySkillRelationEntityList != null) {
                return ItooResult.build(ResultCode.SUCCESS, "查询公司包括的技术成功", companySkillRelationEntityList);
            } else {
                return ItooResult.build(ResultCode.FAIL, "查询公司包括的技术失败");
            }
        } catch (Exception e) {
            logger.error("selectSkillByCompany查询公司包括的技术异常", e);
            return ItooResult.build(ResultCode.FAIL, "查询公司包括的技术异常");
        }

    }

    /**
     * 添加公司下的技术-徐玲博-2017-12-4 10:40:08
     *
     * @param companySkillRelationEntity 公司技术关系实体
     * @return ItooResult 及受影响行
     */
    @ApiOperation(value = "添加公司下的技术",notes = "添加公司下的技术")
    @ResponseBody
    @RequestMapping(value = "/addCompanySkillRelation/", method = RequestMethod.POST)
    public ItooResult addCompanySkillRelation(@RequestBody CompanySkillRelationEntity companySkillRelationEntity) {

        int result;
        try {
            result = companySkillRelationFacade.addCompanySkillRelation(companySkillRelationEntity);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "添加公司技术关系成功", result);
            } else {
                return ItooResult.build(ResultCode.FAIL, "添加公司技术关系失败");
            }
        } catch (Exception e) {
            logger.error("addCompanySkillRelation", e);
            return ItooResult.build(ResultCode.FAIL, "添加公司技术关系异常");
        }


    }

    /**
     * 修改公司下的技术-徐玲博-2017-12-4 10:42:39
     *
     * @param companySkillRelationEntity 公司技术关系实体
     * @return ItooResult 及受影响行
     */
    @ApiOperation(value = "修改公司下的技术",notes = "修改公司下的技术")
    @ResponseBody
    @RequestMapping(value = "updateCompanySkillRelation/", method = RequestMethod.POST)
    public ItooResult updateCompanySkillRelation(@RequestBody CompanySkillRelationEntity companySkillRelationEntity) {

        int result;
        try {
            result = companySkillRelationFacade.updateCompanySkillRelation(companySkillRelationEntity);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "修改公司技术关系成功", result);
            } else {
                return ItooResult.build(ResultCode.FAIL, "修改公司技术关系失败");
            }
        } catch (Exception e) {
            logger.error("updateCompanySkillRelation 修改公司技术关系异常", e);
            return ItooResult.build(ResultCode.FAIL, "修改公司技术关系异常");
        }

    }

    /**
     * 删除某公司下技术-徐玲博-2017-12-4 10:41:41
     *
     * @param id       公司技术关系id
     * @param operator 操作人
     * @return ItooResult及受影响行
     */
    @ApiOperation(value = "删除某公司下技术",notes = "删除某公司下技术")
    @ResponseBody
    @RequestMapping(value = "deleteCompanySkillRelation/{id}/{operator}", method = RequestMethod.POST)
    public ItooResult deleteCompanySkillRelation(@PathVariable String id, @PathVariable String operator) {

        int result;
        try {
            result = companySkillRelationFacade.deleteCompanySkillRelation(id, operator);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "删除公司技术关系成功", result);
            } else {
                return ItooResult.build(ResultCode.FAIL, "删除公司技术关系失败");
            }
        } catch (Exception e) {
            logger.error("deleteCompanySkillRelation 删除公司技术关系异常", e);
            return ItooResult.build(ResultCode.FAIL, "删除公司技术关系异常");
        }

    }

    /**
     * 根据公司id删除关系表中技术-徐玲博-2017-12-5 22:10:55
     *
     * @param companyId 公司id
     * @param operator  操作人
     * @return 受影响行
     */
    @ApiOperation(value = "根据公司id删除关系表中技术",notes = "根据公司id删除关系表中技术")
    @ResponseBody
    @RequestMapping(value = "deleteSkillByCompanyId/{companyId}/{operator}", method = RequestMethod.POST)
    public ItooResult deleteSkillByCompanyId(@PathVariable String companyId, @PathVariable String operator) {

        int result;
        try {
            result = companySkillRelationFacade.deleteSkillByCompanyId(companyId, operator);
            if (result > 0) {
                return ItooResult.build(ResultCode.SUCCESS, "根据公司id删除技术成功", result);
            } else {
                return ItooResult.build(ResultCode.FAIL, "根据公司id删除技术失败");
            }
        } catch (Exception e) {
            logger.error("根据公司id删除技术异常", e);
            return ItooResult.build(ResultCode.FAIL, "根据公司id删除技术异常");
        }
    }
    /**
     *根据技术id删除关系表中公司信息-徐玲博-2017-12-5 23:20:31
     *
     * @param skillId 技术id
     * @param operator 操作人
     * @return 受影响行
     */
    @ApiOperation(value = "根据技术id删除关系表中公司信息",notes = "根据技术id删除关系表中公司信息")
    @ResponseBody
    @RequestMapping(value = "deleteCompanyBySkillId/{skillId}/{operator}",method = RequestMethod.POST)
    public ItooResult deleteCompanyBySkillId(@PathVariable String skillId,@PathVariable String operator){

        int result;
        try {
            result=companySkillRelationFacade.deleteCompanyBySkillId(skillId,operator);
            if(result>0){
                return ItooResult.build(ResultCode.SUCCESS,"根据技术id删除公司关系成功",result);
            }else {
                return ItooResult.build(ResultCode.FAIL,"根据技术id删除公司关系失败");
            }
        } catch (Exception e) {
            logger.error("根据技术id删除公司关系异常",e);
            return  ItooResult.build(ResultCode.FAIL,"根据技术id删除公司关系异常");
        }
    }
}    
