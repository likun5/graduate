package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class NoticeCriteria extends GeneratedCriteria<NoticeCriteria> implements Serializable{

    protected NoticeCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String TITLE = "title";   
    private static final String PUBLISHER_ID = "publisherId";   
    private static final String COLUMN_ID = "columnId";   
    private static final String OPERATOR_ID = "operatorId";   
    private static final String TIMESTAMP_TIME = "timestampTime";   
    private static final String NOTICE_CONTENT = "noticeContent";   

    public NoticeCriteria andTitleIsNull() {
        addCriterion("title is null");
        return this;
    }

    public NoticeCriteria andTitleIsNotNull() {
        addCriterion("title is not null");
        return this;
    }
    public NoticeCriteria andTitleEqualTo(String value) {
        addCriterion("title =", value, TITLE);
        return this;
    }
    public NoticeCriteria andTitleNotEqualTo(String value) {
        addCriterion("title <>", value, TITLE);
        return this;
    }    
    public NoticeCriteria andTitleGreaterThan(String value) {
        addCriterion("title >", value, TITLE);
        return this;
    }    
    public NoticeCriteria andTitleGreaterThanOrEqualTo(String value) {
        addCriterion("title >=", value, TITLE);
        return this;
    }    
    public NoticeCriteria andTitleLessThan(String value) {
        addCriterion("title <", value, TITLE);
        return this;
    }     
    public NoticeCriteria andTitleLessThanOrEqualTo(String value) {
        addCriterion("title <=", value, TITLE);
        return this;
    }
    public NoticeCriteria andTitleIn(List<String> values) {
        addCriterion("title in", values, TITLE);
        return this;
    }
    public NoticeCriteria andTitleNotIn(List<String> values) {
        addCriterion("title not in", values, TITLE);
        return this;
    }
    public NoticeCriteria andTitleBetween(String value1, String value2) {
        addCriterion("title between", value1, value2, TITLE);
        return this;
    }
    public NoticeCriteria andTitleNotBetween(String value1, String value2) {
        addCriterion("title not between", value1, value2, TITLE);
        return this;
    }
        public NoticeCriteria andTitleLike(String value) {
            addCriterion("title like", value, TITLE);
            return this;
        }

        public NoticeCriteria andTitleNotLike(String value) {
            addCriterion("title not like", value, TITLE);
            return this;
        }
    public NoticeCriteria andPublisherIdIsNull() {
        addCriterion("publisher_id is null");
        return this;
    }

    public NoticeCriteria andPublisherIdIsNotNull() {
        addCriterion("publisher_id is not null");
        return this;
    }
    public NoticeCriteria andPublisherIdEqualTo(String value) {
        addCriterion("publisher_id =", value, PUBLISHER_ID);
        return this;
    }
    public NoticeCriteria andPublisherIdNotEqualTo(String value) {
        addCriterion("publisher_id <>", value, PUBLISHER_ID);
        return this;
    }    
    public NoticeCriteria andPublisherIdGreaterThan(String value) {
        addCriterion("publisher_id >", value, PUBLISHER_ID);
        return this;
    }    
    public NoticeCriteria andPublisherIdGreaterThanOrEqualTo(String value) {
        addCriterion("publisher_id >=", value, PUBLISHER_ID);
        return this;
    }    
    public NoticeCriteria andPublisherIdLessThan(String value) {
        addCriterion("publisher_id <", value, PUBLISHER_ID);
        return this;
    }     
    public NoticeCriteria andPublisherIdLessThanOrEqualTo(String value) {
        addCriterion("publisher_id <=", value, PUBLISHER_ID);
        return this;
    }
    public NoticeCriteria andPublisherIdIn(List<String> values) {
        addCriterion("publisher_id in", values, PUBLISHER_ID);
        return this;
    }
    public NoticeCriteria andPublisherIdNotIn(List<String> values) {
        addCriterion("publisher_id not in", values, PUBLISHER_ID);
        return this;
    }
    public NoticeCriteria andPublisherIdBetween(String value1, String value2) {
        addCriterion("publisher_id between", value1, value2, PUBLISHER_ID);
        return this;
    }
    public NoticeCriteria andPublisherIdNotBetween(String value1, String value2) {
        addCriterion("publisher_id not between", value1, value2, PUBLISHER_ID);
        return this;
    }
        public NoticeCriteria andPublisherIdLike(String value) {
            addCriterion("publisher_id like", value, PUBLISHER_ID);
            return this;
        }

        public NoticeCriteria andPublisherIdNotLike(String value) {
            addCriterion("publisher_id not like", value, PUBLISHER_ID);
            return this;
        }
    public NoticeCriteria andColumnIdIsNull() {
        addCriterion("column_id is null");
        return this;
    }

    public NoticeCriteria andColumnIdIsNotNull() {
        addCriterion("column_id is not null");
        return this;
    }
    public NoticeCriteria andColumnIdEqualTo(String value) {
        addCriterion("column_id =", value, COLUMN_ID);
        return this;
    }
    public NoticeCriteria andColumnIdNotEqualTo(String value) {
        addCriterion("column_id <>", value, COLUMN_ID);
        return this;
    }    
    public NoticeCriteria andColumnIdGreaterThan(String value) {
        addCriterion("column_id >", value, COLUMN_ID);
        return this;
    }    
    public NoticeCriteria andColumnIdGreaterThanOrEqualTo(String value) {
        addCriterion("column_id >=", value, COLUMN_ID);
        return this;
    }    
    public NoticeCriteria andColumnIdLessThan(String value) {
        addCriterion("column_id <", value, COLUMN_ID);
        return this;
    }     
    public NoticeCriteria andColumnIdLessThanOrEqualTo(String value) {
        addCriterion("column_id <=", value, COLUMN_ID);
        return this;
    }
    public NoticeCriteria andColumnIdIn(List<String> values) {
        addCriterion("column_id in", values, COLUMN_ID);
        return this;
    }
    public NoticeCriteria andColumnIdNotIn(List<String> values) {
        addCriterion("column_id not in", values, COLUMN_ID);
        return this;
    }
    public NoticeCriteria andColumnIdBetween(String value1, String value2) {
        addCriterion("column_id between", value1, value2, COLUMN_ID);
        return this;
    }
    public NoticeCriteria andColumnIdNotBetween(String value1, String value2) {
        addCriterion("column_id not between", value1, value2, COLUMN_ID);
        return this;
    }
        public NoticeCriteria andColumnIdLike(String value) {
            addCriterion("column_id like", value, COLUMN_ID);
            return this;
        }

        public NoticeCriteria andColumnIdNotLike(String value) {
            addCriterion("column_id not like", value, COLUMN_ID);
            return this;
        }
    public NoticeCriteria andOperatorIdIsNull() {
        addCriterion("operator_id is null");
        return this;
    }

    public NoticeCriteria andOperatorIdIsNotNull() {
        addCriterion("operator_id is not null");
        return this;
    }
    public NoticeCriteria andOperatorIdEqualTo(String value) {
        addCriterion("operator_id =", value, OPERATOR_ID);
        return this;
    }
    public NoticeCriteria andOperatorIdNotEqualTo(String value) {
        addCriterion("operator_id <>", value, OPERATOR_ID);
        return this;
    }    
    public NoticeCriteria andOperatorIdGreaterThan(String value) {
        addCriterion("operator_id >", value, OPERATOR_ID);
        return this;
    }    
    public NoticeCriteria andOperatorIdGreaterThanOrEqualTo(String value) {
        addCriterion("operator_id >=", value, OPERATOR_ID);
        return this;
    }    
    public NoticeCriteria andOperatorIdLessThan(String value) {
        addCriterion("operator_id <", value, OPERATOR_ID);
        return this;
    }     
    public NoticeCriteria andOperatorIdLessThanOrEqualTo(String value) {
        addCriterion("operator_id <=", value, OPERATOR_ID);
        return this;
    }
    public NoticeCriteria andOperatorIdIn(List<String> values) {
        addCriterion("operator_id in", values, OPERATOR_ID);
        return this;
    }
    public NoticeCriteria andOperatorIdNotIn(List<String> values) {
        addCriterion("operator_id not in", values, OPERATOR_ID);
        return this;
    }
    public NoticeCriteria andOperatorIdBetween(String value1, String value2) {
        addCriterion("operator_id between", value1, value2, OPERATOR_ID);
        return this;
    }
    public NoticeCriteria andOperatorIdNotBetween(String value1, String value2) {
        addCriterion("operator_id not between", value1, value2, OPERATOR_ID);
        return this;
    }
        public NoticeCriteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, OPERATOR_ID);
            return this;
        }

        public NoticeCriteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, OPERATOR_ID);
            return this;
        }
    public NoticeCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public NoticeCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public NoticeCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public NoticeCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public NoticeCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public NoticeCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public NoticeCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCriteria andNoticeContentIsNull() {
        addCriterion("notice_content is null");
        return this;
    }

    public NoticeCriteria andNoticeContentIsNotNull() {
        addCriterion("notice_content is not null");
        return this;
    }
    public NoticeCriteria andNoticeContentEqualTo(String value) {
        addCriterion("notice_content =", value, NOTICE_CONTENT);
        return this;
    }
    public NoticeCriteria andNoticeContentNotEqualTo(String value) {
        addCriterion("notice_content <>", value, NOTICE_CONTENT);
        return this;
    }    
    public NoticeCriteria andNoticeContentGreaterThan(String value) {
        addCriterion("notice_content >", value, NOTICE_CONTENT);
        return this;
    }    
    public NoticeCriteria andNoticeContentGreaterThanOrEqualTo(String value) {
        addCriterion("notice_content >=", value, NOTICE_CONTENT);
        return this;
    }    
    public NoticeCriteria andNoticeContentLessThan(String value) {
        addCriterion("notice_content <", value, NOTICE_CONTENT);
        return this;
    }     
    public NoticeCriteria andNoticeContentLessThanOrEqualTo(String value) {
        addCriterion("notice_content <=", value, NOTICE_CONTENT);
        return this;
    }
    public NoticeCriteria andNoticeContentIn(List<String> values) {
        addCriterion("notice_content in", values, NOTICE_CONTENT);
        return this;
    }
    public NoticeCriteria andNoticeContentNotIn(List<String> values) {
        addCriterion("notice_content not in", values, NOTICE_CONTENT);
        return this;
    }
    public NoticeCriteria andNoticeContentBetween(String value1, String value2) {
        addCriterion("notice_content between", value1, value2, NOTICE_CONTENT);
        return this;
    }
    public NoticeCriteria andNoticeContentNotBetween(String value1, String value2) {
        addCriterion("notice_content not between", value1, value2, NOTICE_CONTENT);
        return this;
    }
        public NoticeCriteria andNoticeContentLike(String value) {
            addCriterion("notice_content like", value, NOTICE_CONTENT);
            return this;
        }

        public NoticeCriteria andNoticeContentNotLike(String value) {
            addCriterion("notice_content not like", value, NOTICE_CONTENT);
            return this;
        }
}

