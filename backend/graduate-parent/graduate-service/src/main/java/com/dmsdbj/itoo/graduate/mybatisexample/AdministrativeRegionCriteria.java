package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class AdministrativeRegionCriteria extends GeneratedCriteria<AdministrativeRegionCriteria> implements Serializable{

    protected AdministrativeRegionCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String PID = "pid";   
    private static final String SHORTNAME = "shortname";   
    private static final String NAME = "name";   
    private static final String MERGER_NAME = "mergerName";   
    private static final String LEVEL = "level";   
    private static final String PINYIN = "pinyin";   
    private static final String CODE = "code";   
    private static final String ZIP_CODE = "zipCode";   
    private static final String INITIAL = "initial";   
    private static final String LNG = "lng";   
    private static final String LAT = "lat";   
    private static final String OPERATOR_ID = "operatorId";   
    private static final String TIMESTAMP_TIME = "timestampTime";   

    public AdministrativeRegionCriteria andPidIsNull() {
        addCriterion("pid is null");
        return this;
    }

    public AdministrativeRegionCriteria andPidIsNotNull() {
        addCriterion("pid is not null");
        return this;
    }
    public AdministrativeRegionCriteria andPidEqualTo(String value) {
        addCriterion("pid =", value, PID);
        return this;
    }
    public AdministrativeRegionCriteria andPidNotEqualTo(String value) {
        addCriterion("pid <>", value, PID);
        return this;
    }    
    public AdministrativeRegionCriteria andPidGreaterThan(String value) {
        addCriterion("pid >", value, PID);
        return this;
    }    
    public AdministrativeRegionCriteria andPidGreaterThanOrEqualTo(String value) {
        addCriterion("pid >=", value, PID);
        return this;
    }    
    public AdministrativeRegionCriteria andPidLessThan(String value) {
        addCriterion("pid <", value, PID);
        return this;
    }     
    public AdministrativeRegionCriteria andPidLessThanOrEqualTo(String value) {
        addCriterion("pid <=", value, PID);
        return this;
    }
    public AdministrativeRegionCriteria andPidIn(List<String> values) {
        addCriterion("pid in", values, PID);
        return this;
    }
    public AdministrativeRegionCriteria andPidNotIn(List<String> values) {
        addCriterion("pid not in", values, PID);
        return this;
    }
    public AdministrativeRegionCriteria andPidBetween(String value1, String value2) {
        addCriterion("pid between", value1, value2, PID);
        return this;
    }
    public AdministrativeRegionCriteria andPidNotBetween(String value1, String value2) {
        addCriterion("pid not between", value1, value2, PID);
        return this;
    }
    public AdministrativeRegionCriteria andShortnameIsNull() {
        addCriterion("shortname is null");
        return this;
    }

    public AdministrativeRegionCriteria andShortnameIsNotNull() {
        addCriterion("shortname is not null");
        return this;
    }
    public AdministrativeRegionCriteria andShortnameEqualTo(String value) {
        addCriterion("shortname =", value, SHORTNAME);
        return this;
    }
    public AdministrativeRegionCriteria andShortnameNotEqualTo(String value) {
        addCriterion("shortname <>", value, SHORTNAME);
        return this;
    }    
    public AdministrativeRegionCriteria andShortnameGreaterThan(String value) {
        addCriterion("shortname >", value, SHORTNAME);
        return this;
    }    
    public AdministrativeRegionCriteria andShortnameGreaterThanOrEqualTo(String value) {
        addCriterion("shortname >=", value, SHORTNAME);
        return this;
    }    
    public AdministrativeRegionCriteria andShortnameLessThan(String value) {
        addCriterion("shortname <", value, SHORTNAME);
        return this;
    }     
    public AdministrativeRegionCriteria andShortnameLessThanOrEqualTo(String value) {
        addCriterion("shortname <=", value, SHORTNAME);
        return this;
    }
    public AdministrativeRegionCriteria andShortnameIn(List<String> values) {
        addCriterion("shortname in", values, SHORTNAME);
        return this;
    }
    public AdministrativeRegionCriteria andShortnameNotIn(List<String> values) {
        addCriterion("shortname not in", values, SHORTNAME);
        return this;
    }
    public AdministrativeRegionCriteria andShortnameBetween(String value1, String value2) {
        addCriterion("shortname between", value1, value2, SHORTNAME);
        return this;
    }
    public AdministrativeRegionCriteria andShortnameNotBetween(String value1, String value2) {
        addCriterion("shortname not between", value1, value2, SHORTNAME);
        return this;
    }
        public AdministrativeRegionCriteria andShortnameLike(String value) {
            addCriterion("shortname like", value, SHORTNAME);
            return this;
        }

        public AdministrativeRegionCriteria andShortnameNotLike(String value) {
            addCriterion("shortname not like", value, SHORTNAME);
            return this;
        }
    public AdministrativeRegionCriteria andNameIsNull() {
        addCriterion("name is null");
        return this;
    }

    public AdministrativeRegionCriteria andNameIsNotNull() {
        addCriterion("name is not null");
        return this;
    }
    public AdministrativeRegionCriteria andNameEqualTo(String value) {
        addCriterion("name =", value, NAME);
        return this;
    }
    public AdministrativeRegionCriteria andNameNotEqualTo(String value) {
        addCriterion("name <>", value, NAME);
        return this;
    }    
    public AdministrativeRegionCriteria andNameGreaterThan(String value) {
        addCriterion("name >", value, NAME);
        return this;
    }    
    public AdministrativeRegionCriteria andNameGreaterThanOrEqualTo(String value) {
        addCriterion("name >=", value, NAME);
        return this;
    }    
    public AdministrativeRegionCriteria andNameLessThan(String value) {
        addCriterion("name <", value, NAME);
        return this;
    }     
    public AdministrativeRegionCriteria andNameLessThanOrEqualTo(String value) {
        addCriterion("name <=", value, NAME);
        return this;
    }
    public AdministrativeRegionCriteria andNameIn(List<String> values) {
        addCriterion("name in", values, NAME);
        return this;
    }
    public AdministrativeRegionCriteria andNameNotIn(List<String> values) {
        addCriterion("name not in", values, NAME);
        return this;
    }
    public AdministrativeRegionCriteria andNameBetween(String value1, String value2) {
        addCriterion("name between", value1, value2, NAME);
        return this;
    }
    public AdministrativeRegionCriteria andNameNotBetween(String value1, String value2) {
        addCriterion("name not between", value1, value2, NAME);
        return this;
    }
        public AdministrativeRegionCriteria andNameLike(String value) {
            addCriterion("name like", value, NAME);
            return this;
        }

        public AdministrativeRegionCriteria andNameNotLike(String value) {
            addCriterion("name not like", value, NAME);
            return this;
        }
    public AdministrativeRegionCriteria andMergerNameIsNull() {
        addCriterion("merger_name is null");
        return this;
    }

    public AdministrativeRegionCriteria andMergerNameIsNotNull() {
        addCriterion("merger_name is not null");
        return this;
    }
    public AdministrativeRegionCriteria andMergerNameEqualTo(String value) {
        addCriterion("merger_name =", value, MERGER_NAME);
        return this;
    }
    public AdministrativeRegionCriteria andMergerNameNotEqualTo(String value) {
        addCriterion("merger_name <>", value, MERGER_NAME);
        return this;
    }    
    public AdministrativeRegionCriteria andMergerNameGreaterThan(String value) {
        addCriterion("merger_name >", value, MERGER_NAME);
        return this;
    }    
    public AdministrativeRegionCriteria andMergerNameGreaterThanOrEqualTo(String value) {
        addCriterion("merger_name >=", value, MERGER_NAME);
        return this;
    }    
    public AdministrativeRegionCriteria andMergerNameLessThan(String value) {
        addCriterion("merger_name <", value, MERGER_NAME);
        return this;
    }     
    public AdministrativeRegionCriteria andMergerNameLessThanOrEqualTo(String value) {
        addCriterion("merger_name <=", value, MERGER_NAME);
        return this;
    }
    public AdministrativeRegionCriteria andMergerNameIn(List<String> values) {
        addCriterion("merger_name in", values, MERGER_NAME);
        return this;
    }
    public AdministrativeRegionCriteria andMergerNameNotIn(List<String> values) {
        addCriterion("merger_name not in", values, MERGER_NAME);
        return this;
    }
    public AdministrativeRegionCriteria andMergerNameBetween(String value1, String value2) {
        addCriterion("merger_name between", value1, value2, MERGER_NAME);
        return this;
    }
    public AdministrativeRegionCriteria andMergerNameNotBetween(String value1, String value2) {
        addCriterion("merger_name not between", value1, value2, MERGER_NAME);
        return this;
    }
        public AdministrativeRegionCriteria andMergerNameLike(String value) {
            addCriterion("merger_name like", value, MERGER_NAME);
            return this;
        }

        public AdministrativeRegionCriteria andMergerNameNotLike(String value) {
            addCriterion("merger_name not like", value, MERGER_NAME);
            return this;
        }
    public AdministrativeRegionCriteria andLevelIsNull() {
        addCriterion("level is null");
        return this;
    }

    public AdministrativeRegionCriteria andLevelIsNotNull() {
        addCriterion("level is not null");
        return this;
    }
    public AdministrativeRegionCriteria andLevelEqualTo(Byte value) {
        addCriterion("level =", value, LEVEL);
        return this;
    }
    public AdministrativeRegionCriteria andLevelNotEqualTo(Byte value) {
        addCriterion("level <>", value, LEVEL);
        return this;
    }    
    public AdministrativeRegionCriteria andLevelGreaterThan(Byte value) {
        addCriterion("level >", value, LEVEL);
        return this;
    }    
    public AdministrativeRegionCriteria andLevelGreaterThanOrEqualTo(Byte value) {
        addCriterion("level >=", value, LEVEL);
        return this;
    }    
    public AdministrativeRegionCriteria andLevelLessThan(Byte value) {
        addCriterion("level <", value, LEVEL);
        return this;
    }     
    public AdministrativeRegionCriteria andLevelLessThanOrEqualTo(Byte value) {
        addCriterion("level <=", value, LEVEL);
        return this;
    }
    public AdministrativeRegionCriteria andLevelIn(List<Byte> values) {
        addCriterion("level in", values, LEVEL);
        return this;
    }
    public AdministrativeRegionCriteria andLevelNotIn(List<Byte> values) {
        addCriterion("level not in", values, LEVEL);
        return this;
    }
    public AdministrativeRegionCriteria andLevelBetween(Byte value1, Byte value2) {
        addCriterion("level between", value1, value2, LEVEL);
        return this;
    }
    public AdministrativeRegionCriteria andLevelNotBetween(Byte value1, Byte value2) {
        addCriterion("level not between", value1, value2, LEVEL);
        return this;
    }
    public AdministrativeRegionCriteria andPinyinIsNull() {
        addCriterion("pinyin is null");
        return this;
    }

    public AdministrativeRegionCriteria andPinyinIsNotNull() {
        addCriterion("pinyin is not null");
        return this;
    }
    public AdministrativeRegionCriteria andPinyinEqualTo(String value) {
        addCriterion("pinyin =", value, PINYIN);
        return this;
    }
    public AdministrativeRegionCriteria andPinyinNotEqualTo(String value) {
        addCriterion("pinyin <>", value, PINYIN);
        return this;
    }    
    public AdministrativeRegionCriteria andPinyinGreaterThan(String value) {
        addCriterion("pinyin >", value, PINYIN);
        return this;
    }    
    public AdministrativeRegionCriteria andPinyinGreaterThanOrEqualTo(String value) {
        addCriterion("pinyin >=", value, PINYIN);
        return this;
    }    
    public AdministrativeRegionCriteria andPinyinLessThan(String value) {
        addCriterion("pinyin <", value, PINYIN);
        return this;
    }     
    public AdministrativeRegionCriteria andPinyinLessThanOrEqualTo(String value) {
        addCriterion("pinyin <=", value, PINYIN);
        return this;
    }
    public AdministrativeRegionCriteria andPinyinIn(List<String> values) {
        addCriterion("pinyin in", values, PINYIN);
        return this;
    }
    public AdministrativeRegionCriteria andPinyinNotIn(List<String> values) {
        addCriterion("pinyin not in", values, PINYIN);
        return this;
    }
    public AdministrativeRegionCriteria andPinyinBetween(String value1, String value2) {
        addCriterion("pinyin between", value1, value2, PINYIN);
        return this;
    }
    public AdministrativeRegionCriteria andPinyinNotBetween(String value1, String value2) {
        addCriterion("pinyin not between", value1, value2, PINYIN);
        return this;
    }
        public AdministrativeRegionCriteria andPinyinLike(String value) {
            addCriterion("pinyin like", value, PINYIN);
            return this;
        }

        public AdministrativeRegionCriteria andPinyinNotLike(String value) {
            addCriterion("pinyin not like", value, PINYIN);
            return this;
        }
    public AdministrativeRegionCriteria andCodeIsNull() {
        addCriterion("code is null");
        return this;
    }

    public AdministrativeRegionCriteria andCodeIsNotNull() {
        addCriterion("code is not null");
        return this;
    }
    public AdministrativeRegionCriteria andCodeEqualTo(String value) {
        addCriterion("code =", value, CODE);
        return this;
    }
    public AdministrativeRegionCriteria andCodeNotEqualTo(String value) {
        addCriterion("code <>", value, CODE);
        return this;
    }    
    public AdministrativeRegionCriteria andCodeGreaterThan(String value) {
        addCriterion("code >", value, CODE);
        return this;
    }    
    public AdministrativeRegionCriteria andCodeGreaterThanOrEqualTo(String value) {
        addCriterion("code >=", value, CODE);
        return this;
    }    
    public AdministrativeRegionCriteria andCodeLessThan(String value) {
        addCriterion("code <", value, CODE);
        return this;
    }     
    public AdministrativeRegionCriteria andCodeLessThanOrEqualTo(String value) {
        addCriterion("code <=", value, CODE);
        return this;
    }
    public AdministrativeRegionCriteria andCodeIn(List<String> values) {
        addCriterion("code in", values, CODE);
        return this;
    }
    public AdministrativeRegionCriteria andCodeNotIn(List<String> values) {
        addCriterion("code not in", values, CODE);
        return this;
    }
    public AdministrativeRegionCriteria andCodeBetween(String value1, String value2) {
        addCriterion("code between", value1, value2, CODE);
        return this;
    }
    public AdministrativeRegionCriteria andCodeNotBetween(String value1, String value2) {
        addCriterion("code not between", value1, value2, CODE);
        return this;
    }
        public AdministrativeRegionCriteria andCodeLike(String value) {
            addCriterion("code like", value, CODE);
            return this;
        }

        public AdministrativeRegionCriteria andCodeNotLike(String value) {
            addCriterion("code not like", value, CODE);
            return this;
        }
    public AdministrativeRegionCriteria andZipCodeIsNull() {
        addCriterion("zip_code is null");
        return this;
    }

    public AdministrativeRegionCriteria andZipCodeIsNotNull() {
        addCriterion("zip_code is not null");
        return this;
    }
    public AdministrativeRegionCriteria andZipCodeEqualTo(String value) {
        addCriterion("zip_code =", value, ZIP_CODE);
        return this;
    }
    public AdministrativeRegionCriteria andZipCodeNotEqualTo(String value) {
        addCriterion("zip_code <>", value, ZIP_CODE);
        return this;
    }    
    public AdministrativeRegionCriteria andZipCodeGreaterThan(String value) {
        addCriterion("zip_code >", value, ZIP_CODE);
        return this;
    }    
    public AdministrativeRegionCriteria andZipCodeGreaterThanOrEqualTo(String value) {
        addCriterion("zip_code >=", value, ZIP_CODE);
        return this;
    }    
    public AdministrativeRegionCriteria andZipCodeLessThan(String value) {
        addCriterion("zip_code <", value, ZIP_CODE);
        return this;
    }     
    public AdministrativeRegionCriteria andZipCodeLessThanOrEqualTo(String value) {
        addCriterion("zip_code <=", value, ZIP_CODE);
        return this;
    }
    public AdministrativeRegionCriteria andZipCodeIn(List<String> values) {
        addCriterion("zip_code in", values, ZIP_CODE);
        return this;
    }
    public AdministrativeRegionCriteria andZipCodeNotIn(List<String> values) {
        addCriterion("zip_code not in", values, ZIP_CODE);
        return this;
    }
    public AdministrativeRegionCriteria andZipCodeBetween(String value1, String value2) {
        addCriterion("zip_code between", value1, value2, ZIP_CODE);
        return this;
    }
    public AdministrativeRegionCriteria andZipCodeNotBetween(String value1, String value2) {
        addCriterion("zip_code not between", value1, value2, ZIP_CODE);
        return this;
    }
        public AdministrativeRegionCriteria andZipCodeLike(String value) {
            addCriterion("zip_code like", value, ZIP_CODE);
            return this;
        }

        public AdministrativeRegionCriteria andZipCodeNotLike(String value) {
            addCriterion("zip_code not like", value, ZIP_CODE);
            return this;
        }
    public AdministrativeRegionCriteria andInitialIsNull() {
        addCriterion("initial is null");
        return this;
    }

    public AdministrativeRegionCriteria andInitialIsNotNull() {
        addCriterion("initial is not null");
        return this;
    }
    public AdministrativeRegionCriteria andInitialEqualTo(String value) {
        addCriterion("initial =", value, INITIAL);
        return this;
    }
    public AdministrativeRegionCriteria andInitialNotEqualTo(String value) {
        addCriterion("initial <>", value, INITIAL);
        return this;
    }    
    public AdministrativeRegionCriteria andInitialGreaterThan(String value) {
        addCriterion("initial >", value, INITIAL);
        return this;
    }    
    public AdministrativeRegionCriteria andInitialGreaterThanOrEqualTo(String value) {
        addCriterion("initial >=", value, INITIAL);
        return this;
    }    
    public AdministrativeRegionCriteria andInitialLessThan(String value) {
        addCriterion("initial <", value, INITIAL);
        return this;
    }     
    public AdministrativeRegionCriteria andInitialLessThanOrEqualTo(String value) {
        addCriterion("initial <=", value, INITIAL);
        return this;
    }
    public AdministrativeRegionCriteria andInitialIn(List<String> values) {
        addCriterion("initial in", values, INITIAL);
        return this;
    }
    public AdministrativeRegionCriteria andInitialNotIn(List<String> values) {
        addCriterion("initial not in", values, INITIAL);
        return this;
    }
    public AdministrativeRegionCriteria andInitialBetween(String value1, String value2) {
        addCriterion("initial between", value1, value2, INITIAL);
        return this;
    }
    public AdministrativeRegionCriteria andInitialNotBetween(String value1, String value2) {
        addCriterion("initial not between", value1, value2, INITIAL);
        return this;
    }
        public AdministrativeRegionCriteria andInitialLike(String value) {
            addCriterion("initial like", value, INITIAL);
            return this;
        }

        public AdministrativeRegionCriteria andInitialNotLike(String value) {
            addCriterion("initial not like", value, INITIAL);
            return this;
        }
    public AdministrativeRegionCriteria andLngIsNull() {
        addCriterion("lng is null");
        return this;
    }

    public AdministrativeRegionCriteria andLngIsNotNull() {
        addCriterion("lng is not null");
        return this;
    }
    public AdministrativeRegionCriteria andLngEqualTo(String value) {
        addCriterion("lng =", value, LNG);
        return this;
    }
    public AdministrativeRegionCriteria andLngNotEqualTo(String value) {
        addCriterion("lng <>", value, LNG);
        return this;
    }    
    public AdministrativeRegionCriteria andLngGreaterThan(String value) {
        addCriterion("lng >", value, LNG);
        return this;
    }    
    public AdministrativeRegionCriteria andLngGreaterThanOrEqualTo(String value) {
        addCriterion("lng >=", value, LNG);
        return this;
    }    
    public AdministrativeRegionCriteria andLngLessThan(String value) {
        addCriterion("lng <", value, LNG);
        return this;
    }     
    public AdministrativeRegionCriteria andLngLessThanOrEqualTo(String value) {
        addCriterion("lng <=", value, LNG);
        return this;
    }
    public AdministrativeRegionCriteria andLngIn(List<String> values) {
        addCriterion("lng in", values, LNG);
        return this;
    }
    public AdministrativeRegionCriteria andLngNotIn(List<String> values) {
        addCriterion("lng not in", values, LNG);
        return this;
    }
    public AdministrativeRegionCriteria andLngBetween(String value1, String value2) {
        addCriterion("lng between", value1, value2, LNG);
        return this;
    }
    public AdministrativeRegionCriteria andLngNotBetween(String value1, String value2) {
        addCriterion("lng not between", value1, value2, LNG);
        return this;
    }
        public AdministrativeRegionCriteria andLngLike(String value) {
            addCriterion("lng like", value, LNG);
            return this;
        }

        public AdministrativeRegionCriteria andLngNotLike(String value) {
            addCriterion("lng not like", value, LNG);
            return this;
        }
    public AdministrativeRegionCriteria andLatIsNull() {
        addCriterion("lat is null");
        return this;
    }

    public AdministrativeRegionCriteria andLatIsNotNull() {
        addCriterion("lat is not null");
        return this;
    }
    public AdministrativeRegionCriteria andLatEqualTo(String value) {
        addCriterion("lat =", value, LAT);
        return this;
    }
    public AdministrativeRegionCriteria andLatNotEqualTo(String value) {
        addCriterion("lat <>", value, LAT);
        return this;
    }    
    public AdministrativeRegionCriteria andLatGreaterThan(String value) {
        addCriterion("lat >", value, LAT);
        return this;
    }    
    public AdministrativeRegionCriteria andLatGreaterThanOrEqualTo(String value) {
        addCriterion("lat >=", value, LAT);
        return this;
    }    
    public AdministrativeRegionCriteria andLatLessThan(String value) {
        addCriterion("lat <", value, LAT);
        return this;
    }     
    public AdministrativeRegionCriteria andLatLessThanOrEqualTo(String value) {
        addCriterion("lat <=", value, LAT);
        return this;
    }
    public AdministrativeRegionCriteria andLatIn(List<String> values) {
        addCriterion("lat in", values, LAT);
        return this;
    }
    public AdministrativeRegionCriteria andLatNotIn(List<String> values) {
        addCriterion("lat not in", values, LAT);
        return this;
    }
    public AdministrativeRegionCriteria andLatBetween(String value1, String value2) {
        addCriterion("lat between", value1, value2, LAT);
        return this;
    }
    public AdministrativeRegionCriteria andLatNotBetween(String value1, String value2) {
        addCriterion("lat not between", value1, value2, LAT);
        return this;
    }
        public AdministrativeRegionCriteria andLatLike(String value) {
            addCriterion("lat like", value, LAT);
            return this;
        }

        public AdministrativeRegionCriteria andLatNotLike(String value) {
            addCriterion("lat not like", value, LAT);
            return this;
        }
    public AdministrativeRegionCriteria andOperatorIdIsNull() {
        addCriterion("operator_id is null");
        return this;
    }

    public AdministrativeRegionCriteria andOperatorIdIsNotNull() {
        addCriterion("operator_id is not null");
        return this;
    }
    public AdministrativeRegionCriteria andOperatorIdEqualTo(String value) {
        addCriterion("operator_id =", value, OPERATOR_ID);
        return this;
    }
    public AdministrativeRegionCriteria andOperatorIdNotEqualTo(String value) {
        addCriterion("operator_id <>", value, OPERATOR_ID);
        return this;
    }    
    public AdministrativeRegionCriteria andOperatorIdGreaterThan(String value) {
        addCriterion("operator_id >", value, OPERATOR_ID);
        return this;
    }    
    public AdministrativeRegionCriteria andOperatorIdGreaterThanOrEqualTo(String value) {
        addCriterion("operator_id >=", value, OPERATOR_ID);
        return this;
    }    
    public AdministrativeRegionCriteria andOperatorIdLessThan(String value) {
        addCriterion("operator_id <", value, OPERATOR_ID);
        return this;
    }     
    public AdministrativeRegionCriteria andOperatorIdLessThanOrEqualTo(String value) {
        addCriterion("operator_id <=", value, OPERATOR_ID);
        return this;
    }
    public AdministrativeRegionCriteria andOperatorIdIn(List<String> values) {
        addCriterion("operator_id in", values, OPERATOR_ID);
        return this;
    }
    public AdministrativeRegionCriteria andOperatorIdNotIn(List<String> values) {
        addCriterion("operator_id not in", values, OPERATOR_ID);
        return this;
    }
    public AdministrativeRegionCriteria andOperatorIdBetween(String value1, String value2) {
        addCriterion("operator_id between", value1, value2, OPERATOR_ID);
        return this;
    }
    public AdministrativeRegionCriteria andOperatorIdNotBetween(String value1, String value2) {
        addCriterion("operator_id not between", value1, value2, OPERATOR_ID);
        return this;
    }
        public AdministrativeRegionCriteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, OPERATOR_ID);
            return this;
        }

        public AdministrativeRegionCriteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, OPERATOR_ID);
            return this;
        }
    public AdministrativeRegionCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public AdministrativeRegionCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public AdministrativeRegionCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public AdministrativeRegionCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public AdministrativeRegionCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public AdministrativeRegionCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public AdministrativeRegionCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public AdministrativeRegionCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public AdministrativeRegionCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public AdministrativeRegionCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public AdministrativeRegionCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public AdministrativeRegionCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
}

