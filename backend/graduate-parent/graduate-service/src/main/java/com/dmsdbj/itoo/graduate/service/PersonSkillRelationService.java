package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.PersonSkillRelationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.PersonSkillRelationExample;

import java.util.List;


/**
 * @author : 徐玲博
 * create :2017-11-26 16:14:54
 * DESCRIPTION
 */
public interface PersonSkillRelationService extends BaseService<PersonSkillRelationEntity, PersonSkillRelationExample>{
    /**
     * 查询某人熟悉的所有技术-徐玲博-2017-12-4 10:58:00
     *
     * @param userId 人员id
     * @return 技术列表
     */
    List<PersonSkillRelationEntity> selectSkillByPerson(String userId);

    /**
     * 查询会某个技术的所有人-徐玲博-2017-12-4 10:58:19
     *
     * @param skillId 技术id
     * @return 人员列表
     */
    List<PersonSkillRelationEntity> selectPersonBySkill(String skillId);

    /**
     * 根据技术id删除关系表中毕业生-徐玲博-2017-12-5 23:26:38
     * @param skillId 技术id
     * @param operator 操作人
     * @return 受影响行
     */
    int deletePersonBySkillId(String skillId,String operator);

    /**
     * 根据人员id删除关系表中技术-徐玲博-2017-12-5 23:26:58
     * @param userId 用户id
     * @param operator 操作人
     * @return 受影响行
     */
    int deleteSkillByPersonId(String userId,String operator);

    /**
     * 根据人员id删除人员技能关系记录 -lishuang -2017-12-23 15:32:11
     * @param personId 人员id
     * @param operator 操作员
     * @return 是否删除成功
     */
    boolean deletePersonSkillRelationByPersonId(String personId, String operator);
}
