package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.PersonSkillRelationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.PersonSkillRelationExample;
import org.springframework.stereotype.Repository;

/**
 *@author 徐玲博
 *create: 2017-11-26 16:14:54
 *DESCRIPTION:
 */
@Repository
public interface PersonSkillRelationDao extends BaseDao<PersonSkillRelationEntity,PersonSkillRelationExample> {
}
