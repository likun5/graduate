package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class RoleCriteria extends GeneratedCriteria<RoleCriteria> implements Serializable{

    protected RoleCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String ROLE_NAME = "roleName";   
    private static final String ROLE_DESCRIBE = "roleDescribe";   

    public RoleCriteria andRoleNameIsNull() {
        addCriterion("role_name is null");
        return this;
    }

    public RoleCriteria andRoleNameIsNotNull() {
        addCriterion("role_name is not null");
        return this;
    }
    public RoleCriteria andRoleNameEqualTo(String value) {
        addCriterion("role_name =", value, ROLE_NAME);
        return this;
    }
    public RoleCriteria andRoleNameNotEqualTo(String value) {
        addCriterion("role_name <>", value, ROLE_NAME);
        return this;
    }    
    public RoleCriteria andRoleNameGreaterThan(String value) {
        addCriterion("role_name >", value, ROLE_NAME);
        return this;
    }    
    public RoleCriteria andRoleNameGreaterThanOrEqualTo(String value) {
        addCriterion("role_name >=", value, ROLE_NAME);
        return this;
    }    
    public RoleCriteria andRoleNameLessThan(String value) {
        addCriterion("role_name <", value, ROLE_NAME);
        return this;
    }     
    public RoleCriteria andRoleNameLessThanOrEqualTo(String value) {
        addCriterion("role_name <=", value, ROLE_NAME);
        return this;
    }
    public RoleCriteria andRoleNameIn(List<String> values) {
        addCriterion("role_name in", values, ROLE_NAME);
        return this;
    }
    public RoleCriteria andRoleNameNotIn(List<String> values) {
        addCriterion("role_name not in", values, ROLE_NAME);
        return this;
    }
    public RoleCriteria andRoleNameBetween(String value1, String value2) {
        addCriterion("role_name between", value1, value2, ROLE_NAME);
        return this;
    }
    public RoleCriteria andRoleNameNotBetween(String value1, String value2) {
        addCriterion("role_name not between", value1, value2, ROLE_NAME);
        return this;
    }
        public RoleCriteria andRoleNameLike(String value) {
            addCriterion("role_name like", value, ROLE_NAME);
            return this;
        }

        public RoleCriteria andRoleNameNotLike(String value) {
            addCriterion("role_name not like", value, ROLE_NAME);
            return this;
        }
    public RoleCriteria andRoleDescribeIsNull() {
        addCriterion("role_describe is null");
        return this;
    }

    public RoleCriteria andRoleDescribeIsNotNull() {
        addCriterion("role_describe is not null");
        return this;
    }
    public RoleCriteria andRoleDescribeEqualTo(String value) {
        addCriterion("role_describe =", value, ROLE_DESCRIBE);
        return this;
    }
    public RoleCriteria andRoleDescribeNotEqualTo(String value) {
        addCriterion("role_describe <>", value, ROLE_DESCRIBE);
        return this;
    }    
    public RoleCriteria andRoleDescribeGreaterThan(String value) {
        addCriterion("role_describe >", value, ROLE_DESCRIBE);
        return this;
    }    
    public RoleCriteria andRoleDescribeGreaterThanOrEqualTo(String value) {
        addCriterion("role_describe >=", value, ROLE_DESCRIBE);
        return this;
    }    
    public RoleCriteria andRoleDescribeLessThan(String value) {
        addCriterion("role_describe <", value, ROLE_DESCRIBE);
        return this;
    }     
    public RoleCriteria andRoleDescribeLessThanOrEqualTo(String value) {
        addCriterion("role_describe <=", value, ROLE_DESCRIBE);
        return this;
    }
    public RoleCriteria andRoleDescribeIn(List<String> values) {
        addCriterion("role_describe in", values, ROLE_DESCRIBE);
        return this;
    }
    public RoleCriteria andRoleDescribeNotIn(List<String> values) {
        addCriterion("role_describe not in", values, ROLE_DESCRIBE);
        return this;
    }
    public RoleCriteria andRoleDescribeBetween(String value1, String value2) {
        addCriterion("role_describe between", value1, value2, ROLE_DESCRIBE);
        return this;
    }
    public RoleCriteria andRoleDescribeNotBetween(String value1, String value2) {
        addCriterion("role_describe not between", value1, value2, ROLE_DESCRIBE);
        return this;
    }
        public RoleCriteria andRoleDescribeLike(String value) {
            addCriterion("role_describe like", value, ROLE_DESCRIBE);
            return this;
        }

        public RoleCriteria andRoleDescribeNotLike(String value) {
            addCriterion("role_describe not like", value, ROLE_DESCRIBE);
            return this;
        }
}

