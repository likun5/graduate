package com.dmsdbj.itoo.graduate.facade.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.dmsdbj.itoo.graduate.entity.ext.CompanyModel;
import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import com.dmsdbj.itoo.graduate.entity.ext.TechnologyModel;
import com.dmsdbj.itoo.graduate.facade.CompanyFacade;
import com.dmsdbj.itoo.graduate.service.CompanyService;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author sxm
 * create: 2017-11-16 14:31:07
 * DESCRIPTION
 */
@Component("companyFacade")
@Service
public class CompanyFacadeImpl implements CompanyFacade {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(CompanyFacadeImpl.class);

    @Resource
    private CompanyService companyService;


    /**
     * 添加公司信息-宋学孟-2017年11月23日
     *
     * @param companyModelList 公司及关系表实体
     * @return 影响条数
     */
    @Override
    public int addCompany(List<CompanyModel> companyModelList) {
        int count;
        try {
            count = companyService.addCompany(companyModelList);
        } catch (Exception e) {
            logger.error("添加公司信息异常", e);
            throw new ItooRuntimeException(e);
        }
        return count;
    }

    /**
     * 删除公司-宋学孟-2017年11月23日
     *
     * @param companyId 公司id
     * @param userId    学员id
     * @return 影响条数
     */
    @Override
    public int deleteCompany(String companyId, String userId, String operatorId) {
        int count;
        try {
            count = companyService.deleteCompany(companyId, userId, operatorId);
        } catch (Exception e) {
            logger.error("删除公司异常", e);
            throw new ItooRuntimeException(e);
        }
        return count;
    }

    /**
     * 修改公司相关信息-2017年11月25日
     *
     * @param companyModel 公司及关系表实体
     * @return 影响条数
     */
    @Override
    public int updateCompany(CompanyModel companyModel) {
        int count;
        try {
            count = companyService.updateCompany(companyModel);
        } catch (Exception e) {
            logger.error("修改公司相关信息异常", e);
            throw new ItooRuntimeException(e);
        }
        return count;
    }

    /**
     * 查询公司相关信息-宋学孟-2017年11月25日
     *
     * @param userId 学员Id
     * @return 公司实体
     */
    @Override
    public CompanyModel selectCompanyByUserId(String userId) {

        CompanyModel companyModel;
        try {
            companyModel = companyService.selectCompanyByUserId(userId);
        } catch (Exception e) {
            logger.error("查询公司相关信息异常", e);
            throw new ItooRuntimeException(e);
        }
        return companyModel;
    }

    /**
     * 查询公司相关信息-所有公司（添加流程）-2018年1月18日
     *
     * @param userId 学员Id
     * @return 公司实体list
     */
    @Override
    public List<CompanyModel> selectCompanysByUserId(String userId) {

        List<CompanyModel> companyModelList;
        try {
            companyModelList = companyService.selectCompanysByUserId(userId);
        } catch (Exception e) {
            logger.error("查询公司相关信息-所有公司", e);
            throw new ItooRuntimeException(e);
        }
        return companyModelList;
    }

    /**
     * 查询学员工作经历和薪资-宋学孟-2017年12月4日
     *
     * @param userId 用户id
     * @return 公司薪资list
     */
    @Override
    public List<CompanySalaryModel> selectCompanySalaryByUserId(String userId) {

        int page=0;
        int pageSize = 0;
        List<CompanySalaryModel> companySalaryModelList;
        try {
            companySalaryModelList = companyService.selectCompanySalaryByUserId(userId,page,pageSize);
        } catch (Exception e) {
            logger.error("查询学员工作经历和薪资", e);
            throw new ItooRuntimeException(e);
        }
        return companySalaryModelList;
    }

    /**
     * 查询所有技术点的类别和详细技术点-宋学孟-2018年1月13日
     *
     * @return 技术点list
     */
    @Override
    public List<TechnologyModel> selectSkillPoint() {
        return companyService.selectSkillPoint();
    }

    /**
     * 保存上传图片的路径（一张或多张）-宋学孟-2018年1月14日
     *
     * @param pictureUrl 图片url
     * @param keyId 关联外键id
     * @return 影响条数
     */
    @Deprecated
    @Override
    public int addPictureUrl(String pictureUrl, String keyId) {
        int count;
        try {
            count = companyService.addPictureUrl(pictureUrl, keyId);
        } catch (Exception e) {
            logger.error("保存上传图片的路径", e);
            throw new ItooRuntimeException(e);
        }
        return count;
    }

    /**
     * 根据相应的id，查询对应的实体（包含url）-宋学孟-2018年1月14日
     *
     * @param keyId 关联外键id
     * @return 影响条数
     */
    @Override
    public List<String> selectPictureById(String keyId) {
        List<String> strings;
        try {
            strings = companyService.selectPictureById(keyId);
        } catch (Exception e) {
            logger.error("根据相应的id，查询对应的实体", e);
            throw new ItooRuntimeException(e);
        }
        return strings;
    }

}
