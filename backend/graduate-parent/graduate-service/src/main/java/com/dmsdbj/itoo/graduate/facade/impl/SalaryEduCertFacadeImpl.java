package com.dmsdbj.itoo.graduate.facade.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.dmsdbj.itoo.graduate.entity.ext.PeriodSalaryModel;
import com.dmsdbj.itoo.graduate.facade.SalaryEduCertFacade;
import com.dmsdbj.itoo.graduate.service.SalaryEduCertService;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 薪资学历统计Facade接口
 * 郑晓东
 * 2018年1月19日18点29分
 */
@Component("salaryEduCertFacade")
@Service
public class SalaryEduCertFacadeImpl implements SalaryEduCertFacade {
    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(SalaryEduCertFacadeImpl.class);

    @Autowired
    private SalaryEduCertService salaryEduCertService;

    @Override
    public List<PeriodSalaryModel> selectAMMSalaryByPeriodId(String periodId) {
        try{
            return salaryEduCertService.selectAMMSalaryByPeriodId(periodId);
        }catch (Exception e){
            logger.error("SalaryEduCertFacadeImpl.selectAMMSalaryByPeriodId is error!");
            throw new ItooRuntimeException(e);
        }
    }

    /**
     * 查询今年每期平均、最高、最低薪资统计 郑晓东 2018年1月20日16点38分
     * @return 薪资统计集合
     */
    @Override
    public List<PeriodSalaryModel> selectAMMSalaryInThisYear() {
        try{
            return salaryEduCertService.selectAMMSalaryInThisYear();
        }catch (Exception e){
            logger.error("SalaryEduCertFacadeImpl.selectAMMSalaryInThisYear is error!");
            throw new ItooRuntimeException(e);
        }
    }

    /**
     * 查询每期毕业时平均、最高、最低薪资统计  郑晓东 2018年1月20日
     * @return 薪资统计集合
     */
    @Override
    public List<PeriodSalaryModel> selectAMMSalaryInGradeYear() {
        try{
            return salaryEduCertService.selectAMMSalaryInGradeYear();
        }catch (Exception e){
            logger.error("SalaryEduCertFacadeImpl.selectAMMSalaryInGradeYear is error!");
            throw new ItooRuntimeException(e);
        }
    }
}
