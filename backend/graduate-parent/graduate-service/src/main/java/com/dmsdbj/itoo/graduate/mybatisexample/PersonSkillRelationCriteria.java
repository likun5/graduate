package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class PersonSkillRelationCriteria extends GeneratedCriteria<PersonSkillRelationCriteria> implements Serializable{

    protected PersonSkillRelationCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String USER_ID = "userId";   
    private static final String SKILL_ID = "skillId";   
    private static final String OPERATOR_ID = "operatorId";   
    private static final String TIMESTAMP_TIME = "timestampTime";   

    public PersonSkillRelationCriteria andUserIdIsNull() {
        addCriterion("user_id is null");
        return this;
    }

    public PersonSkillRelationCriteria andUserIdIsNotNull() {
        addCriterion("user_id is not null");
        return this;
    }
    public PersonSkillRelationCriteria andUserIdEqualTo(String value) {
        addCriterion("user_id =", value, USER_ID);
        return this;
    }
    public PersonSkillRelationCriteria andUserIdNotEqualTo(String value) {
        addCriterion("user_id <>", value, USER_ID);
        return this;
    }    
    public PersonSkillRelationCriteria andUserIdGreaterThan(String value) {
        addCriterion("user_id >", value, USER_ID);
        return this;
    }    
    public PersonSkillRelationCriteria andUserIdGreaterThanOrEqualTo(String value) {
        addCriterion("user_id >=", value, USER_ID);
        return this;
    }    
    public PersonSkillRelationCriteria andUserIdLessThan(String value) {
        addCriterion("user_id <", value, USER_ID);
        return this;
    }     
    public PersonSkillRelationCriteria andUserIdLessThanOrEqualTo(String value) {
        addCriterion("user_id <=", value, USER_ID);
        return this;
    }
    public PersonSkillRelationCriteria andUserIdIn(List<String> values) {
        addCriterion("user_id in", values, USER_ID);
        return this;
    }
    public PersonSkillRelationCriteria andUserIdNotIn(List<String> values) {
        addCriterion("user_id not in", values, USER_ID);
        return this;
    }
    public PersonSkillRelationCriteria andUserIdBetween(String value1, String value2) {
        addCriterion("user_id between", value1, value2, USER_ID);
        return this;
    }
    public PersonSkillRelationCriteria andUserIdNotBetween(String value1, String value2) {
        addCriterion("user_id not between", value1, value2, USER_ID);
        return this;
    }
        public PersonSkillRelationCriteria andUserIdLike(String value) {
            addCriterion("user_id like", value, USER_ID);
            return this;
        }

        public PersonSkillRelationCriteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, USER_ID);
            return this;
        }
    public PersonSkillRelationCriteria andSkillIdIsNull() {
        addCriterion("skill_id is null");
        return this;
    }

    public PersonSkillRelationCriteria andSkillIdIsNotNull() {
        addCriterion("skill_id is not null");
        return this;
    }
    public PersonSkillRelationCriteria andSkillIdEqualTo(String value) {
        addCriterion("skill_id =", value, SKILL_ID);
        return this;
    }
    public PersonSkillRelationCriteria andSkillIdNotEqualTo(String value) {
        addCriterion("skill_id <>", value, SKILL_ID);
        return this;
    }    
    public PersonSkillRelationCriteria andSkillIdGreaterThan(String value) {
        addCriterion("skill_id >", value, SKILL_ID);
        return this;
    }    
    public PersonSkillRelationCriteria andSkillIdGreaterThanOrEqualTo(String value) {
        addCriterion("skill_id >=", value, SKILL_ID);
        return this;
    }    
    public PersonSkillRelationCriteria andSkillIdLessThan(String value) {
        addCriterion("skill_id <", value, SKILL_ID);
        return this;
    }     
    public PersonSkillRelationCriteria andSkillIdLessThanOrEqualTo(String value) {
        addCriterion("skill_id <=", value, SKILL_ID);
        return this;
    }
    public PersonSkillRelationCriteria andSkillIdIn(List<String> values) {
        addCriterion("skill_id in", values, SKILL_ID);
        return this;
    }
    public PersonSkillRelationCriteria andSkillIdNotIn(List<String> values) {
        addCriterion("skill_id not in", values, SKILL_ID);
        return this;
    }
    public PersonSkillRelationCriteria andSkillIdBetween(String value1, String value2) {
        addCriterion("skill_id between", value1, value2, SKILL_ID);
        return this;
    }
    public PersonSkillRelationCriteria andSkillIdNotBetween(String value1, String value2) {
        addCriterion("skill_id not between", value1, value2, SKILL_ID);
        return this;
    }
        public PersonSkillRelationCriteria andSkillIdLike(String value) {
            addCriterion("skill_id like", value, SKILL_ID);
            return this;
        }

        public PersonSkillRelationCriteria andSkillIdNotLike(String value) {
            addCriterion("skill_id not like", value, SKILL_ID);
            return this;
        }
    public PersonSkillRelationCriteria andOperatorIdIsNull() {
        addCriterion("operator_id is null");
        return this;
    }

    public PersonSkillRelationCriteria andOperatorIdIsNotNull() {
        addCriterion("operator_id is not null");
        return this;
    }
    public PersonSkillRelationCriteria andOperatorIdEqualTo(String value) {
        addCriterion("operator_id =", value, OPERATOR_ID);
        return this;
    }
    public PersonSkillRelationCriteria andOperatorIdNotEqualTo(String value) {
        addCriterion("operator_id <>", value, OPERATOR_ID);
        return this;
    }    
    public PersonSkillRelationCriteria andOperatorIdGreaterThan(String value) {
        addCriterion("operator_id >", value, OPERATOR_ID);
        return this;
    }    
    public PersonSkillRelationCriteria andOperatorIdGreaterThanOrEqualTo(String value) {
        addCriterion("operator_id >=", value, OPERATOR_ID);
        return this;
    }    
    public PersonSkillRelationCriteria andOperatorIdLessThan(String value) {
        addCriterion("operator_id <", value, OPERATOR_ID);
        return this;
    }     
    public PersonSkillRelationCriteria andOperatorIdLessThanOrEqualTo(String value) {
        addCriterion("operator_id <=", value, OPERATOR_ID);
        return this;
    }
    public PersonSkillRelationCriteria andOperatorIdIn(List<String> values) {
        addCriterion("operator_id in", values, OPERATOR_ID);
        return this;
    }
    public PersonSkillRelationCriteria andOperatorIdNotIn(List<String> values) {
        addCriterion("operator_id not in", values, OPERATOR_ID);
        return this;
    }
    public PersonSkillRelationCriteria andOperatorIdBetween(String value1, String value2) {
        addCriterion("operator_id between", value1, value2, OPERATOR_ID);
        return this;
    }
    public PersonSkillRelationCriteria andOperatorIdNotBetween(String value1, String value2) {
        addCriterion("operator_id not between", value1, value2, OPERATOR_ID);
        return this;
    }
        public PersonSkillRelationCriteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, OPERATOR_ID);
            return this;
        }

        public PersonSkillRelationCriteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, OPERATOR_ID);
            return this;
        }
    public PersonSkillRelationCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public PersonSkillRelationCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public PersonSkillRelationCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public PersonSkillRelationCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public PersonSkillRelationCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public PersonSkillRelationCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public PersonSkillRelationCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public PersonSkillRelationCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public PersonSkillRelationCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public PersonSkillRelationCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public PersonSkillRelationCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public PersonSkillRelationCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
}

