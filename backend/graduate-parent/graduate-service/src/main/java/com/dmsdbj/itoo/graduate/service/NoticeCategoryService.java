package com.dmsdbj.itoo.graduate.service;

import java.util.List;

import com.dmsdbj.itoo.graduate.entity.NoticeCategoryEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.NoticeCategoryExample;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;


/**
 * @author haoguiting
 * create:2017-11-27 11:34:12
 * DESCRIPTION
 */
public interface NoticeCategoryService extends BaseService<NoticeCategoryEntity, NoticeCategoryExample>{

    /**
     * 添加公告分类信息-haoguiting-2017年11月27日
     * @param noticeCatoryEntity 通知类别实体
     * @return 受影响行数
     */
    int addNoticeCategory(NoticeCategoryEntity noticeCatoryEntity);

    /**
     * 删除公告分类信息-haoguiting-2017年11月27日
     * @param id  通知分类ID
     * @param operator 操作人
     * @return 受影响行数
     */
    int deleteNoticeCategory(@Param("id") String id, @Param("operator") String operator);

    /**
     * 修改公告分类信息-haoguiting-2017年11月27日
     * @param noticeCategoryEntity 通知分类实体
     * @return 受影响行
     */
    int updateNoticeCategory(NoticeCategoryEntity noticeCategoryEntity);

    /**
     * 查询全部通知分类-haoguiting-2017年11月27日
     * @return 通知分类list列表
     */
    List<NoticeCategoryEntity> selectNoticeCategoryInfo();

    /**
     * 根分类ID查询通知分类-haoguiting-2017年11月27日
     * @param id 通知类别id
     * @return NoticeCategoryEntity通知类别实体
     */
    NoticeCategoryEntity findById(String id);

    /**
     * 真分页-查询全部通知分类-haoguiting-2018-01-11 14:15:45
     * @param page 页码
     * @param pageSize 每页显示条数
     * @return 类别list
     */
    PageInfo<NoticeCategoryEntity> PageSelectNoticeCategoryInfo(int page, int pageSize);


    /**
     * 根据主键批量删除类别-haoguiting-2017年11月27日
     * @param id  通知分类ID
     * @param operator 操作人
     * @return 影响行数
     */
    int deleteNoticeCategorys(@Param("id") String id, @Param("operator") String operator);
}
