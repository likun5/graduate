package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.graduate.entity.ext.DictionaryTreeModel;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.DictionaryExample;
import com.github.pagehelper.PageInfo;
import java.util.List;


/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
public interface DictionaryService extends BaseService<DictionaryEntity, DictionaryExample>{
    /**
     * 根据字典id查询字典名词 - yyl-2018-01-15 01:48:00
     * @param id 字典id
     * @return String
     */
    String findDictionaryNameById(String id );
    /**
     * 根据字典名称查询字典id - 李爽-2017-11-29 19:25:09
     * @param dictionaryName 字典名称
     * @return String
     */
    String findDictionaryIdByName(String dictionaryName);

    /**
     * 根据字典名称模糊查询字典信息
     * @param strLike
     * @return
     */
    List<DictionaryEntity> fuzzyDictionaryInfoByName(String strLike,String dictionaryTypeName);
    /**
     * 单条添加字典表信息 - 李爽-2017-11-29 19:29:24
     * @param dictionaryEntity 字典实体
     * @return int
     */
    int addDictionary(DictionaryEntity dictionaryEntity);

    /**
     * 批量添加字典表信息 -李爽-2017-11-29 19:29:12
     * @param dictionaryEntityList 字典实体List
     * @return int
     */
    int addDictionary(List<DictionaryEntity> dictionaryEntityList);

    /**
     * 根据主键修改字段信息 -李爽-2017-11-29 19:34:32
     * @param dictionaryEntity 字典实体
     * @return boolean
     */
    boolean updateDictionary(DictionaryEntity dictionaryEntity);

    /**
     * 根据字典id删除字典信息 -李爽-2017-11-29 19:35:27
     * @param id id
     * @return boolean
     */
    boolean deleteDictionary(String id);

    /**
     * 批量删除字典信息，根据id -李爽-2017-11-29 19:36:34
     * @param ids idList
     * @return boolean
     */
    boolean deleteDictionary(List<String> ids);


    /**
     * 根据字典类型查询字典信息 -李爽-2017-12-6 11:35:03
     * @param typeCode 字典类型
     * @return 字典List
     */
    List<DictionaryEntity> selectDictionaryByTypeCode(String typeCode);

    /**
     * 查询所有的字典类型 -李爽-2017-12-6 11:45:10
     * @return 字典List
     */
    List<DictionaryEntity> selectAllDictionaryType();

    /**
     * 分页查询字典类型-hgt-2018年3月10日
     * @return 字典类型list
     */
    PageInfo<DictionaryEntity> PageSelectDictionaryType(int page,int pageSize);

    /**
     *查询字典类型以树形返回-杜娟-2018-2-24 14:04:33
     * @return
     */
    List<DictionaryTreeModel> selectAllDictionaryTypeByTree();

    /**
     * 根据期数模糊查询期数id 李爽-2017-12-13 09:21:05
     * @param grade 期数
     * @return 期数id
     */
    List<DictionaryEntity> selectIdsByNameGrade(String grade);

    /**
     * 根据typecode查询字典内容-杜娟-2018-2-24 18:14:45
     * @param typeCode
     * @return
     */
    List<DictionaryEntity> selectDitNameByTypeCode(String typeCode);
}
