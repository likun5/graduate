package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.DictionaryExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *@author sxm
 *create: 2017-11-16 14:31:07
 *DESCRIPTION:
 */
@Repository
public interface DictionaryDao extends BaseDao<DictionaryEntity,DictionaryExample> {

    /**
     * 查询所有类型名称及类型code -李爽-2017-12-6 14:24:07
     * @return
     */
    List<DictionaryEntity> selectAllDictionaryType();

    /**
     * 根据期数模糊查询期数id 李爽-2017-12-13 09:21:05
     * @param grade 期数
     * @return 期数id
     */
    List<DictionaryEntity> selectIdsByNameGrade(@Param("grade") String grade,@Param("typeCode")String typeCode);
}
