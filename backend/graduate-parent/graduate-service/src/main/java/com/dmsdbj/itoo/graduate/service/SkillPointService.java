package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.graduate.entity.ext.TechnologyModel;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.SkillPointEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.SkillPointExample;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;


/**
 * @author : 徐玲博
 * create :2017-11-16 14:31:07
 * DESCRIPTION 技术点
 */
public interface SkillPointService extends BaseService<SkillPointEntity, SkillPointExample>{
    /**
     * 删除技术点或技术方向-徐玲博-2017-11-20 15:49:59
     *
     * @param id 技术点
     * @param operator 操作人
     * @return 受影响行
     */
    int deleteSkillPoint(String id, String operator);
    /**
     * 查询父Id下的子信息-徐玲博-2017-11-20 15:49:14
     *
     * @param pId 技术点id
     * @return 子技术的实体
     */
    List<String> selectSkillByPId(String pId);
    /**
     * 查询技术点-或技术方向-徐玲博-2017-11-20 16:54:36
     *
     * @param id 用户id
     * @return 技术列表
     */
    List<SkillPointEntity> selectSkillPointById(String id);

    /**
     *查询技术方向-徐玲博-2017-11-21 14:42:51
     *
     * @param  pid 技术方向
     * @return 技术方向列表
     */
    List<SkillPointEntity> selectParentSkillByPid(String pid);

    /**
     * 论坛接口-查询所有的技术-并根据技术方向分类-徐玲博-2017-12-26 15:55:23
     *
     * @return 技术点list
     */
    List<Map<String,List<SkillPointEntity>>> selectSkillPoint();

    /**
     * 查询所有技术（包括技术方向）-徐玲博-2018-1-13 22:52:15
     *
     * @return 技术list
     */
    List<TechnologyModel> selectAllTechnology();

    /**
     * 分页查询所有技术-徐玲博-2018-1-13 23:36:48
     *
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 技术点list
     */
    PageInfo<TechnologyModel> pageAllTechnology(int page, int pageSize);

    /**
     * 查询所有的技术方向实体-徐玲博-2018-1-13 19:15:10
     *
     * @return 技术方向list
     */
    List<SkillPointEntity> selectTechnicalDirection();


    /**
     * 修改技术点或技术方向-徐玲博-2018-1-14 16:21:57
     *
     * @param skillPointEntity 技术实体
     * @return 受影响行
     */
    int updateSkillPoint(SkillPointEntity skillPointEntity);

    /**
     * 分页查询技术方向-haoguiting-2018年3月10日
     *
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 技术方向list
     */
    PageInfo<SkillPointEntity> PageSelectSkillDirection(int page,int pageSize);

    /**
     * 分页查询根据技术方向查询技术点-haoguiting-2018年2月22日
     *
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 技术点list
     */
    PageInfo<SkillPointEntity> PageSelectSkillByDirectionId(String pid,int page,int pageSize);

    /**
     * 模糊查询技术点-haoguiting-2018年2月27日
     *
     * @param strLike  输入的技术点
     * @param pid 技术方向
     * @return 技术点list
     */
    List<SkillPointEntity> fuzzySkillInfoByName(String strLike,String pid);

}
