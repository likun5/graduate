package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.entity.ext.RoleApplicationModel;
import com.dmsdbj.itoo.graduate.entity.ext.UserRoleModel;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.ApplicationExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.ApplicationDao;
import com.dmsdbj.itoo.graduate.service.PortalManagementService;
import com.dmsdbj.itoo.graduate.entity.ApplicationEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : xulingbo 
 * create :2018-01-04 15:54:54
 * DESCRIPTION :
 */
@Service("portalManagementService")
public class PortalManagementServiceImpl extends BaseServiceImpl<ApplicationEntity, ApplicationExample> implements PortalManagementService {

    private static String mapUserId = "userId";

    //注入applicationDao
	@Autowired
    private ApplicationDao applicationDao;

    /**
     * 让BaseServiceImpl获取到Dao
     * @return BaseDao<ApplicationEntity, ApplicationExample>
     */
    @Override
    public  BaseDao<ApplicationEntity, ApplicationExample> getRealDao(){
        return this.applicationDao;
    }

    /**
     * 用于顶栏显示
     * 根据userId查找服务-宋学孟-2018年1月18日
     *
     * @return  //List<RoleApplicationModel>
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<RoleApplicationModel> queryUserServiceApplication(String userId) {
        //拼装查询条件，根据查询
        List<RoleApplicationModel> roleApplicationModelList;
        Map<String, Object> map = new HashMap<>();
        map.put(mapUserId, userId);
        map.put("type", "服务");
        roleApplicationModelList= applicationDao.queryUserServiceApplication(map);
        return roleApplicationModelList;
    }

    /**
     *左侧栏显示-根据userId查找所有的子模块和页面-宋学孟-2018年1月18日
     * @param userId 用户名
     * @param applicationParentId 资源父id
     * @return // List<Map<String, Object>>
     */
    @Override
    public List<Map<String, Object>> queryServiceModuleApplication(String userId, String applicationParentId) {
        //拼装查询条件，根据查询
        Map<String, Object> mapSelect = new HashMap<>();
        mapSelect.put(mapUserId, userId);    //用户id
        mapSelect.put("modul", "模块");
        mapSelect.put("page", "页面");
        List<RoleApplicationModel> applicationList = applicationDao.queryServiceModuleApplication(mapSelect);
        Map<String, Object> map;
        List<Map<String, Object>> result = new ArrayList<>();
        for (RoleApplicationModel item : applicationList) {
           // System.out.println(item.getApplicationParentId());
            if (item.getApplicationParentId() == null || item.getApplicationParentId().equals(applicationParentId)) {//ParentId = 0 表示顶级【文档项】

                map = new HashMap<>();
                map.put("id", item.getId());
                map.put("applicationName", item.getApplicationName());
                map.put("applicationUrl", item.getApplicationUrl());
                map.put("type", item.getType());
                this.getSonTree(map, applicationList);
                result.add(map);
            }
        }
        return result;
    }

    /**
     * 递归方法，构建侧边栏树--王洪玉--2017年10月28日
     */
    private Map<String, Object> getSonTree(Map<String, Object> map, List<RoleApplicationModel> itemList) {
        List<Map<String, Object>> sonList = new ArrayList<>();
        Map<String, Object> sonMap;
        for (RoleApplicationModel item : itemList) {
            if (map.get("id").equals(item.getApplicationParentId())) {
                sonMap = new HashMap<>();
                sonMap.put("id", item.getId());
                sonMap.put("applicationName", item.getApplicationName());
                sonMap.put("applicationUrl", item.getApplicationUrl());
                sonMap.put("type", item.getType());
                sonList.add(sonMap);
                this.getSonTree(sonMap, itemList);
            }
        }
        if (!sonList.isEmpty()) {
            map.put("childs", sonList);
        }
        return map;
    }

    /**
     * 根据用户id和资源id查询按钮信息-宋学孟-2018年1月18日
     *
     * @param userId 用户id
     * @param applicationId 资源id
     * @return 按钮资源信息
     */
    @Override
    public List<RoleApplicationModel> queryModuleButtonApplication(String userId, String applicationId) {
        //拼装查询条件，根据查询
        List<RoleApplicationModel> roleApplicationModelList;
        Map<String, Object> map = new HashMap<>();
        String type = "按钮";
        map.put("applicationId", applicationId);//用户id
        map.put("type", type);
        map.put(mapUserId, userId);
        roleApplicationModelList= applicationDao.queryModuleButtonApplication(map);
        return roleApplicationModelList;
    }

    /**
     * 登录根据userId查询UserEntity-宋学孟-2018年1月18日
     * @param userCode
     * @return
     */
    @Override
    public UserRoleModel selectUserInfoByCode(String userCode,String userPwd) {

        return applicationDao.selectUserInfoByCode(userCode, userPwd);
    }

}
