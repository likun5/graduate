package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.dao.PictureDao;
import com.dmsdbj.itoo.graduate.entity.PictureEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.PictureCriteria;
import com.dmsdbj.itoo.graduate.mybatisexample.PictureExample;
import com.dmsdbj.itoo.graduate.service.PictureService;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.tool.fastdfs.FastDfsUtil;
import com.dmsdbj.itoo.tool.tenancy.TenancyContext;
import org.csource.common.MyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
@Service("pictureService")
public class PictureServiceImpl extends BaseServiceImpl<PictureEntity, PictureExample> implements PictureService {

    static final Logger logger = LoggerFactory.getLogger(PictureServiceImpl.class);
    @Value("${picServerUrl}")
    private String picServerUrl;
    //注入pictureDao
    @Autowired
    private PictureDao pictureDao;

    /**
     * 让BaseServiceImpl获取到Dao
     *
     * @return BaseDao<PictureEntity, PictureExample>
     */
    @Override
    public BaseDao<com.dmsdbj.itoo.graduate.entity.PictureEntity, PictureExample> getRealDao() {
        return this.pictureDao;
    }

    /**
     * 个人上传图片-徐玲博-2018-1-13 11:24:27
     *
     * @param fileUrl     图片的全路径 例如‘E:\\cat.jpg’
     * @param fileExtName 图片扩展名 例如‘jpg、png’等
     * @return 图片在fastDFS的路径
     * @Deprecated （暂时未使用）
     */
    @Deprecated
    @Override
    public String uploadPersonPicture(String fileUrl, String fileExtName) {
        //实例化工具类，实例化时传入配置文件
        FastDfsUtil client;
        //声明文件路径
        String strUrl = null;
        try {
            //实例化传入fast_client.conf文件
            client = new FastDfsUtil("classpath:spring/fast_client.conf");
            //上传图片，返回路径
//            strUrl = picServerUrl + client.uploadFile(fileUrl, fileExtName);
            //上传图片路径不应该
            strUrl =  client.uploadFile(fileUrl, fileExtName);
        } catch (MyException | IOException e) {
            logger.error("upload-上传图片-获取fastDfsClient出错");
            return null;
        }
        return strUrl;
    }

    /**
     * 保存上传图片的路径（一张）-宋学孟-2018年1月14日
     * @param pictureUrl 图片url
     * @param keyId 关联主键id
     * @return 影响行数
     */
    @Override
    public int addPicture(String pictureUrl, String keyId) {

        if(StringUtils.isEmpty(pictureUrl) || StringUtils.isEmpty(keyId)) {
            logger.error("保存上传图片，参数为空");
            return 0;
        }
        PictureEntity pictureEntity = new PictureEntity();
        pictureEntity.setKeyId(keyId);
        pictureEntity.setPictureUrl(pictureUrl);
        return this.insert(pictureEntity);
    }

    /**
     * 保存上传图片的路径（一张或多张）-宋学孟-2018年1月14日
     *
     * @param pictureEntityList 图片实体list
     * @return 影响行数
     * @Deprecated （暂时未使用）
     */
    @Deprecated
    @Override
    public int addPictureUrl(List<PictureEntity> pictureEntityList) {

        return this.insertAll(pictureEntityList);
    }

    /**
     * 根据相应的id，查询对应的实体（包含url）-宋学孟-2018年1月14日
     *
     * @param keyId 关联主键id
     * @return 图片url列表
     */
    @Override
    public List<String> selectPictureById(String keyId) {

        if (StringUtils.isEmpty(keyId)) {
            logger.error("根据相应的id查询对应的图片--参数为可空");
            return new ArrayList<>();
        }
        PictureExample example = new PictureExample();
        PictureCriteria criteria = example.createCriteria();
        criteria.andKeyIdEqualTo(keyId);
        criteria.andIsDeleteEqualTo((byte) 0);
        List<PictureEntity> pictureEntityList = this.selectByExample(example);
        if (CollectionUtils.isEmpty(pictureEntityList)) {
            logger.warn("根据相应的id查询对应的图片--未查到数据");
            return new ArrayList<>();
        }
        List<String> pictureUrls = new ArrayList<>();

        for (PictureEntity pictureEntity : pictureEntityList) {
            if (!StringUtils.isEmpty(pictureEntity.getPictureUrl())) {
                String pictureUrl = pictureEntity.getPictureUrl();
                pictureUrls.add(picServerUrl + pictureUrl);
            }
        }
        return pictureUrls;
    }

    /**
     * 根据与图片表有关表的id，删除图片-徐玲博-2018-2-9 16:16:23
     *
     * @param keyId    例如个人表的userid
     * @param operator 操作人
     * @return 受影行
     */
    @Deprecated
    @Override
    public int deletePictureById(String keyId, String operator) {
        String operator1 = TenancyContext.UserID.get();

        PictureExample pictureExample = new PictureExample();
        PictureCriteria pictureCriteria = pictureExample.createCriteria();
        pictureCriteria.andKeyIdEqualTo(keyId)
                .andIsDeleteEqualTo((byte) 0);

        return pictureDao.deleteByExample(pictureExample, operator1);
    }
}
