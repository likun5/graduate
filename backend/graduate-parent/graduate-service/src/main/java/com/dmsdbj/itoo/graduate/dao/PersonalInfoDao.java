package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.graduate.entity.ext.*;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.PersonalInfoExample;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author : xlb
 * create : 2017-11-19 19:43:44
 * DESCRIPTION : 毕业生信息Dao
 */
@Repository
public interface PersonalInfoDao extends BaseDao<PersonalInfoEntity, PersonalInfoExample> {
    /**
     * 根据登录ID查询用户信息-徐玲博-2017-11-19 11:32:48
     *
     * @param loginId 登录ID
     */
    PersonalInfoEntity selectPersonByLoginId(String loginId);

    /**
     * 通知-查询毕业生信息-徐玲博-2017-12-6 11:55:36
     *
     * @return 毕业生信息列表
     */
    List<PersonalInfoEntity> selectPersonInfo();

    /**
     * 组合查询+模糊查询+分页查询 毕业生信息查询-徐玲博-2018-1-2 11:13:16
     *
     * @param name             毕业生姓名
     * @param company_name     最新公司信息
     * @param grade            期数
     * @param certificate_name 证书名
     * @param salaryStart      薪资start
     * @param salaryEnd        薪资end
     * @return 毕业生信息list
     */
    List<PersonManageModel> selectPersonByCombination(@Param("company_name") String company_name,@Param("salaryStart") String salaryStart, @Param("salaryEnd") String salaryEnd,@Param("certificate_name") String certificate_name,@Param("name") String name,@Param("grade") String grade);


    /**
     * 分页查询-根据地域类型、地域、期数、姓名、性别，查询学员信息-李爽-2018-1-13 17:27:42
     * @return
     */
    List<PersonInfoModel> selectPersonInfoByRegionGradeNameSex(@Param("params")PersonInfoParamsModel personInfoParamsModel);

    /**
     * 查询所有期数及对应期数人员 郑晓东 2018年1月14日18点30分
     * @return
     */
    List<PeriodPersonModel> findPeriodPersonModels();

    List<PersonalInfoEntity> selectAllStudent();

    /**
     * 地域接口-组合查询人员地域信息-徐玲博-2018-1-31 16:52:28
     * @param name 个人名称
     * @param sex 性别
     * @param regionName 地域 省市县
     * @param grade 期数
     * @return 个人地域列表
     */
    List<PersonReginModel> selectPersonRegion(@Param("name") String name,@Param("sex") String sex,@Param("regionName") String regionName,@Param("grade") String grade,@Param("dictionaryAddressId") String dictionaryAddressId);
}
