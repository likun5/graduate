package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.*;
import com.dmsdbj.itoo.graduate.mybatisexample.PersonalInfoExample;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.github.pagehelper.PageInfo;

import java.util.List;


/**
 * @author : 徐玲博
 * create :2017-11-16 14:31:07
 * DESCRIPTION 个人信息
 */
public interface PersonalInfoService extends BaseService<PersonalInfoEntity, PersonalInfoExample> {
    /**
     * 根据id 查询个人信息-徐玲博-2018-1-21 12:15:07
     *
     * @param id 个人id
     * @return 个人信息
     */
    PersonalInfoEntity findByUserId(String id);

    /**
     * 根据id 查询个人信息-包括图像-徐玲博-2018-2-6 15:35:22
     * @param id 用户id
     * @return 用户信息
     */
    PersonInfoModel selectByPersonId(String id);

    /**
     * 根据登录ID查询用户信息-徐玲博-2017-11-19 11:32:48
     *
     * @param loginId 登录ID
     * @return 个人信息实体
     */
    PersonalInfoEntity selectPersonByLoginId(String loginId);

    /**
     * 毕业生-根据用户Id-删除个人信息-徐玲博-2017-11-19 11:43:50
     * 删除该用户，所有相关的信息，例如家庭、公司、薪资、学历等都将被删除
     *
     * @param personId 用户Id
     * @param operator 操作人
     * @return 删除成功与否
     */
    boolean deletePersonInfo(String personId, String operator);

    /**
     * 根据毕业生姓名查询个人信息-徐玲博-2017-11-30 17:14:23
     *
     * @param name 毕业生姓名
     * @return list列表
     */
    List<PersonalInfoEntity> selectPersonByName(String name);


    /**
     * 根据期数查询毕业生 -李爽-2017-12-7 17:36:38
     *
     * @param grade 期数
     * @return 毕业生list
     */
    List<PersonalInfoEntity> selectPersonByGrade(String grade);

    /**
     * 组合查询+模糊查询+分页查询 毕业生信息查询-徐玲博-2018-1-2 11:13:16
     *
     * @param name        毕业生姓名
     * @param company     最新公司信息
     * @param grade       期数
     * @param education   学历
     * @param salaryRange 薪资范围
     * @param page        页码
     * @param pageSize    页大小
     * @return 毕业生信息list
     */
    PageInfo<PersonManageModel> selectPersonByCombination(String name, String company, String grade, String education, String salaryRange, int page, int pageSize);

    /**
     * 模糊查询（姓名or性别or期数） - 李爽-2017-12-7 17:36:52
     *
     * @param queryParam 模糊查询条件
     * @return 毕业生list
     */
    List<PersonalInfoEntity> selectPersonByNameSexGrade(String queryParam);

    /**
     * 通知-查询毕业生信息-徐玲博-2017-12-6 11:55:36
     *
     * @return 毕业生信息列表
     */
    List<PersonGradeModel> selectPersonInfo();


    /**
     * 分页查询-根据地域类型、地域、期数、姓名、性别，查询学员信息-李爽-2018-1-13 17:27:42
     *
     * @return 学员信息
     */
    PageInfo<PersonInfoModel> selectPersonInfoByRegionGradeNameSex(PersonInfoParamsModel personInfoParamsModel, int pageNum, int pageSize);

    /**
     * 查询所有期数及对应期数人员 郑晓东 2018年1月14日18点30分
     *
     * @return
     */
    List<PeriodPersonModel> findPeriodPersonModels();

    /**
     * 数据中心-添加在校生或毕业生信息列表到到Personal表中--徐玲博-2018-1-15 15:02:09
     *
     * @param personalInfoEntityList 个人信息实体
     * @return 受影响行
     */
    int addListToPersonal(List<PersonGradeModel> personalInfoEntityList);

    /**
     * 批量添加人员信息-徐玲博-2018-1-25 15:13:49
     *
     * @param personalInfoEntityList 人员信息表
     * @return 受影响行
     */
    int insertPersonInfoList(List<PersonalInfoEntity> personalInfoEntityList);

    /**
     * 定时同步数据中心-徐玲博-2018-1-15 14:11:28
     *
     * @return 受影响行
     */
    int synchronizeDataWarehouse();

    /**
     * 从人员表中获得字段设置学生参数--徐玲博-2018-1-27 10:53:00
     * @param personalInfoEntityList  人员列表
     * @return 学生列表
     */
    List<StudentEntity> setStudentParam(List<PersonalInfoEntity> personalInfoEntityList);

    /**
     * 查询到的学生信息保存在人员表中-徐玲博-2018-1-27 11:13:14
     * @param studentEntityList 学生列表
     * @return 受影响行
     */
    int insertStudentToPerson(List<StudentEntity> studentEntityList);

    /**
     * 地域接口-组合查询人员地域信息-徐玲博-2018-1-31 16:52:28
     * @param name 个人名称
     * @param sex 性别
     * @param regionName 地域 省市县
     * @param grade 期数
     * @return 个人地域列表
     */
    List<PersonReginModel> selectPersonRegion( String name, String sex, String regionName,String grade,String dictionaryAddressId);

    /**
     * 毕业生-根据毕业生 用户Id-修改个人信息-徐玲博-2017-11-19 11:28:07
     *
     * @param personInfoModel 用户实体
     * @return 受影响行
     */
    int updatePersonInfo(PersonInfoModel personInfoModel);

//------------------------------注册------------------------------------

    /**
     * 用户添加（注册）-袁甜梦-2018年3月11日11:25:59
     * @param userModel
     * @return boolean
     */
    boolean insertUser(UserModel userModel);


    /**
     * 调用权限接口-田晓冰-2018年3月13日17:33:49
     * @param personalInfoEntity 个人信息实体
     */
    boolean addToAuth(PersonalInfoEntity personalInfoEntity);

    //region addPersonInfo-管理员端添加用户-田晓冰-2018年3月15日17:18:37
    /**
     * 管理员端添加用户
     * @param personalInfoEntity 用户信息
     * @return sql执行成功数量
     */
    int addPersonInfo(PersonalInfoEntity personalInfoEntity);
    //endregion

    /**
     * 修改密码-袁甜梦-2018年3月20日09:19:18
     * @param userId 用户id
     * @param userCode 用户code
     * @param password 密码
     * @return 是否修改成功
     */
    boolean updateUserPassword(String userId, String userCode, String password);

    /**
     * 个人重置密码-袁甜梦-2018年3月20日14:05:39
     * @param userCode 用户编码
     * @return 是否更新成功
     */
    boolean resetPassword(String userCode);
}
