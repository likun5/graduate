package com.dmsdbj.itoo.graduate.facade.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.*;
import com.dmsdbj.itoo.graduate.facade.PersonalInfoFacade;
import com.dmsdbj.itoo.graduate.service.PersonalInfoService;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import com.dmsdbj.itoo.tool.uuid.BaseUuidUtils;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : 徐玲博
 * create : 2017-11-21 21:44:16
 * DESCRIPTION 个人信息
 */
@Component("personalInfoFacade")
@Service
public class PersonalInfoFacadeImpl implements PersonalInfoFacade {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(PersonalInfoFacadeImpl.class);

    @Autowired
    private PersonalInfoService personalInfoService;

    /**
     * 根据id 查询个人信息-徐玲博-2017-11-21 21:45:18
     *
     * @param id 个人id
     * @return 个人信息
     */
    @Override
    public PersonalInfoEntity findByUserId(String id) {
        return personalInfoService.findByUserId(id);
    }

    /**
     * 根据id 查询个人信息-包括图像-徐玲博-2018-2-6 15:35:22
     * @param id 用户id
     * @return 用户信息
     */
    @Override
    public PersonInfoModel selectByPersonId(String id) {
        return personalInfoService.selectByPersonId(id);
    }

    /**
     * 毕业生-根据毕业生用户Id-添加个人信息-徐玲博-2017-11-19 11:28:07
     *
     * @param personalInfoEntity 个人信息实体
     * @return 受影响行
     */
    @Override
    public int addPersonInfo(PersonalInfoEntity personalInfoEntity) {
        int flag;

        try {
            personalInfoEntity.setId(BaseUuidUtils.base58Uuid());
            flag=personalInfoService.addPersonInfo(personalInfoEntity);

        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }

        return flag;
    }

    /**
     * 根据登录ID查询用户信息-徐玲博-2017-11-19 11:32:48
     *
     * @param loginId 登录ID
     * @return 个人信息实体
     */
    @Override
    public PersonalInfoEntity selectPersonByLoginId(String loginId) {
        PersonalInfoEntity personalInfoEntity;
        try {
            personalInfoEntity = personalInfoService.selectPersonByLoginId(loginId);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return personalInfoEntity;
    }

    /**
     * 毕业生-根据毕业生 用户Id-修改个人信息-徐玲博-2017-11-19 11:28:07
     *
     * @param personInfoModel 用户实体
     * @return 受影响行
     */
    @Override
    public int updatePersonInfo(PersonInfoModel personInfoModel) {
        int flag;
        try {
            flag = personalInfoService.updatePersonInfo(personInfoModel);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 毕业生-根据用户Id-删除个人信息-徐玲博-2017-11-19 11:43:50
     * 删除该用户，所有相关的信息，例如家庭、公司、薪资、学历等都将被删除
     *
     * @param personId 用户Id
     * @param operator 操作人
     * @param operator 操作人
     * @return 是否删除成功
     */
    @Override
    public boolean deletePersonInfo(String personId, String operator) {
        boolean flag;
        try {
            flag = personalInfoService.deletePersonInfo(personId, operator);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 根据毕业生姓名查询个人信息-徐玲博-2017-11-30 17:14:23
     *
     * @param name 毕业生姓名
     * @return list列表
     */
    @Override
    public List<PersonalInfoEntity> selectPersonByName(String name) {
        return personalInfoService.selectPersonByName(name);
    }

    /**
     * 通知-查询毕业生信息-徐玲博-2017-12-6 11:55:36
     *
     * @return 毕业生信息列表
     */
    @Override
    public List<PersonGradeModel> selectPersonInfo() {
        return personalInfoService.selectPersonInfo();
    }


    /**
     * 根据期数查询毕业生 -李爽-2017-12-7 21:50:28
     *
     * @param grade 期数
     * @return 毕业生list
     */
    @Override
    public List<PersonalInfoEntity> selectPersonByGrade(String grade) {
        return personalInfoService.selectPersonByGrade(grade);
    }

    /**
     * 模糊查询（姓名or性别or期数） - 李爽-2017-12-7 21:50:24
     *
     * @param queryParam 模糊查询条件
     * @return 毕业生list
     */
    @Override
    public List<PersonalInfoEntity> selectPersonByNameSexGrade(String queryParam) {
        return personalInfoService.selectPersonByNameSexGrade(queryParam);
    }

    /**
     * 组合查询+模糊查询+分页查询 毕业生信息查询-徐玲博-2018-1-2 11:13:16
     * Combination 组合
     *
     * @param name        毕业生姓名
     * @param company     最新公司信息
     * @param grade       期数
     * @param education   学历
     * @param salaryRange 薪资范围
     * @param page        页码
     * @param pageSize    页大小
     * @return 毕业生信息list
     */
    @Override
    public PageInfo<PersonManageModel> selectPersonByCombination(String name, String company, String grade, String education, String salaryRange, int page, int pageSize) {
        return personalInfoService.selectPersonByCombination(name, company, grade, education, salaryRange, page, pageSize);
    }

    /**
     * 分页查询-根据地域类型、地域、期数、姓名、性别，查询学员信息-李爽-2018-1-13 17:27:42
     *
     * @return 学员信息
     */
    @Override
    public PageInfo<PersonInfoModel> selectPersonInfoByRegionGradeNameSex(PersonInfoParamsModel personInfoParamsModel, int pageNum, int pageSize) {
        if (pageNum <= 0 || pageSize <= 0 || personInfoParamsModel == null) {
            return new PageInfo<>(null);
        }
        return personalInfoService.selectPersonInfoByRegionGradeNameSex(personInfoParamsModel, pageNum, pageSize);
    }

    /**
     * 查询所有期数及对应期数人员 郑晓东 2018年1月14日18点30分
     *
     * @return
     */
    @Override
    public List<PeriodPersonModel> findPeriodPersonModels() {
        List<PeriodPersonModel> list = new ArrayList<>();
        try {
            list = personalInfoService.findPeriodPersonModels();
        } catch (Exception e) {
            logger.error("查询所有期数及对应期数人员异常 --> PersonalInfoFacade.findPeriodPersonModels is error ");
        }
        return list;
    }

    /**
     * 数据中心-添加在校生或毕业生信息列表到到Personal表中-徐玲博-2018-1-15 15:02:09
     *
     * @param personalInfoEntityList 个人信息实体
     * @return 受影响行
     */
    @Override
    public int addListToPersonal(List<PersonGradeModel> personalInfoEntityList) {
        return personalInfoService.addListToPersonal(personalInfoEntityList);
    }

    /**
     * 批量添加人员信息-徐玲博-2018-1-25 15:13:49
     *
     * @param personalInfoEntityList 人员信息表
     * @return 受影响行
     */
    @Override
    public int insertPersonInfoList(List<PersonalInfoEntity> personalInfoEntityList) {
        return personalInfoService.insertPersonInfoList(personalInfoEntityList);
    }

    /**
     * 定时同步数据中心-徐玲博-2018-1-15 14:11:28
     *
     * @return 受影响行
     */
    @Override
    public int synchronizeDataWarehouse() {
        return personalInfoService.synchronizeDataWarehouse();
    }

    /**
     * 地域接口-组合查询人员地域信息-徐玲博-2018-1-31 16:52:28
     * @param name 个人名称
     * @param sex 性别
     * @param regionName 地域 省市县
     * @param grade 期数
     * @param dictionaryAddressId 地域类型
     * @return 个人地域列表
     */
    @Override
    public List<PersonReginModel> selectPersonRegion(String name, String sex, String regionName, String grade,String dictionaryAddressId) {
        return personalInfoService.selectPersonRegion(name, sex, regionName, grade, dictionaryAddressId);
    }

    //-------------------------------注册---------------------------------------------

    /**
     * 用户添加（注册）-袁甜梦-2018年3月11日11:25:20
     * @param userModel
     * @return boolean
     */
    @Override
    public boolean insertUser(UserModel userModel)
    {
        boolean flag=false;
        try{
            flag=personalInfoService.insertUser(userModel);
        }catch(Exception e){
            logger.error("PersonInfoFacadeImpl insertUser用户注册异常",e);
            throw  new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 修改密码-袁甜梦-2018年3月20日08:56:43
     * @param userId  用户id
     * @param userCode 用户code
     * @param password 密码
     * @return 是否修改成功
     */
    @Override
    public boolean updateUserPassword(String userId, String userCode, String password) {
        boolean flag=false;
        try{
            flag=personalInfoService.updateUserPassword(userId,userCode,password);
        }catch (Exception e){
            logger.error("PersonInfoFacadeImpl updateUserPassword修改密码异常！",e);
            throw  new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 个人重置密码-袁甜梦-2018年3月20日14:03:47
     * @param userCode 用户编码
     * @return 是否更新成功
     */
    @Override
    public boolean resetPassword(String userCode) {
        boolean flag=false;
        try{
            flag=personalInfoService.resetPassword(userCode);
        }catch (Exception e){
            logger.error("PersonInfoFacadeImpl resetPassword个人重置密码异常！",e);
            throw  new ItooRuntimeException(e);
        }
        return flag;
    }


}
