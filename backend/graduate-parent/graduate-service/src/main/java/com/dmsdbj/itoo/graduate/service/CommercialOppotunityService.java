package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.graduate.entity.ext.RegionCommercialModel;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.CommercialOppotunityEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.CommercialOppotunityExample;
import com.github.pagehelper.PageInfo;

import java.util.List;


/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
public interface CommercialOppotunityService extends BaseService<CommercialOppotunityEntity, CommercialOppotunityExample>{

    /**
     * 通过地域ID 查询地域商机--唐凌峰-2018-1-11 09:48:42
     * @param regionId 地域ID
     * @param pageNum  页码
     * @param pageSize 页量
     * @return   PageInfo<RegionCommercialModel>
     */
    PageInfo<RegionCommercialModel> queryRegionCommercialById(String regionId, int pageNum, int pageSize);

    /**
     * 查询提高班所有毕业生的商机信息-唐凌峰-2018-1-29 15:36:10
     * @return List<RegionCommercialModel>
     */
    List<RegionCommercialModel>queryRegionCommercialById();
}
