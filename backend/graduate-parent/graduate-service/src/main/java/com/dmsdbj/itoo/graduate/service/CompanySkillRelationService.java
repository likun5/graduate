package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.CompanySkillRelationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.CompanySkillRelationExample;

import java.util.List;


/**
 * @author : 徐玲博
 * create :2017-12-5 23:04:37
 * DESCRIPTION 公司技术关系表
 */
public interface CompanySkillRelationService extends BaseService<CompanySkillRelationEntity, CompanySkillRelationExample> {
    /**
     * 根据技术id查询所有使用的公司-徐玲博-2017-12-4 10:34:54
     *
     * @param skillId 技术id
     * @return 人员id -关系列表
     */
    List<CompanySkillRelationEntity> selectCompanyBySkill(String skillId);

    /**
     * 根据公司id查询，包括所有的技术-徐玲博-2017-12-4 10:37:25
     *
     * @param companyId 公司id
     * @return 技术id -关系列表
     */
    List<CompanySkillRelationEntity> selectSkillByCompany(String companyId);

    /**
     * 根据公司id删除关系表中技术-徐玲博-2017-12-5 22:10:55
     * @param companyId 公司id
     * @param operator 操作人
     * @return 受影响行
     */
    int deleteSkillByCompanyId(String companyId,String operator);

    /**
     *根据技术id删除关系表中公司信息-徐玲博-2017-12-5 23:20:31
     *
     * @param skillId 技术id
     * @param operator 操作人
     * @return 受影响行
     */
    int deleteCompanyBySkillId(String skillId,String operator);
}
