package com.dmsdbj.itoo.graduate.facade.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
import com.dmsdbj.itoo.graduate.entity.ext.DictionaryTreeModel;
import com.dmsdbj.itoo.graduate.facade.DictionaryFacade;
import com.dmsdbj.itoo.graduate.service.DictionaryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import com.github.pagehelper.PageInfo;
import java.util.List;

/**
 * @author sxm
 * create: 2017-11-16 14:31:07
 * DESCRIPTION
 */
@Component("dictionaryFacade")
@Service
public class DictionaryFacadeImpl implements DictionaryFacade {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(DictionaryFacadeImpl.class);

    @Autowired
    DictionaryService dictionaryService;

    /**
     * 根据id查询Dictionary
     *
     * @param id
     * @return DictionaryEntity
     */
    @Override
    public DictionaryEntity findById(String id) {
        return dictionaryService.findById(id);
    }

    /**
     * 根据字典id查询字典名词 - yyl-2018-01-15 01:48:00
     *
     * @param id 字典id
     * @return String
     */
    @Override
    public String findDictionaryNameById(String id) {
        return dictionaryService.findDictionaryNameById(id);
    }

    /**
     * 根据字典名称查询字典id - 李爽-2017-11-29 19:25:09
     *
     * @param dictionaryName 字典名称
     * @return String
     */
    @Override
    public String findDictionaryIdByName(String dictionaryName) {
        return dictionaryService.findDictionaryIdByName(dictionaryName);
    }

    /**
     * 通过名称模糊查询-杜娟-2018-2-24 15:27:00
     * @param strLike
     * @param dictionaryTypeName
     * @return
     */
    @Override
    public List<DictionaryEntity> fuzzyDictionaryInfoByName(String strLike, String dictionaryTypeName) {
        return dictionaryService.fuzzyDictionaryInfoByName(strLike,dictionaryTypeName);
    }

    /**
     * 添加字典信息 -李爽-2017-11-29 18:53:59
     *
     * @param dictionaryEntity
     * @return
     */
    @Override
    public int addDictionary(DictionaryEntity dictionaryEntity) {
        try {
            return dictionaryService.addDictionary(dictionaryEntity);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ItooRuntimeException("dictionaryFacade----addDictionary--添加字典", e);
        }
    }

    /**
     * 批量添加字典信息 -李爽-2017-12-2 19:18:51
     *
     * @param list
     * @return
     */
    @Override
    public int addDictionary(List<DictionaryEntity> list) {
        try {
            return dictionaryService.addDictionary(list);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ItooRuntimeException("dictionaryFacade----addDictionary--批量添加字典", e);
        }
    }

    /**
     * 修改字典信息 -李爽-2017-11-29 18:54:39
     *
     * @param dictionaryEntity
     * @return
     */
    @Override
    public boolean updateDictionary(DictionaryEntity dictionaryEntity) {
        try {
            return dictionaryService.updateDictionary(dictionaryEntity);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ItooRuntimeException("dictionaryFacade----updateDictionary--根据主键修改字典信息", e);
        }
    }

    /**
     * 根据字典id删除字典信息 -李爽-2017-11-29 18:55:40
     *
     * @param id
     * @return
     */
    @Override
    public boolean deleteDictionary(String id) {
        try {
            return dictionaryService.deleteDictionary(id);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ItooRuntimeException("dictionaryFacade----deleteDictionary--根据字典id删除字典信息", e);
        }
    }

    /**
     * 批量删除字典信息 -李爽-2017-11-29 18:56:32
     *
     * @param ids
     * @return
     */
    @Override
    public boolean deleteDictionary(List<String> ids) {
        try {
            return dictionaryService.deleteDictionary(ids);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ItooRuntimeException("dictionaryFacade----deleteDictionary--批量删除字典信息", e);
        }
    }

    /**
     * 根据字典类型查询字典信息 -李爽-2017-12-6 14:27:55
     *
     * @param typeCode
     * @return
     */
    @Override
    public List<DictionaryEntity> selectDictionaryByTypeCode(String typeCode) {
        try {
            return dictionaryService.selectDictionaryByTypeCode(typeCode);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ItooRuntimeException("dictionaryFacade----selectDictionaryByTypeCode--根据字典类型查询字典信息", e);
        }
    }

    /**
     * 查询所有的字典类型 -李爽-2017-12-6 14:27:51
     *
     * @return
     */
    @Override
    public List<DictionaryEntity> selectAllDictionaryType() {
        try {
            return dictionaryService.selectAllDictionaryType();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ItooRuntimeException("dictionaryFacade----selectAllDictionaryType--查询所有的字典类型", e);
        }
    }

    /**
     * 分页查询字典类型-hgt-2018年3月10日
     * @return 字典类型list
     */
    @Override
    public PageInfo<DictionaryEntity> PageSelectDictionaryType(int page,int pageSize){
        try{
            return dictionaryService.PageSelectDictionaryType(page,pageSize);
        }catch(Exception e){
            logger.error(e.getMessage(),e);
            throw new ItooRuntimeException("dictionaryFacade----selectAllDictionaryType--查询所有的字典类型", e);
        }
    }

    /**
     *查询字典类型以树形返回-杜娟-2018-2-24 14:02:40
     * @return
     */
    @Override
    public List<DictionaryTreeModel> selectAllDictionaryTypeByTree() {
        try {
            return dictionaryService.selectAllDictionaryTypeByTree();
        } catch (Exception e) {
            throw new ItooRuntimeException("dictionaryFacade----selecAllDictionaryByTree--查询所有的字典类型", e);
        }
    }

    @Override
    public List<DictionaryEntity> selectDitNameByTypeCode(String typeCode) {
        try{
            return dictionaryService.selectDictionaryByTypeCode(typeCode);
        } catch (Exception e){
            throw new ItooRuntimeException("dictionaryFacade----selectDictionaryByTypeCode--查询所有的字典类型", e);
        }

    }
}
