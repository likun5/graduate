package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.LandScapeEntity;
import com.dmsdbj.itoo.graduate.facade.LandScapeFacade;
import com.dmsdbj.itoo.graduate.service.LandScapeService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;


/**
 * @authorsxm
 * create: 2017-11-16 14:31:07
 * DESCRIPTION
 */
@Component("landScapeFacade")
@Service
public class LandScapeFacadeImpl implements LandScapeFacade {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(LandScapeFacadeImpl.class);

    @Autowired
    LandScapeService landScapeService;

    /**
     * 根据风景id查询风景-徐玲博-2018-2-1 15:00:12
     *
     * @param id 风景id
     * @return LandScapeEntity 风景实体
     */
    @Override
    public LandScapeEntity findById(String id) {
        return landScapeService.findById(id);
    }

    /**
     * 查询风景-徐玲博-2018-2-1 15:01:00
     *
     * @return 风景列表
     */
    @Override
    public List<LandScapeEntity> queryLandScape(String id) {

        List<LandScapeEntity> landScapeEntity;
        try {
            landScapeEntity = landScapeService.queryLandScape(id);
        } catch (Exception e) {
            logger.error("PlaceManageFacadeImpl.updateRoomInfo:修改特产信息失败！", e);
            throw new ItooRuntimeException(e);
        }
        return landScapeEntity;
    }

    /**
     * 分页查询-查询风景-徐玲博-2018-2-7 19:26:59
     *
     * @param page     第几页
     * @param pageSize 每页几条
     * @return 风景列表
     */
    @Override
    public PageInfo<LandScapeEntity> selectLandScape(String id, int page, int pageSize) {
        return landScapeService.selectLandScape(id, page, pageSize);
    }

    /**
     * 添加景区-徐玲博-2018-2-1 15:04:40
     *
     * @param landScapeEntity 风景实体
     * @return 受影响行
     */
    @Override
    public int addLandScape(LandScapeEntity landScapeEntity) {

        int flag;
        try {
            flag = landScapeService.addLandScape(landScapeEntity);
        } catch (Exception e) {
            logger.error("LandScapeFacadeImpl.addLandScape:添加景区信息失败！", e);
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 更新风景-徐玲博-2018-2-1 15:06:43
     *
     * @param landScapeEntity 风景实体
     * @return 受影响行
     */
    @Override
    public int updateLandSpace(LandScapeEntity landScapeEntity) {
        int flag;
        try {
            flag = landScapeService.updateLandSpace(landScapeEntity);
        } catch (Exception e) {
            logger.error("LandScapeFacadeImpl.updateLandSpace:修改景区信息失败！", e);
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 删除风景-徐玲博-2018-2-1 14:58:45
     *
     * @param landScapeEntityList 景区实体
     * @return 受影响行
     */
    @Override
    public int deleteLandSpace(List<LandScapeEntity> landScapeEntityList) {

        int flag;
        try {
            flag = landScapeService.deleteLandSpace(landScapeEntityList);
        } catch (Exception e) {
            logger.error("PlaceManageFacadeImpl.deleteRoomInfo:删除景区失败！", e);
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 根据ids删除风景-徐玲博-2018-2-8 11:05:52
     *
     * @param ids
     * @param operator
     * @return 受影响行
     */
    @Override
    public int deleteLand(List<String> ids, String operator) {
        return landScapeService.deleteLand(ids, operator);
    }
}
