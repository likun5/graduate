package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.ForumCommentEntity;
import com.dmsdbj.itoo.graduate.facade.ForumCommentFacade;
import com.dmsdbj.itoo.graduate.service.ForumCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *@authorls
 *create: 2017-12-27 09:54:26
 * DESCRIPTION
 */
@Component("forumCommentFacade")
@Service
public class ForumCommentFacadeImpl implements ForumCommentFacade {

    //打印日志相关
	private static final Logger logger = LoggerFactory.getLogger(ForumCommentFacadeImpl.class);

    @Autowired
    ForumCommentService forumCommentService;

    /**
     * 根据id查询ForumComment
     * @param id
     * @return ForumCommentEntity
     */
    @Override
    public ForumCommentEntity findById(String id) {
        return forumCommentService.findById(id);
    }
    
}
