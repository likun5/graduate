package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.AdministrativeRegionEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.AdministrativeRegionExample;

import java.util.List;


/**
 * @author : 孔唯妍
 * @Date : 2017-12-05 09:39:56
 * DESCRIPTION
 */
public interface AdministrativeRegionService extends BaseService<AdministrativeRegionEntity, AdministrativeRegionExample>{

    /**
     * 查询所有的省份 -李爽-2017-12-5 15:00:26
     * @return
     */
    List<AdministrativeRegionEntity> selectProvince();

    /**
     * 根据id查询子行政区，比如根据某一省/市 id，查询其下是市/区县 -李爽-2017-12-5 15:02:36
     * @param id
     * @return
     */
    List<AdministrativeRegionEntity> selectSubRegionById(String id);


}
