package com.dmsdbj.itoo.graduate.facade.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.dmsdbj.itoo.graduate.entity.PersonCompanyRelationEntity;
import com.dmsdbj.itoo.graduate.facade.PersonCompanyRelationFacade;
import com.dmsdbj.itoo.graduate.service.PersonCompanyRelationService;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author徐玲博
 * create: 2017-11-26 16:14:54
 * DESCRIPTION
 */
@Component("personCompanyRelationFacade")
@Service
public class PersonCompanyRelationFacadeImpl implements PersonCompanyRelationFacade {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(PersonCompanyRelationFacadeImpl.class);

    @Resource
    private PersonCompanyRelationService personCompanyRelationService;

    /**
     * 根据id查询PersonCompanyRelation
     *
     * @param id
     * @return PersonCompanyRelationEntity
     */
    @Override
    public PersonCompanyRelationEntity findById(String id) {
        return personCompanyRelationService.findById(id);
    }

    /**
     * 根据个人id查询个人公司关系id的List -lishuang-2017-12-16 17:10:53
     *
     * @param personId 用户id
     * @return 关系id列表
     */
    @Override
    public List<String> selectPersonCompanyRelationIdsByPersonId(String personId) {

        List<String> stringList;
        try {
            stringList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(personId);
        } catch (Exception e) {
            logger.error("根据个人id查询个人公司关系id的List,发生异常",e);
            throw new ItooRuntimeException(e);
        }
        return stringList;
    }

    /**
     * 根据期数查询本期所有人的与公司的关系id的List -lishaugn-2017-12-16 17:10:40
     *
     * @param grade 期数
     * @return 关系id列表
     */
    @Override
    public List<String> selectPersonCompanyRelationIdsByGrade(String grade) {
        try {
            return personCompanyRelationService.selectPersonCompanyRelationIdsByGrade(grade);
        } catch (Exception e) {
            logger.error("根据期数查询本期所有人的与公司的关系id的List,发生异常",e);
            throw new ItooRuntimeException(e);
        }
    }
}
