package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.PictureEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.PictureExample;
import org.springframework.stereotype.Repository;

/**
 *@author sxm
 *create: 2017-11-16 14:31:07
 *DESCRIPTION:
 */
@Repository
public interface PictureDao extends BaseDao<PictureEntity,PictureExample> {
}
