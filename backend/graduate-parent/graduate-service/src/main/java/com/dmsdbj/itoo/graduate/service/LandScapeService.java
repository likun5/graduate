package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.LandScapeEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.LandScapeExample;
import com.github.pagehelper.PageInfo;

import java.util.List;


/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
public interface LandScapeService extends BaseService<LandScapeEntity, LandScapeExample> {

    /**
     * 查询风景-徐玲博-2018-2-1 15:01:00
     *
     * @return 风景列表
     */
    List<LandScapeEntity> queryLandScape(String id);

    /**
     * 分页查询-查询风景-徐玲博-2018-2-7 19:26:59
     *
     * @param page     第几页
     * @param pageSize 每页几条
     * @return 风景列表
     */
    PageInfo<LandScapeEntity> selectLandScape(String id,int page, int pageSize);

    /**
     * 添加景区-徐玲博-2018-2-1 15:04:40
     *
     * @param landScapeEntity 风景实体
     * @return 受影响行
     */
    int addLandScape(LandScapeEntity landScapeEntity);

    /**
     * 更新风景-徐玲博-2018-2-1 15:06:43
     *
     * @param landScapeEntity 风景实体
     * @return 受影响行
     */
    int updateLandSpace(LandScapeEntity landScapeEntity);

    /**
     * 删除风景-徐玲博-2018-2-1 14:58:45
     *
     * @param landScapeEntityList 景区实体
     * @return 受影响行
     */
    int deleteLandSpace(List<LandScapeEntity> landScapeEntityList);

    /**
     * 根据ids删除风景-徐玲博-2018-2-8 11:05:52
     * @param ids
     * @param operator
     * @return 受影响行
     */
    int deleteLand(List<String> ids,String operator);
}
