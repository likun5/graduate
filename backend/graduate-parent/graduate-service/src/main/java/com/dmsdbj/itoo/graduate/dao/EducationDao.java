package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.graduate.entity.ext.PersonalEducationEntity;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.EducationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.EducationExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *@author sxm
 *create: 2017-11-16 14:31:07
 *DESCRIPTION:
 */
@Repository
public interface EducationDao extends BaseDao<EducationEntity,EducationExample> {

    /**
     * 根据个人信息查询学历相关信息
     * @param userId 学员Id
     * @return 公司实体list
     */
    int deleteEducationByUserId(@Param("userId") String userId,@Param("operator") String operator);

    List<EducationEntity> findByCertifycateType( @Param("certificateType") String certificateType);

    /**
     * 查询提高班所有毕业生的学历信息！-唐凌峰-2018年2月7日16:39:22
     * @return List<PersonalEducationEntity>
     */
    List<PersonalEducationEntity>queryPersonalEducation();

    /**
     * 通过个人ID 查询提高班毕业生的学历信息！-唐凌峰-2018-2-8 14:08:19
     * @param persionId
     * @return List<PersonalEducationEntity>
     */
    List<PersonalEducationEntity>findEducationByPersonId(@Param("persionialId") String persionId);

}
