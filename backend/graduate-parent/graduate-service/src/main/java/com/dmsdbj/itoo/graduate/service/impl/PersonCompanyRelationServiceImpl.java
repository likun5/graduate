package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.dao.PersonCompanyRelationDao;
import com.dmsdbj.itoo.graduate.entity.PersonCompanyRelationEntity;
import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.PersonCompanyRelationExample;
import com.dmsdbj.itoo.graduate.service.PersonCompanyRelationService;
import com.dmsdbj.itoo.graduate.service.PersonalInfoService;
import com.dmsdbj.itoo.graduate.tool.ResultEmptyUtil;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 徐玲博
 * create:2017-11-26 16:14:54
 * DESCRIPTION
 */
@Service("personCompanyRelationService")
public class PersonCompanyRelationServiceImpl extends BaseServiceImpl<PersonCompanyRelationEntity, PersonCompanyRelationExample> implements PersonCompanyRelationService {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(PersonCompanyRelationServiceImpl.class);
    //注入personCompanyRelationDao
	@Resource
    private PersonCompanyRelationDao personCompanyRelationDao;

	//注入personalInfoService
	@Resource
	private PersonalInfoService personalInfoService;


    /**
     * 让BaseServiceImpl获取到Dao
     * @return BaseDao<PersonCompanyRelationEntity,PersonCompanyRelationExample>
     */
    @Override
    public  BaseDao<PersonCompanyRelationEntity, PersonCompanyRelationExample> getRealDao(){
        return this.personCompanyRelationDao;
    }

    /**
     * 根据个人id查询个人公司关系id的List -lishuang-2017-12-16 17:10:53
     * @param personId 用户id
     * @return 关系id列表
     */
    @Override
    public List<String> selectPersonCompanyRelationIdsByPersonId(String personId) {

        if(StringUtils.isEmpty(personId)){
            logger.error("根据个人id查询个人公司关系id的List,参数personId为空");
            return null;
        }
        List<String> resultList = personCompanyRelationDao.selectPersonCompanyRelationIdsByPersonId(personId);
        return ResultEmptyUtil.getReturnValue(resultList);
    }

    /**
     * 根据期数查询本期所有人的与公司的关系id的List -lishaugn-2017-12-16 17:10:40
     * @param grade 期数
     * @return 人与公司的关系id集合
     */
    @Override
    public List<String> selectPersonCompanyRelationIdsByGrade(String grade) {


        //根据年级，查询所有人
        List<PersonalInfoEntity> personalInfoEntityList = personalInfoService.selectPersonByGrade(grade);
        List<String> personalIdList = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(personalInfoEntityList)){
            for(PersonalInfoEntity  personalInfoEntity : personalInfoEntityList){
                personalIdList.add(personalInfoEntity.getId());
            }
        }
        //根据多个人的id查询人与公司的关系id集合

        return selectPersonCompanyRelationIdsByPersonIds(personalIdList);
    }

    /**
     * 根据个人id的list查询个人公司关系id的List -lishuang-2017-12-16 17:20:22
     * @param personIdlist  个人id的集合
     * @return 个人公司关系id的集合
     */
    @Override
    public List<String> selectPersonCompanyRelationIdsByPersonIds(List<String> personIdlist) {

        if(CollectionUtils.isNotEmpty(personIdlist)){
            return personCompanyRelationDao.selectPersonCompanyRelationIdsByPersonIds(personIdlist);
        }
        logger.error("根据个人id的list查询个人公司关系id的List,参数personIdlist为空");
        return null;
    }


    /**
     * 根据人员id删除人员公司关系表记录 -lishuang-2017-12-23 08:38:05
     * @param personId 人员id
     * @param operator 操作员
     * @return 是否成功
     */
    @Override
    public boolean deletePersonCompanyRelationByPersonId(String personId,String operator) {

        if(StringUtils.isAnyEmpty(personId,operator)){
            logger.error("根据人员id删除人员公司关系表记录,参数为空");
            return false;
        }
        personCompanyRelationDao.selectPersonCompanyRelationIdsByPersonId(personId);
        PersonCompanyRelationExample example = new PersonCompanyRelationExample();
        example.createCriteria()
                .andUserIdEqualTo(personId);
        int resultNum = personCompanyRelationDao.deleteByExample(example,operator);
        return resultNum > 0;
    }
}
