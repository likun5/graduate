package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.graduate.entity.ext.RegionCommercialModel;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.CommercialOppotunityEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.CommercialOppotunityExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *@author sxm
 *create: 2017-11-16 14:31:07
 *DESCRIPTION: 商机模块
 */
@Repository
public interface CommercialOppotunityDao extends BaseDao<CommercialOppotunityEntity,CommercialOppotunityExample> {
    List<RegionCommercialModel> queryRegionCommercialById(@Param("regionId") String regionId);


    /**
     * 方法重载 查询提高班所有毕业生的商机信息！ 唐凌峰-2018年1月29日14:45:00
     * @return
     */
    List<RegionCommercialModel>queryRegionCommercialById();

}
