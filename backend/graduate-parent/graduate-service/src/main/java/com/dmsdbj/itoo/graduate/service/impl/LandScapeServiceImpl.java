package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.uuid.BaseUuidUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.LandScapeExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.LandScapeDao;
import com.dmsdbj.itoo.graduate.service.LandScapeService;
import com.dmsdbj.itoo.graduate.entity.LandScapeEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.LandScapeCriteria;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
@Service("landScapeService")
public class LandScapeServiceImpl extends BaseServiceImpl<LandScapeEntity, LandScapeExample> implements LandScapeService {


    //注入landScapeDao
    @Autowired
    private LandScapeDao landScapeDao;

    /**
     * 让BaseServiceImpl获取到Dao
     * @return BaseDao<LandScapeEntity, LandScapeExample>
     */
    @Override
    public  BaseDao<LandScapeEntity, LandScapeExample> getRealDao(){
        return this.landScapeDao;
    }

    /**
     *查询风景-徐玲博-2018-2-1 15:01:00
     *@return 风景列表
     */
    @Override
    public List<LandScapeEntity> queryLandScape(String id) {

        LandScapeExample  landScapeExample = new LandScapeExample();
        LandScapeCriteria criteria = landScapeExample.createCriteria();
        criteria.andIsDeleteEqualTo((byte) 0);
        criteria.andUserIdEqualTo(id);
        landScapeExample.setOrderByClause("create_time desc");
        return landScapeDao.selectByExample(landScapeExample);
    }

    /**
     * 分页查询-查询风景-徐玲博-2018-2-7 19:26:59
     *
     * @param page     第几页
     * @param pageSize 每页几条
     * @return 风景列表
     */
    @Override
    public PageInfo<LandScapeEntity> selectLandScape(String id,int page, int pageSize) {
        PageHelper.startPage(page, pageSize);
        return new PageInfo<>(queryLandScape(id));
    }

    /**
     *添加景区-徐玲博-2018-2-1 15:04:40
     *@param landScapeEntity 风景实体
     *@return 受影响行
     */
    @Override
    public int addLandScape(LandScapeEntity landScapeEntity){
        landScapeEntity.setId(BaseUuidUtils.base58Uuid());
        //TODO 风景保存地域
//        landScapeEntity.setRegionId();//地域id
//        landScapeEntity.setAddress();//地域地址
        return landScapeDao.insert(landScapeEntity);
    }

    /**
     *更新风景-徐玲博-2018-2-1 15:06:43
     *@param landScapeEntity 风景实体
     *@return 受影响行
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateLandSpace(LandScapeEntity landScapeEntity){

//        if (landScapeEntity == null) {
//            //logger.info("传入的景区实体为Null");
//            return -10;//参数为空
//        }
//        if (landScapeEntity.getId() == null || "".equals(landScapeEntity.getId())) {
//            //logger.info("传入的景区实体中的主键为null");
//            return -10;//必要参数为空
//        }

        return this.updateById( landScapeEntity);
    }

    /**
     *删除风景-徐玲博-2018-2-1 14:58:45
     *@param landScapeEntityList 景区实体
     *@return 受影响行
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteLandSpace(List<LandScapeEntity> landScapeEntityList){

        return 0;

    }

    /**
     * 根据ids删除风景-徐玲博-2018-2-8 11:05:52
     * @param ids
     * @param operator
     * @return 受影响行
     */
    @Override
    public int deleteLand(List<String> ids, String operator) {
        return landScapeDao.deleteByIds(ids, operator);
    }
}
