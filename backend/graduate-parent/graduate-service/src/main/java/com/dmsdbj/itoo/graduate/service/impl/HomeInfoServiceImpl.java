package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.entity.ext.HomeInfoModel;
import com.dmsdbj.itoo.graduate.mybatisexample.HomeInfoCriteria;
import com.dmsdbj.itoo.graduate.service.DictionaryService;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.HomeInfoExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.HomeInfoDao;
import com.dmsdbj.itoo.graduate.service.HomeInfoService;
import com.dmsdbj.itoo.graduate.entity.HomeInfoEntity;
import org.springframework.transaction.annotation.Transactional;


import java.util.ArrayList;
import java.util.List;

/**
 * @author : 徐玲博
 * create : 2017-11-16 14:31:07
 * DESCRIPTION 家庭信息ServiceImpl
 */
@Service("homeInfoService")
public class HomeInfoServiceImpl extends BaseServiceImpl<HomeInfoEntity, HomeInfoExample> implements HomeInfoService {


    //注入homeInfoDao
    @Autowired
    private HomeInfoDao homeInfoDao;

    @Autowired
    private DictionaryService dictionaryService;
    //打印日志

    /**
     * 让BaseServiceImpl获取到Dao
     *
     * @return BaseDao<HomeInfoEntity,HomeInfoExample>
     */
    @Override
    public BaseDao<HomeInfoEntity, HomeInfoExample> getRealDao() {
        return this.homeInfoDao;
    }

    /**
     * 【毕业生接口】根据用户id删除家庭成员信息-徐玲博-2017-12-23 17:03:06
     *
     * @param userId 用户id
     * @param operator 操作人
     * @return 受影响行数
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteHomeInfo(String userId, String operator) {
        HomeInfoExample homeInfoExample=new HomeInfoExample();
        HomeInfoCriteria homeInfoCriteria=homeInfoExample.createCriteria();
        homeInfoCriteria.andUserIdEqualTo(userId);

        return this.deleteByExample(homeInfoExample,operator);
    }

    /**
     * 查询家庭信息列表-徐玲博-2017-11-20 11:04:24
     *
     * @param userId 当前登录用户ID
     * @return list列表
     */
    @Override
    public PageInfo<HomeInfoModel> selectHomePersonInfo(String userId, int page, int pageSize) {
        PageInfo<HomeInfoEntity> pageInfo;
        PageInfo<HomeInfoModel> p;

        PageHelper.startPage(page, pageSize);
        HomeInfoExample homeInfoExample = new HomeInfoExample();
        HomeInfoCriteria homeInfoCriteria = homeInfoExample.createCriteria();
        homeInfoCriteria.andUserIdEqualTo(userId)
                        .andIsDeleteEqualTo((byte)0);
        homeInfoExample.setOrderByClause("create_time DESC");//以添加时间降序排序
        List<HomeInfoEntity> homeInfoEntityList ;
        List<HomeInfoModel> homeInfoModelList=new ArrayList<>();
        try {
            homeInfoEntityList = this.selectByExample(homeInfoExample);
            pageInfo=new PageInfo<>(homeInfoEntityList);

            //将家庭信息关系id循环成关系名称
            for(HomeInfoEntity homeInfoEntity:pageInfo.getList()){
                //根据关系id查询关系名称
                String relationName=dictionaryService.findDictionaryNameById(homeInfoEntity.getRelationshipId());
                HomeInfoModel homeInfoModel=new HomeInfoModel();
                homeInfoModel.setRelationName(relationName);
                BeanUtils.copyProperties(homeInfoEntity,homeInfoModel);
                homeInfoModelList.add(homeInfoModel);
            }
            p=new PageInfo<>(homeInfoModelList);
            p.setTotal(pageInfo.getTotal());
            p.setPages(pageInfo.getPages());
            p.setStartRow(pageInfo.getStartRow());
            p.setEndRow(pageInfo.getEndRow());


        } catch (Exception e) {
            throw new ItooRuntimeException("家庭成员信息为空");
        }
        return p;
    }
}
