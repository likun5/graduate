package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.dao.SalaryDao;
import com.dmsdbj.itoo.graduate.entity.SalaryEntity;
import com.dmsdbj.itoo.graduate.entity.ext.PeriodSalaryModel;
import com.dmsdbj.itoo.graduate.mybatisexample.SalaryExample;
import com.dmsdbj.itoo.graduate.service.PersonCompanyRelationService;
import com.dmsdbj.itoo.graduate.service.PersonalInfoService;
import com.dmsdbj.itoo.graduate.service.SalaryEduCertService;
import com.dmsdbj.itoo.graduate.service.SalaryService;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 薪资学历统计实现类
 * 郑晓东
 * 2018年1月17日14点27分
 */
@Service("salaryEduCertService")
public class SalaryEduCertServiceImpl extends BaseServiceImpl<SalaryEntity, SalaryExample> implements SalaryEduCertService {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(SalaryEduCertServiceImpl.class);

    @Autowired
    private SalaryDao salaryDao;
    @Autowired
    private PersonalInfoService personalInfoService;
    @Autowired
    private PersonCompanyRelationService personCompanyRelationService;
    @Autowired
    private SalaryService salaryService;

    @Override
    public BaseDao<SalaryEntity, SalaryExample> getRealDao() {
        return this.salaryDao;
    }

    /**
     * 查询每期每年平均、最高、最低工资
     *
     * @param periodId
     * @return
     */
    @Override
    public List<PeriodSalaryModel> selectAMMSalaryByPeriodId(String periodId) {
        if (periodId.isEmpty()) {
            logger.info("查询每期每年平均、最高、最低工资参数为空 "+new Date());
            return null;
        }
        return salaryDao.selectAMMSalaryByPeriodId(periodId);
    }

    /**
     * 查询今年每期平均、最高、最低薪资统计 郑晓东 2018年1月20日16点38分
     * @return 薪资统计集合
     */
    @Override
    public List<PeriodSalaryModel> selectAMMSalaryInThisYear() {
        return salaryDao.selectAMMSalaryInThisYear();
    }

    /**
     * 查询每期毕业时平均、最高、最低薪资统计  郑晓东 2018年1月20日
     * @return 薪资统计集合
     */
    @Override
    public List<PeriodSalaryModel> selectAMMSalaryInGradeYear() {
        return salaryDao.selectAMMSalaryInGradeYear();
    }
}
