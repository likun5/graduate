package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.ApplicationEntity;
import com.dmsdbj.itoo.graduate.entity.ext.RoleApplicationModel;
import com.dmsdbj.itoo.graduate.entity.ext.UserRoleModel;
import com.dmsdbj.itoo.graduate.facade.PortalManagementFacade;
import com.dmsdbj.itoo.graduate.service.PortalManagementService;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 *@author :xulingbo 
 *create : 2018-01-04 15:54:54
 * DESCRIPTION ：
 */
@Component("portalManagementFacade")
@Service
public class PortalManagementFacadeImpl implements PortalManagementFacade {

    //打印日志相关
	private static final Logger logger = LoggerFactory.getLogger(PortalManagementFacadeImpl.class);

    @Autowired
    PortalManagementService portalManagementService;

    /**
     * 用于顶栏显示
     * 根据userId查找服务-宋学孟-2018年1月18日
     *
     * @return List<RoleApplicationModel>
     */
    @Override
    public List<RoleApplicationModel> queryUserServiceApplication(String userId) {
        List<RoleApplicationModel> list;
        try {
            list = portalManagementService.queryUserServiceApplication(userId);
        } catch (Exception e) {
            logger.error("PortalManagementFacadeImpl.queryUserServiceApplication 根据用户id查询服务失败:{}", e);
            throw new ItooRuntimeException(e);

        }
        return list;
    }

    /**
     * 左侧栏显示-根据userId查找所有的子模块和页面-宋学孟-2018年1月18日
     * @param userId 用户id
     * @param applicationParentId 应用id
     * @return  //List<Map<String, Object>>
     */
    @Override
    public List<Map<String, Object>> queryServiceModuleApplication(String userId, String applicationParentId) {
        List<Map<String, Object>> list;
        try {
            list = portalManagementService.queryServiceModuleApplication(userId, applicationParentId);
        } catch (Exception e) {
            logger.error("PortalManagementFacadeImpl.queryServiceModuleApplication 根据用户id查询服务下的页面和模块失败:{}", e);
            throw new ItooRuntimeException(e);
        }
        return list;
    }

    /**
     * 根据id查询Application
     * @param id
     * @return ApplicationEntity
     */
    @Override
    public ApplicationEntity findById(String id) {
        return portalManagementService.findById(id);
    }

    /**
     * 用portal的左侧栏显示，根据userId和applicationID查找页面中的按钮-宋学孟-2018年1月18日
     * @param userId 用户ID
     * @param applicationId 资源ID
     * @return 按钮资源信息
     */
    @Override
    public List<RoleApplicationModel> queryModuleButtonApplication(String userId, String applicationId) {
        List<RoleApplicationModel> list;
        try {
            list = portalManagementService.queryModuleButtonApplication(userId, applicationId);
        } catch (Exception e) {
            logger.error("PortalManagementFacadeImpl.queryModuleButtonApplication 根据userId和applicationId 查询页面按钮失败:{}", e);
            throw new ItooRuntimeException(e);

        }
        return list;
    }

    /**
     * 登录根据userId查询UserEntity-宋学孟-2018年1月18日
     * @param userCode
     * @return
     */
    @Override
    public UserRoleModel selectUserInfoByCode(String userCode, String userPwd) {
        UserRoleModel userRoleModel;
        try {
            userRoleModel = portalManagementService.selectUserInfoByCode(userCode, userPwd);
        } catch (Exception e) {
            logger.error("登录根据userId查询UserEntity失败:{}", e);
            throw new ItooRuntimeException(e);

        }
        return userRoleModel;
    }
    
}
