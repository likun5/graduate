package com.dmsdbj.itoo.graduate.facade.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.dmsdbj.itoo.graduate.entity.NoticeCategoryEntity;
import com.dmsdbj.itoo.graduate.facade.NoticeCategoryFacade;
import com.dmsdbj.itoo.graduate.service.NoticeCategoryService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author hgt
 * create: 2017-11-27 11:22:13
 * DESCRIPTION
 */
@Component("noticeCategoryFacade")
@Service
public class NoticeCategoryFacadeImpl implements NoticeCategoryFacade {

//    //打印日志相关
//	private static final Logger logger = LoggerFactory.getLogger(NoticeCategoryFacadeImpl.class);

    @Autowired
    NoticeCategoryService noticeCategoryService;

    /**
     * 根分类ID查询通知分类-haoguiting-2017年11月27日
     *
     * @param id 通知类别id
     * @return NoticeCategoryEntity通知类别实体
     */
    @Override
    public NoticeCategoryEntity findById(String id) {
        NoticeCategoryEntity noticeCategoryEntity;
        noticeCategoryEntity = noticeCategoryService.findById(id);
        return noticeCategoryEntity;
    }

    /**
     * 查询全部通知分类信息（没有参数）-郝贵婷-2017年11月27日
     *
     * @return 通知分类list列表
     */
    @Override
    public List<NoticeCategoryEntity> selectNoticeCategoryInfo() {
        List<NoticeCategoryEntity> noticeCategoryEntityList;
        noticeCategoryEntityList = noticeCategoryService.selectNoticeCategoryInfo();

        return noticeCategoryEntityList;

    }

    /**
     * 添加分类信息-郝贵婷-2017年11月27日
     *
     * @param noticeCategory 通知类别实体
     * @return 受影响行数
     */
    @Override
    public int addNoticeCategory(NoticeCategoryEntity noticeCategory) {
        int count = 0;
        count = noticeCategoryService.addNoticeCategory(noticeCategory);
        return count;
    }

    /**
     * 修改分类信息-郝贵婷-2017年11月27日
     *
     * @param noticeCategory 通知分类实体
     * @return 受影响行
     */
    @Override
    public int updateNoticeCategory(NoticeCategoryEntity noticeCategory) {
        int count = 0;
        count = noticeCategoryService.updateNoticeCategory(noticeCategory);
        return count;
    }

    /**
     * 删除分类信息-郝贵婷-2017年11月27日
     *
     * @param noticeCatagoryID  通知分类ID
     * @param operator 操作人
     * @return 受影响行数
     */
    @Override
    public int deleteNoticeCategory(String noticeCatagoryID, String operator) {
        int count = 0;
        count = noticeCategoryService.deleteNoticeCategory(noticeCatagoryID, operator);
        return count;
    }

    /**
     * 假分页-查询全部通知分类-郝贵婷-2018-01-11 14:15:45
     *
     * @param page 页码
     * @param pageSize 每页显示条数
     * @return 类别list
     */
    @Override
    public PageInfo<NoticeCategoryEntity> PageSelectNoticeCategoryInfo(int page, int pageSize) {
        return noticeCategoryService.PageSelectNoticeCategoryInfo(page, pageSize);
    }

    /**
     * 根据主键批量删除类别-郝贵婷-2017年11月27日
     *
     * @param id  通知分类ID
     * @param operator 操作人
     * @return 影响行数
     */
    @Override
    public int deleteNoticeCategorys(String id, String operator) {
        int count = 0;
        count = noticeCategoryService.deleteNoticeCategorys(id, operator);
        return count;
    }
}
