package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.PersonCompanyRelationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.PersonCompanyRelationExample;

import java.util.List;


/**
 * @author sxm
 * create:2017-11-26 16:14:54
 * DESCRIPTION
 */
public interface PersonCompanyRelationService extends BaseService<PersonCompanyRelationEntity, PersonCompanyRelationExample>{

    /**
     * 根据个人id查询个人公司关系id的List -lishuang-2017-12-16 17:10:53
     * @param personId 用户id
     * @return 关系id列表
     */
    List<String> selectPersonCompanyRelationIdsByPersonId(String personId);


    /**
     * 根据期数查询本期所有人的与公司的关系id的List -lishaugn-2017-12-16 17:10:40
     * @param grade 期数
     * @return 关系id列表
     */
    List<String> selectPersonCompanyRelationIdsByGrade(String grade);


    /**
     * 根据个人id的list查询个人公司关系id的List -lishuang-2017-12-16 17:20:22
     * @param personIdlist  个人id的集合
     * @return 个人公司关系id的集合
     */
    List<String> selectPersonCompanyRelationIdsByPersonIds( List<String> personIdlist);

    /**
     * 根据人员id删除人员公司关系表记录 -lishuang-2017-12-23 08:38:05
     * @param personId 人员id
     * @param operator 操作员
     * @return 是否成功
     */
    boolean deletePersonCompanyRelationByPersonId(String personId, String operator);
}
