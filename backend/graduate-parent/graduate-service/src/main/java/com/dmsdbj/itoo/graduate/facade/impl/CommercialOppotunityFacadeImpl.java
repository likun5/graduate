package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.CommercialOppotunityEntity;
import com.dmsdbj.itoo.graduate.entity.ext.RegionCommercialModel;
import com.dmsdbj.itoo.graduate.facade.CommercialOppotunityFacade;
import com.dmsdbj.itoo.graduate.service.CommercialOppotunityService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *@authorsxm
 *create: 2017-11-16 14:31:07
 * DESCRIPTION
 */
@Component("commercialOppotunityFacade")
@Service
public class CommercialOppotunityFacadeImpl implements CommercialOppotunityFacade {

    //打印日志相关
	private static final Logger logger = LoggerFactory.getLogger(CommercialOppotunityFacadeImpl.class);

    @Autowired
    CommercialOppotunityService commercialOppotunityService;

    /**
     * 根据id查询CommercialOppotunity
     * @param id
     * @return CommercialOppotunityEntity
     */
    @Override
    public CommercialOppotunityEntity findById(String id) {
        return commercialOppotunityService.findById(id);
    }

    /**
     * 通过地域id 查询地域商机-唐凌峰-2018-1-11 10:36:59
     * @param regionId 地域ID
     * @param pageNum  页码
     * @param pageSize 页量
     * @return PageInfo<RegionCommercialModel>
     */
    @Override
    public PageInfo<RegionCommercialModel> queryRegionCommercialById(String regionId, int pageNum, int pageSize) {
        PageInfo<RegionCommercialModel> regionCommercialModelList=null;

        try{
            regionCommercialModelList=commercialOppotunityService.queryRegionCommercialById(regionId,pageNum,pageSize);
            if(regionCommercialModelList !=null){
                return  regionCommercialModelList;
            }
        }catch(Exception e){
            logger.error("查询失败！",e);
        }
        return null;
    }

    /**
     * 方法重载-查询提高班毕业生所有商机信息-唐凌峰
     * @return List<RegionCommercialModel>
     */
    @Override
    public List<RegionCommercialModel> queryRegionCommercialById() {
        try{
            return commercialOppotunityService.queryRegionCommercialById();
        }catch (Exception e)
        {
            logger.error("queryRegionCommercialById 方法查询失败！");
        }
        return null;
    }

}
