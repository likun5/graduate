package com.dmsdbj.itoo.graduate.mybatisexample;

import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.2
 * DESCRIPTION:将distinct由boolean型改成String型
 * create:2017年12月18日
 * 
 */
public class CompanySkillRelationExample implements Serializable {
    protected String orderByClause;

    protected String distinct;

    private List<CompanySkillRelationCriteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    /**
     * 构造函数
     */
    public CompanySkillRelationExample() {
        oredCriteria = new ArrayList<>();
    }

    /**
     *以某字段升序或降序排序---比如：orderByClause=字段名 ASC  表示升序
     * @param orderByClause 排序条件
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     *获得升序降序排序条件
     * @return String 排序条件
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     *去除重复，boolean型，true为选择不重复的记录
     * @param distinct distinct去重标记
     */
    public void setDistinct(String distinct) {
        this.distinct = distinct;
    }

    /**
     *
     * @return boolean distinct是否有效
     */
    public String isDistinct() {
        return distinct;
    }

    /**
     *
     * @return List CompanySkillRelationCriteriaList集合
     */
    public List<CompanySkillRelationCriteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     *
     * @param criteria Criteria条件
     */
    public void or(CompanySkillRelationCriteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     *
     * @return CompanySkillRelationCriteria Criteria条件
     */
    public CompanySkillRelationCriteria or() {
        CompanySkillRelationCriteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     *
     * @return CompanySkillRelationCriteria Criteria条件
     */
    public CompanySkillRelationCriteria createCriteria() {
        CompanySkillRelationCriteria criteria = createCriteriaInternal();
        if (oredCriteria.isEmpty()) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     *
     * @return CompanySkillRelationCriteria Criteria条件
     */
    protected CompanySkillRelationCriteria createCriteriaInternal() {
        return new CompanySkillRelationCriteria();
    }

    /**
     * 清空查询条件
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = null;
    }

    /**
     *
     * @param limit 限制条数
     */
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     *
     * @return Integer 限制条数
     */
    public Integer getLimit() {
        return limit;
    }

    /**
     *
     * @param offset 偏移量
     */
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    /**
     *
     * @return Integer 偏移量
     */
    public Integer getOffset() {
        return offset;
    }


}