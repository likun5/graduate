package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class ForumCommentCriteria extends GeneratedCriteria<ForumCommentCriteria> implements Serializable{

    protected ForumCommentCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String FORUM_ID = "forumId";   
    private static final String CONTENT = "content";   
    private static final String OPERATORID = "operatorid";   
    private static final String TIMESTAMP_TIME = "timestampTime";   

    public ForumCommentCriteria andForumIdIsNull() {
        addCriterion("forum_id is null");
        return this;
    }

    public ForumCommentCriteria andForumIdIsNotNull() {
        addCriterion("forum_id is not null");
        return this;
    }
    public ForumCommentCriteria andForumIdEqualTo(String value) {
        addCriterion("forum_id =", value, FORUM_ID);
        return this;
    }
    public ForumCommentCriteria andForumIdNotEqualTo(String value) {
        addCriterion("forum_id <>", value, FORUM_ID);
        return this;
    }    
    public ForumCommentCriteria andForumIdGreaterThan(String value) {
        addCriterion("forum_id >", value, FORUM_ID);
        return this;
    }    
    public ForumCommentCriteria andForumIdGreaterThanOrEqualTo(String value) {
        addCriterion("forum_id >=", value, FORUM_ID);
        return this;
    }    
    public ForumCommentCriteria andForumIdLessThan(String value) {
        addCriterion("forum_id <", value, FORUM_ID);
        return this;
    }     
    public ForumCommentCriteria andForumIdLessThanOrEqualTo(String value) {
        addCriterion("forum_id <=", value, FORUM_ID);
        return this;
    }
    public ForumCommentCriteria andForumIdIn(List<String> values) {
        addCriterion("forum_id in", values, FORUM_ID);
        return this;
    }
    public ForumCommentCriteria andForumIdNotIn(List<String> values) {
        addCriterion("forum_id not in", values, FORUM_ID);
        return this;
    }
    public ForumCommentCriteria andForumIdBetween(String value1, String value2) {
        addCriterion("forum_id between", value1, value2, FORUM_ID);
        return this;
    }
    public ForumCommentCriteria andForumIdNotBetween(String value1, String value2) {
        addCriterion("forum_id not between", value1, value2, FORUM_ID);
        return this;
    }
        public ForumCommentCriteria andForumIdLike(String value) {
            addCriterion("forum_id like", value, FORUM_ID);
            return this;
        }

        public ForumCommentCriteria andForumIdNotLike(String value) {
            addCriterion("forum_id not like", value, FORUM_ID);
            return this;
        }
    public ForumCommentCriteria andContentIsNull() {
        addCriterion("content is null");
        return this;
    }

    public ForumCommentCriteria andContentIsNotNull() {
        addCriterion("content is not null");
        return this;
    }
    public ForumCommentCriteria andContentEqualTo(String value) {
        addCriterion("content =", value, CONTENT);
        return this;
    }
    public ForumCommentCriteria andContentNotEqualTo(String value) {
        addCriterion("content <>", value, CONTENT);
        return this;
    }    
    public ForumCommentCriteria andContentGreaterThan(String value) {
        addCriterion("content >", value, CONTENT);
        return this;
    }    
    public ForumCommentCriteria andContentGreaterThanOrEqualTo(String value) {
        addCriterion("content >=", value, CONTENT);
        return this;
    }    
    public ForumCommentCriteria andContentLessThan(String value) {
        addCriterion("content <", value, CONTENT);
        return this;
    }     
    public ForumCommentCriteria andContentLessThanOrEqualTo(String value) {
        addCriterion("content <=", value, CONTENT);
        return this;
    }
    public ForumCommentCriteria andContentIn(List<String> values) {
        addCriterion("content in", values, CONTENT);
        return this;
    }
    public ForumCommentCriteria andContentNotIn(List<String> values) {
        addCriterion("content not in", values, CONTENT);
        return this;
    }
    public ForumCommentCriteria andContentBetween(String value1, String value2) {
        addCriterion("content between", value1, value2, CONTENT);
        return this;
    }
    public ForumCommentCriteria andContentNotBetween(String value1, String value2) {
        addCriterion("content not between", value1, value2, CONTENT);
        return this;
    }
        public ForumCommentCriteria andContentLike(String value) {
            addCriterion("content like", value, CONTENT);
            return this;
        }

        public ForumCommentCriteria andContentNotLike(String value) {
            addCriterion("content not like", value, CONTENT);
            return this;
        }
    public ForumCommentCriteria andOperatoridIsNull() {
        addCriterion("operatorid is null");
        return this;
    }

    public ForumCommentCriteria andOperatoridIsNotNull() {
        addCriterion("operatorid is not null");
        return this;
    }
    public ForumCommentCriteria andOperatoridEqualTo(String value) {
        addCriterion("operatorid =", value, OPERATORID);
        return this;
    }
    public ForumCommentCriteria andOperatoridNotEqualTo(String value) {
        addCriterion("operatorid <>", value, OPERATORID);
        return this;
    }    
    public ForumCommentCriteria andOperatoridGreaterThan(String value) {
        addCriterion("operatorid >", value, OPERATORID);
        return this;
    }    
    public ForumCommentCriteria andOperatoridGreaterThanOrEqualTo(String value) {
        addCriterion("operatorid >=", value, OPERATORID);
        return this;
    }    
    public ForumCommentCriteria andOperatoridLessThan(String value) {
        addCriterion("operatorid <", value, OPERATORID);
        return this;
    }     
    public ForumCommentCriteria andOperatoridLessThanOrEqualTo(String value) {
        addCriterion("operatorid <=", value, OPERATORID);
        return this;
    }
    public ForumCommentCriteria andOperatoridIn(List<String> values) {
        addCriterion("operatorid in", values, OPERATORID);
        return this;
    }
    public ForumCommentCriteria andOperatoridNotIn(List<String> values) {
        addCriterion("operatorid not in", values, OPERATORID);
        return this;
    }
    public ForumCommentCriteria andOperatoridBetween(String value1, String value2) {
        addCriterion("operatorid between", value1, value2, OPERATORID);
        return this;
    }
    public ForumCommentCriteria andOperatoridNotBetween(String value1, String value2) {
        addCriterion("operatorid not between", value1, value2, OPERATORID);
        return this;
    }
        public ForumCommentCriteria andOperatoridLike(String value) {
            addCriterion("operatorid like", value, OPERATORID);
            return this;
        }

        public ForumCommentCriteria andOperatoridNotLike(String value) {
            addCriterion("operatorid not like", value, OPERATORID);
            return this;
        }
    public ForumCommentCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public ForumCommentCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public ForumCommentCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public ForumCommentCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public ForumCommentCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public ForumCommentCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public ForumCommentCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public ForumCommentCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public ForumCommentCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public ForumCommentCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public ForumCommentCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public ForumCommentCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
}

