package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.HomeInfoEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.HomeInfoExample;
import org.springframework.stereotype.Repository;

/**
 * @author 徐玲博
 * create: 2017-11-21 21:41:29
 * DESCRIPTION: 家庭信息类
 */
@Repository
public interface HomeInfoDao extends BaseDao<HomeInfoEntity, HomeInfoExample> {

}
