package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.SkillPointEntity;
import com.dmsdbj.itoo.graduate.entity.ext.TechnologyModel;
import com.dmsdbj.itoo.graduate.facade.SkillPointFacade;
import com.dmsdbj.itoo.graduate.mybatisexample.SkillPointCriteria;
import com.dmsdbj.itoo.graduate.mybatisexample.SkillPointExample;
import com.dmsdbj.itoo.graduate.service.SkillPointService;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * @author : 徐玲博
 * create : 2017-11-21 21:46:22
 * DESCRIPTION 技术点
 */
@Component("skillPointFacade")
@Service
public class SkillPointFacadeImpl implements SkillPointFacade {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(SkillPointFacadeImpl.class);

    @Autowired
    private SkillPointService skillPointService;

    /**
     * 根据id查询SkillPoint
     *
     * @param id 技术点id
     * @return SkillPointEntity 技术点（方向）实体
     */
    @Override
    public SkillPointEntity findById(String id) {
        return skillPointService.findById(id);
    }

    /**
     * 添加技术点或技术方向-徐玲博-2017-11-20 15:13:47
     *
     * @param skillPointEntity 技术点实体
     * @return 受影响行
     */
    @Override
    public int addSkillPoint(SkillPointEntity skillPointEntity) {
        int flag;
        try {
            flag = skillPointService.insert(skillPointEntity);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 修改技术点或技术方向-徐玲博-2017-11-20 15:17:05
     *
     * @param skillPointEntity 技术点实体
     * @return 受影响行
     */
    @Override
    public int updateSkillPoint(SkillPointEntity skillPointEntity) {
        int flag;
        try {
            String id=skillPointEntity.getId();
            SkillPointExample skillPointExample=new SkillPointExample();
            SkillPointCriteria skillPointCriteria=skillPointExample.createCriteria();
            skillPointCriteria.andIdEqualTo(id);
            skillPointCriteria.andIsDeleteEqualTo((byte)0);
            flag=skillPointService.updateByExample(skillPointEntity,skillPointExample);
//            flag=skillPointService.updateSkillPoint(skillPointEntity);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 删除技术点或删除技术方向-徐玲博-2017-11-20 15:18:20
     *
     * @param id       技术点id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deleteSkillPoint(String id, String operator) {
        int flag;
        try {
            flag = skillPointService.deleteSkillPoint(id, operator);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 查询技术点-或技术方向-徐玲博-2017-11-20 16:54:36
     *
     * @param id 技术点Id
     * @return 技术点list
     */
    @Override
    public List<SkillPointEntity> selectSkillPointById(String id) {
        List<SkillPointEntity> skillPointEntityList;
        try {
            skillPointEntityList = skillPointService.selectSkillPointById(id);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return skillPointEntityList;
    }

    /**
     * 论坛接口-查询所有的技术-并根据技术方向分类-徐玲博-2017-12-26 15:55:23
     *
     * @return 技术点list
     */
    @Override
    public List<Map<String, List<SkillPointEntity>>> selectSkillPoint() {
        return skillPointService.selectSkillPoint();
    }

    /**
     * 查询所有技术（包括技术方向）-徐玲博-2018-1-13 22:52:15
     *
     * @return 技术点list
     */
    @Override
    public List<TechnologyModel> selectAllTechnology() {
        return skillPointService.selectAllTechnology();
    }
    /**
     * 分页查询所有技术-徐玲博-2018-1-13 23:45:51
     *
     * @return 技术点list
     */
    @Override
    public PageInfo<TechnologyModel> pageAllTechnology(int page,int pageSize) {
        return skillPointService.pageAllTechnology(page,pageSize);
    }

    /**
     * 查询技术方向-徐玲博-2017-11-21 14:42:51
     *
     * @param pid 技术方向
     * @return 技术方向列表
     */
    @Override
    public List<SkillPointEntity> selectParentSkillByPid(String pid) {
        List<SkillPointEntity> skillPointEntityList;
        try {
            skillPointEntityList = skillPointService.selectParentSkillByPid(pid);
        } catch (Exception e) {
//            logger.error(ResultCode.getResultCode(ConstantUtils.ERROR_CODE),"根据导师code查导师", e);
            throw new ItooRuntimeException(e);
        }
        return skillPointEntityList;
    }

    /**
     * 查询所有的技术方向实体-徐玲博-2018-1-13 19:15:10
     *
     * @return 技术方向list
     */
    @Override
    public List<SkillPointEntity> selectTechnicalDirection() {
        return skillPointService.selectTechnicalDirection();
    }

    /**
     * 分页查询技术方向-haoguiting-2018年3月10日
     *
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 技术点list
     */
    @Override
    public PageInfo<SkillPointEntity> PageSelectSkillDirection(int page,int pageSize){
        return skillPointService.PageSelectSkillDirection(page,pageSize);
    }

    /**
     * 分页查询根据技术方向查询技术点-haoguiting-2018年2月22日
     * 修改人：
     * @param pid 技术方向
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 技术点list
     */
    @Override
    public PageInfo<SkillPointEntity> PageSelectSkillByDirectionId(String pid,int page,int pageSize){
        return skillPointService.PageSelectSkillByDirectionId(pid,page,pageSize);
    }


    /**
     * 模糊查询技术点-haoguiting-2018年2月27日
     *
     * @return 技术点list
     */
    @Override
    public List<SkillPointEntity> fuzzySkillInfoByName(String strLike,String pid){
        return skillPointService.fuzzySkillInfoByName(strLike,pid);
    }
}
