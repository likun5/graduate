package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class SalaryCriteria extends GeneratedCriteria<SalaryCriteria> implements Serializable{

    protected SalaryCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String PC_RELATION_ID = "pcRelationId";   
    private static final String SALARY_CHANGE_TIME = "salaryChangeTime";   
    private static final String SALARY = "salary";   
    private static final String TIMES_PER_YEAR = "timesPerYear";   
    private static final String POSSESSION = "possession";   
    private static final String ANNUAL_SALARY = "annualSalary";   
    private static final String OPERATOR_ID = "operatorId";   
    private static final String TIMESTAMP_TIME = "timestampTime";   

    public SalaryCriteria andPcRelationIdIsNull() {
        addCriterion("pc_relation_id is null");
        return this;
    }

    public SalaryCriteria andPcRelationIdIsNotNull() {
        addCriterion("pc_relation_id is not null");
        return this;
    }
    public SalaryCriteria andPcRelationIdEqualTo(String value) {
        addCriterion("pc_relation_id =", value, PC_RELATION_ID);
        return this;
    }
    public SalaryCriteria andPcRelationIdNotEqualTo(String value) {
        addCriterion("pc_relation_id <>", value, PC_RELATION_ID);
        return this;
    }    
    public SalaryCriteria andPcRelationIdGreaterThan(String value) {
        addCriterion("pc_relation_id >", value, PC_RELATION_ID);
        return this;
    }    
    public SalaryCriteria andPcRelationIdGreaterThanOrEqualTo(String value) {
        addCriterion("pc_relation_id >=", value, PC_RELATION_ID);
        return this;
    }    
    public SalaryCriteria andPcRelationIdLessThan(String value) {
        addCriterion("pc_relation_id <", value, PC_RELATION_ID);
        return this;
    }     
    public SalaryCriteria andPcRelationIdLessThanOrEqualTo(String value) {
        addCriterion("pc_relation_id <=", value, PC_RELATION_ID);
        return this;
    }
    public SalaryCriteria andPcRelationIdIn(List<String> values) {
        addCriterion("pc_relation_id in", values, PC_RELATION_ID);
        return this;
    }
    public SalaryCriteria andPcRelationIdNotIn(List<String> values) {
        addCriterion("pc_relation_id not in", values, PC_RELATION_ID);
        return this;
    }
    public SalaryCriteria andPcRelationIdBetween(String value1, String value2) {
        addCriterion("pc_relation_id between", value1, value2, PC_RELATION_ID);
        return this;
    }
    public SalaryCriteria andPcRelationIdNotBetween(String value1, String value2) {
        addCriterion("pc_relation_id not between", value1, value2, PC_RELATION_ID);
        return this;
    }
        public SalaryCriteria andPcRelationIdLike(String value) {
            addCriterion("pc_relation_id like", value, PC_RELATION_ID);
            return this;
        }

        public SalaryCriteria andPcRelationIdNotLike(String value) {
            addCriterion("pc_relation_id not like", value, PC_RELATION_ID);
            return this;
        }
    public SalaryCriteria andSalaryChangeTimeIsNull() {
        addCriterion("salary_change_time is null");
        return this;
    }

    public SalaryCriteria andSalaryChangeTimeIsNotNull() {
        addCriterion("salary_change_time is not null");
        return this;
    }
    public SalaryCriteria andSalaryChangeTimeEqualTo(Date value) {
        addCriterion("salary_change_time =", value, SALARY_CHANGE_TIME);
        return this;
    }
    public SalaryCriteria andSalaryChangeTimeNotEqualTo(Date value) {
        addCriterion("salary_change_time <>", value, SALARY_CHANGE_TIME);
        return this;
    }    
    public SalaryCriteria andSalaryChangeTimeGreaterThan(Date value) {
        addCriterion("salary_change_time >", value, SALARY_CHANGE_TIME);
        return this;
    }    
    public SalaryCriteria andSalaryChangeTimeGreaterThanOrEqualTo(Date value) {
        addCriterion("salary_change_time >=", value, SALARY_CHANGE_TIME);
        return this;
    }    
    public SalaryCriteria andSalaryChangeTimeLessThan(Date value) {
        addCriterion("salary_change_time <", value, SALARY_CHANGE_TIME);
        return this;
    }     
    public SalaryCriteria andSalaryChangeTimeLessThanOrEqualTo(Date value) {
        addCriterion("salary_change_time <=", value, SALARY_CHANGE_TIME);
        return this;
    }
    public SalaryCriteria andSalaryChangeTimeIn(List<Date> values) {
        addCriterion("salary_change_time in", values, SALARY_CHANGE_TIME);
        return this;
    }
    public SalaryCriteria andSalaryChangeTimeNotIn(List<Date> values) {
        addCriterion("salary_change_time not in", values, SALARY_CHANGE_TIME);
        return this;
    }
    public SalaryCriteria andSalaryChangeTimeBetween(Date value1, Date value2) {
        addCriterion("salary_change_time between", value1, value2, SALARY_CHANGE_TIME);
        return this;
    }
    public SalaryCriteria andSalaryChangeTimeNotBetween(Date value1, Date value2) {
        addCriterion("salary_change_time not between", value1, value2, SALARY_CHANGE_TIME);
        return this;
    }
    public SalaryCriteria andSalaryIsNull() {
        addCriterion("salary is null");
        return this;
    }

    public SalaryCriteria andSalaryIsNotNull() {
        addCriterion("salary is not null");
        return this;
    }
    public SalaryCriteria andSalaryEqualTo(Double value) {
        addCriterion("salary =", value, SALARY);
        return this;
    }
    public SalaryCriteria andSalaryNotEqualTo(Double value) {
        addCriterion("salary <>", value, SALARY);
        return this;
    }    
    public SalaryCriteria andSalaryGreaterThan(Double value) {
        addCriterion("salary >", value, SALARY);
        return this;
    }    
    public SalaryCriteria andSalaryGreaterThanOrEqualTo(Double value) {
        addCriterion("salary >=", value, SALARY);
        return this;
    }    
    public SalaryCriteria andSalaryLessThan(Double value) {
        addCriterion("salary <", value, SALARY);
        return this;
    }     
    public SalaryCriteria andSalaryLessThanOrEqualTo(Double value) {
        addCriterion("salary <=", value, SALARY);
        return this;
    }
    public SalaryCriteria andSalaryIn(List<Double> values) {
        addCriterion("salary in", values, SALARY);
        return this;
    }
    public SalaryCriteria andSalaryNotIn(List<Double> values) {
        addCriterion("salary not in", values, SALARY);
        return this;
    }
    public SalaryCriteria andSalaryBetween(Double value1, Double value2) {
        addCriterion("salary between", value1, value2, SALARY);
        return this;
    }
    public SalaryCriteria andSalaryNotBetween(Double value1, Double value2) {
        addCriterion("salary not between", value1, value2, SALARY);
        return this;
    }
    public SalaryCriteria andTimesPerYearIsNull() {
        addCriterion("times_per_year is null");
        return this;
    }

    public SalaryCriteria andTimesPerYearIsNotNull() {
        addCriterion("times_per_year is not null");
        return this;
    }
    public SalaryCriteria andTimesPerYearEqualTo(Long value) {
        addCriterion("times_per_year =", value, TIMES_PER_YEAR);
        return this;
    }
    public SalaryCriteria andTimesPerYearNotEqualTo(Long value) {
        addCriterion("times_per_year <>", value, TIMES_PER_YEAR);
        return this;
    }    
    public SalaryCriteria andTimesPerYearGreaterThan(Long value) {
        addCriterion("times_per_year >", value, TIMES_PER_YEAR);
        return this;
    }    
    public SalaryCriteria andTimesPerYearGreaterThanOrEqualTo(Long value) {
        addCriterion("times_per_year >=", value, TIMES_PER_YEAR);
        return this;
    }    
    public SalaryCriteria andTimesPerYearLessThan(Long value) {
        addCriterion("times_per_year <", value, TIMES_PER_YEAR);
        return this;
    }     
    public SalaryCriteria andTimesPerYearLessThanOrEqualTo(Long value) {
        addCriterion("times_per_year <=", value, TIMES_PER_YEAR);
        return this;
    }
    public SalaryCriteria andTimesPerYearIn(List<Long> values) {
        addCriterion("times_per_year in", values, TIMES_PER_YEAR);
        return this;
    }
    public SalaryCriteria andTimesPerYearNotIn(List<Long> values) {
        addCriterion("times_per_year not in", values, TIMES_PER_YEAR);
        return this;
    }
    public SalaryCriteria andTimesPerYearBetween(Long value1, Long value2) {
        addCriterion("times_per_year between", value1, value2, TIMES_PER_YEAR);
        return this;
    }
    public SalaryCriteria andTimesPerYearNotBetween(Long value1, Long value2) {
        addCriterion("times_per_year not between", value1, value2, TIMES_PER_YEAR);
        return this;
    }
    public SalaryCriteria andPossessionIsNull() {
        addCriterion("possession is null");
        return this;
    }

    public SalaryCriteria andPossessionIsNotNull() {
        addCriterion("possession is not null");
        return this;
    }
    public SalaryCriteria andPossessionEqualTo(String value) {
        addCriterion("possession =", value, POSSESSION);
        return this;
    }
    public SalaryCriteria andPossessionNotEqualTo(String value) {
        addCriterion("possession <>", value, POSSESSION);
        return this;
    }    
    public SalaryCriteria andPossessionGreaterThan(String value) {
        addCriterion("possession >", value, POSSESSION);
        return this;
    }    
    public SalaryCriteria andPossessionGreaterThanOrEqualTo(String value) {
        addCriterion("possession >=", value, POSSESSION);
        return this;
    }    
    public SalaryCriteria andPossessionLessThan(String value) {
        addCriterion("possession <", value, POSSESSION);
        return this;
    }     
    public SalaryCriteria andPossessionLessThanOrEqualTo(String value) {
        addCriterion("possession <=", value, POSSESSION);
        return this;
    }
    public SalaryCriteria andPossessionIn(List<String> values) {
        addCriterion("possession in", values, POSSESSION);
        return this;
    }
    public SalaryCriteria andPossessionNotIn(List<String> values) {
        addCriterion("possession not in", values, POSSESSION);
        return this;
    }
    public SalaryCriteria andPossessionBetween(String value1, String value2) {
        addCriterion("possession between", value1, value2, POSSESSION);
        return this;
    }
    public SalaryCriteria andPossessionNotBetween(String value1, String value2) {
        addCriterion("possession not between", value1, value2, POSSESSION);
        return this;
    }
        public SalaryCriteria andPossessionLike(String value) {
            addCriterion("possession like", value, POSSESSION);
            return this;
        }

        public SalaryCriteria andPossessionNotLike(String value) {
            addCriterion("possession not like", value, POSSESSION);
            return this;
        }
    public SalaryCriteria andAnnualSalaryIsNull() {
        addCriterion("annual_salary is null");
        return this;
    }

    public SalaryCriteria andAnnualSalaryIsNotNull() {
        addCriterion("annual_salary is not null");
        return this;
    }
    public SalaryCriteria andAnnualSalaryEqualTo(String value) {
        addCriterion("annual_salary =", value, ANNUAL_SALARY);
        return this;
    }
    public SalaryCriteria andAnnualSalaryNotEqualTo(String value) {
        addCriterion("annual_salary <>", value, ANNUAL_SALARY);
        return this;
    }    
    public SalaryCriteria andAnnualSalaryGreaterThan(String value) {
        addCriterion("annual_salary >", value, ANNUAL_SALARY);
        return this;
    }    
    public SalaryCriteria andAnnualSalaryGreaterThanOrEqualTo(String value) {
        addCriterion("annual_salary >=", value, ANNUAL_SALARY);
        return this;
    }    
    public SalaryCriteria andAnnualSalaryLessThan(String value) {
        addCriterion("annual_salary <", value, ANNUAL_SALARY);
        return this;
    }     
    public SalaryCriteria andAnnualSalaryLessThanOrEqualTo(String value) {
        addCriterion("annual_salary <=", value, ANNUAL_SALARY);
        return this;
    }
    public SalaryCriteria andAnnualSalaryIn(List<String> values) {
        addCriterion("annual_salary in", values, ANNUAL_SALARY);
        return this;
    }
    public SalaryCriteria andAnnualSalaryNotIn(List<String> values) {
        addCriterion("annual_salary not in", values, ANNUAL_SALARY);
        return this;
    }
    public SalaryCriteria andAnnualSalaryBetween(String value1, String value2) {
        addCriterion("annual_salary between", value1, value2, ANNUAL_SALARY);
        return this;
    }
    public SalaryCriteria andAnnualSalaryNotBetween(String value1, String value2) {
        addCriterion("annual_salary not between", value1, value2, ANNUAL_SALARY);
        return this;
    }
        public SalaryCriteria andAnnualSalaryLike(String value) {
            addCriterion("annual_salary like", value, ANNUAL_SALARY);
            return this;
        }

        public SalaryCriteria andAnnualSalaryNotLike(String value) {
            addCriterion("annual_salary not like", value, ANNUAL_SALARY);
            return this;
        }
    public SalaryCriteria andOperatorIdIsNull() {
        addCriterion("operator_id is null");
        return this;
    }

    public SalaryCriteria andOperatorIdIsNotNull() {
        addCriterion("operator_id is not null");
        return this;
    }
    public SalaryCriteria andOperatorIdEqualTo(String value) {
        addCriterion("operator_id =", value, OPERATOR_ID);
        return this;
    }
    public SalaryCriteria andOperatorIdNotEqualTo(String value) {
        addCriterion("operator_id <>", value, OPERATOR_ID);
        return this;
    }    
    public SalaryCriteria andOperatorIdGreaterThan(String value) {
        addCriterion("operator_id >", value, OPERATOR_ID);
        return this;
    }    
    public SalaryCriteria andOperatorIdGreaterThanOrEqualTo(String value) {
        addCriterion("operator_id >=", value, OPERATOR_ID);
        return this;
    }    
    public SalaryCriteria andOperatorIdLessThan(String value) {
        addCriterion("operator_id <", value, OPERATOR_ID);
        return this;
    }     
    public SalaryCriteria andOperatorIdLessThanOrEqualTo(String value) {
        addCriterion("operator_id <=", value, OPERATOR_ID);
        return this;
    }
    public SalaryCriteria andOperatorIdIn(List<String> values) {
        addCriterion("operator_id in", values, OPERATOR_ID);
        return this;
    }
    public SalaryCriteria andOperatorIdNotIn(List<String> values) {
        addCriterion("operator_id not in", values, OPERATOR_ID);
        return this;
    }
    public SalaryCriteria andOperatorIdBetween(String value1, String value2) {
        addCriterion("operator_id between", value1, value2, OPERATOR_ID);
        return this;
    }
    public SalaryCriteria andOperatorIdNotBetween(String value1, String value2) {
        addCriterion("operator_id not between", value1, value2, OPERATOR_ID);
        return this;
    }
        public SalaryCriteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, OPERATOR_ID);
            return this;
        }

        public SalaryCriteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, OPERATOR_ID);
            return this;
        }
    public SalaryCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public SalaryCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public SalaryCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public SalaryCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public SalaryCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public SalaryCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public SalaryCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public SalaryCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public SalaryCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public SalaryCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public SalaryCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public SalaryCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
}

