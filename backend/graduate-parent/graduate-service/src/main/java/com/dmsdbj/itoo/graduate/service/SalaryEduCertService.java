package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.graduate.entity.SalaryEntity;
import com.dmsdbj.itoo.graduate.entity.ext.PeriodSalaryModel;
import com.dmsdbj.itoo.graduate.mybatisexample.SalaryExample;
import com.dmsdbj.itoo.tool.base.service.BaseService;

import java.util.List;

/**
 * 薪资学历统计接口
 * 郑晓东
 * 2018年1月17日14点27分
 */
public interface SalaryEduCertService extends BaseService<SalaryEntity,SalaryExample> {

    /**
     * 根据期数ID查询每期每年平均、最高、最低工资   郑晓东
     * @param periodId  期数ID
     * @return List<PeriodSalaryModel>
     */
    List<PeriodSalaryModel> selectAMMSalaryByPeriodId(String periodId);

    /**
     * 查询今年每期平均、最高、最低薪资统计 郑晓东 2018年1月20日16点38分
     * @return 薪资统计集合
     */
    List<PeriodSalaryModel> selectAMMSalaryInThisYear();

    /**
     * 查询每期毕业时平均、最高、最低薪资统计  郑晓东 2018年1月20日
     * @return 薪资统计集合
     */
    List<PeriodSalaryModel> selectAMMSalaryInGradeYear();
}
