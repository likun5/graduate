package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.PeriodPersonModel;
import com.dmsdbj.itoo.graduate.entity.ext.PersonGradeModel;
import com.dmsdbj.itoo.graduate.entity.ext.PersonInfoModel;
import com.dmsdbj.itoo.graduate.mybatisexample.BrowseCriteria;
import com.dmsdbj.itoo.graduate.service.DictionaryService;
import com.dmsdbj.itoo.graduate.service.PersonalInfoService;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.BrowseExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.BrowseDao;
import com.dmsdbj.itoo.graduate.service.BrowseService;
import com.dmsdbj.itoo.graduate.entity.BrowseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 徐玲博
 * create:2017-11-26 16:14:54
 * DESCRIPTION 浏览记录service实现层
 */
@Service("browseService")
public class BrowseServiceImpl extends BaseServiceImpl<BrowseEntity, BrowseExample> implements BrowseService {

    private static int batchCount=500;
    //注入browseDao
	@Autowired
    private BrowseDao browseDao;

    @Autowired
    private PersonalInfoService personalInfoService;
    private DictionaryService dictionaryService;

    /**
     * 让BaseServiceImpl获取到Dao
     * @return BaseDao<BrowseEntity, BrowseExample>基础dao
     */
    @Override
    public  BaseDao<BrowseEntity, BrowseExample> getRealDao(){
        return this.browseDao;
    }

    /**
     * 根据通知ID、是否浏览查询浏览人信息-郝贵婷-2017年12月3日
     * @param noticeId 通知id
     * @param isBrowse 是否浏览
     * @return 浏览人list列表
     */
    @Override
    public List<PersonInfoModel> selectBrowseByNoticeId(String noticeId, String isBrowse) {
        List<BrowseEntity> browseEntityList;
        Map<String, Object> map = new HashMap<>();
        map.put("noticeId", noticeId);
        map.put("isBrowse", isBrowse);
        browseEntityList= browseDao.selectBrowseByNoticeId(map);

        List<PersonInfoModel > personalInfoEntityList = new ArrayList<>();
        for(BrowseEntity browser :browseEntityList){
            PersonInfoModel  person = personalInfoService.selectByPersonId(browser.getBrowsePersonId());
            personalInfoEntityList.add(person);
        }
        return personalInfoEntityList;

    }

    /**
     * 更新浏览记录-郝贵婷-2017年12月3日
     * @param noticeId 通知id
     * @param browsePersonId  浏览人id
     * @return 受影响行
     */
    public int updateBrowseIsBrowse(String noticeId, String browsePersonId){
        Map<String, Object> map = new HashMap<>();
        map.put("noticeId", noticeId);
        map.put("browsePersonId", browsePersonId);
        return browseDao.updateBrowseIsBrowse(map);
    }

    /**
     * 根据浏览人ID查询已浏览记录-郝贵婷-2017年12月3日
     * @param browsePersonId 浏览人id
     * @return 受影响行
     */
    public int selectBrowsedByBrowsePersonId(String browsePersonId){
        return browseDao.selectBrowsedByBrowsePersonId(browsePersonId);
    }

    /**
     * 根据浏览人ID查询未浏览记录-郝贵婷-2017年12月3日
     * @param browsePersonId 浏览人id
     * @return 受影响行
     */
    public int selectNoBrowseByBrowsePersonId(String browsePersonId){
        return browseDao.selectNoBrowseByBrowsePersonId(browsePersonId);
    }

    /**
     * 分页-根据通知ID、是否浏览查询浏览人信息-郝贵婷-2018年01月11日
     * @param noticeId  通知Id
     * @param isBrowse  是否浏览
     * @param page 页码
     * @param  pageSize 页大小
     * @return 浏览人list
     */
    @Override
    public PageInfo<PersonalInfoEntity> PageSelectBrowseByNoticeId(String noticeId, String isBrowse , int page, int pageSize){
        List<BrowseEntity> browseEntityList;
        Map<String, Object> map = new HashMap<>();
        map.put("noticeId", noticeId);
        map.put("isBrowse", isBrowse);
        browseEntityList= browseDao.selectBrowseByNoticeId(map);

        List<PersonalInfoEntity> personalInfoEntityList = new ArrayList<>();
        for(BrowseEntity browser :browseEntityList){
            PersonalInfoEntity person = personalInfoService.findById(browser.getBrowsePersonId());
            personalInfoEntityList.add(person);
        }
        return new PageInfo<>(personalInfoEntityList);
    }

    /**
     * 查询所有期数及对应期数人员 郝贵婷 -2018年1月14日
     * @return 期数及人员list列表
     */
    @Override
    public List<PeriodPersonModel> findPeriodPersonModels() {
        return personalInfoService.findPeriodPersonModels();
    }

    /**
     * 根据通知ID查询全部浏览人-郝贵婷-2017-12-5 14:15:45
     * @param noticeId 通知id
     * @return 受影响行
     */
    @Override
    public List<PersonalInfoEntity>selectAllBrowserByNoticeId(String noticeId ){
        List<BrowseEntity> browseEntityList;
        browseEntityList= browseDao.selectAllBrowserByNoticeId(noticeId);

        List<PersonalInfoEntity> personalInfoEntityList = new ArrayList<>();
        for(BrowseEntity browser :browseEntityList){
            //PersonalInfoEntity person = personalInfoService.findById(browser.getBrowsePersonId());
            PersonalInfoEntity person = personalInfoService.findByUserId(browser.getBrowsePersonId());
            personalInfoEntityList.add(person);
        }
        return personalInfoEntityList;
    }

    /**
     * 批量更新-----有错---未更改-2018年1月24日
     * @param records 浏览人list
     * @return 受影响条数
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateAll(List<BrowseEntity> records) {
        int result = 0;
        if (records != null) {
            int recCount = records.size();
            if (recCount > 0 && recCount <= batchCount) {
                for (BrowseEntity record : records) {

                }
                result = getRealDao().insertAll(records);
            }
        }
        return result;
    }


    /**
     * 根据条件删除IDri-hgt-2018年1月24日
     * @param noticeId 通知id
     * @param operator 操作人
     * @return 受影响条数
     */
    public int deleteByNoticeId(String noticeId,String operator ){
        int count= 0;
        BrowseExample browseExample=new BrowseExample();
        BrowseCriteria criteria=browseExample.createCriteria();
        criteria.andNoticeIdEqualTo(noticeId);
        count=browseDao.deleteByExample(browseExample,operator);
        return count;
    }

    /**
     * 查询全部接受人员（树形结构）-hgt-2018-1-30
     * @return 接受人list
     */
    public List<PersonGradeModel> selectAllBrowsersIncludeGradeName(){
        return personalInfoService.selectPersonInfo();
    }

    /**
     * 查询全部期数（构建树形结构）--hgt-2018-1-30
     * @param typeCode  字典类别
     * @return 期数list
     */
    @Override
    public List<DictionaryEntity> selectAllGrade(String typeCode){

        return dictionaryService.selectDictionaryByTypeCode(typeCode);
    }
}
