package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.PersonCompanyRelationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.PersonCompanyRelationExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *@author 徐玲博
 *create: 2017-11-26 16:14:54
 *DESCRIPTION:
 */
@Repository
public interface PersonCompanyRelationDao extends BaseDao<PersonCompanyRelationEntity,PersonCompanyRelationExample> {

    /**
     * 根据个人id查询个人公司关系id的List -lishuang-2017-12-16 17:10:53
     * @param personId 个人id
     * @return 个人公司关系id的集合
     */
    List<String> selectPersonCompanyRelationIdsByPersonId(@Param("userId") String personId);


    /**
     * 根据个人id的list查询个人公司关系id的List -lishuang-2017-12-16 17:20:22
     * @param userIds  个人id的集合
     * @return 个人公司关系id的集合
     */
    List<String> selectPersonCompanyRelationIdsByPersonIds(@Param("userIds") List<String> userIds);


}
