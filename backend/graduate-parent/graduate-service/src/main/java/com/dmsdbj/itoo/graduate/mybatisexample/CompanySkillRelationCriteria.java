package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class CompanySkillRelationCriteria extends GeneratedCriteria<CompanySkillRelationCriteria> implements Serializable{

    protected CompanySkillRelationCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String SKILL_ID = "skillId";   
    private static final String COMPANY_ID = "companyId";   
    private static final String RECOMMEND_RESOURCE = "recommendResource";   
    private static final String OPERATOR_ID = "operatorId";   
    private static final String TIMESTAMP_TIME = "timestampTime";   

    public CompanySkillRelationCriteria andSkillIdIsNull() {
        addCriterion("skill_id is null");
        return this;
    }

    public CompanySkillRelationCriteria andSkillIdIsNotNull() {
        addCriterion("skill_id is not null");
        return this;
    }
    public CompanySkillRelationCriteria andSkillIdEqualTo(String value) {
        addCriterion("skill_id =", value, SKILL_ID);
        return this;
    }
    public CompanySkillRelationCriteria andSkillIdNotEqualTo(String value) {
        addCriterion("skill_id <>", value, SKILL_ID);
        return this;
    }    
    public CompanySkillRelationCriteria andSkillIdGreaterThan(String value) {
        addCriterion("skill_id >", value, SKILL_ID);
        return this;
    }    
    public CompanySkillRelationCriteria andSkillIdGreaterThanOrEqualTo(String value) {
        addCriterion("skill_id >=", value, SKILL_ID);
        return this;
    }    
    public CompanySkillRelationCriteria andSkillIdLessThan(String value) {
        addCriterion("skill_id <", value, SKILL_ID);
        return this;
    }     
    public CompanySkillRelationCriteria andSkillIdLessThanOrEqualTo(String value) {
        addCriterion("skill_id <=", value, SKILL_ID);
        return this;
    }
    public CompanySkillRelationCriteria andSkillIdIn(List<String> values) {
        addCriterion("skill_id in", values, SKILL_ID);
        return this;
    }
    public CompanySkillRelationCriteria andSkillIdNotIn(List<String> values) {
        addCriterion("skill_id not in", values, SKILL_ID);
        return this;
    }
    public CompanySkillRelationCriteria andSkillIdBetween(String value1, String value2) {
        addCriterion("skill_id between", value1, value2, SKILL_ID);
        return this;
    }
    public CompanySkillRelationCriteria andSkillIdNotBetween(String value1, String value2) {
        addCriterion("skill_id not between", value1, value2, SKILL_ID);
        return this;
    }
        public CompanySkillRelationCriteria andSkillIdLike(String value) {
            addCriterion("skill_id like", value, SKILL_ID);
            return this;
        }

        public CompanySkillRelationCriteria andSkillIdNotLike(String value) {
            addCriterion("skill_id not like", value, SKILL_ID);
            return this;
        }
    public CompanySkillRelationCriteria andCompanyIdIsNull() {
        addCriterion("company_id is null");
        return this;
    }

    public CompanySkillRelationCriteria andCompanyIdIsNotNull() {
        addCriterion("company_id is not null");
        return this;
    }
    public CompanySkillRelationCriteria andCompanyIdEqualTo(String value) {
        addCriterion("company_id =", value, COMPANY_ID);
        return this;
    }
    public CompanySkillRelationCriteria andCompanyIdNotEqualTo(String value) {
        addCriterion("company_id <>", value, COMPANY_ID);
        return this;
    }    
    public CompanySkillRelationCriteria andCompanyIdGreaterThan(String value) {
        addCriterion("company_id >", value, COMPANY_ID);
        return this;
    }    
    public CompanySkillRelationCriteria andCompanyIdGreaterThanOrEqualTo(String value) {
        addCriterion("company_id >=", value, COMPANY_ID);
        return this;
    }    
    public CompanySkillRelationCriteria andCompanyIdLessThan(String value) {
        addCriterion("company_id <", value, COMPANY_ID);
        return this;
    }     
    public CompanySkillRelationCriteria andCompanyIdLessThanOrEqualTo(String value) {
        addCriterion("company_id <=", value, COMPANY_ID);
        return this;
    }
    public CompanySkillRelationCriteria andCompanyIdIn(List<String> values) {
        addCriterion("company_id in", values, COMPANY_ID);
        return this;
    }
    public CompanySkillRelationCriteria andCompanyIdNotIn(List<String> values) {
        addCriterion("company_id not in", values, COMPANY_ID);
        return this;
    }
    public CompanySkillRelationCriteria andCompanyIdBetween(String value1, String value2) {
        addCriterion("company_id between", value1, value2, COMPANY_ID);
        return this;
    }
    public CompanySkillRelationCriteria andCompanyIdNotBetween(String value1, String value2) {
        addCriterion("company_id not between", value1, value2, COMPANY_ID);
        return this;
    }
        public CompanySkillRelationCriteria andCompanyIdLike(String value) {
            addCriterion("company_id like", value, COMPANY_ID);
            return this;
        }

        public CompanySkillRelationCriteria andCompanyIdNotLike(String value) {
            addCriterion("company_id not like", value, COMPANY_ID);
            return this;
        }
    public CompanySkillRelationCriteria andRecommendResourceIsNull() {
        addCriterion("recommend_resource is null");
        return this;
    }

    public CompanySkillRelationCriteria andRecommendResourceIsNotNull() {
        addCriterion("recommend_resource is not null");
        return this;
    }
    public CompanySkillRelationCriteria andRecommendResourceEqualTo(String value) {
        addCriterion("recommend_resource =", value, RECOMMEND_RESOURCE);
        return this;
    }
    public CompanySkillRelationCriteria andRecommendResourceNotEqualTo(String value) {
        addCriterion("recommend_resource <>", value, RECOMMEND_RESOURCE);
        return this;
    }    
    public CompanySkillRelationCriteria andRecommendResourceGreaterThan(String value) {
        addCriterion("recommend_resource >", value, RECOMMEND_RESOURCE);
        return this;
    }    
    public CompanySkillRelationCriteria andRecommendResourceGreaterThanOrEqualTo(String value) {
        addCriterion("recommend_resource >=", value, RECOMMEND_RESOURCE);
        return this;
    }    
    public CompanySkillRelationCriteria andRecommendResourceLessThan(String value) {
        addCriterion("recommend_resource <", value, RECOMMEND_RESOURCE);
        return this;
    }     
    public CompanySkillRelationCriteria andRecommendResourceLessThanOrEqualTo(String value) {
        addCriterion("recommend_resource <=", value, RECOMMEND_RESOURCE);
        return this;
    }
    public CompanySkillRelationCriteria andRecommendResourceIn(List<String> values) {
        addCriterion("recommend_resource in", values, RECOMMEND_RESOURCE);
        return this;
    }
    public CompanySkillRelationCriteria andRecommendResourceNotIn(List<String> values) {
        addCriterion("recommend_resource not in", values, RECOMMEND_RESOURCE);
        return this;
    }
    public CompanySkillRelationCriteria andRecommendResourceBetween(String value1, String value2) {
        addCriterion("recommend_resource between", value1, value2, RECOMMEND_RESOURCE);
        return this;
    }
    public CompanySkillRelationCriteria andRecommendResourceNotBetween(String value1, String value2) {
        addCriterion("recommend_resource not between", value1, value2, RECOMMEND_RESOURCE);
        return this;
    }
        public CompanySkillRelationCriteria andRecommendResourceLike(String value) {
            addCriterion("recommend_resource like", value, RECOMMEND_RESOURCE);
            return this;
        }

        public CompanySkillRelationCriteria andRecommendResourceNotLike(String value) {
            addCriterion("recommend_resource not like", value, RECOMMEND_RESOURCE);
            return this;
        }
    public CompanySkillRelationCriteria andOperatorIdIsNull() {
        addCriterion("operator_id is null");
        return this;
    }

    public CompanySkillRelationCriteria andOperatorIdIsNotNull() {
        addCriterion("operator_id is not null");
        return this;
    }
    public CompanySkillRelationCriteria andOperatorIdEqualTo(String value) {
        addCriterion("operator_id =", value, OPERATOR_ID);
        return this;
    }
    public CompanySkillRelationCriteria andOperatorIdNotEqualTo(String value) {
        addCriterion("operator_id <>", value, OPERATOR_ID);
        return this;
    }    
    public CompanySkillRelationCriteria andOperatorIdGreaterThan(String value) {
        addCriterion("operator_id >", value, OPERATOR_ID);
        return this;
    }    
    public CompanySkillRelationCriteria andOperatorIdGreaterThanOrEqualTo(String value) {
        addCriterion("operator_id >=", value, OPERATOR_ID);
        return this;
    }    
    public CompanySkillRelationCriteria andOperatorIdLessThan(String value) {
        addCriterion("operator_id <", value, OPERATOR_ID);
        return this;
    }     
    public CompanySkillRelationCriteria andOperatorIdLessThanOrEqualTo(String value) {
        addCriterion("operator_id <=", value, OPERATOR_ID);
        return this;
    }
    public CompanySkillRelationCriteria andOperatorIdIn(List<String> values) {
        addCriterion("operator_id in", values, OPERATOR_ID);
        return this;
    }
    public CompanySkillRelationCriteria andOperatorIdNotIn(List<String> values) {
        addCriterion("operator_id not in", values, OPERATOR_ID);
        return this;
    }
    public CompanySkillRelationCriteria andOperatorIdBetween(String value1, String value2) {
        addCriterion("operator_id between", value1, value2, OPERATOR_ID);
        return this;
    }
    public CompanySkillRelationCriteria andOperatorIdNotBetween(String value1, String value2) {
        addCriterion("operator_id not between", value1, value2, OPERATOR_ID);
        return this;
    }
        public CompanySkillRelationCriteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, OPERATOR_ID);
            return this;
        }

        public CompanySkillRelationCriteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, OPERATOR_ID);
            return this;
        }
    public CompanySkillRelationCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public CompanySkillRelationCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public CompanySkillRelationCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public CompanySkillRelationCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public CompanySkillRelationCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public CompanySkillRelationCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public CompanySkillRelationCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public CompanySkillRelationCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public CompanySkillRelationCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public CompanySkillRelationCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public CompanySkillRelationCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public CompanySkillRelationCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
}

