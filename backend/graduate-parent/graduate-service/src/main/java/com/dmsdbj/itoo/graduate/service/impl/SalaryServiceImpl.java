package com.dmsdbj.itoo.graduate.service.impl;


import com.dmsdbj.itoo.graduate.dao.SalaryDao;
import com.dmsdbj.itoo.graduate.entity.*;
import com.dmsdbj.itoo.graduate.entity.ext.CompanyModel;
import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import com.dmsdbj.itoo.graduate.facade.impl.SalaryFacadeImpl;
import com.dmsdbj.itoo.graduate.mybatisexample.*;
import com.dmsdbj.itoo.graduate.service.*;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import javafx.print.Collation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.dmsdbj.itoo.graduate.facade.impl.CompanyFacadeImpl;
import java.io.Console;
import java.util.ArrayList;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: yyl
 * @create:2017-11-26 16:14:54
 * @DESCRIPTION
 */
@Service("salaryService")
public class SalaryServiceImpl extends BaseServiceImpl<SalaryEntity, SalaryExample> implements SalaryService {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(SalaryFacadeImpl.class);
    //注入字典表Service
    @Autowired
    private DictionaryService dictionaryService;
    //注入个人信息service
    @Autowired
    private PersonalInfoService personalInfoService;
    //注入个人公司关系service
    @Autowired
    private PersonCompanyRelationService personCompanyRelationService;
    //注入公司关系service
    @Autowired
    private CompanyService companyService;

    @Autowired
    private SalaryDao salaryDao;

    @Autowired
    private SalaryService salaryService;
    /**
     * 让BaseServiceImpl获取到Dao
     * @return BaseDao<SalaryEntity, SalaryExample>
     */
    @Override
    public BaseDao<SalaryEntity, SalaryExample> getRealDao() {
        return this.salaryDao;
    }
    /**
     * 查询所有的信息-于云丽-2017-12-31
     * @param page，pageSize
     * @return 薪资实体分页信息PageInfo<SalaryEntity>
     */
    @Override
    public  PageInfo<SalaryEntity> findAll(int page, int pageSize){
        //实例化查询实体
        PageInfo<SalaryEntity> pageInfo;
        PageHelper.startPage(page, pageSize);
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        //加入查询条件和排序条件并进行查询
        byte isDeleteByte = (byte)0;
        salaryCriteria.andIsDeleteEqualTo(isDeleteByte);
        salaryExample.setOrderByClause("create_time desc");
        List <SalaryEntity> list = super.selectByExample(salaryExample);
        //遍历list，查询公司信息,放入薪资中做关联
        for (SalaryEntity aList : list) {
            String relationId = aList.getPcRelationId();
            String companyId = null;
            String companyName = null;
            PersonCompanyRelationEntity personCompanyRelationEntity = new PersonCompanyRelationEntity();
            CompanyModel companyModel = new CompanyModel();
            CompanyEntity companyEntity = new CompanyEntity();
            PersonCompanyRelationExample personCompanyRelationExample = new PersonCompanyRelationExample();
            PersonCompanyRelationCriteria personCompanyRelationCriteria = personCompanyRelationExample.createCriteria();
            personCompanyRelationCriteria.andIdEqualTo(relationId);
            //查询到公司id
            List<PersonCompanyRelationEntity> listPersonCompany = personCompanyRelationService.selectByExample(personCompanyRelationExample);

            if (listPersonCompany.size() != 0) {
                personCompanyRelationEntity = listPersonCompany.get(0);
            }
            companyId = personCompanyRelationEntity.getCompanyId();
            //查询公司信息(回头改成返回公司modal的信息)
            companyEntity = companyService.findById(companyId);
            //实体属性前台无法获取，依旧保存
            //list.get(i).setCompanyEntity(companyEntity);
            //重新加入一个公司名称属性，后续封装成为单独的一个modual实体，不应该放在自动生成的实体中。
//             if(list.get(i).getCompanyEntity()!=null) {
//                 companyName = list.get(i).getCompanyEntity().getCompanyName();
//                 list.get(i).setCompanyName(companyName);
//             }
        }


        pageInfo=new PageInfo<>(list);
        return pageInfo;
}

            /**
             * 根据年级查询个人薪资列表
             * @param :Grade  年级
             * @return : List<SalaryEntity>
             */
            public List<SalaryEntity> selectHighestSalaryByGrade(String grade){
                String userId = null;
                String personCompanyRelation = null;
                Map<String ,List<String>> map =new HashMap<>();
                //根据期数查询出个人id
                List<PersonalInfoEntity> listPerson = personalInfoService.selectPersonByGrade(grade);
                List<String> listPersonCompanyList = null;
                List<SalaryEntity> listSalaryLists = new ArrayList<>();
                List<String> listCompanyPersonRelation = new ArrayList<>();
                //根据个人信息查询个人公司关系列表，连同用户信息放入一个map（这一段没用到？谁改过？yyl）
                if (listPerson.size()>0){
                    for (PersonalInfoEntity person : listPerson) {
                        userId = person.getId();
                        listPersonCompanyList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(userId);
                        map.put(userId, listPersonCompanyList);
                    }
                }
                //根据个人信息查询个人公司关系列表，将个人公司关系列表放入一个list
                for (List<String> personCompanyList : map.values()){
                    for (String aPersonCompanyList : personCompanyList) listPersonCompanyList.add(aPersonCompanyList);
                }

                //根据个人公司关系查询平均工资
                 listSalaryLists = this.selecthighestSalaryByRelatoinId(listPersonCompanyList);
                //返回对应的薪资列表和姓名
                return listSalaryLists;

         }

    /**
     * 根据年级查询个人薪资列表
     * @param :Grade  年级
     * @return ： 薪资实体列表List<SalaryEntity>
     */
    public List<SalaryEntity> selectLowestSalaryByGrade(String grade){
        String userId = null;
        String personCompanyRelation = null;
        Map<String ,List<String>> map =new HashMap<>();
        //根据期数查询出个人id
        List<PersonalInfoEntity> listPerson = personalInfoService.selectPersonByGrade(grade);
        List<String> listPersonCompanyList = null;
        List<SalaryEntity> listSalaryLists = new ArrayList<>();
        List<String> listCompanyPersonRelation = new ArrayList<>();
        if (listPerson.size()>0){
            for (PersonalInfoEntity person : listPerson) {
                userId = person.getId();
                listPersonCompanyList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(userId);
                map.put(userId, listPersonCompanyList);
            }
        }
        //所有的信息放入一个list
        for (List<String> personCompanyList : map.values()){
            for (String aPersonCompanyList : personCompanyList) listPersonCompanyList.add(aPersonCompanyList);
        }

        //根据个人公司关系查询最低工资
        listSalaryLists = this.selectlowestSalaryByRelatoinId(listPersonCompanyList);
        //返回对应的薪资列表和姓名
        return listSalaryLists;



    }

    /**
     * 根据年级名称查询个人薪资列表
     * @param :Grade  年级
     * @return ：当前年级平均薪资Double
     */
    public Double selectAverageSalaryByGrade(String grade){
        String userId = null;
        String personCompanyRelation = null;
        Map<String ,List<String>> map =new HashMap<>();
        Double averageSalary =0.0;
        //根据期数查询出个人id
        List<PersonalInfoEntity> listPerson = personalInfoService.selectPersonByGrade(grade);
        List<String> listPersonCompanyList = null;
        List<SalaryEntity> listSalaryLists = new ArrayList<>();
        List<String> listCompanyPersonRelation = new ArrayList<>();
        //根据个人信息查询出个人薪资关系，将个人公司信息放入一个list，list连同个人id放入一个map
        if (listPerson.size()>0){
            for (PersonalInfoEntity person : listPerson) {
                userId = person.getId();
                listPersonCompanyList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(userId);
                map.put(userId, listPersonCompanyList);
            }
        }
        //个人公司关系放入一个list
        for (List<String> personCompanyList : map.values()){
            for (String aPersonCompanyList : personCompanyList) listPersonCompanyList.add(aPersonCompanyList);
        }

        //根据个人公司关系查询平均工资
        averageSalary = this.selectAverageSalaryByRelatoinId(listPersonCompanyList);
        //返回对应的薪资列表和姓名
        return averageSalary;



    }

        /**
         * 根据期数id查询本年度最高薪资-于云丽-2018-01-14
         * @param ：年级 gradeId
         * @return ：最高薪资值
         */
        public  Double  selectHighSalaryByGradeId(String gradeId){

            String userId = null;
            String personCompanyRelation = null;
            //根据期数查询出个人id
            List <SalaryEntity> highestSalaryList = new ArrayList<>();
            String gradeName = dictionaryService.findDictionaryNameById(gradeId);
            List<PersonalInfoEntity> listPerson = personalInfoService.selectPersonByGrade(gradeName);
            List<String> listPersonCompanyList = new ArrayList<>();
            Double highestSalary = 0.0;
            //根据个人信息查询个人公司关系列表，并获取最高薪资人员列表
            if (listPerson.size()>0){
                for (PersonalInfoEntity person : listPerson) {
                    userId = person.getId();
                    listPersonCompanyList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(userId);
                    //根据个人公司关系查询所有人最高薪资
                    Date now = new Date(2017 - 12 - 31);//临时数据
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//改日期格式
                    String date = dateFormat.format(now);
                    //获取所查询人数的最高薪资
                    highestSalaryList = this.selecthighestSalaryByRelatoinId(listPersonCompanyList, now);

                }
            }
                  //获取最高薪资值
                   highestSalary =highestSalaryList.get(0).getSalary();
                   return highestSalary;
        }




        /**
         * 根据期数id查询本年度最低薪资-于云丽-2018-01-14
         * @param  gradeId
         * @return 最低薪资值Double类型
         */
        public  Double  selectLowestSalaryByGradeId(String gradeId){

            String userId = null;
            String personCompanyRelation = null;
            //根据期数查询出个人id
            List <SalaryEntity> lowestSalaryList = new ArrayList<>();
            String gradeName = dictionaryService.findDictionaryNameById(gradeId);
            List<PersonalInfoEntity> listPerson = personalInfoService.selectPersonByGrade(gradeName);
            List<String> listPersonCompanyList = new ArrayList<>();
            Double lowestSalary = 0.0;
            //根据个人信息查询所有人最低薪资
            if (listPerson.size()>0){
                for (PersonalInfoEntity person : listPerson) {
                    userId = person.getId();
                    listPersonCompanyList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(userId);
                    //根据个人公司关系查询所有人最高薪资
                    Date now = new Date(2017 - 12 - 31);//临时数据
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//改日期格式
                    String date = dateFormat.format(now);
                    //获取所查询人数的最高薪资
                    lowestSalaryList = this.selectlowestSalaryByRelatoinId(listPersonCompanyList, now);
                }
            }
            lowestSalary =lowestSalaryList.get(0).getSalary();
            return lowestSalary;
        }


    /**
     * 根据期数id查询本年度平均薪资-于云丽-2018-01-14
     * @param  gradeId
     * @return 平均薪资值Double类型
     */
    public  Double  selectAverageSalaryByGradeId(String gradeId){
        String userId = null;
        String personCompanyRelation = null;
        //根据期数查询出个人id
        List <SalaryEntity> averageSalaryList = new ArrayList<>();
        String gradeName = dictionaryService.findDictionaryNameById(gradeId);
        List<PersonalInfoEntity> listPerson = personalInfoService.selectPersonByGrade(gradeName);
        List<String> listPersonCompanyList = new ArrayList<>();
        Double averageSalary = 0.0;
        //根据个人信息查询所有人平均薪资
        if (listPerson.size()>0){
            for (PersonalInfoEntity person : listPerson) {
                userId = person.getId();
                listPersonCompanyList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(userId);
                //根据个人公司关系查询所有人最高薪资
                Date now = new Date(2017 - 12 - 31);//临时数据
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//改日期格式
                String date = dateFormat.format(now);
                //获取所查询人数的平均薪资
                averageSalary = this.selectAverageSalaryByRelatoinId(listPersonCompanyList, now);
            }
        }
        return averageSalary;
    }

    /**
     * 根据人员信息查询薪资实体-于云丽-2018-1-14
     * @param  personId
     * @return 薪资实体list<SalaryEntity>
     */
    public List<SalaryEntity>  selectByPersonId(String personId){
        //String personCompanyRelation = "NdDWVEroLj5yeGyCG75reM";
        List<String> listPersonCompanyList = null;
        List<SalaryEntity> listSalaryList = null;
        PersonalInfoExample personalInfoExample=new PersonalInfoExample();
        PersonalInfoCriteria personalInfoCriteria=personalInfoExample.createCriteria();
        personalInfoCriteria.andIdEqualTo(personId);
        List<PersonalInfoEntity> listPerson = personalInfoService.selectByExample(personalInfoExample);
        if (listPerson.size()>0){
            for (PersonalInfoEntity person : listPerson) {
                String userId = person.getId();
                listPersonCompanyList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(userId);
                //根据个人公司关系查询个人薪资
                listSalaryList = this.selectSalaryInfoByRelationIds(listPersonCompanyList);
            }
        }
        return listSalaryList;
    }

    @Override
    /**
     * 增加薪资信息-于云丽-2018-1-14
     * @param  ：薪资实体record
     * @return ：成功则返回影响行数，否则返回-1
     */
    public int insert(SalaryEntity record) {
        /*
          如果薪资信息不为空则返回插入返回的结果，如果薪资信息为空，则返回-1
         */
        if (record != null) {
            return super.insert(record);
        } else {
            return -1;
        }
    }

    @Override
    /**
     * 删除薪资信息-于云丽-2018-1-14
     * @param  ：薪资实体record
     * @return ：成功则返回影响行数，否则返回-1
     */
    public int deleteById(String id, String operator) {
        /*
          如果薪资信息不为空则返回删除返回的结果，如果薪资信息为空，则返回-1
         */
        if (id != null && operator != null) {
            return super.deleteById(id, operator);
        } else {
            return -1;
        }

    }

    @Override
    /**
     * 更新薪资信息-于云丽-2018-1-14
     * @param  ：薪资实体record
     * @return ：成功则返回影响行数，否则返回-1
     */
    public int updateById(SalaryEntity record) {

        /*
          如果薪资信息不为空则返回的结果，如果薪资信息为空，则返回-1
         */
        if (record != null) {
            return super.updateById(record);
        } else {
            return -1;
        }
    }

    /**
     * 根据公司和个人关系列表Id查询薪资相关信息-于云丽-2017年12月4日
     * @param relationIdList 公司和个人关系表Ids
     * @return 薪资实体list<SalaryEntity>
     */
    @Override
    public List<SalaryEntity> selectSalaryInfoByRelationIds(List<String> relationIdList) {

        //查询信息相关信息
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andPcRelationIdIn(relationIdList);
        salaryCriteria.andIsDeleteEqualTo((byte) 0);   //添加查询条件，isdelete为0-haoguiting-2018年3月7日
        salaryExample.setOrderByClause("salary_change_time  ASC");
        return this.selectByExample(salaryExample);
    }
    /**
     * 根据公司和个人关系表Id查询薪资相关信息-于云丽-2017年12月4日
     * @param relationIdList 公司和个人关系表Ids，分页信息page，pageSize
     * @return 薪资实体list
     */
    @Override
    public List<SalaryEntity> pageSelectSalaryInfoByRelationIds(List<String> relationIdList,int page,int pageSize) {
        PageHelper.startPage(page,pageSize);
        //查询信息相关信息
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andPcRelationIdIn(relationIdList);
        salaryCriteria.andIsDeleteEqualTo((byte) 0);   //添加查询条件，isdelete为0-haoguiting-2018年3月7日
        salaryExample.setOrderByClause("salary_change_time  ASC");
//        List<SalaryEntity> salaryEntityList = this.selectByExample(salaryExample);
        PageInfo<SalaryEntity> salaryEntityPageInfo = new PageInfo<>(this.selectByExample(salaryExample));
        return salaryEntityPageInfo.getList();
    }

    /**
     * 根据公司和个人关系表Id查询薪资相关信息-yyl-2017年12月4日
     * @param ：relationId 公司和个人关系表Id
     * @return ：薪资实体List<SalaryEntity>
     */
    @Override
    public List<SalaryEntity> selectSalaryInfoByRelationId(String relationId) {
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andPcRelationIdEqualTo(relationId);
        return super.selectByExample(salaryExample);
    }

    /**
     * 根据公司和个人关系表Id删除薪资相关信息-yyl-2017年12月4日     *
     * @param ：relationId 公司和个人关系表Id,操作用户operator
     * @param ：operator
     * @return：影响行数
     */
    @Override
    public int deleteSalaryInfoByRelationId(String relationId, String operator) {
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andPcRelationIdEqualTo(relationId);
        return super.deleteByExample(salaryExample, operator);
    }

    /**
     * 根据公司和个人关系表Ids删除薪资相关信息-yyl-2017年12月4日
     * @param ：relationIdList 公司和个人关系表Ids,操作用户operator
     * @param ：操作员operator
     * @return：影响行数
     */
    @Override
    public int deleteSalaryInfoByRelationIds(List<String> relationIdList, String operator) {
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andPcRelationIdIn(relationIdList);
        return super.deleteByExample(salaryExample, operator);
    }



    /**
     * 根据关系Id、年限查询薪资-时间列表
     * @param :Name  姓名
     * @param ：date 年份
     * @return：薪资实体List<SalaryEntity>
     */
    @Override
    public List<SalaryEntity> selectByPcRelation(String pcRelation, Date date) {
        //获取本年度薪资排序列表
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andSalaryChangeTimeLessThanOrEqualTo(date);
        salaryCriteria.andPcRelationIdEqualTo(pcRelation);
        return super.selectByExample(salaryExample);
    }
    /**
     * 根据薪资相关信息查询薪资实体列表
     * @param :salaryExample
     * @return：薪资实体List<SalaryEntity>
     */
    @Override
    public List<SalaryEntity> selectByExample(SalaryExample salaryExample) {
        return super.selectByExample(salaryExample);
    }

    /**
     * 根据年级查询个人薪资列表
     * @param :grade  年级
     * @return：薪资实体List<SalaryEntity>
     */
        public List<SalaryEntity>  selectSalaryByGrade(String grade){
        String userId = null;
        String personCompanyRelation = null;
        Map<String ,List<SalaryEntity>> map =new HashMap<>();
        //根据期数查询出个人id
        List<PersonalInfoEntity> listPerson = personalInfoService.selectPersonByGrade(grade);
        List<String> listPersonCompanyList = null;
        List<SalaryEntity> listSalaryList = new ArrayList<>();
        List<SalaryEntity> listSalaryLists = new ArrayList<>();
        if (listPerson.size()>0){
            for (PersonalInfoEntity person : listPerson) {
                userId = person.getId();
                listPersonCompanyList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(userId);
                //根据个人公司关系查询个人薪资
                listSalaryList = this.selectSalaryInfoByRelationIds(listPersonCompanyList);

                map.put(userId, listSalaryList);
            }
        }
            //所有的信息放入一个list
            for (List<SalaryEntity> salaryLists : map.values()){
                for (SalaryEntity salaryList : salaryLists) listSalaryLists.add(salaryList);
                }

        //返回对应的薪资列表和姓名
        return listSalaryLists;
    }



    /**
     * 根据年级查询个人薪资列表
     * @param :Name  年级
     * @return： 薪资实体和用户列表Map<String ,List<SalaryEntity>>
     */
    public Map<String ,List<SalaryEntity>>  selectByGrade(String grade){
        String userId = null;
        String personCompanyRelation = null;
        //根据期数查询出个人id
        List<PersonalInfoEntity> listPerson = personalInfoService.selectPersonByGrade(grade);
        List<String> listPersonCompanyList = null;
        List<SalaryEntity> listSalaryList = null;
        Map<String ,List<SalaryEntity>> listPersonSalaryMap = new HashMap<>();
        //通过id查询出个人和公司关系(多个同名人员id)
        if (listPerson.size()>0){
            for (PersonalInfoEntity person : listPerson) {
                userId = person.getId();
                listPersonCompanyList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(userId);
                //根据个人公司关系查询个人薪资
                listSalaryList = this.selectSalaryInfoByRelationIds(listPersonCompanyList);
                listPersonSalaryMap.put(userId, listSalaryList);
            }
        }
        //返回对应的薪资列表和姓名
        return listPersonSalaryMap;
    }


    /**
     * 根据年级查询个人薪资列表
     * @param :Name  姓名
     * @return： 薪资实体列表List<SalaryEntity>
     */
    public  List<SalaryEntity>  selectByName(String name){
        String userId = null;
        String personCompanyRelation = null;
        //根据姓名查询出个人id
        List<PersonalInfoEntity> listPerson = personalInfoService.selectPersonByName(name);
        List<String> listPersonCompanyList = null;
        List<SalaryEntity> listSalaryList = null;

         //目前不考虑重名情况

            PersonalInfoEntity person=listPerson.get(0);
            userId = person.getId();
            listPersonCompanyList = personCompanyRelationService.selectPersonCompanyRelationIdsByPersonId(userId);
            //根据个人公司关系查询个人薪资
            listSalaryList = this.selectSalaryInfoByRelationIds(listPersonCompanyList);
        //返回对应的薪资列表和姓名
        return listSalaryList;
    }


    /**
     * 根据个人公司关系查询个人薪资列表
     * @param :pcRelation  个人公司关系
     * @return：薪资实体列表List<SalaryEntity>
     */
    public List<SalaryEntity> selectByPcRelation(String pcRelation) {
        //获取本年度薪资排序列表
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andPcRelationIdEqualTo(pcRelation);
        return super.selectByExample(salaryExample);
    }



    /**
     * 根据期数相应人员列表查询本期最高工资
     * @param :个人公司关系列表relationIds
     * @param ：日期date
     * @return ：最高工资的记录列表List
     */
    @Override
    public List selecthighestSalaryByRelatoinId(List<String> relationIds, Date date) {

        //获取本年度薪资排序列表
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andSalaryChangeTimeLessThanOrEqualTo(date);
        List<SalaryEntity> list = super.selectByExample(salaryExample);
        //对重复的个人按照薪资时间节点排序
        Map<String,SalaryEntity> map = new HashMap();
        for (SalaryEntity salaryEntity : list) {
            map.put(salaryEntity.getPcRelationId(), salaryEntity);
            SalaryEntity salaryEntity1 = new SalaryEntity();
            if (map.containsKey(salaryEntity.getPcRelationId())) {
                salaryEntity1=map.get(salaryEntity.getPcRelationId());
                if(salaryEntity1.getSalaryChangeTime().before(salaryEntity.getSalaryChangeTime())){
                    map.put(salaryEntity.getPcRelationId(),salaryEntity);
                }
            }
        }
        //将不重复发的薪资信息进行降序排序
        List highestList = new ArrayList() ;
        Iterator<Map.Entry<String ,SalaryEntity>> it = map.entrySet().iterator();
        List <Double> listsort= new ArrayList();
        Double highest=0.0;
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            listsort.add(entry.getValue().getSalary());
        }
        Collections.reverse(listsort);
        //获取最低的薪资信息，并将所有的最高薪资列表放入highest中，返回最高薪资列表
        highest = list.get(0).getSalary();
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            if (Objects.equals(highest, entry.getValue().getSalary())){
                highestList.add(entry.getValue()) ;
            }
        }
        return highestList;
    }

    /**
     * 根据期数相应人员列表查询本期最高工资
     * @param :relationIds
     * @return 最高工资的记录列表
     */
    @Override
    public List selecthighestSalaryByRelatoinId(List<String> relationIds ) {

        //获取本年度薪资排序列表
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        List<SalaryEntity> list = super.selectByExample(salaryExample);
        //对重复的个人按照薪资时间节点排序
        Map<String,SalaryEntity> map = new HashMap();
        for (SalaryEntity salaryEntity : list) {
            map.put(salaryEntity.getPcRelationId(), salaryEntity);
            SalaryEntity salaryEntity1 = new SalaryEntity();
            if (map.containsKey(salaryEntity.getPcRelationId())) {
                salaryEntity1=map.get(salaryEntity.getPcRelationId());
                if(salaryEntity1.getSalaryChangeTime().before(salaryEntity.getSalaryChangeTime())){
                    map.put(salaryEntity.getPcRelationId(),salaryEntity);
                }
            }
        }
        //将不重复发的薪资信息进行降序排序
        List highestList = new ArrayList() ;
        Iterator<Map.Entry<String ,SalaryEntity>> it = map.entrySet().iterator();
        List <Double> listsort= new ArrayList();
        Double highest=0.0;
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            listsort.add(entry.getValue().getSalary());
        }
        Collections.reverse(listsort);
        //获取最低的薪资信息，并将所有的最高薪资列表放入highest中，返回最高薪资列表
        highest = list.get(0).getSalary();
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            if (Objects.equals(highest, entry.getValue().getSalary())){
                highestList.add(entry.getValue()) ;
            }
        }
        return highestList;
    }
    /**
     * 根据期数查询本期最低工资(带年份)
     * @param ：个人公司关系列表relationIds
     * @param ：date
     * @return ：最低的记录列表
     */
    @Override
    public List selectlowestSalaryByRelatoinId(List<String> relationIds, Date date) {
        //获取本年度薪资排序列表
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andSalaryChangeTimeLessThanOrEqualTo(date);
        salaryCriteria.andPcRelationIdIn(relationIds);
        List<SalaryEntity> list = super.selectByExample(salaryExample);
        //对重复的个人按照薪资时间节点排序
        Map<String,SalaryEntity> map = new HashMap();
        for (SalaryEntity salaryEntity : list) {
            map.put(salaryEntity.getPcRelationId(), salaryEntity);
            SalaryEntity salaryEntity1 = new SalaryEntity();
            if (map.containsKey(salaryEntity.getPcRelationId())) {
                salaryEntity1=map.get(salaryEntity.getPcRelationId());
                if(salaryEntity1.getSalaryChangeTime().before(salaryEntity.getSalaryChangeTime())){
                    map.put(salaryEntity.getPcRelationId(),salaryEntity);
                }
            }
        }
        //将不重复发的薪资信息进行升序排序
        List lowestList = new ArrayList() ;
        Iterator<Map.Entry<String ,SalaryEntity>> it = map.entrySet().iterator();
        List <Double> listsort= new ArrayList();
        //List<Double>  doubleList= new ArrayList<Double>();

        Double lowest=0.0;
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            listsort.add(entry.getValue().getSalary());
        }
        Collections.sort(listsort);
        //获取最低的薪资信息，并将所有的最低薪资列表放入lowestlist中，返回最低薪资列表
        lowest = list.get(0).getSalary();
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            if (Objects.equals(lowest, entry.getValue().getSalary())){
                lowestList.add(entry.getValue()) ;
            }
        }
        return lowestList;
    }
    /**
     * 根据期数查询本期最低工资(带年份)
     * @param ：个人公司关系列表relationIds
     * @return ：最低的记录列表
     */
    @Override
    public List selectlowestSalaryByRelatoinId(List<String> relationIds ) {
        //获取本年度薪资排序列表
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andPcRelationIdIn(relationIds);
        List<SalaryEntity> list = super.selectByExample(salaryExample);
        //对重复的个人按照薪资时间节点排序
        Map<String,SalaryEntity> map = new HashMap();
        for (SalaryEntity salaryEntity : list) {
            map.put(salaryEntity.getPcRelationId(), salaryEntity);
            SalaryEntity salaryEntity1 = new SalaryEntity();
            if (map.containsKey(salaryEntity.getPcRelationId())) {
                salaryEntity1=map.get(salaryEntity.getPcRelationId());
                if(salaryEntity1.getSalaryChangeTime().before(salaryEntity.getSalaryChangeTime())){
                    map.put(salaryEntity.getPcRelationId(),salaryEntity);
                }
            }
        }
        //将不重复发的薪资信息进行升序排序
        List lowestList = new ArrayList() ;
        Iterator<Map.Entry<String ,SalaryEntity>> it = map.entrySet().iterator();
        List <Double> listsort= new ArrayList();
        Double lowest=0.0;
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            listsort.add(entry.getValue().getSalary());
        }
        Collections.sort(listsort);
        //获取最低的薪资信息，并将所有的最低薪资列表放入lowestlist中，返回最低薪资列表
        lowest = list.get(0).getSalary();
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            if (Objects.equals(lowest, entry.getValue().getSalary())){
                lowestList.add(entry.getValue()) ;
            }
        }
        return lowestList;
    }
    /**
     * 根据期数查询本期平均工资
     * @param relationIds
     * @param date
     * @return 平均工资数值double类型
     */
    @Override
    public Double selectAverageSalaryByRelatoinId(List<String> relationIds, Date date) {
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andPcRelationIdIn(relationIds);
        salaryCriteria.andSalaryChangeTimeLessThanOrEqualTo(date);
        List<SalaryEntity> list = super.selectByExample(salaryExample);

        Map<String,SalaryEntity> map = new HashMap();
        Double sum = 0.0;
        for (SalaryEntity salaryEntity : list) {
            map.put(salaryEntity.getPcRelationId(), salaryEntity);
            SalaryEntity salaryEntity1 = new SalaryEntity();
            if (map.containsKey(salaryEntity.getPcRelationId())) {
                salaryEntity1=map.get(salaryEntity.getPcRelationId());
                if(salaryEntity1.getSalaryChangeTime().before(salaryEntity.getSalaryChangeTime())){
                    map.put(salaryEntity.getPcRelationId(),salaryEntity);
                }
            }
        }

        for (Map.Entry<String, SalaryEntity> entry : map.entrySet()) {
            sum += entry.getValue().getSalary();
        }
        return sum/map.size();

    }
    /**
     * 根据期数查询本期平均工资
     * @param ：个人公司关系列表relationIds
     * @return 平均工资的记录
     */
    @Override
    public Double selectAverageSalaryByRelatoinId(List<String> relationIds ) {
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andPcRelationIdIn(relationIds);
        List<SalaryEntity> list = super.selectByExample(salaryExample);

        Map<String,SalaryEntity> map = new HashMap();
        Double sum = 0.0;
        for (SalaryEntity salaryEntity : list) {
            map.put(salaryEntity.getPcRelationId(), salaryEntity);
            SalaryEntity salaryEntity1 = new SalaryEntity();
            if (map.containsKey(salaryEntity.getPcRelationId())) {
                salaryEntity1=map.get(salaryEntity.getPcRelationId());
                if(salaryEntity1.getSalaryChangeTime().before(salaryEntity.getSalaryChangeTime())){
                    map.put(salaryEntity.getPcRelationId(),salaryEntity);
                }
            }
        }

        for (Map.Entry<String, SalaryEntity> entry : map.entrySet()) {
            sum += entry.getValue().getSalary();
        }
        return sum/map.size();

    }

    /**
     * 整体本年度最高薪资统计
     * @param date
     * @return 本年度全体中的工资和期数List
     */
    @Override
    public List selecthighestSalaryofAll(Date date) {
        //获取本年度薪资排序列表
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andSalaryChangeTimeLessThanOrEqualTo(date);
        List<SalaryEntity> list = super.selectByExample(salaryExample);
        //对重复的个人按照薪资时间节点排序
        Map<String,SalaryEntity> map = new HashMap();
        for (SalaryEntity salaryEntity : list) {
            map.put(salaryEntity.getPcRelationId(), salaryEntity);
            SalaryEntity salaryEntity1 = new SalaryEntity();
            if (map.containsKey(salaryEntity.getPcRelationId())) {
                salaryEntity1=map.get(salaryEntity.getPcRelationId());
                if(salaryEntity1.getSalaryChangeTime().before(salaryEntity.getSalaryChangeTime())){
                    map.put(salaryEntity.getPcRelationId(),salaryEntity);
                }
            }
        }
        //将不重复发的薪资信息进行降序排序
        List highestList = new ArrayList() ;
        Iterator<Map.Entry<String ,SalaryEntity>> it = map.entrySet().iterator();
        List <Double> listsort= new ArrayList();
        Double lowest=0.0;
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            listsort.add(entry.getValue().getSalary());
        }
        Collections.reverse(listsort);
        //获取最低的薪资信息，并将所有的最低薪资列表放入lowestlist中，返回最低薪资列表
        lowest = list.get(0).getSalary();
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            if (Objects.equals(lowest, entry.getValue().getSalary())){
                highestList.add(entry.getValue()) ;
            }
        }
        return highestList;
    }

    /**
     * 整体本年度最低薪资统计
     * @param ：date
     * @return ：本年度最低工资和期数List
     */
    @Override
    public List selectlowestSalaryofAll(Date date) {
        //获取本年度薪资排序列表
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andSalaryChangeTimeLessThanOrEqualTo(date);
        List<SalaryEntity> list = super.selectByExample(salaryExample);
        //对重复的个人按照薪资时间节点排序
        Map<String,SalaryEntity> map = new HashMap();
        for (SalaryEntity salaryEntity : list) {
            map.put(salaryEntity.getPcRelationId(), salaryEntity);
            SalaryEntity salaryEntity1 = new SalaryEntity();
            if (map.containsKey(salaryEntity.getPcRelationId())) {
                salaryEntity1=map.get(salaryEntity.getPcRelationId());
                if(salaryEntity1.getSalaryChangeTime().before(salaryEntity.getSalaryChangeTime())){
                    map.put(salaryEntity.getPcRelationId(),salaryEntity);
                }
            }
        }
        //将不重复发的薪资信息进行升序排序
        List lowestList = new ArrayList() ;
        Iterator<Map.Entry<String ,SalaryEntity>> it = map.entrySet().iterator();
        List <Double> listsort= new ArrayList();
        Double lowest=0.0;
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            listsort.add(entry.getValue().getSalary());
        }
        Collections.sort(listsort);
        //获取最低的薪资信息，并将所有的最低薪资列表放入lowestlist中，返回最低薪资列表
        lowest = list.get(0).getSalary();
        while(it.hasNext()){
            Map.Entry<String ,SalaryEntity> entry = it.next();
            if (Objects.equals(lowest, entry.getValue().getSalary())){
                lowestList.add(entry.getValue()) ;
            }
        }
        return lowestList;

    }

    /**
     * 整体本年度薪资统计
     * @param date
     * @return 本年度平均工资值
     */
    @Override
    public Double selectAverageSalaryOfAll(Date date) {
        SalaryExample salaryExample = new SalaryExample();
        SalaryCriteria salaryCriteria = salaryExample.createCriteria();
        salaryCriteria.andSalaryChangeTimeLessThanOrEqualTo(date);
        List<SalaryEntity> list = super.selectByExample(salaryExample);

        Map<String,SalaryEntity> map = new HashMap();
        Double sum = 0.0;
        for (SalaryEntity salaryEntity : list) {
            map.put(salaryEntity.getPcRelationId(), salaryEntity);
            SalaryEntity salaryEntity1 = new SalaryEntity();
            if (map.containsKey(salaryEntity.getPcRelationId())) {
                salaryEntity1=map.get(salaryEntity.getPcRelationId());
                if(salaryEntity1.getSalaryChangeTime().before(salaryEntity.getSalaryChangeTime())){
                    map.put(salaryEntity.getPcRelationId(),salaryEntity);
                }
            }
        }

        for (Map.Entry<String, SalaryEntity> entry : map.entrySet()) {
            sum += entry.getValue().getSalary();
        }
        return sum/map.size();

    }

    /**
     * 时间转换方法
     *
     * @param timeStamp@return 转换时间
     */
    @Override
    public java.util.Date covertTimeStampToDate(java.util.Date timeStamp) {
        return null;
    }

    /**
     *根据毕业生id查询薪资公司相关信息-haoguiting--2018年3月7日
     * @param ：userId 毕业生id
     * @param ：page 页码
     * @param ：pageSize 页大小
     * @return：公司薪资实体 PageInfo<CompanySalaryModel>
     */
    @Override
    public PageInfo<CompanySalaryModel> PageSelectSalaryByUserId(String userId,int page, int pageSize){
        PageHelper.startPage(page,pageSize);
        //方法一：调用公司关系表方法，逻辑在公司service实现
//        List<CompanySalaryModel> companySalaryModelList=companyService.selectCompanySalaryByUserId(userId,page,pageSize);

        //方法二：sql语句在salary里，不调用公司关系方法
        List<CompanySalaryModel> companySalaryModel=salaryDao.selectSalaryByUserId(userId);

        //方法三：调用公司关系方法，逻辑在salary的service实现
        //1.根据用户id查询关系表中个人与公司的关系id
//        List<CompanyModel> companyModelList=companyService.selectCompanysByUserId(userId);
//        List<String> relationList=companyModelList.stream().map(m->m.getPersonCompanyId().toString()).collect(Collectors.toList());
//        //2.根据关系id查询薪资表，获得薪资数据
//        List<SalaryEntity> salaryEntityList=salaryService.selectSalaryInfoByRelationIds(relationList);

        return new PageInfo<>(companySalaryModel);

    }

}






