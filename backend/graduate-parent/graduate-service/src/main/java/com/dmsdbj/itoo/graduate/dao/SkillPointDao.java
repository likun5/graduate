package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.SkillPointEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.SkillPointExample;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *@author :  徐玲博
 *create : 2017-11-21 21:41:58
 *DESCRIPTION :  技术点Dao
 */
@Repository
public interface SkillPointDao extends BaseDao<SkillPointEntity,SkillPointExample> {

    /**
     * 根据技术方向查询技术点-徐玲博-2017-11-21 14:42:51
     * @param  pid 技术方向
     * @return 技术方向列表
     */
    List<SkillPointEntity> selectParentSkillByPid( String pid);

    /**
     * 查询技术方向-徐玲博-2017-11-21 14:42:51
     * @return 技术方向list
     */
    List<SkillPointEntity> selectTechnicalDirection();


    /**
     * 分页查询根据技术方向查询技术点-haoguiting-2018年2月22日
     * @return 技术点list
     */
    List<SkillPointEntity> PageSelectSkillByDirectionId(String pid);
}
