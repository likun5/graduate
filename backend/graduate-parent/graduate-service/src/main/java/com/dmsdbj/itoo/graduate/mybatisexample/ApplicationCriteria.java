package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class ApplicationCriteria extends GeneratedCriteria<ApplicationCriteria> implements Serializable{

    protected ApplicationCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String APPLICATION_NAME = "applicationName";   
    private static final String TYPE = "type";   
    private static final String APPLICATION_PARENT_ID = "applicationParentId";   
    private static final String APPLICATION_URL = "applicationUrl";   
    private static final String PERMISSION = "permission";   
    private static final String PERMISSION_NAME = "permissionName";   
    private static final String IS_MANAGED_MODULE = "isManagedModule";   
    private static final String DESCRIPTION = "description";   
    private static final String SORT_ID = "sortId";   
    private static final String APPLICATION_ICON = "applicationIcon";   
    private static final String APPLICATION_PARENT_IDS = "applicationParentIds";   
    private static final String IS_STATE = "isState";   

    public ApplicationCriteria andApplicationNameIsNull() {
        addCriterion("application_name is null");
        return this;
    }

    public ApplicationCriteria andApplicationNameIsNotNull() {
        addCriterion("application_name is not null");
        return this;
    }
    public ApplicationCriteria andApplicationNameEqualTo(String value) {
        addCriterion("application_name =", value, APPLICATION_NAME);
        return this;
    }
    public ApplicationCriteria andApplicationNameNotEqualTo(String value) {
        addCriterion("application_name <>", value, APPLICATION_NAME);
        return this;
    }    
    public ApplicationCriteria andApplicationNameGreaterThan(String value) {
        addCriterion("application_name >", value, APPLICATION_NAME);
        return this;
    }    
    public ApplicationCriteria andApplicationNameGreaterThanOrEqualTo(String value) {
        addCriterion("application_name >=", value, APPLICATION_NAME);
        return this;
    }    
    public ApplicationCriteria andApplicationNameLessThan(String value) {
        addCriterion("application_name <", value, APPLICATION_NAME);
        return this;
    }     
    public ApplicationCriteria andApplicationNameLessThanOrEqualTo(String value) {
        addCriterion("application_name <=", value, APPLICATION_NAME);
        return this;
    }
    public ApplicationCriteria andApplicationNameIn(List<String> values) {
        addCriterion("application_name in", values, APPLICATION_NAME);
        return this;
    }
    public ApplicationCriteria andApplicationNameNotIn(List<String> values) {
        addCriterion("application_name not in", values, APPLICATION_NAME);
        return this;
    }
    public ApplicationCriteria andApplicationNameBetween(String value1, String value2) {
        addCriterion("application_name between", value1, value2, APPLICATION_NAME);
        return this;
    }
    public ApplicationCriteria andApplicationNameNotBetween(String value1, String value2) {
        addCriterion("application_name not between", value1, value2, APPLICATION_NAME);
        return this;
    }
        public ApplicationCriteria andApplicationNameLike(String value) {
            addCriterion("application_name like", value, APPLICATION_NAME);
            return this;
        }

        public ApplicationCriteria andApplicationNameNotLike(String value) {
            addCriterion("application_name not like", value, APPLICATION_NAME);
            return this;
        }
    public ApplicationCriteria andTypeIsNull() {
        addCriterion("type is null");
        return this;
    }

    public ApplicationCriteria andTypeIsNotNull() {
        addCriterion("type is not null");
        return this;
    }
    public ApplicationCriteria andTypeEqualTo(String value) {
        addCriterion("type =", value, TYPE);
        return this;
    }
    public ApplicationCriteria andTypeNotEqualTo(String value) {
        addCriterion("type <>", value, TYPE);
        return this;
    }    
    public ApplicationCriteria andTypeGreaterThan(String value) {
        addCriterion("type >", value, TYPE);
        return this;
    }    
    public ApplicationCriteria andTypeGreaterThanOrEqualTo(String value) {
        addCriterion("type >=", value, TYPE);
        return this;
    }    
    public ApplicationCriteria andTypeLessThan(String value) {
        addCriterion("type <", value, TYPE);
        return this;
    }     
    public ApplicationCriteria andTypeLessThanOrEqualTo(String value) {
        addCriterion("type <=", value, TYPE);
        return this;
    }
    public ApplicationCriteria andTypeIn(List<String> values) {
        addCriterion("type in", values, TYPE);
        return this;
    }
    public ApplicationCriteria andTypeNotIn(List<String> values) {
        addCriterion("type not in", values, TYPE);
        return this;
    }
    public ApplicationCriteria andTypeBetween(String value1, String value2) {
        addCriterion("type between", value1, value2, TYPE);
        return this;
    }
    public ApplicationCriteria andTypeNotBetween(String value1, String value2) {
        addCriterion("type not between", value1, value2, TYPE);
        return this;
    }
        public ApplicationCriteria andTypeLike(String value) {
            addCriterion("type like", value, TYPE);
            return this;
        }

        public ApplicationCriteria andTypeNotLike(String value) {
            addCriterion("type not like", value, TYPE);
            return this;
        }
    public ApplicationCriteria andApplicationParentIdIsNull() {
        addCriterion("application_parent_id is null");
        return this;
    }

    public ApplicationCriteria andApplicationParentIdIsNotNull() {
        addCriterion("application_parent_id is not null");
        return this;
    }
    public ApplicationCriteria andApplicationParentIdEqualTo(String value) {
        addCriterion("application_parent_id =", value, APPLICATION_PARENT_ID);
        return this;
    }
    public ApplicationCriteria andApplicationParentIdNotEqualTo(String value) {
        addCriterion("application_parent_id <>", value, APPLICATION_PARENT_ID);
        return this;
    }    
    public ApplicationCriteria andApplicationParentIdGreaterThan(String value) {
        addCriterion("application_parent_id >", value, APPLICATION_PARENT_ID);
        return this;
    }    
    public ApplicationCriteria andApplicationParentIdGreaterThanOrEqualTo(String value) {
        addCriterion("application_parent_id >=", value, APPLICATION_PARENT_ID);
        return this;
    }    
    public ApplicationCriteria andApplicationParentIdLessThan(String value) {
        addCriterion("application_parent_id <", value, APPLICATION_PARENT_ID);
        return this;
    }     
    public ApplicationCriteria andApplicationParentIdLessThanOrEqualTo(String value) {
        addCriterion("application_parent_id <=", value, APPLICATION_PARENT_ID);
        return this;
    }
    public ApplicationCriteria andApplicationParentIdIn(List<String> values) {
        addCriterion("application_parent_id in", values, APPLICATION_PARENT_ID);
        return this;
    }
    public ApplicationCriteria andApplicationParentIdNotIn(List<String> values) {
        addCriterion("application_parent_id not in", values, APPLICATION_PARENT_ID);
        return this;
    }
    public ApplicationCriteria andApplicationParentIdBetween(String value1, String value2) {
        addCriterion("application_parent_id between", value1, value2, APPLICATION_PARENT_ID);
        return this;
    }
    public ApplicationCriteria andApplicationParentIdNotBetween(String value1, String value2) {
        addCriterion("application_parent_id not between", value1, value2, APPLICATION_PARENT_ID);
        return this;
    }
        public ApplicationCriteria andApplicationParentIdLike(String value) {
            addCriterion("application_parent_id like", value, APPLICATION_PARENT_ID);
            return this;
        }

        public ApplicationCriteria andApplicationParentIdNotLike(String value) {
            addCriterion("application_parent_id not like", value, APPLICATION_PARENT_ID);
            return this;
        }
    public ApplicationCriteria andApplicationUrlIsNull() {
        addCriterion("application_url is null");
        return this;
    }

    public ApplicationCriteria andApplicationUrlIsNotNull() {
        addCriterion("application_url is not null");
        return this;
    }
    public ApplicationCriteria andApplicationUrlEqualTo(String value) {
        addCriterion("application_url =", value, APPLICATION_URL);
        return this;
    }
    public ApplicationCriteria andApplicationUrlNotEqualTo(String value) {
        addCriterion("application_url <>", value, APPLICATION_URL);
        return this;
    }    
    public ApplicationCriteria andApplicationUrlGreaterThan(String value) {
        addCriterion("application_url >", value, APPLICATION_URL);
        return this;
    }    
    public ApplicationCriteria andApplicationUrlGreaterThanOrEqualTo(String value) {
        addCriterion("application_url >=", value, APPLICATION_URL);
        return this;
    }    
    public ApplicationCriteria andApplicationUrlLessThan(String value) {
        addCriterion("application_url <", value, APPLICATION_URL);
        return this;
    }     
    public ApplicationCriteria andApplicationUrlLessThanOrEqualTo(String value) {
        addCriterion("application_url <=", value, APPLICATION_URL);
        return this;
    }
    public ApplicationCriteria andApplicationUrlIn(List<String> values) {
        addCriterion("application_url in", values, APPLICATION_URL);
        return this;
    }
    public ApplicationCriteria andApplicationUrlNotIn(List<String> values) {
        addCriterion("application_url not in", values, APPLICATION_URL);
        return this;
    }
    public ApplicationCriteria andApplicationUrlBetween(String value1, String value2) {
        addCriterion("application_url between", value1, value2, APPLICATION_URL);
        return this;
    }
    public ApplicationCriteria andApplicationUrlNotBetween(String value1, String value2) {
        addCriterion("application_url not between", value1, value2, APPLICATION_URL);
        return this;
    }
        public ApplicationCriteria andApplicationUrlLike(String value) {
            addCriterion("application_url like", value, APPLICATION_URL);
            return this;
        }

        public ApplicationCriteria andApplicationUrlNotLike(String value) {
            addCriterion("application_url not like", value, APPLICATION_URL);
            return this;
        }
    public ApplicationCriteria andPermissionIsNull() {
        addCriterion("permission is null");
        return this;
    }

    public ApplicationCriteria andPermissionIsNotNull() {
        addCriterion("permission is not null");
        return this;
    }
    public ApplicationCriteria andPermissionEqualTo(String value) {
        addCriterion("permission =", value, PERMISSION);
        return this;
    }
    public ApplicationCriteria andPermissionNotEqualTo(String value) {
        addCriterion("permission <>", value, PERMISSION);
        return this;
    }    
    public ApplicationCriteria andPermissionGreaterThan(String value) {
        addCriterion("permission >", value, PERMISSION);
        return this;
    }    
    public ApplicationCriteria andPermissionGreaterThanOrEqualTo(String value) {
        addCriterion("permission >=", value, PERMISSION);
        return this;
    }    
    public ApplicationCriteria andPermissionLessThan(String value) {
        addCriterion("permission <", value, PERMISSION);
        return this;
    }     
    public ApplicationCriteria andPermissionLessThanOrEqualTo(String value) {
        addCriterion("permission <=", value, PERMISSION);
        return this;
    }
    public ApplicationCriteria andPermissionIn(List<String> values) {
        addCriterion("permission in", values, PERMISSION);
        return this;
    }
    public ApplicationCriteria andPermissionNotIn(List<String> values) {
        addCriterion("permission not in", values, PERMISSION);
        return this;
    }
    public ApplicationCriteria andPermissionBetween(String value1, String value2) {
        addCriterion("permission between", value1, value2, PERMISSION);
        return this;
    }
    public ApplicationCriteria andPermissionNotBetween(String value1, String value2) {
        addCriterion("permission not between", value1, value2, PERMISSION);
        return this;
    }
        public ApplicationCriteria andPermissionLike(String value) {
            addCriterion("permission like", value, PERMISSION);
            return this;
        }

        public ApplicationCriteria andPermissionNotLike(String value) {
            addCriterion("permission not like", value, PERMISSION);
            return this;
        }
    public ApplicationCriteria andPermissionNameIsNull() {
        addCriterion("permission_name is null");
        return this;
    }

    public ApplicationCriteria andPermissionNameIsNotNull() {
        addCriterion("permission_name is not null");
        return this;
    }
    public ApplicationCriteria andPermissionNameEqualTo(String value) {
        addCriterion("permission_name =", value, PERMISSION_NAME);
        return this;
    }
    public ApplicationCriteria andPermissionNameNotEqualTo(String value) {
        addCriterion("permission_name <>", value, PERMISSION_NAME);
        return this;
    }    
    public ApplicationCriteria andPermissionNameGreaterThan(String value) {
        addCriterion("permission_name >", value, PERMISSION_NAME);
        return this;
    }    
    public ApplicationCriteria andPermissionNameGreaterThanOrEqualTo(String value) {
        addCriterion("permission_name >=", value, PERMISSION_NAME);
        return this;
    }    
    public ApplicationCriteria andPermissionNameLessThan(String value) {
        addCriterion("permission_name <", value, PERMISSION_NAME);
        return this;
    }     
    public ApplicationCriteria andPermissionNameLessThanOrEqualTo(String value) {
        addCriterion("permission_name <=", value, PERMISSION_NAME);
        return this;
    }
    public ApplicationCriteria andPermissionNameIn(List<String> values) {
        addCriterion("permission_name in", values, PERMISSION_NAME);
        return this;
    }
    public ApplicationCriteria andPermissionNameNotIn(List<String> values) {
        addCriterion("permission_name not in", values, PERMISSION_NAME);
        return this;
    }
    public ApplicationCriteria andPermissionNameBetween(String value1, String value2) {
        addCriterion("permission_name between", value1, value2, PERMISSION_NAME);
        return this;
    }
    public ApplicationCriteria andPermissionNameNotBetween(String value1, String value2) {
        addCriterion("permission_name not between", value1, value2, PERMISSION_NAME);
        return this;
    }
        public ApplicationCriteria andPermissionNameLike(String value) {
            addCriterion("permission_name like", value, PERMISSION_NAME);
            return this;
        }

        public ApplicationCriteria andPermissionNameNotLike(String value) {
            addCriterion("permission_name not like", value, PERMISSION_NAME);
            return this;
        }
    public ApplicationCriteria andIsManagedModuleIsNull() {
        addCriterion("is_managed_module is null");
        return this;
    }

    public ApplicationCriteria andIsManagedModuleIsNotNull() {
        addCriterion("is_managed_module is not null");
        return this;
    }
    public ApplicationCriteria andIsManagedModuleEqualTo(String value) {
        addCriterion("is_managed_module =", value, IS_MANAGED_MODULE);
        return this;
    }
    public ApplicationCriteria andIsManagedModuleNotEqualTo(String value) {
        addCriterion("is_managed_module <>", value, IS_MANAGED_MODULE);
        return this;
    }    
    public ApplicationCriteria andIsManagedModuleGreaterThan(String value) {
        addCriterion("is_managed_module >", value, IS_MANAGED_MODULE);
        return this;
    }    
    public ApplicationCriteria andIsManagedModuleGreaterThanOrEqualTo(String value) {
        addCriterion("is_managed_module >=", value, IS_MANAGED_MODULE);
        return this;
    }    
    public ApplicationCriteria andIsManagedModuleLessThan(String value) {
        addCriterion("is_managed_module <", value, IS_MANAGED_MODULE);
        return this;
    }     
    public ApplicationCriteria andIsManagedModuleLessThanOrEqualTo(String value) {
        addCriterion("is_managed_module <=", value, IS_MANAGED_MODULE);
        return this;
    }
    public ApplicationCriteria andIsManagedModuleIn(List<String> values) {
        addCriterion("is_managed_module in", values, IS_MANAGED_MODULE);
        return this;
    }
    public ApplicationCriteria andIsManagedModuleNotIn(List<String> values) {
        addCriterion("is_managed_module not in", values, IS_MANAGED_MODULE);
        return this;
    }
    public ApplicationCriteria andIsManagedModuleBetween(String value1, String value2) {
        addCriterion("is_managed_module between", value1, value2, IS_MANAGED_MODULE);
        return this;
    }
    public ApplicationCriteria andIsManagedModuleNotBetween(String value1, String value2) {
        addCriterion("is_managed_module not between", value1, value2, IS_MANAGED_MODULE);
        return this;
    }
        public ApplicationCriteria andIsManagedModuleLike(String value) {
            addCriterion("is_managed_module like", value, IS_MANAGED_MODULE);
            return this;
        }

        public ApplicationCriteria andIsManagedModuleNotLike(String value) {
            addCriterion("is_managed_module not like", value, IS_MANAGED_MODULE);
            return this;
        }
    public ApplicationCriteria andDescriptionIsNull() {
        addCriterion("description is null");
        return this;
    }

    public ApplicationCriteria andDescriptionIsNotNull() {
        addCriterion("description is not null");
        return this;
    }
    public ApplicationCriteria andDescriptionEqualTo(String value) {
        addCriterion("description =", value, DESCRIPTION);
        return this;
    }
    public ApplicationCriteria andDescriptionNotEqualTo(String value) {
        addCriterion("description <>", value, DESCRIPTION);
        return this;
    }    
    public ApplicationCriteria andDescriptionGreaterThan(String value) {
        addCriterion("description >", value, DESCRIPTION);
        return this;
    }    
    public ApplicationCriteria andDescriptionGreaterThanOrEqualTo(String value) {
        addCriterion("description >=", value, DESCRIPTION);
        return this;
    }    
    public ApplicationCriteria andDescriptionLessThan(String value) {
        addCriterion("description <", value, DESCRIPTION);
        return this;
    }     
    public ApplicationCriteria andDescriptionLessThanOrEqualTo(String value) {
        addCriterion("description <=", value, DESCRIPTION);
        return this;
    }
    public ApplicationCriteria andDescriptionIn(List<String> values) {
        addCriterion("description in", values, DESCRIPTION);
        return this;
    }
    public ApplicationCriteria andDescriptionNotIn(List<String> values) {
        addCriterion("description not in", values, DESCRIPTION);
        return this;
    }
    public ApplicationCriteria andDescriptionBetween(String value1, String value2) {
        addCriterion("description between", value1, value2, DESCRIPTION);
        return this;
    }
    public ApplicationCriteria andDescriptionNotBetween(String value1, String value2) {
        addCriterion("description not between", value1, value2, DESCRIPTION);
        return this;
    }
        public ApplicationCriteria andDescriptionLike(String value) {
            addCriterion("description like", value, DESCRIPTION);
            return this;
        }

        public ApplicationCriteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, DESCRIPTION);
            return this;
        }
    public ApplicationCriteria andSortIdIsNull() {
        addCriterion("sort_id is null");
        return this;
    }

    public ApplicationCriteria andSortIdIsNotNull() {
        addCriterion("sort_id is not null");
        return this;
    }
    public ApplicationCriteria andSortIdEqualTo(String value) {
        addCriterion("sort_id =", value, SORT_ID);
        return this;
    }
    public ApplicationCriteria andSortIdNotEqualTo(String value) {
        addCriterion("sort_id <>", value, SORT_ID);
        return this;
    }    
    public ApplicationCriteria andSortIdGreaterThan(String value) {
        addCriterion("sort_id >", value, SORT_ID);
        return this;
    }    
    public ApplicationCriteria andSortIdGreaterThanOrEqualTo(String value) {
        addCriterion("sort_id >=", value, SORT_ID);
        return this;
    }    
    public ApplicationCriteria andSortIdLessThan(String value) {
        addCriterion("sort_id <", value, SORT_ID);
        return this;
    }     
    public ApplicationCriteria andSortIdLessThanOrEqualTo(String value) {
        addCriterion("sort_id <=", value, SORT_ID);
        return this;
    }
    public ApplicationCriteria andSortIdIn(List<String> values) {
        addCriterion("sort_id in", values, SORT_ID);
        return this;
    }
    public ApplicationCriteria andSortIdNotIn(List<String> values) {
        addCriterion("sort_id not in", values, SORT_ID);
        return this;
    }
    public ApplicationCriteria andSortIdBetween(String value1, String value2) {
        addCriterion("sort_id between", value1, value2, SORT_ID);
        return this;
    }
    public ApplicationCriteria andSortIdNotBetween(String value1, String value2) {
        addCriterion("sort_id not between", value1, value2, SORT_ID);
        return this;
    }
        public ApplicationCriteria andSortIdLike(String value) {
            addCriterion("sort_id like", value, SORT_ID);
            return this;
        }

        public ApplicationCriteria andSortIdNotLike(String value) {
            addCriterion("sort_id not like", value, SORT_ID);
            return this;
        }
    public ApplicationCriteria andApplicationIconIsNull() {
        addCriterion("application_icon is null");
        return this;
    }

    public ApplicationCriteria andApplicationIconIsNotNull() {
        addCriterion("application_icon is not null");
        return this;
    }
    public ApplicationCriteria andApplicationIconEqualTo(String value) {
        addCriterion("application_icon =", value, APPLICATION_ICON);
        return this;
    }
    public ApplicationCriteria andApplicationIconNotEqualTo(String value) {
        addCriterion("application_icon <>", value, APPLICATION_ICON);
        return this;
    }    
    public ApplicationCriteria andApplicationIconGreaterThan(String value) {
        addCriterion("application_icon >", value, APPLICATION_ICON);
        return this;
    }    
    public ApplicationCriteria andApplicationIconGreaterThanOrEqualTo(String value) {
        addCriterion("application_icon >=", value, APPLICATION_ICON);
        return this;
    }    
    public ApplicationCriteria andApplicationIconLessThan(String value) {
        addCriterion("application_icon <", value, APPLICATION_ICON);
        return this;
    }     
    public ApplicationCriteria andApplicationIconLessThanOrEqualTo(String value) {
        addCriterion("application_icon <=", value, APPLICATION_ICON);
        return this;
    }
    public ApplicationCriteria andApplicationIconIn(List<String> values) {
        addCriterion("application_icon in", values, APPLICATION_ICON);
        return this;
    }
    public ApplicationCriteria andApplicationIconNotIn(List<String> values) {
        addCriterion("application_icon not in", values, APPLICATION_ICON);
        return this;
    }
    public ApplicationCriteria andApplicationIconBetween(String value1, String value2) {
        addCriterion("application_icon between", value1, value2, APPLICATION_ICON);
        return this;
    }
    public ApplicationCriteria andApplicationIconNotBetween(String value1, String value2) {
        addCriterion("application_icon not between", value1, value2, APPLICATION_ICON);
        return this;
    }
        public ApplicationCriteria andApplicationIconLike(String value) {
            addCriterion("application_icon like", value, APPLICATION_ICON);
            return this;
        }

        public ApplicationCriteria andApplicationIconNotLike(String value) {
            addCriterion("application_icon not like", value, APPLICATION_ICON);
            return this;
        }
    public ApplicationCriteria andApplicationParentIdsIsNull() {
        addCriterion("application_parent_ids is null");
        return this;
    }

    public ApplicationCriteria andApplicationParentIdsIsNotNull() {
        addCriterion("application_parent_ids is not null");
        return this;
    }
    public ApplicationCriteria andApplicationParentIdsEqualTo(String value) {
        addCriterion("application_parent_ids =", value, APPLICATION_PARENT_IDS);
        return this;
    }
    public ApplicationCriteria andApplicationParentIdsNotEqualTo(String value) {
        addCriterion("application_parent_ids <>", value, APPLICATION_PARENT_IDS);
        return this;
    }    
    public ApplicationCriteria andApplicationParentIdsGreaterThan(String value) {
        addCriterion("application_parent_ids >", value, APPLICATION_PARENT_IDS);
        return this;
    }    
    public ApplicationCriteria andApplicationParentIdsGreaterThanOrEqualTo(String value) {
        addCriterion("application_parent_ids >=", value, APPLICATION_PARENT_IDS);
        return this;
    }    
    public ApplicationCriteria andApplicationParentIdsLessThan(String value) {
        addCriterion("application_parent_ids <", value, APPLICATION_PARENT_IDS);
        return this;
    }     
    public ApplicationCriteria andApplicationParentIdsLessThanOrEqualTo(String value) {
        addCriterion("application_parent_ids <=", value, APPLICATION_PARENT_IDS);
        return this;
    }
    public ApplicationCriteria andApplicationParentIdsIn(List<String> values) {
        addCriterion("application_parent_ids in", values, APPLICATION_PARENT_IDS);
        return this;
    }
    public ApplicationCriteria andApplicationParentIdsNotIn(List<String> values) {
        addCriterion("application_parent_ids not in", values, APPLICATION_PARENT_IDS);
        return this;
    }
    public ApplicationCriteria andApplicationParentIdsBetween(String value1, String value2) {
        addCriterion("application_parent_ids between", value1, value2, APPLICATION_PARENT_IDS);
        return this;
    }
    public ApplicationCriteria andApplicationParentIdsNotBetween(String value1, String value2) {
        addCriterion("application_parent_ids not between", value1, value2, APPLICATION_PARENT_IDS);
        return this;
    }
        public ApplicationCriteria andApplicationParentIdsLike(String value) {
            addCriterion("application_parent_ids like", value, APPLICATION_PARENT_IDS);
            return this;
        }

        public ApplicationCriteria andApplicationParentIdsNotLike(String value) {
            addCriterion("application_parent_ids not like", value, APPLICATION_PARENT_IDS);
            return this;
        }
    public ApplicationCriteria andIsStateIsNull() {
        addCriterion("is_state is null");
        return this;
    }

    public ApplicationCriteria andIsStateIsNotNull() {
        addCriterion("is_state is not null");
        return this;
    }
    public ApplicationCriteria andIsStateEqualTo(String value) {
        addCriterion("is_state =", value, IS_STATE);
        return this;
    }
    public ApplicationCriteria andIsStateNotEqualTo(String value) {
        addCriterion("is_state <>", value, IS_STATE);
        return this;
    }    
    public ApplicationCriteria andIsStateGreaterThan(String value) {
        addCriterion("is_state >", value, IS_STATE);
        return this;
    }    
    public ApplicationCriteria andIsStateGreaterThanOrEqualTo(String value) {
        addCriterion("is_state >=", value, IS_STATE);
        return this;
    }    
    public ApplicationCriteria andIsStateLessThan(String value) {
        addCriterion("is_state <", value, IS_STATE);
        return this;
    }     
    public ApplicationCriteria andIsStateLessThanOrEqualTo(String value) {
        addCriterion("is_state <=", value, IS_STATE);
        return this;
    }
    public ApplicationCriteria andIsStateIn(List<String> values) {
        addCriterion("is_state in", values, IS_STATE);
        return this;
    }
    public ApplicationCriteria andIsStateNotIn(List<String> values) {
        addCriterion("is_state not in", values, IS_STATE);
        return this;
    }
    public ApplicationCriteria andIsStateBetween(String value1, String value2) {
        addCriterion("is_state between", value1, value2, IS_STATE);
        return this;
    }
    public ApplicationCriteria andIsStateNotBetween(String value1, String value2) {
        addCriterion("is_state not between", value1, value2, IS_STATE);
        return this;
    }
        public ApplicationCriteria andIsStateLike(String value) {
            addCriterion("is_state like", value, IS_STATE);
            return this;
        }

        public ApplicationCriteria andIsStateNotLike(String value) {
            addCriterion("is_state not like", value, IS_STATE);
            return this;
        }
}

