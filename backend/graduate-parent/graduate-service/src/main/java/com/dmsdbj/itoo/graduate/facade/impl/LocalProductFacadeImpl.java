package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.facade.LocalProductFacade;
import com.dmsdbj.itoo.graduate.service.LocalProductService;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import com.dmsdbj.itoo.graduate.entity.LocalProductEntity;

/**
 *@authorsxm
 *create: 2017-11-16 14:31:07
 * DESCRIPTION
 */
@Component("localProductFacade")
@Service
public class LocalProductFacadeImpl implements LocalProductFacade {

    //打印日志相关
	private static final Logger logger = LoggerFactory.getLogger(LocalProductFacadeImpl.class);

    @Autowired
    LocalProductService localProductService;

    /**
     * 根据id查询LocalProduct
     * @param id
     * @return LocalProductEntity
     */
    @Override
    public LocalProductEntity findById(String id) {
        return localProductService.findById(id);
    }

    /**
     *查询所有特产信息
     *@param
     *@return List<LocalProductEntity> 房间类型实体集合
     */
    @Override
    public List<LocalProductEntity> queryLocalProduct(){
        List<LocalProductEntity> localProductEntity =null;
        try{
            localProductEntity = localProductService.queryLocalProduct();
        }catch (Exception e){
            logger.error("查询特产信息失败",e);
            throw new ItooRuntimeException(e);
        }
        return localProductEntity;
    }

    /**
     *添加特产信息
     *@param
     *@return List<LocalProductEntity> 房间类型实体集合
     */
    @Override
    public boolean addLocalProduct(LocalProductEntity localProductEntity){

        boolean flag;
        try {
            flag = localProductService.addLocalProduct(localProductEntity);
        } catch (Exception e) {
            logger.error("PlaceManageFacadeImpl.addRoomInfo:添加特产信息失败！", e);
            throw new ItooRuntimeException(e);
        }
        return flag;
    }


    /**
     *删除特产
     *@param
     *@return List<LocalProductEntity> 特产实体集合
     */
    @Override
    public boolean deleteLocalProduct(List<String> productIds){

        boolean flag;
        try {
            flag = localProductService.deleteLocalProduct(productIds);
        } catch (Exception e) {
            logger.error("PlaceManageFacadeImpl.deleteRoomInfo:删除特产失败！", e);
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     *更新特产
     *@param
     *@return List<LocalProductEntity> 特产实体集合
     */
    @Override
    public int updateLocalProduct(LocalProductEntity localProductEntity){

        int flag;
        try {
            flag = localProductService.updateLocalProduct(localProductEntity);
        } catch (Exception e) {
            logger.error("PlaceManageFacadeImpl.updateRoomInfo:修改特产信息失败！", e);
            throw new ItooRuntimeException(e);
        }
        return flag;
    }
    
}
