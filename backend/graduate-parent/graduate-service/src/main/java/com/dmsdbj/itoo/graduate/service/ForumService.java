package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.ForumEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.ForumExample;



/**
 * @author 孔唯妍
 * create:2017-12-27 09:54:26
 * DESCRIPTION
 */
public interface ForumService extends BaseService<ForumEntity, ForumExample>{

}
