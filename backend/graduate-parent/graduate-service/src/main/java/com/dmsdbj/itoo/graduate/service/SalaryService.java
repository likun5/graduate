package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.SalaryEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.SalaryExample;
import com.github.pagehelper.PageInfo;

import java.sql.Date;
import java.util.List;
import java.util.Map;


/**
 * @author yyl
 * create:2017-11-26 16:14:54
 * DESCRIPTION  薪资信息service
 */
public interface SalaryService extends BaseService<SalaryEntity, SalaryExample>{

    /**
     * 查询所有的信息-于云丽-2017-12-31
     * @param page，pageSize
     * @return 薪资实体分页信息PageInfo<SalaryEntity>
     */
     PageInfo<SalaryEntity> findAll(int page, int pageSize);
     /**
     * 根据人员id查询所有的薪资信息-于云丽-2018-01-14
     * @param personId
     * @return 薪资实体List<SalaryEntity>
     */
    List<SalaryEntity>  selectByPersonId(String personId);

    /**
     * 根据年级id查询本年最高薪资-于云丽-2018-01-14
     * @param  gradeId
     * @return 最高薪资值
     */
    Double selectHighSalaryByGradeId(String gradeId);


    /**
     * 根据年级id查询本年级最低薪资-于云丽-2018-01-14
     * @param  gradeId
     * @return 最低薪资值
     */
    Double selectLowestSalaryByGradeId(String gradeId);

    /**
     * 根据年级id查询本年平均薪资-于云丽-2018-01-14
     * @param  gradeId
     * @return 平均薪资值
     */
    Double selectAverageSalaryByGradeId(String gradeId);

    /**
     * 根据公司和个人关系表Id查询薪资相关信息-于云丽-2017年12月4日
     * @param relationIdList 公司和个人关系表id组成的list
     * @return 薪资实体list
     */
    List<SalaryEntity> selectSalaryInfoByRelationIds(List<String> relationIdList);
    /**
     * 根据公司和个人关系表Id查询薪资相关信息-于云丽-2017年12月4日
     * @param relationIdList，page，pageSize 公司和个人关系表id组成的list及分页信息
     * @return 薪资实体list
     */
    List<SalaryEntity> pageSelectSalaryInfoByRelationIds(List<String> relationIdList,int page,int pageSize);
    /**
     * 根据公司和个人关系表Id查询薪资相关信息-yyl-2017年12月5日
     * @param relationId 公司和个人关系表Id
     * @return 薪资实体list
     */
    List<SalaryEntity> selectSalaryInfoByRelationId(String relationId);
    /**
     * 根据公司和个人关系表Id删除薪资相关信息-yyl-2017年12月5日
     * @param operator 操作用户operator
     * @param relationId 公司和个人关系表Id
     * @return  删除结果
     */
    int deleteSalaryInfoByRelationId(String relationId,String operator);
    /**
     * 根据公司和个人关系表Ids删除薪资相关信息-yyl-2017年12月5日
     * @param relationIdList 公司和个人关系表Ids,操作用户operator
     * @return  int 影响行数
     */
    int deleteSalaryInfoByRelationIds(List<String> relationIdList,String operator);
    /**
     * 根据个人姓名查询薪资
     * @param  ：name
     * @return ：薪资实体列表List<SalaryEntity>
     */
    List<SalaryEntity>  selectByName(String name);
    /**
     * 根据年级名称查询薪资
     * @param  ：Grade年级名称
     * @return 薪资列表及用户id组成的Map<String ,List<SalaryEntity>>
     */
    Map<String ,List<SalaryEntity>> selectByGrade(String Grade);
    /**
     * 根据年级查询最低薪资
     * @param  ：年级
     * @return 薪资列表 List<SalaryEntity>
     */
    List<SalaryEntity>  selectLowestSalaryByGrade(String grade);
    /**
     * 根据年级查询最高薪资
     * @param  ：年级名称
     * @return ：薪资列表List<SalaryEntity>
     */
    List<SalaryEntity>  selectHighestSalaryByGrade(String grade);
    /**
     * 根据年级查询平均薪资列表
     * @param  ：年级名称
     * @return ：平均薪资值Double类型
     */
    Double selectAverageSalaryByGrade(String grade);
    /**
     * 根据个人关系、时间查询薪资-时间列表
     * @param  : pcRelation个人公司关系、date年份
     * @return ：List<SalaryEntity> ，只有一条信息
     */
    List<SalaryEntity> selectByPcRelation(String pcRelation, Date date);
    /**
     * 根据个人公司关系列表查询某一年份本期平均工资
     * @param  :relationIds个人公司关系列表、date年份
     * @return ：平均工资值
     */
    Double selectAverageSalaryByRelatoinId(List<String> relationIds, Date date);
    /**
     * 根据期数查询本期平均工资
     * @param  :relationIds个人公司关系列表
     * @return 平均工资的记录
     */
    Double selectAverageSalaryByRelatoinId(List<String> relationIds );
    /**
     * 根据期数查询本期最低工资（带截止年份）
     * @param  :relationIds个人公司关系列表，date年份
     * @return ：最低工资包含的薪资列表
     */
    List selectlowestSalaryByRelatoinId(List<String> relationIds,Date date);
     /**
     * 根据期数查询本期最低工资
     * @param : relationIds个人公司关系列表
     * @return :最低工资包含的薪资列表
     */
    List selectlowestSalaryByRelatoinId(List<String> relationIds );
    /**
     * 根据期数查询本期最高工资（带截止年份）
     * @param  : relationIds个人公司关系列表、date年份
     * @return ：最高工资的记录
     */
    List selecthighestSalaryByRelatoinId(List<String> relationIds,Date date);
    /**
     * 根据relationIds个人公司关系列表查询最高薪资
     * @param  :relationIds个人公司关系列表
     * @return ：最高工资的记录
     */
    List selecthighestSalaryByRelatoinId(List<String> relationIds );
    /**
     * 整体某年度最高薪资统计
     * @param : 年份
     * @return ：该年度最高薪资信息列表
     */
    List selecthighestSalaryofAll(Date date);
    /**
     * 整体本年度最低薪资统计
     * @param  ：年份
     * @return ：本年度最低工资list
     */
    List selectlowestSalaryofAll(Date date);
    /**
     * 整体本年度平均薪资统计
     * @param  ：年份
     * @return ：本年度平均工资值
     */
    Double selectAverageSalaryOfAll(Date date);

    /**
     * 时间转换方法
     * @param  ：时间戳
     * @return 转换时间
     */
    java.util.Date covertTimeStampToDate(java.util.Date timeStamp);

    /**
     *根据毕业生id查询薪资公司相关信息-haoguiting--2018年3月7日
     * @param ：userId，page，pageSize
     * @return ：公司薪资相关分页信息PageInfo<CompanySalaryModel>
     */
    PageInfo<CompanySalaryModel> PageSelectSalaryByUserId(String userId,int page, int pageSize);
}
