package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.graduate.entity.ext.CompanyModel;
import com.dmsdbj.itoo.graduate.entity.ext.EducationModel;
import com.dmsdbj.itoo.graduate.entity.ext.PersonalEducationEntity;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.EducationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.EducationExample;
import com.github.pagehelper.PageInfo;


import java.util.List;


/**
 * @author yyl
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
public interface EducationService extends BaseService<EducationEntity, EducationExample>{

    /**
     * 根据ID查找
     * @param id 学历ID
     * @return 学历实体
     */
    EducationEntity findById(String id);

    /**
     * 根据姓名查找
     * @param name 学历姓名
     * @return  学历实体
     */
    List<EducationEntity>  findByName(String name);

    /**
     * 根据人员id查询学历信息
     * @param personId 人员ID
     * @return 学历实体
     */

    List<EducationEntity> findByPersonId(String personId);

    /**
     * 查询所有学历信息
     * @param page 第几页
     * @param pageSize 每页大小
     * @return 带分页学历实体
     */

    PageInfo<EducationEntity>   findAll(int page, int pageSize);

    /**
     * 查询个人所有学历信息
     * @param page 第几页
     * @param pageSize 每页大小
     * @param UserId
     * @return 带分页学历实体
     */
    PageInfo<EducationEntity>   findPersonAll(int page, int pageSize, String UserId);

    /**
     * 新增学历信息
     * @param record 学历
     * @return int 0，1成功与否
     */
    @Override
    int insert(EducationEntity record) ;


    /**
     * 删除学历信息
     * @param id 学历ID
     * @param operator 操作员ID
     * @return int 0，1：是否成功
     */

    int deleteById(String id, String operator);




    /**
     * 根据人员id删除学历信息记录
     * @param userId 用户ID
     * @param operator 操作员ID
     * @return int 0，1：是否成功
     */
    int deleteByPersonId(String userId, String operator);



    /**
     * 编辑学历信息
     * @param educationModel 学历实体
     * @return int 0，1：是否成功
     */

    int updateById(EducationModel educationModel) ;



    /**
     * 根据学历类型查询学历信息列表
     * @param cerfificateId 学历类型
     * @return 学历集合
     */
    List<EducationEntity> findByCertificateType(String cerfificateId);

    /**
     * 保存上传图片的路径（一张）-于云丽-2018年1月22日
     * @param pictureUrl 图片地址
     * @param keyId 学历ID
     * @return int 0，1：是否成功
     */
    int addPictureUrl(String pictureUrl, String keyId);

    /**
     * 查询提高班所有毕业生的毕业生学历信息-唐凌峰-2018年2月7日16:42:05
     * @return  List<PersonalEducationEntity> 学历新合集
     */
    List<PersonalEducationEntity>queryPersonalEducation();

    /**
     * 通过ID查询毕业生学历信息 -唐凌峰-2018年2月8日14:40:51
     * @param persionId 学历ID
     * @return List<PersonalEducationEntity> 学历新合集
     */
    List<PersonalEducationEntity>findEducationByPersonId(String persionId);

    /**
     * list添加多张图片和学历信息
     * @param EducationModelList  学历和图片list合集
     * @return int 0，1：是否成功
     */
    int addEducationModel(List<EducationModel> EducationModelList);

    /**
     * 根据学历ID查询相应的学历信息和图片urls
     * @param id 学历ID
     * @return  学历信息实体
     */
    EducationModel queryEducationModel(String id);
}

