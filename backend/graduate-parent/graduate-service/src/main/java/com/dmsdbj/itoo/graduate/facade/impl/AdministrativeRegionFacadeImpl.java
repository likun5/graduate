package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.AdministrativeRegionEntity;
import com.dmsdbj.itoo.graduate.facade.AdministrativeRegionFacade;
import com.dmsdbj.itoo.graduate.service.AdministrativeRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.List;

/**
 *@author李爽
 *create: 2017-12-05 09:39:56
 * DESCRIPTION
 */
@Component("administrativeRegionFacade")
@Service
public class AdministrativeRegionFacadeImpl implements AdministrativeRegionFacade {

    //打印日志相关
	private static final Logger logger = LoggerFactory.getLogger(AdministrativeRegionFacadeImpl.class);

	@Resource
    AdministrativeRegionService administrativeRegionService2;

    @Autowired
    AdministrativeRegionService administrativeRegionService;

    /**
     * 根据id查询AdministrativeRegion
     * @param id
     * @return AdministrativeRegionEntity
     */
    @Override
    public AdministrativeRegionEntity findById(String id) {
        return administrativeRegionService.findById(id);
    }

    /**
     * 查询所有的省份 -李爽-2017-12-5 15:00:26
     * @return
     */
    @Override
    public List<AdministrativeRegionEntity> selectProvince() {
        return administrativeRegionService.selectProvince();
    }

    /**
     * 根据id查询子行政区，比如根据某一省/市 id，查询其下是市/区县 -李爽-2017-12-5 15:02:36
     * @param id
     * @return
     */
    @Override
    public List<AdministrativeRegionEntity> selectSubRegionById(String id) {
        return administrativeRegionService.selectSubRegionById(id);
    }
}
