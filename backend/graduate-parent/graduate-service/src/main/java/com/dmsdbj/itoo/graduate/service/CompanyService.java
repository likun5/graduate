package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.graduate.entity.CompanyEntity;
import com.dmsdbj.itoo.graduate.entity.ext.CompanyModel;
import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import com.dmsdbj.itoo.graduate.entity.ext.TechnologyModel;
import com.dmsdbj.itoo.graduate.mybatisexample.CompanyExample;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.github.pagehelper.PageInfo;

import java.util.List;


/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
public interface CompanyService extends BaseService<CompanyEntity, CompanyExample>{

    /**
     * 添加公司信息-宋学孟-2017年11月23日
     * @param companyModelList 公司及关系表实体
     * @return 影响条数
     */
    int addCompany(List<CompanyModel> companyModelList);

    /**
     * 删除公司-宋学孟-2017年11月23日
     * @param companyId 公司id
     * @param userId 学员id
     * @return 影响条数
     */
    int deleteCompany(String companyId, String userId, String operatorId);

    /**
     * 修改公司相关信息-2017年11月25日
     * @param companyModel 公司及关系表实体
     * @return 影响条数
     */
    int updateCompany(CompanyModel companyModel);

    /**
     * 查询公司相关信息-所有公司（添加流程）-2018年1月18日
     *
     * @param userId 学员Id
     * @return 公司实体list
     */
    List<CompanyModel> selectCompanysByUserId(String userId);

    /**
     * 查询公司相关信息-宋学孟-2017年11月25日
     * @param userId 学员Id
     * @return 公司实体
     */
    CompanyModel selectCompanyByUserId(String userId);

    /**
     * 查询学员工作经历和薪资-宋学孟-2017年12月4日
     * @param userId 用户id
     * @return 公司实体list
     */
    List<CompanySalaryModel> selectCompanySalaryByUserId(String userId,int page, int pageSize);


    /**
     * 分页查询学员工作经历和薪资-宋学孟-2018年2月8日
     * @param userId 用户id
     * @return 公司薪资list
     */
    PageInfo<CompanySalaryModel> selectCompanySalaryPageByUserId(String userId, int page, int pageSize);

    /**
     * 查询所有技术点的类别和详细技术点-宋学孟-2018年1月13日
     * @return 技术点list
     */
    List<TechnologyModel> selectSkillPoint();

    /**
     * 保存上传图片的路径（一张）-宋学孟-2018年1月14日
     *
     * @param pictureUrl 图片url
     * @param keyId 关联外键id
     * @return 影响条数
     */
    int addPictureUrl(String pictureUrl, String keyId);

    /**
     * 根据相应的id，查询对应的实体（包含url）-宋学孟-2018年1月14日
     *
     * @param keyId 关联外键id
     * @return 图片url列表
     */
    List<String> selectPictureById(String keyId);
}
