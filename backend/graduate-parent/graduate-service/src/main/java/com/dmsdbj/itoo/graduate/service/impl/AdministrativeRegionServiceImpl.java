package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.dao.AdministrativeRegionDao;
import com.dmsdbj.itoo.graduate.entity.AdministrativeRegionEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.AdministrativeRegionExample;
import com.dmsdbj.itoo.graduate.service.AdministrativeRegionService;
import com.dmsdbj.itoo.graduate.tool.ResultEmptyUtil;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : 李爽
 * @Date :2017-12-05 09:39:56
 * DESCRIPTION
 */
@Service("administrativeRegionService")
public class AdministrativeRegionServiceImpl extends BaseServiceImpl<AdministrativeRegionEntity, AdministrativeRegionExample> implements AdministrativeRegionService {


    //注入administrativeRegionDao
    @Autowired
    private AdministrativeRegionDao administrativeRegionDao;

    /**
     * 让BaseServiceImpl获取到Dao
     * @return BaseDao<AdministrativeRegionEntity,AdministrativeRegionExample>
     */
    @Override
    public BaseDao<AdministrativeRegionEntity, AdministrativeRegionExample> getRealDao(){
        return this.administrativeRegionDao;
    }

    /**
     * 查询所有的省份 -李爽-2017-12-5 15:00:26
     * @return
     */
    @Override
    public List<AdministrativeRegionEntity> selectProvince() {
        AdministrativeRegionExample example = new AdministrativeRegionExample();
        example.createCriteria()
                .andLevelEqualTo((byte)1)
                .andIsDeleteEqualTo((byte) 0);
        example.setOrderByClause("`initial` ASC");
        List<AdministrativeRegionEntity> administrativeRegionEntityList = administrativeRegionDao.selectByExample(example);

        return ResultEmptyUtil.getReturnValue(administrativeRegionEntityList);
    }

    /**
     * 根据id查询子行政区，比如根据某一省/市 id，查询其下是市/区县 -李爽-2017-12-5 15:02:36
     * @param id
     * @return
     */
    @Override
    public List<AdministrativeRegionEntity> selectSubRegionById(String id) {
        if(StringUtils.isEmpty(id)){
            return null;
        }
        AdministrativeRegionExample example = new AdministrativeRegionExample();
        example.createCriteria()
                .andPidEqualTo(id)
                .andIsDeleteEqualTo((byte) 0);
        example.setOrderByClause("`initial` ASC");
        List<AdministrativeRegionEntity> administrativeRegionEntityList = administrativeRegionDao.selectByExample(example);
        return ResultEmptyUtil.getReturnValue(administrativeRegionEntityList);
    }
}
