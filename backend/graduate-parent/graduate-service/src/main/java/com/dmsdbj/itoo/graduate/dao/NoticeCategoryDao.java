package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.NoticeCategoryEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.NoticeCategoryExample;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 *@author hgt
 *create: 2017-11-27 10:23:11
 *DESCRIPTION: 公告分类信息dao
 */
@Repository
public interface NoticeCategoryDao extends BaseDao<NoticeCategoryEntity,NoticeCategoryExample> {
    /**
     * 查询全部通知分类-2017年11月27日
     * @return 通知分类实体list
     */

    List<NoticeCategoryEntity> selectNoticeCategoryInfo();

    /**
     * 修改通知分类信息-郝贵婷-2017年11月27日
     * @param noticeCategoryEntity 通知分类实体
     * @return 受影响行
     */
    int updateNoticeCategory(NoticeCategoryEntity noticeCategoryEntity);
}
