package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class UserCriteria extends GeneratedCriteria<UserCriteria> implements Serializable{

    protected UserCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String PASSWORD = "password";   
    private static final String USER_CODE = "userCode";   
    private static final String USER_REAL_NAME = "userRealName";   
    private static final String EMAIL = "email";   
    private static final String TEL_NUM = "telNum";   
    private static final String SCHOOL_NO = "schoolNo";   

    public UserCriteria andPasswordIsNull() {
        addCriterion("password is null");
        return this;
    }

    public UserCriteria andPasswordIsNotNull() {
        addCriterion("password is not null");
        return this;
    }
    public UserCriteria andPasswordEqualTo(String value) {
        addCriterion("password =", value, PASSWORD);
        return this;
    }
    public UserCriteria andPasswordNotEqualTo(String value) {
        addCriterion("password <>", value, PASSWORD);
        return this;
    }    
    public UserCriteria andPasswordGreaterThan(String value) {
        addCriterion("password >", value, PASSWORD);
        return this;
    }    
    public UserCriteria andPasswordGreaterThanOrEqualTo(String value) {
        addCriterion("password >=", value, PASSWORD);
        return this;
    }    
    public UserCriteria andPasswordLessThan(String value) {
        addCriterion("password <", value, PASSWORD);
        return this;
    }     
    public UserCriteria andPasswordLessThanOrEqualTo(String value) {
        addCriterion("password <=", value, PASSWORD);
        return this;
    }
    public UserCriteria andPasswordIn(List<String> values) {
        addCriterion("password in", values, PASSWORD);
        return this;
    }
    public UserCriteria andPasswordNotIn(List<String> values) {
        addCriterion("password not in", values, PASSWORD);
        return this;
    }
    public UserCriteria andPasswordBetween(String value1, String value2) {
        addCriterion("password between", value1, value2, PASSWORD);
        return this;
    }
    public UserCriteria andPasswordNotBetween(String value1, String value2) {
        addCriterion("password not between", value1, value2, PASSWORD);
        return this;
    }
        public UserCriteria andPasswordLike(String value) {
            addCriterion("password like", value, PASSWORD);
            return this;
        }

        public UserCriteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, PASSWORD);
            return this;
        }
    public UserCriteria andUserCodeIsNull() {
        addCriterion("user_code is null");
        return this;
    }

    public UserCriteria andUserCodeIsNotNull() {
        addCriterion("user_code is not null");
        return this;
    }
    public UserCriteria andUserCodeEqualTo(String value) {
        addCriterion("user_code =", value, USER_CODE);
        return this;
    }
    public UserCriteria andUserCodeNotEqualTo(String value) {
        addCriterion("user_code <>", value, USER_CODE);
        return this;
    }    
    public UserCriteria andUserCodeGreaterThan(String value) {
        addCriterion("user_code >", value, USER_CODE);
        return this;
    }    
    public UserCriteria andUserCodeGreaterThanOrEqualTo(String value) {
        addCriterion("user_code >=", value, USER_CODE);
        return this;
    }    
    public UserCriteria andUserCodeLessThan(String value) {
        addCriterion("user_code <", value, USER_CODE);
        return this;
    }     
    public UserCriteria andUserCodeLessThanOrEqualTo(String value) {
        addCriterion("user_code <=", value, USER_CODE);
        return this;
    }
    public UserCriteria andUserCodeIn(List<String> values) {
        addCriterion("user_code in", values, USER_CODE);
        return this;
    }
    public UserCriteria andUserCodeNotIn(List<String> values) {
        addCriterion("user_code not in", values, USER_CODE);
        return this;
    }
    public UserCriteria andUserCodeBetween(String value1, String value2) {
        addCriterion("user_code between", value1, value2, USER_CODE);
        return this;
    }
    public UserCriteria andUserCodeNotBetween(String value1, String value2) {
        addCriterion("user_code not between", value1, value2, USER_CODE);
        return this;
    }
        public UserCriteria andUserCodeLike(String value) {
            addCriterion("user_code like", value, USER_CODE);
            return this;
        }

        public UserCriteria andUserCodeNotLike(String value) {
            addCriterion("user_code not like", value, USER_CODE);
            return this;
        }
    public UserCriteria andUserRealNameIsNull() {
        addCriterion("user_real_name is null");
        return this;
    }

    public UserCriteria andUserRealNameIsNotNull() {
        addCriterion("user_real_name is not null");
        return this;
    }
    public UserCriteria andUserRealNameEqualTo(String value) {
        addCriterion("user_real_name =", value, USER_REAL_NAME);
        return this;
    }
    public UserCriteria andUserRealNameNotEqualTo(String value) {
        addCriterion("user_real_name <>", value, USER_REAL_NAME);
        return this;
    }    
    public UserCriteria andUserRealNameGreaterThan(String value) {
        addCriterion("user_real_name >", value, USER_REAL_NAME);
        return this;
    }    
    public UserCriteria andUserRealNameGreaterThanOrEqualTo(String value) {
        addCriterion("user_real_name >=", value, USER_REAL_NAME);
        return this;
    }    
    public UserCriteria andUserRealNameLessThan(String value) {
        addCriterion("user_real_name <", value, USER_REAL_NAME);
        return this;
    }     
    public UserCriteria andUserRealNameLessThanOrEqualTo(String value) {
        addCriterion("user_real_name <=", value, USER_REAL_NAME);
        return this;
    }
    public UserCriteria andUserRealNameIn(List<String> values) {
        addCriterion("user_real_name in", values, USER_REAL_NAME);
        return this;
    }
    public UserCriteria andUserRealNameNotIn(List<String> values) {
        addCriterion("user_real_name not in", values, USER_REAL_NAME);
        return this;
    }
    public UserCriteria andUserRealNameBetween(String value1, String value2) {
        addCriterion("user_real_name between", value1, value2, USER_REAL_NAME);
        return this;
    }
    public UserCriteria andUserRealNameNotBetween(String value1, String value2) {
        addCriterion("user_real_name not between", value1, value2, USER_REAL_NAME);
        return this;
    }
        public UserCriteria andUserRealNameLike(String value) {
            addCriterion("user_real_name like", value, USER_REAL_NAME);
            return this;
        }

        public UserCriteria andUserRealNameNotLike(String value) {
            addCriterion("user_real_name not like", value, USER_REAL_NAME);
            return this;
        }
    public UserCriteria andEmailIsNull() {
        addCriterion("email is null");
        return this;
    }

    public UserCriteria andEmailIsNotNull() {
        addCriterion("email is not null");
        return this;
    }
    public UserCriteria andEmailEqualTo(String value) {
        addCriterion("email =", value, EMAIL);
        return this;
    }
    public UserCriteria andEmailNotEqualTo(String value) {
        addCriterion("email <>", value, EMAIL);
        return this;
    }    
    public UserCriteria andEmailGreaterThan(String value) {
        addCriterion("email >", value, EMAIL);
        return this;
    }    
    public UserCriteria andEmailGreaterThanOrEqualTo(String value) {
        addCriterion("email >=", value, EMAIL);
        return this;
    }    
    public UserCriteria andEmailLessThan(String value) {
        addCriterion("email <", value, EMAIL);
        return this;
    }     
    public UserCriteria andEmailLessThanOrEqualTo(String value) {
        addCriterion("email <=", value, EMAIL);
        return this;
    }
    public UserCriteria andEmailIn(List<String> values) {
        addCriterion("email in", values, EMAIL);
        return this;
    }
    public UserCriteria andEmailNotIn(List<String> values) {
        addCriterion("email not in", values, EMAIL);
        return this;
    }
    public UserCriteria andEmailBetween(String value1, String value2) {
        addCriterion("email between", value1, value2, EMAIL);
        return this;
    }
    public UserCriteria andEmailNotBetween(String value1, String value2) {
        addCriterion("email not between", value1, value2, EMAIL);
        return this;
    }
        public UserCriteria andEmailLike(String value) {
            addCriterion("email like", value, EMAIL);
            return this;
        }

        public UserCriteria andEmailNotLike(String value) {
            addCriterion("email not like", value, EMAIL);
            return this;
        }
    public UserCriteria andTelNumIsNull() {
        addCriterion("tel_num is null");
        return this;
    }

    public UserCriteria andTelNumIsNotNull() {
        addCriterion("tel_num is not null");
        return this;
    }
    public UserCriteria andTelNumEqualTo(String value) {
        addCriterion("tel_num =", value, TEL_NUM);
        return this;
    }
    public UserCriteria andTelNumNotEqualTo(String value) {
        addCriterion("tel_num <>", value, TEL_NUM);
        return this;
    }    
    public UserCriteria andTelNumGreaterThan(String value) {
        addCriterion("tel_num >", value, TEL_NUM);
        return this;
    }    
    public UserCriteria andTelNumGreaterThanOrEqualTo(String value) {
        addCriterion("tel_num >=", value, TEL_NUM);
        return this;
    }    
    public UserCriteria andTelNumLessThan(String value) {
        addCriterion("tel_num <", value, TEL_NUM);
        return this;
    }     
    public UserCriteria andTelNumLessThanOrEqualTo(String value) {
        addCriterion("tel_num <=", value, TEL_NUM);
        return this;
    }
    public UserCriteria andTelNumIn(List<String> values) {
        addCriterion("tel_num in", values, TEL_NUM);
        return this;
    }
    public UserCriteria andTelNumNotIn(List<String> values) {
        addCriterion("tel_num not in", values, TEL_NUM);
        return this;
    }
    public UserCriteria andTelNumBetween(String value1, String value2) {
        addCriterion("tel_num between", value1, value2, TEL_NUM);
        return this;
    }
    public UserCriteria andTelNumNotBetween(String value1, String value2) {
        addCriterion("tel_num not between", value1, value2, TEL_NUM);
        return this;
    }
        public UserCriteria andTelNumLike(String value) {
            addCriterion("tel_num like", value, TEL_NUM);
            return this;
        }

        public UserCriteria andTelNumNotLike(String value) {
            addCriterion("tel_num not like", value, TEL_NUM);
            return this;
        }
    public UserCriteria andSchoolNoIsNull() {
        addCriterion("school_no is null");
        return this;
    }

    public UserCriteria andSchoolNoIsNotNull() {
        addCriterion("school_no is not null");
        return this;
    }
    public UserCriteria andSchoolNoEqualTo(String value) {
        addCriterion("school_no =", value, SCHOOL_NO);
        return this;
    }
    public UserCriteria andSchoolNoNotEqualTo(String value) {
        addCriterion("school_no <>", value, SCHOOL_NO);
        return this;
    }    
    public UserCriteria andSchoolNoGreaterThan(String value) {
        addCriterion("school_no >", value, SCHOOL_NO);
        return this;
    }    
    public UserCriteria andSchoolNoGreaterThanOrEqualTo(String value) {
        addCriterion("school_no >=", value, SCHOOL_NO);
        return this;
    }    
    public UserCriteria andSchoolNoLessThan(String value) {
        addCriterion("school_no <", value, SCHOOL_NO);
        return this;
    }     
    public UserCriteria andSchoolNoLessThanOrEqualTo(String value) {
        addCriterion("school_no <=", value, SCHOOL_NO);
        return this;
    }
    public UserCriteria andSchoolNoIn(List<String> values) {
        addCriterion("school_no in", values, SCHOOL_NO);
        return this;
    }
    public UserCriteria andSchoolNoNotIn(List<String> values) {
        addCriterion("school_no not in", values, SCHOOL_NO);
        return this;
    }
    public UserCriteria andSchoolNoBetween(String value1, String value2) {
        addCriterion("school_no between", value1, value2, SCHOOL_NO);
        return this;
    }
    public UserCriteria andSchoolNoNotBetween(String value1, String value2) {
        addCriterion("school_no not between", value1, value2, SCHOOL_NO);
        return this;
    }
        public UserCriteria andSchoolNoLike(String value) {
            addCriterion("school_no like", value, SCHOOL_NO);
            return this;
        }

        public UserCriteria andSchoolNoNotLike(String value) {
            addCriterion("school_no not like", value, SCHOOL_NO);
            return this;
        }
}

