package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.mybatisexample.CompanySkillRelationCriteria;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.CompanySkillRelationExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.CompanySkillRelationDao;
import com.dmsdbj.itoo.graduate.service.CompanySkillRelationService;
import com.dmsdbj.itoo.graduate.entity.CompanySkillRelationEntity;

import java.util.List;

/**
 * @author : 徐玲博
 * create :2017-11-26 16:14:54
 * DESCRIPTION 公司技术关系ServiceImpl
 */
@Service("companySkillRelationService")
public class CompanySkillRelationServiceImpl extends BaseServiceImpl<CompanySkillRelationEntity, CompanySkillRelationExample> implements CompanySkillRelationService {


    //注入companySkillRelationDao
    @Autowired
    private CompanySkillRelationDao companySkillRelationDao;

    /**
     * 让BaseServiceImpl获取到Dao
     *
     * @return BaseDao<CompanySkillRelationEntity,CompanySkillRelationExample>
     */
    @Override
    public BaseDao<CompanySkillRelationEntity, CompanySkillRelationExample> getRealDao() {
        return this.companySkillRelationDao;
    }

    /**
     * 根据技术id查询所有使用的公司-徐玲博-2017-12-4 10:34:54
     *
     * @param skillId 技术id
     * @return 人员id -关系列表
     */
    @Override
    public List<CompanySkillRelationEntity> selectCompanyBySkill(String skillId) {
        CompanySkillRelationExample companySkillRelationExample = new CompanySkillRelationExample();
        CompanySkillRelationCriteria companySkillRelationCriteria = companySkillRelationExample.createCriteria();
        companySkillRelationCriteria.andSkillIdEqualTo(skillId).andIsDeleteEqualTo((byte)0);
        return companySkillRelationDao.selectByExample(companySkillRelationExample);
    }

    /**
     * 根据公司id查询，包括所有的技术-徐玲博-2017-12-4 10:37:25
     *
     * @param companyId 公司id
     * @return 技术id -关系列表
     */
    @Override
    public List<CompanySkillRelationEntity> selectSkillByCompany(String companyId) {
        CompanySkillRelationExample companySkillRelationExample = new CompanySkillRelationExample();
        CompanySkillRelationCriteria companySkillRelationCriteria = companySkillRelationExample.createCriteria();
        companySkillRelationCriteria.andCompanyIdEqualTo(companyId).andIsDeleteEqualTo((byte)0);
        return companySkillRelationDao.selectByExample(companySkillRelationExample);
    }
    /**
     * 根据公司id删除关系表中技术-徐玲博-2017-12-5 22:10:55
     * @param companyId 公司id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deleteSkillByCompanyId(String companyId, String operator) {
        CompanySkillRelationExample companySkillRelationExample=new CompanySkillRelationExample();
        CompanySkillRelationCriteria companySkillRelationCriteria=companySkillRelationExample.createCriteria();
        companySkillRelationCriteria.andCompanyIdEqualTo(companyId);
        return companySkillRelationDao.deleteByExample(companySkillRelationExample,operator);
    }
    /**
     *根据技术id删除关系表中公司信息-徐玲博-2017-12-5 23:20:31
     *
     * @param skillId 技术id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deleteCompanyBySkillId(String skillId, String operator) {
        CompanySkillRelationExample companySkillRelationExample=new CompanySkillRelationExample();
        CompanySkillRelationCriteria companySkillRelationCriteria=companySkillRelationExample.createCriteria();
        companySkillRelationCriteria.andSkillIdEqualTo(skillId);
        return companySkillRelationDao.deleteByExample(companySkillRelationExample,operator);
    }
}
