package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.PictureEntity;
import com.dmsdbj.itoo.graduate.facade.PictureFacade;
import com.dmsdbj.itoo.graduate.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @authorsxm
 * create: 2017-11-16 14:31:07
 * DESCRIPTION
 */
@Component("pictureFacade")
@Service
public class PictureFacadeImpl implements PictureFacade {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(PictureFacadeImpl.class);

    @Autowired
    PictureService pictureService;

    /**
     * 根据id查询Picture
     *
     * @param id 主键
     * @return PictureEntity 实体
     */
    @Deprecated
    @Override
    public PictureEntity findById(String id) {
        return pictureService.findById(id);
    }


    /**
     * 个人上传图片-徐玲博-2018-1-13 11:24:27
     *
     * @param fileUrl     图片的全路径 例如‘E:\\cat.jpg’
     * @param fileExtName 图片扩展名 例如‘jpg、png’等
     * @return 图片在fastDFS的路径
     */
    @Deprecated
    @Override
    public String uploadPersonPicture(String fileUrl, String fileExtName) {
        try {
            return pictureService.uploadPersonPicture(fileUrl, fileExtName);
        } catch (Exception e) {
            logger.error("个人上传图片异常",e);
            return null;
        }
    }

    /**
     * 保存上传图片的路径（一张）-宋学孟-2018年1月14日
     * @param pictureUrl 图片url
     * @param keyId 关联主键id
     * @return 影响行数
     */
    @Deprecated
    @Override
    public int addPicture(String pictureUrl, String keyId) {

        try {
            return pictureService.addPicture(pictureUrl, keyId);
        } catch (Exception e) {
            logger.error("保存上传图片的路径异常",e);
            return 0;
        }
    }

    /**
     * 保存上传图片的路径（一张或多张）-宋学孟-2018年1月14日
     *
     * @param pictureEntityList 图片实体list
     * @return 影响行数
     */
    @Deprecated
    @Override
    public int addPictureUrl(List<PictureEntity> pictureEntityList) {

        try {
            return pictureService.addPictureUrl(pictureEntityList);
        } catch (Exception e) {
            logger.error("保存上传图片的路径异常",e);
            return 0;
        }
    }

    /**
     * 根据相应的id，查询对应的实体（包含url）-宋学孟-2018年1月14日
     *
     * @param keyId 关联主键id
     * @return 图片url列表
     */
//    @Deprecated
    @Override
    public List<String> selectPictureById(String keyId) {

        try {
            return pictureService.selectPictureById(keyId);
        } catch (Exception e) {
            logger.error("根据相应的id，查询对应的实体（包含url）异常",e);
            return new ArrayList<>();
        }
    }

    /**
     * 根据与图片表有关表的id，删除图片-徐玲博-2018-2-9 16:16:23
     *
     * @param keyId    例如个人表的userid
     * @param operator 操作人
     * @return 受影行
     */
    @Deprecated
    @Override
    public int deletePictureById(String keyId, String operator) {
        try {
            return pictureService.deletePictureById(keyId, operator);
        } catch (Exception e) {
            logger.error("根据与图片表有关表的id，删除图片异常",e);
            return 0;
        }
    }
}
