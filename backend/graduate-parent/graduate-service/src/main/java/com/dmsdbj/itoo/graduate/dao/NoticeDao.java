package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.graduate.entity.ext.NoticeBrowseModel;
import com.dmsdbj.itoo.graduate.entity.ext.NoticeNoticeCategoryModel;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.NoticeEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.NoticeExample;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 *@author hgt
 *create: 2017-11-27 10:23:11
 *DESCRIPTION: 通知信息dao
 */
@Repository
public interface NoticeDao extends BaseDao<NoticeEntity,NoticeExample> {

    /**
     * 根据分类ID查询通知-郝贵婷-2017年12月2日
     * @param columnId 通知类别id
     * @return NoticeEntity通知实体
     */
    List<NoticeNoticeCategoryModel> selectNoticeNoticeCategoryByColumnId(String columnId);

    /**
     * 查询全部通知信息-郝贵婷-2017年11月27日
     * @return 通知list
     */
    List<NoticeNoticeCategoryModel> selectNoticeNoticeCategory();

    /**
     * 根据ID查询通知-郝贵婷-2017年11月27日
     * @param id 通知id
     * @return 通知实体
     */
    NoticeNoticeCategoryModel findNoticeNoticeCategoryModelById(String id);

    /**
     * 添加通知信息-郝贵婷-2017年11月27日
     * @param noticeBrowseModel 通知浏览实体
     * @return 受影响行数
     */
    int addNotice(NoticeBrowseModel noticeBrowseModel);


    /**
     * 根据通知分类统计通知数量-郝贵婷-2018年1月9日
     * @param columnId 通知类别id
     * @return 受影响行数
     */
    int selectNoticeCountByColumnId(String columnId);

}
