package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.PersonSkillRelationEntity;
import com.dmsdbj.itoo.graduate.facade.PersonSkillRelationFacade;
import com.dmsdbj.itoo.graduate.service.PersonSkillRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author :徐玲博
 * create : 2017-11-26 16:14:54
 * DESCRIPTION 个人技术关系类
 */
@Component("personSkillRelationFacade")
@Service
public class PersonSkillRelationFacadeImpl implements PersonSkillRelationFacade {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(PersonSkillRelationFacadeImpl.class);

    @Autowired
    private PersonSkillRelationService personSkillRelationService;

    /**
     * 根据id查询PersonSkillRelation-徐玲博-2017-12-5 08:49:59
     *
     * @param id 关系id
     * @return PersonSkillRelationEntity
     */
    @Override
    public PersonSkillRelationEntity findById(String id) {
        return personSkillRelationService.findById(id);
    }

    /**
     * 查询某人熟悉的所有技术-徐玲博-2017-12-4 10:58:00
     *
     * @param userId 人员id
     * @return 技术列表
     */
    @Override
    public List<PersonSkillRelationEntity> selectSkillByPerson(String userId) {
        return personSkillRelationService.selectSkillByPerson(userId);
    }

    /**
     * 查询会某个技术的所有人-徐玲博-2017-12-4 10:58:19
     *
     * @param skillId 技术id
     * @return 人员列表
     */
    @Override
    public List<PersonSkillRelationEntity> selectPersonBySkill(String skillId) {
        return personSkillRelationService.selectPersonBySkill(skillId);
    }

    /**
     * 添加个人和技术的关系-徐玲博-2017-12-4 11:03:39
     *
     * @param personSkillRelationEntity 个人技术关系实体
     * @return 受影响行
     */
    @Override
    public int addPersonSkillRelation(PersonSkillRelationEntity personSkillRelationEntity) {
        return personSkillRelationService.insert(personSkillRelationEntity);
    }

    /**
     * 修改个人和技术的关系-徐玲博-2017-12-4 11:03:39
     *
     * @param personSkillRelationEntity 关系实体
     * @return 受影响行
     */
    @Override
    public int updatePersonSkillRelation(PersonSkillRelationEntity personSkillRelationEntity) {
        return personSkillRelationService.updateById(personSkillRelationEntity);
    }

    /**
     * 删除个人和技术的关系-徐玲博-2017-12-4 11:03:39
     *
     * @param id       用户id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deletePersonSkillRelation(String id, String operator) {
        return personSkillRelationService.deleteById(id, operator);
    }
    /**
     * 根据技术id删除关系表中毕业生-徐玲博-2017-12-5 23:26:38
     * @param skillId 技术id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deletePersonBySkillId(String skillId, String operator) {
        return personSkillRelationService.deletePersonBySkillId(skillId,operator);
    }
    /**
     * 根据人员id删除关系表中技术-徐玲博-2017-12-5 23:26:58
     * @param userId 用户id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deleteSkillByPersonId(String userId, String operator) {
        return personSkillRelationService.deleteSkillByPersonId(userId,operator);
    }


}
