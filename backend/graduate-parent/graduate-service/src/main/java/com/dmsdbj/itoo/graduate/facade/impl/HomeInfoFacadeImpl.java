package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.HomeInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.HomeInfoModel;
import com.dmsdbj.itoo.graduate.facade.HomeInfoFacade;
import com.dmsdbj.itoo.graduate.service.HomeInfoService;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author : 徐玲博
 * create : 2017-11-21 21:45:53
 * DESCRIPTION 家庭信息
 */
@Component("homeInfoFacade")
@Service
public class HomeInfoFacadeImpl implements HomeInfoFacade {

    //打印日志相关

    @Autowired
    private HomeInfoService homeInfoService;

    /**
     * 根据id查询HomeInfo-徐玲博-2017-12-5 08:55:03
     *
     * @param id 家庭成员id
     * @return HomeInfoEntity
     */
    @Override
    public HomeInfoEntity findById(String id) {
        return homeInfoService.findById(id);
    }

    /**
     * 添加家庭成员-徐玲博-2017-11-20 11:03:07
     *
     * @param homeInfoEntity 家庭成员信息实体
     * @return 受影响行
     */
    @Override
    public int addHomeInfo(HomeInfoEntity homeInfoEntity) {
        int flag;
        try {
            flag=homeInfoService.insert(homeInfoEntity);
        }catch (Exception e){
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 删除家庭成员信息-徐玲博-2017-11-20 11:03:48
     *
     * @param id           家庭成员ID
     * @param operator  操作人
     * @return 受影响行
     */
    @Override
    public int deleteHomeInfo(String id, String operator ) {
        int flag;
        try {
            flag=homeInfoService.deleteById(id, operator );
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return flag;
    }

    /**
     * 查询家庭信息列表-徐玲博-2017-11-20 11:04:24
     *
     * @param userId 当前登录用户ID
     * @return 家庭信息列表
     */
    @Override
    public PageInfo<HomeInfoModel> selectHomePersonInfo(String userId, int page, int pageSize) {
        PageInfo<HomeInfoModel> homeInfoModelPageInfo;
        try {
            homeInfoModelPageInfo=homeInfoService.selectHomePersonInfo(userId,page,pageSize);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return homeInfoModelPageInfo;
    }
    /**
     * 修改家庭成员-徐玲博-2017-11-20 11:06:56
     * @param homeInfoEntity 家庭成员实体
     * @return 受影响行
     */
    @Override
    public int updateHomeInfo(HomeInfoEntity homeInfoEntity) {
        int flag;
        try {
            flag=homeInfoService.updateById(homeInfoEntity);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return flag;
    }
}
