package com.dmsdbj.itoo.graduate.service;
import java.util.List;

import com.dmsdbj.itoo.graduate.entity.ext.NoticeNoticeCategoryModel;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.NoticeEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.NoticeExample;
import com.dmsdbj.itoo.graduate.entity.ext.NoticeBrowseModel;
import com.github.pagehelper.PageInfo;


/**
 * @author hgt
 * create:2017-11-27 11:34:12
 * DESCRIPTION 通知service
 */
public interface NoticeService extends BaseService<NoticeEntity, NoticeExample>{
    /**
     * 添加通知信息-haoguiting-2017年11月27日
     * @param noticeBrowseModel 通知浏览Model
     * @return  受影响行数
     */
    int addNotice(NoticeBrowseModel noticeBrowseModel);

    /**
     * 删除通知信息-haoguiting-2017年11月27日
     * @param noticeID 通知id
     * @param operator  操作人
     * @return 受影响行数
     */
    int deleteNotice(String noticeID,String operator);

    /**
     * 修改通知信息-haoguiting-2017年11月27日
     * @param noticeEntity 通知实体
     * @return 受影响行数
     */
    int updateNotice(NoticeEntity noticeEntity);



    /**
     * 根据ID查询通知-haoguiting-2017年11月27日
     * @param id 通知id
     * @return NoticeEntity通知实体
     */
    NoticeNoticeCategoryModel findNoticeNoticeCategoryModelById(String id);

    /**
     * 根分类ID查询通知-haoguiting-2017年12月2日
     * @param columnId 通知类别
     * @return  通知list
     */
    List<NoticeNoticeCategoryModel> selectNoticeNoticeCategoryByColumnId(String columnId);

    /**
     * 查询全部通知信息-haoguiting-2017年11月27日
     * @return 通知list
     */
    List<NoticeNoticeCategoryModel> selectNoticeNoticeCategory();

    /**
     * 根据通知分类统计通知数量-haoguiting-2018年1月9日
     * @param columnId 通知类别id
     * @return 受影响行数
     */
    int selectNoticeCountByColumnId(String columnId);


    /**
     * 真分页-查询全部通知信息-haoguiting-2018-01-11 14:15:45
     * @param page 页码
     * @param  pageSize 页大小
     * @return 通知list
     */
    PageInfo<NoticeNoticeCategoryModel> PageSelectNoticeNoticeCategory(int page, int pageSize);

    /**
     * 真分页-根据分类ID查询通知-haoguiting-2018年01月10日
     * @param columnId  分类Id
     * @param page 页码
     * @param  pageSize 页大小
     * @return 通知list
     */
    PageInfo<NoticeNoticeCategoryModel> PageSelectNoticeNoticeCategoryByColumnId(String columnId,int page, int pageSize);

    /**
     * 更新通知信息（包括接受人）-haoguiting-2017-12-5 14:15:45
     * @param noticeBrowseModel 浏览记录实体
     * @return 受影响行
     */
    int updateNoticeBrowse(NoticeBrowseModel noticeBrowseModel);
}
