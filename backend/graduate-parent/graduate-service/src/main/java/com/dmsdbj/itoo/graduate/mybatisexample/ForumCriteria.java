package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class ForumCriteria extends GeneratedCriteria<ForumCriteria> implements Serializable{

    protected ForumCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String SKILL_ID = "skillId";   
    private static final String TITLE = "title";   
    private static final String CONTENT = "content";   
    private static final String OPERATORID = "operatorid";   
    private static final String TIMESTAMP_TIME = "timestampTime";   

    public ForumCriteria andSkillIdIsNull() {
        addCriterion("skill_id is null");
        return this;
    }

    public ForumCriteria andSkillIdIsNotNull() {
        addCriterion("skill_id is not null");
        return this;
    }
    public ForumCriteria andSkillIdEqualTo(String value) {
        addCriterion("skill_id =", value, SKILL_ID);
        return this;
    }
    public ForumCriteria andSkillIdNotEqualTo(String value) {
        addCriterion("skill_id <>", value, SKILL_ID);
        return this;
    }    
    public ForumCriteria andSkillIdGreaterThan(String value) {
        addCriterion("skill_id >", value, SKILL_ID);
        return this;
    }    
    public ForumCriteria andSkillIdGreaterThanOrEqualTo(String value) {
        addCriterion("skill_id >=", value, SKILL_ID);
        return this;
    }    
    public ForumCriteria andSkillIdLessThan(String value) {
        addCriterion("skill_id <", value, SKILL_ID);
        return this;
    }     
    public ForumCriteria andSkillIdLessThanOrEqualTo(String value) {
        addCriterion("skill_id <=", value, SKILL_ID);
        return this;
    }
    public ForumCriteria andSkillIdIn(List<String> values) {
        addCriterion("skill_id in", values, SKILL_ID);
        return this;
    }
    public ForumCriteria andSkillIdNotIn(List<String> values) {
        addCriterion("skill_id not in", values, SKILL_ID);
        return this;
    }
    public ForumCriteria andSkillIdBetween(String value1, String value2) {
        addCriterion("skill_id between", value1, value2, SKILL_ID);
        return this;
    }
    public ForumCriteria andSkillIdNotBetween(String value1, String value2) {
        addCriterion("skill_id not between", value1, value2, SKILL_ID);
        return this;
    }
        public ForumCriteria andSkillIdLike(String value) {
            addCriterion("skill_id like", value, SKILL_ID);
            return this;
        }

        public ForumCriteria andSkillIdNotLike(String value) {
            addCriterion("skill_id not like", value, SKILL_ID);
            return this;
        }
    public ForumCriteria andTitleIsNull() {
        addCriterion("title is null");
        return this;
    }

    public ForumCriteria andTitleIsNotNull() {
        addCriterion("title is not null");
        return this;
    }
    public ForumCriteria andTitleEqualTo(String value) {
        addCriterion("title =", value, TITLE);
        return this;
    }
    public ForumCriteria andTitleNotEqualTo(String value) {
        addCriterion("title <>", value, TITLE);
        return this;
    }    
    public ForumCriteria andTitleGreaterThan(String value) {
        addCriterion("title >", value, TITLE);
        return this;
    }    
    public ForumCriteria andTitleGreaterThanOrEqualTo(String value) {
        addCriterion("title >=", value, TITLE);
        return this;
    }    
    public ForumCriteria andTitleLessThan(String value) {
        addCriterion("title <", value, TITLE);
        return this;
    }     
    public ForumCriteria andTitleLessThanOrEqualTo(String value) {
        addCriterion("title <=", value, TITLE);
        return this;
    }
    public ForumCriteria andTitleIn(List<String> values) {
        addCriterion("title in", values, TITLE);
        return this;
    }
    public ForumCriteria andTitleNotIn(List<String> values) {
        addCriterion("title not in", values, TITLE);
        return this;
    }
    public ForumCriteria andTitleBetween(String value1, String value2) {
        addCriterion("title between", value1, value2, TITLE);
        return this;
    }
    public ForumCriteria andTitleNotBetween(String value1, String value2) {
        addCriterion("title not between", value1, value2, TITLE);
        return this;
    }
        public ForumCriteria andTitleLike(String value) {
            addCriterion("title like", value, TITLE);
            return this;
        }

        public ForumCriteria andTitleNotLike(String value) {
            addCriterion("title not like", value, TITLE);
            return this;
        }
    public ForumCriteria andContentIsNull() {
        addCriterion("content is null");
        return this;
    }

    public ForumCriteria andContentIsNotNull() {
        addCriterion("content is not null");
        return this;
    }
    public ForumCriteria andContentEqualTo(String value) {
        addCriterion("content =", value, CONTENT);
        return this;
    }
    public ForumCriteria andContentNotEqualTo(String value) {
        addCriterion("content <>", value, CONTENT);
        return this;
    }    
    public ForumCriteria andContentGreaterThan(String value) {
        addCriterion("content >", value, CONTENT);
        return this;
    }    
    public ForumCriteria andContentGreaterThanOrEqualTo(String value) {
        addCriterion("content >=", value, CONTENT);
        return this;
    }    
    public ForumCriteria andContentLessThan(String value) {
        addCriterion("content <", value, CONTENT);
        return this;
    }     
    public ForumCriteria andContentLessThanOrEqualTo(String value) {
        addCriterion("content <=", value, CONTENT);
        return this;
    }
    public ForumCriteria andContentIn(List<String> values) {
        addCriterion("content in", values, CONTENT);
        return this;
    }
    public ForumCriteria andContentNotIn(List<String> values) {
        addCriterion("content not in", values, CONTENT);
        return this;
    }
    public ForumCriteria andContentBetween(String value1, String value2) {
        addCriterion("content between", value1, value2, CONTENT);
        return this;
    }
    public ForumCriteria andContentNotBetween(String value1, String value2) {
        addCriterion("content not between", value1, value2, CONTENT);
        return this;
    }
        public ForumCriteria andContentLike(String value) {
            addCriterion("content like", value, CONTENT);
            return this;
        }

        public ForumCriteria andContentNotLike(String value) {
            addCriterion("content not like", value, CONTENT);
            return this;
        }
    public ForumCriteria andOperatoridIsNull() {
        addCriterion("operatorid is null");
        return this;
    }

    public ForumCriteria andOperatoridIsNotNull() {
        addCriterion("operatorid is not null");
        return this;
    }
    public ForumCriteria andOperatoridEqualTo(String value) {
        addCriterion("operatorid =", value, OPERATORID);
        return this;
    }
    public ForumCriteria andOperatoridNotEqualTo(String value) {
        addCriterion("operatorid <>", value, OPERATORID);
        return this;
    }    
    public ForumCriteria andOperatoridGreaterThan(String value) {
        addCriterion("operatorid >", value, OPERATORID);
        return this;
    }    
    public ForumCriteria andOperatoridGreaterThanOrEqualTo(String value) {
        addCriterion("operatorid >=", value, OPERATORID);
        return this;
    }    
    public ForumCriteria andOperatoridLessThan(String value) {
        addCriterion("operatorid <", value, OPERATORID);
        return this;
    }     
    public ForumCriteria andOperatoridLessThanOrEqualTo(String value) {
        addCriterion("operatorid <=", value, OPERATORID);
        return this;
    }
    public ForumCriteria andOperatoridIn(List<String> values) {
        addCriterion("operatorid in", values, OPERATORID);
        return this;
    }
    public ForumCriteria andOperatoridNotIn(List<String> values) {
        addCriterion("operatorid not in", values, OPERATORID);
        return this;
    }
    public ForumCriteria andOperatoridBetween(String value1, String value2) {
        addCriterion("operatorid between", value1, value2, OPERATORID);
        return this;
    }
    public ForumCriteria andOperatoridNotBetween(String value1, String value2) {
        addCriterion("operatorid not between", value1, value2, OPERATORID);
        return this;
    }
        public ForumCriteria andOperatoridLike(String value) {
            addCriterion("operatorid like", value, OPERATORID);
            return this;
        }

        public ForumCriteria andOperatoridNotLike(String value) {
            addCriterion("operatorid not like", value, OPERATORID);
            return this;
        }
    public ForumCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public ForumCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public ForumCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public ForumCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public ForumCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public ForumCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public ForumCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public ForumCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public ForumCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public ForumCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public ForumCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public ForumCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
}

