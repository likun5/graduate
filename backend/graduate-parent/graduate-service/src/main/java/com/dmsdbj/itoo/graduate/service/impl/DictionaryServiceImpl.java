package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.dao.DictionaryDao;
import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
import com.dmsdbj.itoo.graduate.entity.ext.DictionaryTreeModel;
import com.dmsdbj.itoo.graduate.mybatisexample.DictionaryExample;
import com.dmsdbj.itoo.graduate.service.DictionaryService;
import com.dmsdbj.itoo.graduate.tool.DictionaryTypeCode;
import com.dmsdbj.itoo.graduate.tool.ResultEmptyUtil;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.tool.uuid.BaseUuidUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
@Service("dictionaryService")
public class DictionaryServiceImpl extends BaseServiceImpl<DictionaryEntity, DictionaryExample> implements DictionaryService {


    //注入dictionaryDao
    @Autowired
    private DictionaryDao dictionaryDao;

    /**
     * 让BaseServiceImpl获取到Dao
     * @return BaseDao<DictionaryEntity, DictionaryExample>
     */
    @Override
    public  BaseDao<DictionaryEntity, DictionaryExample> getRealDao(){
        return this.dictionaryDao;
    }

    /**
     * 根据字典id查询字典名称 - 李爽-2017-11-29 19:25:09
     * @param id 字典id
     * @return 字典名称
     */
    @Override
    public String findDictionaryNameById(String id) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        DictionaryExample example = new DictionaryExample();
        example.createCriteria()
                .andIdEqualTo(id)
                .andIsDeleteEqualTo((byte) 0);
        List<DictionaryEntity> list = dictionaryDao.selectByExample(example);
        String dictionaryName = "";
        for (DictionaryEntity dictionary : list) {
            dictionaryName = dictionary.getDictName();
        }
        return dictionaryName;
    }



    /**
     * 根据字典名称查询字典id - 李爽-2017-11-29 19:25:09
     * @param dictionaryName
     * @return
     */
    @Override
    public String findDictionaryIdByName(String dictionaryName) {
        if (StringUtils.isEmpty(dictionaryName)) {
            return null;
        }
        DictionaryExample example = new DictionaryExample();
        example.createCriteria()
                .andDictNameEqualTo(dictionaryName)
                .andIsDeleteEqualTo((byte) 0);
        List<DictionaryEntity> list = dictionaryDao.selectByExample(example);
        String dictionaryId = "";
        for (DictionaryEntity dictionary : list) {
            dictionaryId = dictionary.getId();
        }
        return dictionaryId;
    }

    /**
     * 模糊查询-杜娟
     * @param strLike
     * @param dictionaryTypeName
     * @return
     */
    @Override
    public List<DictionaryEntity> fuzzyDictionaryInfoByName(String strLike,String dictionaryTypeName) {
        DictionaryExample example = new DictionaryExample();
        if (StringUtils.isEmpty(strLike)) {
            example.createCriteria()
                    .andDictTypeNameEqualTo(dictionaryTypeName)
                    .andIsDeleteEqualTo((byte) 0);
        }else{
            example.createCriteria()
                    .andDictNameLike("%" + strLike + "%")
                    .andDictTypeNameEqualTo(dictionaryTypeName)
                    .andIsDeleteEqualTo((byte) 0);
        }
        List<DictionaryEntity> list = dictionaryDao.selectByExample(example);

        List<DictionaryEntity>  dictionaryEntities = new ArrayList<>();
        for (DictionaryEntity dictionary : list) {

             DictionaryEntity info = new DictionaryEntity();
             info.setDictName(dictionary.getDictName());
             info.setTypeCode(dictionary.getTypeCode());
             info.setRemark(dictionary.getRemark());
             info.setId(dictionary.getId());
             dictionaryEntities.add(info);
        }
        return dictionaryEntities;
    }


    /**
     * 根据字典编码从字典表中查询是否有数据 -李爽-2017-12-2 19:04:09
     * @param code
     * @return
     */
    public boolean findIsHasCode(String code) {
        if(StringUtils.isEmpty(code)){
            return false;
        }
        DictionaryExample dictionaryExample = new DictionaryExample();
        dictionaryExample.createCriteria()
                .andIdEqualTo(code)
                .andIsDeleteEqualTo((byte) 0);
        List<DictionaryEntity> list = dictionaryDao.selectByExample(dictionaryExample);
        return !(list == null || list.isEmpty());
    }

    /**
     * 单条添加字典表信息 - 李爽-2017-11-29 19:29:24
     * @param dictionaryEntity
     * @return
     */
    @Override
    public int addDictionary(DictionaryEntity dictionaryEntity) {
        if (dictionaryEntity == null) {
            return 0;
        }
        if (this.findIsHasCode(dictionaryEntity.getId())) {
            return -10;
        }
        dictionaryEntity.setId(BaseUuidUtils.base58Uuid());
        return dictionaryDao.insert(dictionaryEntity);
    }

    /**
     * 批量添加字典表信息 -李爽-2017-11-29 19:29:12
     * @param dictionaryEntityList 字典list
     * @return 批量添加成功条数
     */
    @Override
    public int addDictionary(List<DictionaryEntity> dictionaryEntityList) {

        if (CollectionUtils.isEmpty(dictionaryEntityList)) {
            return 0;
        }
        for (DictionaryEntity dictionaryEntity : dictionaryEntityList) {
            if (this.findIsHasCode(dictionaryEntity.getId())) {
                return -10;
            }
        }
        //给这些字典添加上id
        for (DictionaryEntity dictionaryEntity : dictionaryEntityList) {
            dictionaryEntity.setId(BaseUuidUtils.base58Uuid());
        }
        return dictionaryDao.insertAll(dictionaryEntityList);
    }

    /**
     * 根据主键修改字段信息 -李爽-2017-11-29 19:34:32
     * @param dictionaryEntity
     * @return
     */
    @Override
    public boolean updateDictionary(DictionaryEntity dictionaryEntity) {
        return dictionaryEntity != null && !StringUtils.isEmpty(dictionaryEntity.getId()) && (dictionaryDao.updateById(dictionaryEntity) > 0 ? true : false);

    }

    /**
     * 根据字典id删除字典信息 -李爽-2017-11-29 19:35:27
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteDictionary(String id) {
        if (StringUtils.isEmpty(id)) {
            return false;
        }
        //TODO:权限控制，获取用户名，多租户
        return dictionaryDao.deleteById(id, "大米时代") > 0;

    }

    /**
     * 批量删除字典信息，根据id -李爽-2017-11-29 19:36:34
     * @param ids
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteDictionary(List<String> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return false;
        }
        //TODO:权限控制，获取用户名
        return dictionaryDao.deleteByIds(ids, "大米时代") == ids.size();

    }

    /**
     * 根据字典类型查询字典信息 -杜娟-2017-12-6 11:35:03
     * @param typeCode
     * @return
     */
    @Override
    public List<DictionaryEntity> selectDictionaryByTypeCode(String typeCode) {
        if(StringUtils.isEmpty(typeCode)){
            return null;
        }
        DictionaryExample example = new DictionaryExample();
        example.createCriteria()
                .andTypeCodeEqualTo(typeCode)
                .andIsDeleteEqualTo((byte)0);
        example.setOrderByClause("`dict_name` DESC");
        List<DictionaryEntity> dictionaryEntityList = dictionaryDao.selectByExample(example);
        return ResultEmptyUtil.getReturnValue(dictionaryEntityList);
    }

    /**
     * 查询所有的字典类型 -李爽-2017-12-6 11:45:10
     * @return
     */
    @Override
    public List<DictionaryEntity> selectAllDictionaryType() {
        List<DictionaryEntity> list = dictionaryDao.selectAllDictionaryType();
        return ResultEmptyUtil.getReturnValue(list);
    }

    /**
     * 分页查询字典类型-hgt-2018年3月10日
     * @return 字典类型list
     */
    @Override
    public PageInfo<DictionaryEntity> PageSelectDictionaryType(int page,int pageSize){
        PageHelper.startPage(page,pageSize);
        List<DictionaryEntity> dictionaryEntityList=dictionaryDao.selectAllDictionaryType();
        return new PageInfo<>(dictionaryEntityList);
    }

    /**
     *树形-杜娟
     * @return
     */
    @Override
    public List<DictionaryTreeModel> selectAllDictionaryTypeByTree() {

        List<DictionaryEntity> list = this.selectAllDictionaryType();
        List<DictionaryTreeModel> tree = new ArrayList<>();
        for (DictionaryEntity tList :list ){
           DictionaryTreeModel dictionaryTreeModel = new DictionaryTreeModel();
            dictionaryTreeModel.setId(tList.getTypeCode());
            dictionaryTreeModel.setName(tList.getDictTypeName());
            dictionaryTreeModel.setpId("111");
            dictionaryTreeModel.setnId("111");
            tree.add(dictionaryTreeModel);
        }

        return ResultEmptyUtil.getReturnValue(tree);
    }
    /**
     * 根据期数模糊查询期数id 李爽-2017-12-13 09:21:05
     * @param grade 期数
     * @return 期数id
     */
    @Override
    public List<DictionaryEntity> selectIdsByNameGrade(String grade) {
        if(StringUtils.isEmpty(grade)){
            return null;
        }
        List<DictionaryEntity> dictionaryEntityList = dictionaryDao.selectIdsByNameGrade(grade, DictionaryTypeCode.GRADE);
        return ResultEmptyUtil.getReturnValue(dictionaryEntityList);
    }

    @Override
    public List<DictionaryEntity> selectDitNameByTypeCode(String typeCode) {
        if (StringUtils.isEmpty(typeCode)){
            return null;
        }
        DictionaryExample example = new DictionaryExample();
         example.createCriteria().andTypeCodeEqualTo(typeCode);

        return dictionaryDao.selectByExample(example);
    }
}
