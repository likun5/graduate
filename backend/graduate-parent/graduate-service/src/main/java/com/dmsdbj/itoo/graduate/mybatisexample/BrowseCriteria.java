package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class BrowseCriteria extends GeneratedCriteria<BrowseCriteria> implements Serializable{

    protected BrowseCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String BROWSE_PERSON_ID = "browsePersonId";   
    private static final String NOTICE_ID = "noticeId";   
    private static final String IS_BROWSE = "isBrowse";   
    private static final String BROWSE_TIME = "browseTime";   
    private static final String OPERATOR_ID = "operatorId";   
    private static final String TIMESTAMP_TIME = "timestampTime";   

    public BrowseCriteria andBrowsePersonIdIsNull() {
        addCriterion("browse_person_id is null");
        return this;
    }

    public BrowseCriteria andBrowsePersonIdIsNotNull() {
        addCriterion("browse_person_id is not null");
        return this;
    }
    public BrowseCriteria andBrowsePersonIdEqualTo(String value) {
        addCriterion("browse_person_id =", value, BROWSE_PERSON_ID);
        return this;
    }
    public BrowseCriteria andBrowsePersonIdNotEqualTo(String value) {
        addCriterion("browse_person_id <>", value, BROWSE_PERSON_ID);
        return this;
    }    
    public BrowseCriteria andBrowsePersonIdGreaterThan(String value) {
        addCriterion("browse_person_id >", value, BROWSE_PERSON_ID);
        return this;
    }    
    public BrowseCriteria andBrowsePersonIdGreaterThanOrEqualTo(String value) {
        addCriterion("browse_person_id >=", value, BROWSE_PERSON_ID);
        return this;
    }    
    public BrowseCriteria andBrowsePersonIdLessThan(String value) {
        addCriterion("browse_person_id <", value, BROWSE_PERSON_ID);
        return this;
    }     
    public BrowseCriteria andBrowsePersonIdLessThanOrEqualTo(String value) {
        addCriterion("browse_person_id <=", value, BROWSE_PERSON_ID);
        return this;
    }
    public BrowseCriteria andBrowsePersonIdIn(List<String> values) {
        addCriterion("browse_person_id in", values, BROWSE_PERSON_ID);
        return this;
    }
    public BrowseCriteria andBrowsePersonIdNotIn(List<String> values) {
        addCriterion("browse_person_id not in", values, BROWSE_PERSON_ID);
        return this;
    }
    public BrowseCriteria andBrowsePersonIdBetween(String value1, String value2) {
        addCriterion("browse_person_id between", value1, value2, BROWSE_PERSON_ID);
        return this;
    }
    public BrowseCriteria andBrowsePersonIdNotBetween(String value1, String value2) {
        addCriterion("browse_person_id not between", value1, value2, BROWSE_PERSON_ID);
        return this;
    }
        public BrowseCriteria andBrowsePersonIdLike(String value) {
            addCriterion("browse_person_id like", value, BROWSE_PERSON_ID);
            return this;
        }

        public BrowseCriteria andBrowsePersonIdNotLike(String value) {
            addCriterion("browse_person_id not like", value, BROWSE_PERSON_ID);
            return this;
        }
    public BrowseCriteria andNoticeIdIsNull() {
        addCriterion("notice_id is null");
        return this;
    }

    public BrowseCriteria andNoticeIdIsNotNull() {
        addCriterion("notice_id is not null");
        return this;
    }
    public BrowseCriteria andNoticeIdEqualTo(String value) {
        addCriterion("notice_id =", value, NOTICE_ID);
        return this;
    }
    public BrowseCriteria andNoticeIdNotEqualTo(String value) {
        addCriterion("notice_id <>", value, NOTICE_ID);
        return this;
    }    
    public BrowseCriteria andNoticeIdGreaterThan(String value) {
        addCriterion("notice_id >", value, NOTICE_ID);
        return this;
    }    
    public BrowseCriteria andNoticeIdGreaterThanOrEqualTo(String value) {
        addCriterion("notice_id >=", value, NOTICE_ID);
        return this;
    }    
    public BrowseCriteria andNoticeIdLessThan(String value) {
        addCriterion("notice_id <", value, NOTICE_ID);
        return this;
    }     
    public BrowseCriteria andNoticeIdLessThanOrEqualTo(String value) {
        addCriterion("notice_id <=", value, NOTICE_ID);
        return this;
    }
    public BrowseCriteria andNoticeIdIn(List<String> values) {
        addCriterion("notice_id in", values, NOTICE_ID);
        return this;
    }
    public BrowseCriteria andNoticeIdNotIn(List<String> values) {
        addCriterion("notice_id not in", values, NOTICE_ID);
        return this;
    }
    public BrowseCriteria andNoticeIdBetween(String value1, String value2) {
        addCriterion("notice_id between", value1, value2, NOTICE_ID);
        return this;
    }
    public BrowseCriteria andNoticeIdNotBetween(String value1, String value2) {
        addCriterion("notice_id not between", value1, value2, NOTICE_ID);
        return this;
    }
        public BrowseCriteria andNoticeIdLike(String value) {
            addCriterion("notice_id like", value, NOTICE_ID);
            return this;
        }

        public BrowseCriteria andNoticeIdNotLike(String value) {
            addCriterion("notice_id not like", value, NOTICE_ID);
            return this;
        }
    public BrowseCriteria andIsBrowseIsNull() {
        addCriterion("is_browse is null");
        return this;
    }

    public BrowseCriteria andIsBrowseIsNotNull() {
        addCriterion("is_browse is not null");
        return this;
    }
    public BrowseCriteria andIsBrowseEqualTo(String value) {
        addCriterion("is_browse =", value, IS_BROWSE);
        return this;
    }
    public BrowseCriteria andIsBrowseNotEqualTo(String value) {
        addCriterion("is_browse <>", value, IS_BROWSE);
        return this;
    }    
    public BrowseCriteria andIsBrowseGreaterThan(String value) {
        addCriterion("is_browse >", value, IS_BROWSE);
        return this;
    }    
    public BrowseCriteria andIsBrowseGreaterThanOrEqualTo(String value) {
        addCriterion("is_browse >=", value, IS_BROWSE);
        return this;
    }    
    public BrowseCriteria andIsBrowseLessThan(String value) {
        addCriterion("is_browse <", value, IS_BROWSE);
        return this;
    }     
    public BrowseCriteria andIsBrowseLessThanOrEqualTo(String value) {
        addCriterion("is_browse <=", value, IS_BROWSE);
        return this;
    }
    public BrowseCriteria andIsBrowseIn(List<String> values) {
        addCriterion("is_browse in", values, IS_BROWSE);
        return this;
    }
    public BrowseCriteria andIsBrowseNotIn(List<String> values) {
        addCriterion("is_browse not in", values, IS_BROWSE);
        return this;
    }
    public BrowseCriteria andIsBrowseBetween(String value1, String value2) {
        addCriterion("is_browse between", value1, value2, IS_BROWSE);
        return this;
    }
    public BrowseCriteria andIsBrowseNotBetween(String value1, String value2) {
        addCriterion("is_browse not between", value1, value2, IS_BROWSE);
        return this;
    }
        public BrowseCriteria andIsBrowseLike(String value) {
            addCriterion("is_browse like", value, IS_BROWSE);
            return this;
        }

        public BrowseCriteria andIsBrowseNotLike(String value) {
            addCriterion("is_browse not like", value, IS_BROWSE);
            return this;
        }
    public BrowseCriteria andBrowseTimeIsNull() {
        addCriterion("browse_time is null");
        return this;
    }

    public BrowseCriteria andBrowseTimeIsNotNull() {
        addCriterion("browse_time is not null");
        return this;
    }
    public BrowseCriteria andBrowseTimeEqualTo(String value) {
        addCriterion("browse_time =", value, BROWSE_TIME);
        return this;
    }
    public BrowseCriteria andBrowseTimeNotEqualTo(String value) {
        addCriterion("browse_time <>", value, BROWSE_TIME);
        return this;
    }    
    public BrowseCriteria andBrowseTimeGreaterThan(String value) {
        addCriterion("browse_time >", value, BROWSE_TIME);
        return this;
    }    
    public BrowseCriteria andBrowseTimeGreaterThanOrEqualTo(String value) {
        addCriterion("browse_time >=", value, BROWSE_TIME);
        return this;
    }    
    public BrowseCriteria andBrowseTimeLessThan(String value) {
        addCriterion("browse_time <", value, BROWSE_TIME);
        return this;
    }     
    public BrowseCriteria andBrowseTimeLessThanOrEqualTo(String value) {
        addCriterion("browse_time <=", value, BROWSE_TIME);
        return this;
    }
    public BrowseCriteria andBrowseTimeIn(List<String> values) {
        addCriterion("browse_time in", values, BROWSE_TIME);
        return this;
    }
    public BrowseCriteria andBrowseTimeNotIn(List<String> values) {
        addCriterion("browse_time not in", values, BROWSE_TIME);
        return this;
    }
    public BrowseCriteria andBrowseTimeBetween(String value1, String value2) {
        addCriterion("browse_time between", value1, value2, BROWSE_TIME);
        return this;
    }
    public BrowseCriteria andBrowseTimeNotBetween(String value1, String value2) {
        addCriterion("browse_time not between", value1, value2, BROWSE_TIME);
        return this;
    }
        public BrowseCriteria andBrowseTimeLike(String value) {
            addCriterion("browse_time like", value, BROWSE_TIME);
            return this;
        }

        public BrowseCriteria andBrowseTimeNotLike(String value) {
            addCriterion("browse_time not like", value, BROWSE_TIME);
            return this;
        }
    public BrowseCriteria andOperatorIdIsNull() {
        addCriterion("operator_id is null");
        return this;
    }

    public BrowseCriteria andOperatorIdIsNotNull() {
        addCriterion("operator_id is not null");
        return this;
    }
    public BrowseCriteria andOperatorIdEqualTo(String value) {
        addCriterion("operator_id =", value, OPERATOR_ID);
        return this;
    }
    public BrowseCriteria andOperatorIdNotEqualTo(String value) {
        addCriterion("operator_id <>", value, OPERATOR_ID);
        return this;
    }    
    public BrowseCriteria andOperatorIdGreaterThan(String value) {
        addCriterion("operator_id >", value, OPERATOR_ID);
        return this;
    }    
    public BrowseCriteria andOperatorIdGreaterThanOrEqualTo(String value) {
        addCriterion("operator_id >=", value, OPERATOR_ID);
        return this;
    }    
    public BrowseCriteria andOperatorIdLessThan(String value) {
        addCriterion("operator_id <", value, OPERATOR_ID);
        return this;
    }     
    public BrowseCriteria andOperatorIdLessThanOrEqualTo(String value) {
        addCriterion("operator_id <=", value, OPERATOR_ID);
        return this;
    }
    public BrowseCriteria andOperatorIdIn(List<String> values) {
        addCriterion("operator_id in", values, OPERATOR_ID);
        return this;
    }
    public BrowseCriteria andOperatorIdNotIn(List<String> values) {
        addCriterion("operator_id not in", values, OPERATOR_ID);
        return this;
    }
    public BrowseCriteria andOperatorIdBetween(String value1, String value2) {
        addCriterion("operator_id between", value1, value2, OPERATOR_ID);
        return this;
    }
    public BrowseCriteria andOperatorIdNotBetween(String value1, String value2) {
        addCriterion("operator_id not between", value1, value2, OPERATOR_ID);
        return this;
    }
        public BrowseCriteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, OPERATOR_ID);
            return this;
        }

        public BrowseCriteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, OPERATOR_ID);
            return this;
        }
    public BrowseCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public BrowseCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public BrowseCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public BrowseCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public BrowseCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public BrowseCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public BrowseCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public BrowseCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public BrowseCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public BrowseCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public BrowseCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public BrowseCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
}

