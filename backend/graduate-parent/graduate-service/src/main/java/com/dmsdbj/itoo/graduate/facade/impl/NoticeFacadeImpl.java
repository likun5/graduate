package com.dmsdbj.itoo.graduate.facade.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.dmsdbj.itoo.graduate.entity.NoticeEntity;
import com.dmsdbj.itoo.graduate.entity.ext.NoticeBrowseModel;
import com.dmsdbj.itoo.graduate.entity.ext.NoticeNoticeCategoryModel;
import com.dmsdbj.itoo.graduate.facade.NoticeFacade;
import com.dmsdbj.itoo.graduate.service.NoticeService;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;

/**
 *@author hgt
 *create: 2017-11-27 11:22:13
 * DESCRIPTION 通知Facade实现
 */
@Component("noticeFacade")
@Service
public class NoticeFacadeImpl implements NoticeFacade {

    //打印日志相关
	private static final Logger logger = LoggerFactory.getLogger(NoticeFacadeImpl.class);

    @Autowired
    NoticeService noticeService;




    /**
     * 添加通知信息-郝贵婷-2017年12月4日
     * @param noticeBrowseModel 通知浏览Model
     * @return  受影响行数
     */
    @Override
    //@Transactional()
    public int addNotice(NoticeBrowseModel noticeBrowseModel){
        int count=0;
        count= noticeService.addNotice(noticeBrowseModel);
        return count;
    }


    /**
     * 删除通知信息-郝贵婷-2017年12月4日
     * @param  noticeID 通知id
     * @param operator 操作人
     * @return  受影响行数
     */
    @Override
    public int deleteNotice(String noticeID,String operator) {
        int count=0;
            count= noticeService.deleteNotice(noticeID, operator);
        return count;
    }

    /**
     * 修改通知信息-郝贵婷-2017年12月4日
     * @param notice 通知实体
     * @return  受影响行数
     */
    @Override
    public int updateNotice(NoticeEntity notice){
        int count=0;
            count= noticeService.updateNotice(notice);
        return count;
    }


    /**
     * 根据通知分类统计通知数量-郝贵婷-2018年1月9日
     * @param columnId 通知类别
     * @return 受影响行数
     */
    @Override
    public int selectNoticeCountByColumnId(String columnId) {
        int count=0;
            count =noticeService.selectNoticeCountByColumnId(columnId);
        return  count;
    }


    /**
     * 根据id查询Notice -郝贵婷-2017年11月27日
     * @param id 通知id
     * @return NoticeEntity 通知实体
     */
    @Override
    public NoticeNoticeCategoryModel findNoticeNoticeCategoryModelById(String id) {
        NoticeNoticeCategoryModel noticeNoticeCategoryModel;
            noticeNoticeCategoryModel=noticeService.findNoticeNoticeCategoryModelById(id);
        return noticeNoticeCategoryModel;
    }

    /**
     * 查询全部通知信息-郝贵婷-2017年11月27日
     * @return 通知list
     */
    @Override
    public List<NoticeNoticeCategoryModel> selectNoticeNoticeCategory(){
        List<NoticeNoticeCategoryModel> noticeNoticeCategoryModelList;
            noticeNoticeCategoryModelList=noticeService.selectNoticeNoticeCategory();
        return noticeNoticeCategoryModelList;
    }

    /**
     * 根分类ID查询通知-郝贵婷-2017年12月2日
     * @param columnId 通知类别
     * @return 通知list
     */
    public List<NoticeNoticeCategoryModel> selectNoticeNoticeCategoryByColumnId(String columnId){
        List<NoticeNoticeCategoryModel> noticeNoticeCategoryModelList;
            noticeNoticeCategoryModelList=noticeService.selectNoticeNoticeCategoryByColumnId(columnId);
        return noticeNoticeCategoryModelList;
    }

    /**
     * 假分页-查询全部通知信息-郝贵婷-2018-01-11 14:15:45
     * @param page 页码
     * @param  pageSize 页大小
     * @return 通知list
     */
    @Override
    public PageInfo<NoticeNoticeCategoryModel> PageSelectNoticeNoticeCategory(int page, int pageSize){
        return noticeService.PageSelectNoticeNoticeCategory(page,pageSize);
    }

    /**
     * 假分页-根据分类ID查询通知-郝贵婷-2018年01月10日
     * @param columnId  分类Id
     * @param page 页码
     * @param  pageSize 页大小
     * @return 通知list
     */
    @Override
    public PageInfo<NoticeNoticeCategoryModel> PageSelectNoticeNoticeCategoryByColumnId(String columnId,int page, int pageSize){
        return noticeService.PageSelectNoticeNoticeCategoryByColumnId(columnId,page,pageSize);
    }


    /**
     * 更新通知信息（包括接受人）-郝贵婷-2017-12-5 14:15:45
     * @param noticeBrowseModel 浏览记录实体
     * @return 受影响行
     */
    public int updateNoticeBrowse(NoticeBrowseModel noticeBrowseModel){
        int resultCount=0;
            resultCount=noticeService.updateNoticeBrowse(noticeBrowseModel);
        return resultCount;
    }
}
