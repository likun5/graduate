package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.ForumCommentExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.ForumCommentDao;
import com.dmsdbj.itoo.graduate.service.ForumCommentService;
import com.dmsdbj.itoo.graduate.entity.ForumCommentEntity;

/**
 * @author ls
 * create:2017-12-27 09:54:26
 * DESCRIPTION
 */ 
@Service("forumCommentService")
public class ForumCommentServiceImpl extends BaseServiceImpl<ForumCommentEntity, ForumCommentExample> implements ForumCommentService {


	@Autowired
    private ForumCommentDao forumCommentDao;

    /**
     *
     * @return BaseDao<ForumCommentEntity, ForumCommentExample>
     */
    @Override
    public  BaseDao<ForumCommentEntity, ForumCommentExample> getRealDao(){
        return this.forumCommentDao;
    }
}
