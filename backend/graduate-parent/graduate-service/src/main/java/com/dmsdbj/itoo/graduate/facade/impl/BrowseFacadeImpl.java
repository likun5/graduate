package com.dmsdbj.itoo.graduate.facade.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.dmsdbj.itoo.graduate.entity.BrowseEntity;
import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.PeriodPersonModel;
import com.dmsdbj.itoo.graduate.entity.ext.PersonGradeModel;
import com.dmsdbj.itoo.graduate.entity.ext.PersonInfoModel;
import com.dmsdbj.itoo.graduate.facade.BrowseFacade;
import com.dmsdbj.itoo.graduate.service.BrowseService;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 徐玲博
 * create: 2017-11-26 16:14:54
 * DESCRIPTION
 */
@Component("browseFacade")
@Service
public class BrowseFacadeImpl implements BrowseFacade {

    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(BrowseFacadeImpl.class);

    @Autowired
    BrowseService browseService;

    /**
     * 根据id查询Browse
     *
     * @param id
     * @return BrowseEntity
     */
    @Override
    public BrowseEntity findById(String id) {
        return browseService.findById(id);
    }

    /**
     * 根据浏览人ID查询未浏览记录-郝贵婷-2017年12月3日
     *
     * @param browsePersonId 浏览人id
     * @return 受影响行
     */
    @Override
    public int selectNoBrowseByBrowsePersonId(String browsePersonId) {
        int count = 0;
        count = browseService.selectNoBrowseByBrowsePersonId(browsePersonId);
        return count;
    }

    /**
     * 根据浏览人ID查询已浏览记录-郝贵婷-2017年12月3日
     *
     * @param browsePersonId 浏览人id
     * @return 受影响行
     */
    @Override
    public int selectBrowsedByBrowsePersonId(String browsePersonId) {
        int count = 0;
        count = browseService.selectBrowsedByBrowsePersonId(browsePersonId);
        return count;
    }


    /**
     * 更新浏览记录-郝贵婷-2017年12月3日
     *
     * @param noticeId 通知id
     * @param browsePersonId  浏览人id
     * @return 受影响行
     */
    @Override
    public int updateBrowseIsBrowse(String noticeId, String browsePersonId) {
        int count = 0;
        count = browseService.updateBrowseIsBrowse(noticeId, browsePersonId);
        return count;
    }

    /**
     * 根据通知ID、是否浏览查询浏览人信息-郝贵婷-2017年12月3日
     *
     * @param noticeId 通知id
     * @param isBrowse 是否浏览
     * @return 浏览人list列表
     */
    @Override
    public List<PersonInfoModel> selectBrowseByNoticeId(String noticeId, String isBrowse) {
        List<PersonInfoModel> personalInfoEntityList;
        try {
            personalInfoEntityList = browseService.selectBrowseByNoticeId(noticeId, isBrowse);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return personalInfoEntityList;
    }

    /**
     * 分页-根据通知ID、是否浏览查询浏览人信息-郝贵婷-2018年01月11日
     *
     * @param noticeId  通知Id
     * @param isBrowse  是否浏览
     * @param page 页码
     * @param  pageSize 页大小
     * @return 浏览人list
     */
    @Override
    public PageInfo<PersonalInfoEntity> PageSelectBrowseByNoticeId(String noticeId, String isBrowse, int page, int pageSize) {
        return browseService.PageSelectBrowseByNoticeId(noticeId, isBrowse, page, pageSize);
    }


    /**
     * 查询所有期数及对应期数人员 郝贵婷 -2018年1月14日
     *
     * @return 期数及人员list列表
     */
    @Override
    public List<PeriodPersonModel> findPeriodPersonModels() {
        return browseService.findPeriodPersonModels();
    }

    /**
     * 根据通知ID查询全部浏览人-郝贵婷-2017-12-5 14:15:45 selectAllBrowserByNoticeId
     *
     * @param noticeId 通知id
     * @return 受影响行
     */
    @Override
    public List<PersonalInfoEntity> selectAllBrowserByNoticeId(String noticeId) {
        List<PersonalInfoEntity> personalInfoEntityList;
        try {
            personalInfoEntityList = browseService.selectAllBrowserByNoticeId(noticeId);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return personalInfoEntityList;
    }

    /**
     * 查询全部接受人员（树形结构）-hgt-2018-1-30
     * @return 接收人list
     */
    public List<PersonGradeModel> selectAllBrowsersIncludeGradeName() {
        List<PersonGradeModel> personGradeModelList;
        try {
            personGradeModelList = browseService.selectAllBrowsersIncludeGradeName();
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return personGradeModelList;
    }


    /**
     * 查询全部期数（构建树形结构）--hgt-2018-1-30
     * @param typeCode  字典类别
     * @return 期数list
     */
    @Override
    public List<DictionaryEntity> selectAllGrade(String typeCode) {
        try {
            return browseService.selectAllGrade(typeCode);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }

    }
}
