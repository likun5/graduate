package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class PictureCriteria extends GeneratedCriteria<PictureCriteria> implements Serializable{

    protected PictureCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String KEY_ID = "keyId";   
    private static final String PICTURE_TYPE_ID = "pictureTypeId";   
    private static final String OPERATOR_ID = "operatorId";   
    private static final String TIMESTAMP_TIME = "timestampTime";   
    private static final String PICTURE_URL = "pictureUrl";   

    public PictureCriteria andKeyIdIsNull() {
        addCriterion("key_id is null");
        return this;
    }

    public PictureCriteria andKeyIdIsNotNull() {
        addCriterion("key_id is not null");
        return this;
    }
    public PictureCriteria andKeyIdEqualTo(String value) {
        addCriterion("key_id =", value, KEY_ID);
        return this;
    }
    public PictureCriteria andKeyIdNotEqualTo(String value) {
        addCriterion("key_id <>", value, KEY_ID);
        return this;
    }    
    public PictureCriteria andKeyIdGreaterThan(String value) {
        addCriterion("key_id >", value, KEY_ID);
        return this;
    }    
    public PictureCriteria andKeyIdGreaterThanOrEqualTo(String value) {
        addCriterion("key_id >=", value, KEY_ID);
        return this;
    }    
    public PictureCriteria andKeyIdLessThan(String value) {
        addCriterion("key_id <", value, KEY_ID);
        return this;
    }     
    public PictureCriteria andKeyIdLessThanOrEqualTo(String value) {
        addCriterion("key_id <=", value, KEY_ID);
        return this;
    }
    public PictureCriteria andKeyIdIn(List<String> values) {
        addCriterion("key_id in", values, KEY_ID);
        return this;
    }
    public PictureCriteria andKeyIdNotIn(List<String> values) {
        addCriterion("key_id not in", values, KEY_ID);
        return this;
    }
    public PictureCriteria andKeyIdBetween(String value1, String value2) {
        addCriterion("key_id between", value1, value2, KEY_ID);
        return this;
    }
    public PictureCriteria andKeyIdNotBetween(String value1, String value2) {
        addCriterion("key_id not between", value1, value2, KEY_ID);
        return this;
    }
        public PictureCriteria andKeyIdLike(String value) {
            addCriterion("key_id like", value, KEY_ID);
            return this;
        }

        public PictureCriteria andKeyIdNotLike(String value) {
            addCriterion("key_id not like", value, KEY_ID);
            return this;
        }
    public PictureCriteria andPictureTypeIdIsNull() {
        addCriterion("picture_type_id is null");
        return this;
    }

    public PictureCriteria andPictureTypeIdIsNotNull() {
        addCriterion("picture_type_id is not null");
        return this;
    }
    public PictureCriteria andPictureTypeIdEqualTo(String value) {
        addCriterion("picture_type_id =", value, PICTURE_TYPE_ID);
        return this;
    }
    public PictureCriteria andPictureTypeIdNotEqualTo(String value) {
        addCriterion("picture_type_id <>", value, PICTURE_TYPE_ID);
        return this;
    }    
    public PictureCriteria andPictureTypeIdGreaterThan(String value) {
        addCriterion("picture_type_id >", value, PICTURE_TYPE_ID);
        return this;
    }    
    public PictureCriteria andPictureTypeIdGreaterThanOrEqualTo(String value) {
        addCriterion("picture_type_id >=", value, PICTURE_TYPE_ID);
        return this;
    }    
    public PictureCriteria andPictureTypeIdLessThan(String value) {
        addCriterion("picture_type_id <", value, PICTURE_TYPE_ID);
        return this;
    }     
    public PictureCriteria andPictureTypeIdLessThanOrEqualTo(String value) {
        addCriterion("picture_type_id <=", value, PICTURE_TYPE_ID);
        return this;
    }
    public PictureCriteria andPictureTypeIdIn(List<String> values) {
        addCriterion("picture_type_id in", values, PICTURE_TYPE_ID);
        return this;
    }
    public PictureCriteria andPictureTypeIdNotIn(List<String> values) {
        addCriterion("picture_type_id not in", values, PICTURE_TYPE_ID);
        return this;
    }
    public PictureCriteria andPictureTypeIdBetween(String value1, String value2) {
        addCriterion("picture_type_id between", value1, value2, PICTURE_TYPE_ID);
        return this;
    }
    public PictureCriteria andPictureTypeIdNotBetween(String value1, String value2) {
        addCriterion("picture_type_id not between", value1, value2, PICTURE_TYPE_ID);
        return this;
    }
        public PictureCriteria andPictureTypeIdLike(String value) {
            addCriterion("picture_type_id like", value, PICTURE_TYPE_ID);
            return this;
        }

        public PictureCriteria andPictureTypeIdNotLike(String value) {
            addCriterion("picture_type_id not like", value, PICTURE_TYPE_ID);
            return this;
        }
    public PictureCriteria andOperatorIdIsNull() {
        addCriterion("operator_id is null");
        return this;
    }

    public PictureCriteria andOperatorIdIsNotNull() {
        addCriterion("operator_id is not null");
        return this;
    }
    public PictureCriteria andOperatorIdEqualTo(String value) {
        addCriterion("operator_id =", value, OPERATOR_ID);
        return this;
    }
    public PictureCriteria andOperatorIdNotEqualTo(String value) {
        addCriterion("operator_id <>", value, OPERATOR_ID);
        return this;
    }    
    public PictureCriteria andOperatorIdGreaterThan(String value) {
        addCriterion("operator_id >", value, OPERATOR_ID);
        return this;
    }    
    public PictureCriteria andOperatorIdGreaterThanOrEqualTo(String value) {
        addCriterion("operator_id >=", value, OPERATOR_ID);
        return this;
    }    
    public PictureCriteria andOperatorIdLessThan(String value) {
        addCriterion("operator_id <", value, OPERATOR_ID);
        return this;
    }     
    public PictureCriteria andOperatorIdLessThanOrEqualTo(String value) {
        addCriterion("operator_id <=", value, OPERATOR_ID);
        return this;
    }
    public PictureCriteria andOperatorIdIn(List<String> values) {
        addCriterion("operator_id in", values, OPERATOR_ID);
        return this;
    }
    public PictureCriteria andOperatorIdNotIn(List<String> values) {
        addCriterion("operator_id not in", values, OPERATOR_ID);
        return this;
    }
    public PictureCriteria andOperatorIdBetween(String value1, String value2) {
        addCriterion("operator_id between", value1, value2, OPERATOR_ID);
        return this;
    }
    public PictureCriteria andOperatorIdNotBetween(String value1, String value2) {
        addCriterion("operator_id not between", value1, value2, OPERATOR_ID);
        return this;
    }
        public PictureCriteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, OPERATOR_ID);
            return this;
        }

        public PictureCriteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, OPERATOR_ID);
            return this;
        }
    public PictureCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public PictureCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public PictureCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public PictureCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public PictureCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public PictureCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public PictureCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public PictureCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public PictureCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public PictureCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public PictureCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public PictureCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public PictureCriteria andPictureUrlIsNull() {
        addCriterion("picture_url is null");
        return this;
    }

    public PictureCriteria andPictureUrlIsNotNull() {
        addCriterion("picture_url is not null");
        return this;
    }
    public PictureCriteria andPictureUrlEqualTo(String value) {
        addCriterion("picture_url =", value, PICTURE_URL);
        return this;
    }
    public PictureCriteria andPictureUrlNotEqualTo(String value) {
        addCriterion("picture_url <>", value, PICTURE_URL);
        return this;
    }    
    public PictureCriteria andPictureUrlGreaterThan(String value) {
        addCriterion("picture_url >", value, PICTURE_URL);
        return this;
    }    
    public PictureCriteria andPictureUrlGreaterThanOrEqualTo(String value) {
        addCriterion("picture_url >=", value, PICTURE_URL);
        return this;
    }    
    public PictureCriteria andPictureUrlLessThan(String value) {
        addCriterion("picture_url <", value, PICTURE_URL);
        return this;
    }     
    public PictureCriteria andPictureUrlLessThanOrEqualTo(String value) {
        addCriterion("picture_url <=", value, PICTURE_URL);
        return this;
    }
    public PictureCriteria andPictureUrlIn(List<String> values) {
        addCriterion("picture_url in", values, PICTURE_URL);
        return this;
    }
    public PictureCriteria andPictureUrlNotIn(List<String> values) {
        addCriterion("picture_url not in", values, PICTURE_URL);
        return this;
    }
    public PictureCriteria andPictureUrlBetween(String value1, String value2) {
        addCriterion("picture_url between", value1, value2, PICTURE_URL);
        return this;
    }
    public PictureCriteria andPictureUrlNotBetween(String value1, String value2) {
        addCriterion("picture_url not between", value1, value2, PICTURE_URL);
        return this;
    }
        public PictureCriteria andPictureUrlLike(String value) {
            addCriterion("picture_url like", value, PICTURE_URL);
            return this;
        }

        public PictureCriteria andPictureUrlNotLike(String value) {
            addCriterion("picture_url not like", value, PICTURE_URL);
            return this;
        }
}

