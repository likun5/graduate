package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import com.dmsdbj.itoo.graduate.entity.ext.PeriodSalaryModel;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.SalaryEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.SalaryExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *@author 徐玲博
 *create: 2017-11-26 16:14:54
 *DESCRIPTION:
 */
@Repository
public interface SalaryDao extends BaseDao<SalaryEntity,SalaryExample> {
    /**
     * 根据期数ID查询每期的每年平均、最高、最低薪资
     * @param periodId 期数ID
     * @return 每期的每年平均、最高、最低薪资统计数据
     */
    List<PeriodSalaryModel> selectAMMSalaryByPeriodId(@Param("periodId") String periodId);

    /**
     * 查询今年每期平均、最高、最低薪资统计 郑晓东 2018年1月20日16点38分
     * @return 薪资统计集合
     */
    List<PeriodSalaryModel> selectAMMSalaryInThisYear();

    /**
     * 查询每期毕业时平均、最高、最低薪资统计  郑晓东 2018年1月20日
     * @return 薪资统计集合
     */
    List<PeriodSalaryModel> selectAMMSalaryInGradeYear();


    /**
     * 根据毕业生id查询薪资信息-haoguiting-2018年3月14日
     * @param userId 登录用户id
     * @return  薪资实体
     */
    List<CompanySalaryModel> selectSalaryByUserId(String userId);
}
