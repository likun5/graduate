package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class PersonCompanyRelationCriteria extends GeneratedCriteria<PersonCompanyRelationCriteria> implements Serializable{

    protected PersonCompanyRelationCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String USER_ID = "userId";   
    private static final String COMPANY_ID = "companyId";   
    private static final String ENTRY_TIME = "entryTime";   
    private static final String QUIT_TIME = "quitTime";   
    private static final String FUNNY_THING = "funnyThing";   
    private static final String POSITION = "position";   
    private static final String RECRUITMENT_INFO = "recruitmentInfo";   
    private static final String IS_DEPARTURE = "isDeparture";   
    private static final String OPERATOR_ID = "operatorId";   
    private static final String TIMESTAMP_TIME = "timestampTime";   

    public PersonCompanyRelationCriteria andUserIdIsNull() {
        addCriterion("user_id is null");
        return this;
    }

    public PersonCompanyRelationCriteria andUserIdIsNotNull() {
        addCriterion("user_id is not null");
        return this;
    }
    public PersonCompanyRelationCriteria andUserIdEqualTo(String value) {
        addCriterion("user_id =", value, USER_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andUserIdNotEqualTo(String value) {
        addCriterion("user_id <>", value, USER_ID);
        return this;
    }    
    public PersonCompanyRelationCriteria andUserIdGreaterThan(String value) {
        addCriterion("user_id >", value, USER_ID);
        return this;
    }    
    public PersonCompanyRelationCriteria andUserIdGreaterThanOrEqualTo(String value) {
        addCriterion("user_id >=", value, USER_ID);
        return this;
    }    
    public PersonCompanyRelationCriteria andUserIdLessThan(String value) {
        addCriterion("user_id <", value, USER_ID);
        return this;
    }     
    public PersonCompanyRelationCriteria andUserIdLessThanOrEqualTo(String value) {
        addCriterion("user_id <=", value, USER_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andUserIdIn(List<String> values) {
        addCriterion("user_id in", values, USER_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andUserIdNotIn(List<String> values) {
        addCriterion("user_id not in", values, USER_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andUserIdBetween(String value1, String value2) {
        addCriterion("user_id between", value1, value2, USER_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andUserIdNotBetween(String value1, String value2) {
        addCriterion("user_id not between", value1, value2, USER_ID);
        return this;
    }
        public PersonCompanyRelationCriteria andUserIdLike(String value) {
            addCriterion("user_id like", value, USER_ID);
            return this;
        }

        public PersonCompanyRelationCriteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, USER_ID);
            return this;
        }
    public PersonCompanyRelationCriteria andCompanyIdIsNull() {
        addCriterion("company_id is null");
        return this;
    }

    public PersonCompanyRelationCriteria andCompanyIdIsNotNull() {
        addCriterion("company_id is not null");
        return this;
    }
    public PersonCompanyRelationCriteria andCompanyIdEqualTo(String value) {
        addCriterion("company_id =", value, COMPANY_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andCompanyIdNotEqualTo(String value) {
        addCriterion("company_id <>", value, COMPANY_ID);
        return this;
    }    
    public PersonCompanyRelationCriteria andCompanyIdGreaterThan(String value) {
        addCriterion("company_id >", value, COMPANY_ID);
        return this;
    }    
    public PersonCompanyRelationCriteria andCompanyIdGreaterThanOrEqualTo(String value) {
        addCriterion("company_id >=", value, COMPANY_ID);
        return this;
    }    
    public PersonCompanyRelationCriteria andCompanyIdLessThan(String value) {
        addCriterion("company_id <", value, COMPANY_ID);
        return this;
    }     
    public PersonCompanyRelationCriteria andCompanyIdLessThanOrEqualTo(String value) {
        addCriterion("company_id <=", value, COMPANY_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andCompanyIdIn(List<String> values) {
        addCriterion("company_id in", values, COMPANY_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andCompanyIdNotIn(List<String> values) {
        addCriterion("company_id not in", values, COMPANY_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andCompanyIdBetween(String value1, String value2) {
        addCriterion("company_id between", value1, value2, COMPANY_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andCompanyIdNotBetween(String value1, String value2) {
        addCriterion("company_id not between", value1, value2, COMPANY_ID);
        return this;
    }
        public PersonCompanyRelationCriteria andCompanyIdLike(String value) {
            addCriterion("company_id like", value, COMPANY_ID);
            return this;
        }

        public PersonCompanyRelationCriteria andCompanyIdNotLike(String value) {
            addCriterion("company_id not like", value, COMPANY_ID);
            return this;
        }
    public PersonCompanyRelationCriteria andEntryTimeIsNull() {
        addCriterion("entry_time is null");
        return this;
    }

    public PersonCompanyRelationCriteria andEntryTimeIsNotNull() {
        addCriterion("entry_time is not null");
        return this;
    }
    public PersonCompanyRelationCriteria andEntryTimeEqualTo(Date value) {
        addCriterion("entry_time =", value, ENTRY_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andEntryTimeNotEqualTo(Date value) {
        addCriterion("entry_time <>", value, ENTRY_TIME);
        return this;
    }    
    public PersonCompanyRelationCriteria andEntryTimeGreaterThan(Date value) {
        addCriterion("entry_time >", value, ENTRY_TIME);
        return this;
    }    
    public PersonCompanyRelationCriteria andEntryTimeGreaterThanOrEqualTo(Date value) {
        addCriterion("entry_time >=", value, ENTRY_TIME);
        return this;
    }    
    public PersonCompanyRelationCriteria andEntryTimeLessThan(Date value) {
        addCriterion("entry_time <", value, ENTRY_TIME);
        return this;
    }     
    public PersonCompanyRelationCriteria andEntryTimeLessThanOrEqualTo(Date value) {
        addCriterion("entry_time <=", value, ENTRY_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andEntryTimeIn(List<Date> values) {
        addCriterion("entry_time in", values, ENTRY_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andEntryTimeNotIn(List<Date> values) {
        addCriterion("entry_time not in", values, ENTRY_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andEntryTimeBetween(Date value1, Date value2) {
        addCriterion("entry_time between", value1, value2, ENTRY_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andEntryTimeNotBetween(Date value1, Date value2) {
        addCriterion("entry_time not between", value1, value2, ENTRY_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andQuitTimeIsNull() {
        addCriterion("quit_time is null");
        return this;
    }

    public PersonCompanyRelationCriteria andQuitTimeIsNotNull() {
        addCriterion("quit_time is not null");
        return this;
    }
    public PersonCompanyRelationCriteria andQuitTimeEqualTo(Date value) {
        addCriterion("quit_time =", value, QUIT_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andQuitTimeNotEqualTo(Date value) {
        addCriterion("quit_time <>", value, QUIT_TIME);
        return this;
    }    
    public PersonCompanyRelationCriteria andQuitTimeGreaterThan(Date value) {
        addCriterion("quit_time >", value, QUIT_TIME);
        return this;
    }    
    public PersonCompanyRelationCriteria andQuitTimeGreaterThanOrEqualTo(Date value) {
        addCriterion("quit_time >=", value, QUIT_TIME);
        return this;
    }    
    public PersonCompanyRelationCriteria andQuitTimeLessThan(Date value) {
        addCriterion("quit_time <", value, QUIT_TIME);
        return this;
    }     
    public PersonCompanyRelationCriteria andQuitTimeLessThanOrEqualTo(Date value) {
        addCriterion("quit_time <=", value, QUIT_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andQuitTimeIn(List<Date> values) {
        addCriterion("quit_time in", values, QUIT_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andQuitTimeNotIn(List<Date> values) {
        addCriterion("quit_time not in", values, QUIT_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andQuitTimeBetween(Date value1, Date value2) {
        addCriterion("quit_time between", value1, value2, QUIT_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andQuitTimeNotBetween(Date value1, Date value2) {
        addCriterion("quit_time not between", value1, value2, QUIT_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andFunnyThingIsNull() {
        addCriterion("funny_thing is null");
        return this;
    }

    public PersonCompanyRelationCriteria andFunnyThingIsNotNull() {
        addCriterion("funny_thing is not null");
        return this;
    }
    public PersonCompanyRelationCriteria andFunnyThingEqualTo(String value) {
        addCriterion("funny_thing =", value, FUNNY_THING);
        return this;
    }
    public PersonCompanyRelationCriteria andFunnyThingNotEqualTo(String value) {
        addCriterion("funny_thing <>", value, FUNNY_THING);
        return this;
    }    
    public PersonCompanyRelationCriteria andFunnyThingGreaterThan(String value) {
        addCriterion("funny_thing >", value, FUNNY_THING);
        return this;
    }    
    public PersonCompanyRelationCriteria andFunnyThingGreaterThanOrEqualTo(String value) {
        addCriterion("funny_thing >=", value, FUNNY_THING);
        return this;
    }    
    public PersonCompanyRelationCriteria andFunnyThingLessThan(String value) {
        addCriterion("funny_thing <", value, FUNNY_THING);
        return this;
    }     
    public PersonCompanyRelationCriteria andFunnyThingLessThanOrEqualTo(String value) {
        addCriterion("funny_thing <=", value, FUNNY_THING);
        return this;
    }
    public PersonCompanyRelationCriteria andFunnyThingIn(List<String> values) {
        addCriterion("funny_thing in", values, FUNNY_THING);
        return this;
    }
    public PersonCompanyRelationCriteria andFunnyThingNotIn(List<String> values) {
        addCriterion("funny_thing not in", values, FUNNY_THING);
        return this;
    }
    public PersonCompanyRelationCriteria andFunnyThingBetween(String value1, String value2) {
        addCriterion("funny_thing between", value1, value2, FUNNY_THING);
        return this;
    }
    public PersonCompanyRelationCriteria andFunnyThingNotBetween(String value1, String value2) {
        addCriterion("funny_thing not between", value1, value2, FUNNY_THING);
        return this;
    }
        public PersonCompanyRelationCriteria andFunnyThingLike(String value) {
            addCriterion("funny_thing like", value, FUNNY_THING);
            return this;
        }

        public PersonCompanyRelationCriteria andFunnyThingNotLike(String value) {
            addCriterion("funny_thing not like", value, FUNNY_THING);
            return this;
        }
    public PersonCompanyRelationCriteria andPositionIsNull() {
        addCriterion("position is null");
        return this;
    }

    public PersonCompanyRelationCriteria andPositionIsNotNull() {
        addCriterion("position is not null");
        return this;
    }
    public PersonCompanyRelationCriteria andPositionEqualTo(String value) {
        addCriterion("position =", value, POSITION);
        return this;
    }
    public PersonCompanyRelationCriteria andPositionNotEqualTo(String value) {
        addCriterion("position <>", value, POSITION);
        return this;
    }    
    public PersonCompanyRelationCriteria andPositionGreaterThan(String value) {
        addCriterion("position >", value, POSITION);
        return this;
    }    
    public PersonCompanyRelationCriteria andPositionGreaterThanOrEqualTo(String value) {
        addCriterion("position >=", value, POSITION);
        return this;
    }    
    public PersonCompanyRelationCriteria andPositionLessThan(String value) {
        addCriterion("position <", value, POSITION);
        return this;
    }     
    public PersonCompanyRelationCriteria andPositionLessThanOrEqualTo(String value) {
        addCriterion("position <=", value, POSITION);
        return this;
    }
    public PersonCompanyRelationCriteria andPositionIn(List<String> values) {
        addCriterion("position in", values, POSITION);
        return this;
    }
    public PersonCompanyRelationCriteria andPositionNotIn(List<String> values) {
        addCriterion("position not in", values, POSITION);
        return this;
    }
    public PersonCompanyRelationCriteria andPositionBetween(String value1, String value2) {
        addCriterion("position between", value1, value2, POSITION);
        return this;
    }
    public PersonCompanyRelationCriteria andPositionNotBetween(String value1, String value2) {
        addCriterion("position not between", value1, value2, POSITION);
        return this;
    }
        public PersonCompanyRelationCriteria andPositionLike(String value) {
            addCriterion("position like", value, POSITION);
            return this;
        }

        public PersonCompanyRelationCriteria andPositionNotLike(String value) {
            addCriterion("position not like", value, POSITION);
            return this;
        }
    public PersonCompanyRelationCriteria andRecruitmentInfoIsNull() {
        addCriterion("recruitment_info is null");
        return this;
    }

    public PersonCompanyRelationCriteria andRecruitmentInfoIsNotNull() {
        addCriterion("recruitment_info is not null");
        return this;
    }
    public PersonCompanyRelationCriteria andRecruitmentInfoEqualTo(String value) {
        addCriterion("recruitment_info =", value, RECRUITMENT_INFO);
        return this;
    }
    public PersonCompanyRelationCriteria andRecruitmentInfoNotEqualTo(String value) {
        addCriterion("recruitment_info <>", value, RECRUITMENT_INFO);
        return this;
    }    
    public PersonCompanyRelationCriteria andRecruitmentInfoGreaterThan(String value) {
        addCriterion("recruitment_info >", value, RECRUITMENT_INFO);
        return this;
    }    
    public PersonCompanyRelationCriteria andRecruitmentInfoGreaterThanOrEqualTo(String value) {
        addCriterion("recruitment_info >=", value, RECRUITMENT_INFO);
        return this;
    }    
    public PersonCompanyRelationCriteria andRecruitmentInfoLessThan(String value) {
        addCriterion("recruitment_info <", value, RECRUITMENT_INFO);
        return this;
    }     
    public PersonCompanyRelationCriteria andRecruitmentInfoLessThanOrEqualTo(String value) {
        addCriterion("recruitment_info <=", value, RECRUITMENT_INFO);
        return this;
    }
    public PersonCompanyRelationCriteria andRecruitmentInfoIn(List<String> values) {
        addCriterion("recruitment_info in", values, RECRUITMENT_INFO);
        return this;
    }
    public PersonCompanyRelationCriteria andRecruitmentInfoNotIn(List<String> values) {
        addCriterion("recruitment_info not in", values, RECRUITMENT_INFO);
        return this;
    }
    public PersonCompanyRelationCriteria andRecruitmentInfoBetween(String value1, String value2) {
        addCriterion("recruitment_info between", value1, value2, RECRUITMENT_INFO);
        return this;
    }
    public PersonCompanyRelationCriteria andRecruitmentInfoNotBetween(String value1, String value2) {
        addCriterion("recruitment_info not between", value1, value2, RECRUITMENT_INFO);
        return this;
    }
        public PersonCompanyRelationCriteria andRecruitmentInfoLike(String value) {
            addCriterion("recruitment_info like", value, RECRUITMENT_INFO);
            return this;
        }

        public PersonCompanyRelationCriteria andRecruitmentInfoNotLike(String value) {
            addCriterion("recruitment_info not like", value, RECRUITMENT_INFO);
            return this;
        }
    public PersonCompanyRelationCriteria andIsDepartureIsNull() {
        addCriterion("is_departure is null");
        return this;
    }

    public PersonCompanyRelationCriteria andIsDepartureIsNotNull() {
        addCriterion("is_departure is not null");
        return this;
    }
    public PersonCompanyRelationCriteria andIsDepartureEqualTo(Byte value) {
        addCriterion("is_departure =", value, IS_DEPARTURE);
        return this;
    }
    public PersonCompanyRelationCriteria andIsDepartureNotEqualTo(Byte value) {
        addCriterion("is_departure <>", value, IS_DEPARTURE);
        return this;
    }    
    public PersonCompanyRelationCriteria andIsDepartureGreaterThan(Byte value) {
        addCriterion("is_departure >", value, IS_DEPARTURE);
        return this;
    }    
    public PersonCompanyRelationCriteria andIsDepartureGreaterThanOrEqualTo(Byte value) {
        addCriterion("is_departure >=", value, IS_DEPARTURE);
        return this;
    }    
    public PersonCompanyRelationCriteria andIsDepartureLessThan(Byte value) {
        addCriterion("is_departure <", value, IS_DEPARTURE);
        return this;
    }     
    public PersonCompanyRelationCriteria andIsDepartureLessThanOrEqualTo(Byte value) {
        addCriterion("is_departure <=", value, IS_DEPARTURE);
        return this;
    }
    public PersonCompanyRelationCriteria andIsDepartureIn(List<Byte> values) {
        addCriterion("is_departure in", values, IS_DEPARTURE);
        return this;
    }
    public PersonCompanyRelationCriteria andIsDepartureNotIn(List<Byte> values) {
        addCriterion("is_departure not in", values, IS_DEPARTURE);
        return this;
    }
    public PersonCompanyRelationCriteria andIsDepartureBetween(Byte value1, Byte value2) {
        addCriterion("is_departure between", value1, value2, IS_DEPARTURE);
        return this;
    }
    public PersonCompanyRelationCriteria andIsDepartureNotBetween(Byte value1, Byte value2) {
        addCriterion("is_departure not between", value1, value2, IS_DEPARTURE);
        return this;
    }
    public PersonCompanyRelationCriteria andOperatorIdIsNull() {
        addCriterion("operator_id is null");
        return this;
    }

    public PersonCompanyRelationCriteria andOperatorIdIsNotNull() {
        addCriterion("operator_id is not null");
        return this;
    }
    public PersonCompanyRelationCriteria andOperatorIdEqualTo(String value) {
        addCriterion("operator_id =", value, OPERATOR_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andOperatorIdNotEqualTo(String value) {
        addCriterion("operator_id <>", value, OPERATOR_ID);
        return this;
    }    
    public PersonCompanyRelationCriteria andOperatorIdGreaterThan(String value) {
        addCriterion("operator_id >", value, OPERATOR_ID);
        return this;
    }    
    public PersonCompanyRelationCriteria andOperatorIdGreaterThanOrEqualTo(String value) {
        addCriterion("operator_id >=", value, OPERATOR_ID);
        return this;
    }    
    public PersonCompanyRelationCriteria andOperatorIdLessThan(String value) {
        addCriterion("operator_id <", value, OPERATOR_ID);
        return this;
    }     
    public PersonCompanyRelationCriteria andOperatorIdLessThanOrEqualTo(String value) {
        addCriterion("operator_id <=", value, OPERATOR_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andOperatorIdIn(List<String> values) {
        addCriterion("operator_id in", values, OPERATOR_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andOperatorIdNotIn(List<String> values) {
        addCriterion("operator_id not in", values, OPERATOR_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andOperatorIdBetween(String value1, String value2) {
        addCriterion("operator_id between", value1, value2, OPERATOR_ID);
        return this;
    }
    public PersonCompanyRelationCriteria andOperatorIdNotBetween(String value1, String value2) {
        addCriterion("operator_id not between", value1, value2, OPERATOR_ID);
        return this;
    }
        public PersonCompanyRelationCriteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, OPERATOR_ID);
            return this;
        }

        public PersonCompanyRelationCriteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, OPERATOR_ID);
            return this;
        }
    public PersonCompanyRelationCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public PersonCompanyRelationCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public PersonCompanyRelationCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public PersonCompanyRelationCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public PersonCompanyRelationCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public PersonCompanyRelationCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public PersonCompanyRelationCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public PersonCompanyRelationCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
}

