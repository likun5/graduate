package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.AdministrativeRegionEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.AdministrativeRegionExample;
import org.springframework.stereotype.Repository;

/**
 *@author 李爽
 *create: 2017-12-05 09:39:56
 *DESCRIPTION:
 */
@Repository
public interface AdministrativeRegionDao extends BaseDao<AdministrativeRegionEntity,AdministrativeRegionExample> {
}
