package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class RegionInformationCriteria extends GeneratedCriteria<RegionInformationCriteria> implements Serializable{

    protected RegionInformationCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String POI_ID = "poiId";   
    private static final String LNG = "lng";   
    private static final String LAT = "lat";   
    private static final String REGION_ADDRESS = "regionAddress";   
    private static final String DICTIONARY_ADDRESS_ID = "dictionaryAddressId";   
    private static final String TIMESTAMP_TIME = "timestampTime";   

    public RegionInformationCriteria andPoiIdIsNull() {
        addCriterion("poi_id is null");
        return this;
    }

    public RegionInformationCriteria andPoiIdIsNotNull() {
        addCriterion("poi_id is not null");
        return this;
    }
    public RegionInformationCriteria andPoiIdEqualTo(String value) {
        addCriterion("poi_id =", value, POI_ID);
        return this;
    }
    public RegionInformationCriteria andPoiIdNotEqualTo(String value) {
        addCriterion("poi_id <>", value, POI_ID);
        return this;
    }    
    public RegionInformationCriteria andPoiIdGreaterThan(String value) {
        addCriterion("poi_id >", value, POI_ID);
        return this;
    }    
    public RegionInformationCriteria andPoiIdGreaterThanOrEqualTo(String value) {
        addCriterion("poi_id >=", value, POI_ID);
        return this;
    }    
    public RegionInformationCriteria andPoiIdLessThan(String value) {
        addCriterion("poi_id <", value, POI_ID);
        return this;
    }     
    public RegionInformationCriteria andPoiIdLessThanOrEqualTo(String value) {
        addCriterion("poi_id <=", value, POI_ID);
        return this;
    }
    public RegionInformationCriteria andPoiIdIn(List<String> values) {
        addCriterion("poi_id in", values, POI_ID);
        return this;
    }
    public RegionInformationCriteria andPoiIdNotIn(List<String> values) {
        addCriterion("poi_id not in", values, POI_ID);
        return this;
    }
    public RegionInformationCriteria andPoiIdBetween(String value1, String value2) {
        addCriterion("poi_id between", value1, value2, POI_ID);
        return this;
    }
    public RegionInformationCriteria andPoiIdNotBetween(String value1, String value2) {
        addCriterion("poi_id not between", value1, value2, POI_ID);
        return this;
    }
        public RegionInformationCriteria andPoiIdLike(String value) {
            addCriterion("poi_id like", value, POI_ID);
            return this;
        }

        public RegionInformationCriteria andPoiIdNotLike(String value) {
            addCriterion("poi_id not like", value, POI_ID);
            return this;
        }
    public RegionInformationCriteria andLngIsNull() {
        addCriterion("lng is null");
        return this;
    }

    public RegionInformationCriteria andLngIsNotNull() {
        addCriterion("lng is not null");
        return this;
    }
    public RegionInformationCriteria andLngEqualTo(String value) {
        addCriterion("lng =", value, LNG);
        return this;
    }
    public RegionInformationCriteria andLngNotEqualTo(String value) {
        addCriterion("lng <>", value, LNG);
        return this;
    }    
    public RegionInformationCriteria andLngGreaterThan(String value) {
        addCriterion("lng >", value, LNG);
        return this;
    }    
    public RegionInformationCriteria andLngGreaterThanOrEqualTo(String value) {
        addCriterion("lng >=", value, LNG);
        return this;
    }    
    public RegionInformationCriteria andLngLessThan(String value) {
        addCriterion("lng <", value, LNG);
        return this;
    }     
    public RegionInformationCriteria andLngLessThanOrEqualTo(String value) {
        addCriterion("lng <=", value, LNG);
        return this;
    }
    public RegionInformationCriteria andLngIn(List<String> values) {
        addCriterion("lng in", values, LNG);
        return this;
    }
    public RegionInformationCriteria andLngNotIn(List<String> values) {
        addCriterion("lng not in", values, LNG);
        return this;
    }
    public RegionInformationCriteria andLngBetween(String value1, String value2) {
        addCriterion("lng between", value1, value2, LNG);
        return this;
    }
    public RegionInformationCriteria andLngNotBetween(String value1, String value2) {
        addCriterion("lng not between", value1, value2, LNG);
        return this;
    }
        public RegionInformationCriteria andLngLike(String value) {
            addCriterion("lng like", value, LNG);
            return this;
        }

        public RegionInformationCriteria andLngNotLike(String value) {
            addCriterion("lng not like", value, LNG);
            return this;
        }
    public RegionInformationCriteria andLatIsNull() {
        addCriterion("lat is null");
        return this;
    }

    public RegionInformationCriteria andLatIsNotNull() {
        addCriterion("lat is not null");
        return this;
    }
    public RegionInformationCriteria andLatEqualTo(String value) {
        addCriterion("lat =", value, LAT);
        return this;
    }
    public RegionInformationCriteria andLatNotEqualTo(String value) {
        addCriterion("lat <>", value, LAT);
        return this;
    }    
    public RegionInformationCriteria andLatGreaterThan(String value) {
        addCriterion("lat >", value, LAT);
        return this;
    }    
    public RegionInformationCriteria andLatGreaterThanOrEqualTo(String value) {
        addCriterion("lat >=", value, LAT);
        return this;
    }    
    public RegionInformationCriteria andLatLessThan(String value) {
        addCriterion("lat <", value, LAT);
        return this;
    }     
    public RegionInformationCriteria andLatLessThanOrEqualTo(String value) {
        addCriterion("lat <=", value, LAT);
        return this;
    }
    public RegionInformationCriteria andLatIn(List<String> values) {
        addCriterion("lat in", values, LAT);
        return this;
    }
    public RegionInformationCriteria andLatNotIn(List<String> values) {
        addCriterion("lat not in", values, LAT);
        return this;
    }
    public RegionInformationCriteria andLatBetween(String value1, String value2) {
        addCriterion("lat between", value1, value2, LAT);
        return this;
    }
    public RegionInformationCriteria andLatNotBetween(String value1, String value2) {
        addCriterion("lat not between", value1, value2, LAT);
        return this;
    }
        public RegionInformationCriteria andLatLike(String value) {
            addCriterion("lat like", value, LAT);
            return this;
        }

        public RegionInformationCriteria andLatNotLike(String value) {
            addCriterion("lat not like", value, LAT);
            return this;
        }
    public RegionInformationCriteria andRegionAddressIsNull() {
        addCriterion("region_address is null");
        return this;
    }

    public RegionInformationCriteria andRegionAddressIsNotNull() {
        addCriterion("region_address is not null");
        return this;
    }
    public RegionInformationCriteria andRegionAddressEqualTo(String value) {
        addCriterion("region_address =", value, REGION_ADDRESS);
        return this;
    }
    public RegionInformationCriteria andRegionAddressNotEqualTo(String value) {
        addCriterion("region_address <>", value, REGION_ADDRESS);
        return this;
    }    
    public RegionInformationCriteria andRegionAddressGreaterThan(String value) {
        addCriterion("region_address >", value, REGION_ADDRESS);
        return this;
    }    
    public RegionInformationCriteria andRegionAddressGreaterThanOrEqualTo(String value) {
        addCriterion("region_address >=", value, REGION_ADDRESS);
        return this;
    }    
    public RegionInformationCriteria andRegionAddressLessThan(String value) {
        addCriterion("region_address <", value, REGION_ADDRESS);
        return this;
    }     
    public RegionInformationCriteria andRegionAddressLessThanOrEqualTo(String value) {
        addCriterion("region_address <=", value, REGION_ADDRESS);
        return this;
    }
    public RegionInformationCriteria andRegionAddressIn(List<String> values) {
        addCriterion("region_address in", values, REGION_ADDRESS);
        return this;
    }
    public RegionInformationCriteria andRegionAddressNotIn(List<String> values) {
        addCriterion("region_address not in", values, REGION_ADDRESS);
        return this;
    }
    public RegionInformationCriteria andRegionAddressBetween(String value1, String value2) {
        addCriterion("region_address between", value1, value2, REGION_ADDRESS);
        return this;
    }
    public RegionInformationCriteria andRegionAddressNotBetween(String value1, String value2) {
        addCriterion("region_address not between", value1, value2, REGION_ADDRESS);
        return this;
    }
        public RegionInformationCriteria andRegionAddressLike(String value) {
            addCriterion("region_address like", value, REGION_ADDRESS);
            return this;
        }

        public RegionInformationCriteria andRegionAddressNotLike(String value) {
            addCriterion("region_address not like", value, REGION_ADDRESS);
            return this;
        }
    public RegionInformationCriteria andDictionaryAddressIdIsNull() {
        addCriterion("dictionary_address_id is null");
        return this;
    }

    public RegionInformationCriteria andDictionaryAddressIdIsNotNull() {
        addCriterion("dictionary_address_id is not null");
        return this;
    }
    public RegionInformationCriteria andDictionaryAddressIdEqualTo(String value) {
        addCriterion("dictionary_address_id =", value, DICTIONARY_ADDRESS_ID);
        return this;
    }
    public RegionInformationCriteria andDictionaryAddressIdNotEqualTo(String value) {
        addCriterion("dictionary_address_id <>", value, DICTIONARY_ADDRESS_ID);
        return this;
    }    
    public RegionInformationCriteria andDictionaryAddressIdGreaterThan(String value) {
        addCriterion("dictionary_address_id >", value, DICTIONARY_ADDRESS_ID);
        return this;
    }    
    public RegionInformationCriteria andDictionaryAddressIdGreaterThanOrEqualTo(String value) {
        addCriterion("dictionary_address_id >=", value, DICTIONARY_ADDRESS_ID);
        return this;
    }    
    public RegionInformationCriteria andDictionaryAddressIdLessThan(String value) {
        addCriterion("dictionary_address_id <", value, DICTIONARY_ADDRESS_ID);
        return this;
    }     
    public RegionInformationCriteria andDictionaryAddressIdLessThanOrEqualTo(String value) {
        addCriterion("dictionary_address_id <=", value, DICTIONARY_ADDRESS_ID);
        return this;
    }
    public RegionInformationCriteria andDictionaryAddressIdIn(List<String> values) {
        addCriterion("dictionary_address_id in", values, DICTIONARY_ADDRESS_ID);
        return this;
    }
    public RegionInformationCriteria andDictionaryAddressIdNotIn(List<String> values) {
        addCriterion("dictionary_address_id not in", values, DICTIONARY_ADDRESS_ID);
        return this;
    }
    public RegionInformationCriteria andDictionaryAddressIdBetween(String value1, String value2) {
        addCriterion("dictionary_address_id between", value1, value2, DICTIONARY_ADDRESS_ID);
        return this;
    }
    public RegionInformationCriteria andDictionaryAddressIdNotBetween(String value1, String value2) {
        addCriterion("dictionary_address_id not between", value1, value2, DICTIONARY_ADDRESS_ID);
        return this;
    }
        public RegionInformationCriteria andDictionaryAddressIdLike(String value) {
            addCriterion("dictionary_address_id like", value, DICTIONARY_ADDRESS_ID);
            return this;
        }

        public RegionInformationCriteria andDictionaryAddressIdNotLike(String value) {
            addCriterion("dictionary_address_id not like", value, DICTIONARY_ADDRESS_ID);
            return this;
        }
    public RegionInformationCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public RegionInformationCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public RegionInformationCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public RegionInformationCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public RegionInformationCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public RegionInformationCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public RegionInformationCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public RegionInformationCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public RegionInformationCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public RegionInformationCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public RegionInformationCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public RegionInformationCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
}

