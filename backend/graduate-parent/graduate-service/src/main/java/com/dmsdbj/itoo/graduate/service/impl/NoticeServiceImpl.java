package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.dao.NoticeDao;
import com.dmsdbj.itoo.graduate.entity.BrowseEntity;
import com.dmsdbj.itoo.graduate.entity.NoticeEntity;
import com.dmsdbj.itoo.graduate.entity.ext.NoticeBrowseModel;
import com.dmsdbj.itoo.graduate.entity.ext.NoticeNoticeCategoryModel;
import com.dmsdbj.itoo.graduate.mybatisexample.NoticeExample;
import com.dmsdbj.itoo.graduate.service.BrowseService;
import com.dmsdbj.itoo.graduate.service.NoticeService;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author haoguiting
 * create:2017-11-27 11:33:54
 * DESCRIPTION 通知service实现
 */
@Service("noticeService")
public class NoticeServiceImpl extends BaseServiceImpl<NoticeEntity, NoticeExample> implements NoticeService {


    //注入noticeDao
    @Autowired
    private NoticeDao noticeDao;
    @Autowired
    private BrowseService browseService;

    /**
     * 让BaseServiceImpl获取到Dao
     *
     * @return BaseDao<NoticeEntity, NoticeExample> 通知BaseDao
     */
    @Override
    public BaseDao<NoticeEntity, NoticeExample> getRealDao() {
        return this.noticeDao;
    }

    /**
     * 添加通知信息-郝贵婷-2017年11月27日
     *
     * @param noticeBrowseModel 通知浏览实体
     * @return 受影响行数
     */
    @Transactional(rollbackFor = Exception.class)
    public int addNotice(NoticeBrowseModel noticeBrowseModel) {
        int count = 0;
        int count1 = 0;
        NoticeEntity noticeEntity = new NoticeEntity();
        BeanUtils.copyProperties(noticeBrowseModel, noticeEntity);
        count = this.insert(noticeEntity);

        for (BrowseEntity browser : noticeBrowseModel.getBrowsePersons()) {
            browser.setNoticeId(noticeEntity.getId());
            browser.setIsBrowse("0");   //设置默认是否浏览为否
        }
        count1 = browseService.insertAll(noticeBrowseModel.getBrowsePersons());
        return count + count1 - 1;
    }

    /**
     * 删除通知信息-郝贵婷-2017年11月27日
     *
     * @param noticeID 通知id
     * @param operator 操作人
     * @return 受影响行数
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteNotice(String noticeID, String operator) {
        return super.deleteById(noticeID, operator);
    }

    /**
     * 修改通知信息-郝贵婷-2017年11月27日
     *
     * @param noticeEntity 通知实体
     * @return 受影响行数
     */
    public int updateNotice(NoticeEntity noticeEntity) {
        return this.updateById(noticeEntity);
    }


    /**
     * 根据通知分类统计通知数量-郝贵婷-2018年1月9日
     *
     * @param columnId 通知类别
     * @return 受影响行数
     */
    public int selectNoticeCountByColumnId(String columnId) {
        return noticeDao.selectNoticeCountByColumnId(columnId);
    }

    /**
     * 根据ID查询通知-郝贵婷-2017年11月27日
     * @param id 通知id
     * @return 通知实体
     */
    @Override()
    public NoticeNoticeCategoryModel findNoticeNoticeCategoryModelById(String id) {
        return noticeDao.findNoticeNoticeCategoryModelById(id);
    }

    /**
     * 查询全部通知信息-郝贵婷-2017年11月27日
     *
     * @return 通知list
     */
    @Override()
    public List<NoticeNoticeCategoryModel> selectNoticeNoticeCategory() {
        return noticeDao.selectNoticeNoticeCategory();
    }

    /**
     * 根据分类ID查询通知-郝贵婷-2017年12月2日
     *
     * @param columnId 通知类别id
     * @return 通知list
     */
    @Override
    public List<NoticeNoticeCategoryModel> selectNoticeNoticeCategoryByColumnId(String columnId) {
        return noticeDao.selectNoticeNoticeCategoryByColumnId(columnId);
    }

    /**
     * 假分页-查询全部通知信息-郝贵婷-2018-01-11 14:15:45
     *
     * @param page 页码
     * @param  pageSize 页大小
     * @return 通知list
     */
    @Override
    public PageInfo<NoticeNoticeCategoryModel> PageSelectNoticeNoticeCategory(int page, int pageSize) {
        PageHelper.startPage(page, pageSize);
        List<NoticeNoticeCategoryModel> noticeCategoryEntityList = noticeDao.selectNoticeNoticeCategory();
        return new PageInfo<>(noticeCategoryEntityList);
    }

    /**
     * 假分页-根据分类ID查询通知-郝贵婷-2018年01月10日
     *
     * @param columnId  分类Id
     * @param page 页码
     * @param  pageSize 页大小
     * @return 通知list
     */
    @Override
    public PageInfo<NoticeNoticeCategoryModel> PageSelectNoticeNoticeCategoryByColumnId(String columnId, int page, int pageSize) {
        PageHelper.startPage(page, pageSize);
        List<NoticeNoticeCategoryModel> noticeCategoryEntityList = noticeDao.selectNoticeNoticeCategoryByColumnId(columnId);
        return new PageInfo<>(noticeCategoryEntityList);
    }


    /**
     * 更新通知信息（包括接受人）-郝贵婷-2017-12-5 14:15:45
     *
     * @param noticeBrowseModel 浏览记录实体
     * @return 受影响行
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateNoticeBrowse(NoticeBrowseModel noticeBrowseModel) {
        int count = 0;
        int resultcount = 0;
        int resultcount1 = 0;
        NoticeEntity noticeEntity = new NoticeEntity();
        BeanUtils.copyProperties(noticeBrowseModel, noticeEntity);
        count = this.updateById(noticeEntity);    //更新通知
        String noticeId = noticeBrowseModel.getId();
        String operator = noticeBrowseModel.getOperator();
        //第一步删除接收人
        resultcount = browseService.deleteByNoticeId(noticeId, operator);
        //第二步添加接收人
        for (BrowseEntity browser : noticeBrowseModel.getBrowsePersons()) {
            browser.setNoticeId(noticeEntity.getId());
        }
        resultcount1 = browseService.insertAll(noticeBrowseModel.getBrowsePersons());

        return count + resultcount + resultcount1 - 2;
    }

}
