package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.dao.EducationDao;
import com.dmsdbj.itoo.graduate.entity.EducationEntity;
import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.EducationModel;
import com.dmsdbj.itoo.graduate.entity.ext.PersonalEducationEntity;
import com.dmsdbj.itoo.graduate.facade.impl.CompanyFacadeImpl;
import com.dmsdbj.itoo.graduate.mybatisexample.EducationCriteria;
import com.dmsdbj.itoo.graduate.mybatisexample.EducationExample;
import com.dmsdbj.itoo.graduate.mybatisexample.PersonalInfoCriteria;
import com.dmsdbj.itoo.graduate.mybatisexample.PersonalInfoExample;
import com.dmsdbj.itoo.graduate.service.EducationService;
import com.dmsdbj.itoo.graduate.service.PersonalInfoService;
import com.dmsdbj.itoo.graduate.service.PictureService;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.tool.uuid.BaseUuidUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
@Service("educationService")
public class EducationServiceImpl extends BaseServiceImpl<EducationEntity, EducationExample> implements EducationService {

    //打印日志相关
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CompanyFacadeImpl.class);

    //注入educationDao
    @Autowired
    private EducationDao educationDao;
    @Autowired
    private PersonalInfoService personalInfoService;
    @Autowired
    private PictureService pictureService;

    @Value("${picServerUrl}")
    private String picServerUrl;
    /**
     * 让BaseServiceImpl获取到Dao
     *
     * @return BaseDao<EducationEntity, EducationExample>
     */
    @Override
    public BaseDao<EducationEntity, EducationExample> getRealDao() {
        return this.educationDao;
    }


    /**
     * 根据学历类型查询学历信息
     *
     * @param certificateType 学历类型
     * @return 学历信息实体
     */
    @Override
    public List<EducationEntity> findByCertificateType(String certificateType) {
        return this.educationDao.findByCertifycateType(certificateType);
    }

    /**
     * 根据ID查询学历信息
     *
     * @param personId 人员ID
     * @return 学历信息实体
     */
    @Override
    public List<EducationEntity> findByPersonId(String personId) {
        EducationExample educationExample = new EducationExample();
        EducationCriteria educationCriteria = educationExample.createCriteria();
        byte isDeleteByte = (byte) 0;
        educationCriteria.andIsDeleteEqualTo(isDeleteByte);
        educationCriteria.andUserIdEqualTo(personId);
        List<EducationEntity> list = super.selectByExample(educationExample);
        return super.selectByExample(educationExample);
    }

    /**
     * 根据姓名查询学历信息
     *
     * @param name 学历姓名
     * @return 学历信息实体
     */
    @Override
    public List<EducationEntity> findByName(String name) {
        String personCompanyRelation = null;
        List<String> listPersonCompanyList = null;
        List<EducationEntity> listEducationList = null;
        PersonalInfoExample personalInfoExample = new PersonalInfoExample();
        PersonalInfoCriteria personalInfoCriteria = personalInfoExample.createCriteria();
        personalInfoCriteria.andNameEqualTo(name);
        List<PersonalInfoEntity> listPerson = personalInfoService.selectByExample(personalInfoExample);

        for (int i = 0; i < listPerson.size(); i++) {
            //根据userId查询学历信息列表
            listEducationList = this.findByPersonId(listPerson.get(i).getId());
        }
        return listEducationList;

        //没处理重名的
    }

    /**
     * 查询出所有的学历信息
     *
     * @param page     第几页
     * @param pageSize 每页大小
     * @return 带分页信息的实体
     */
    @Override

    public PageInfo<EducationEntity> findAll(int page, int pageSize) {
        PageInfo<EducationEntity> pageInfo;
        PageHelper.startPage(page, pageSize);
        EducationExample educationExample = new EducationExample();
        EducationCriteria educationCriteria = educationExample.createCriteria();
        byte isDeleteByte = (byte) 0;
        educationCriteria.andIsDeleteEqualTo(isDeleteByte);
        List<EducationEntity> list = super.selectByExample(educationExample);
        pageInfo = new PageInfo<>(list);
        return pageInfo;

    }

    /**
     * 分页搜索学生信息
     *
     * @param page     第几页
     * @param pageSize 每页大小
     * @param UserId   用户ID
     * @return 带分页信息的实体
     */
    @Override
    public PageInfo<EducationEntity> findPersonAll(int page, int pageSize, String UserId) {
        PageInfo<EducationEntity> pageInfo;
        PageHelper.startPage(page, pageSize);
        EducationExample educationExample = new EducationExample();
        EducationCriteria educationCriteria = educationExample.createCriteria();
        byte isDeleteByte = (byte) 0;
        educationCriteria.andIsDeleteEqualTo(isDeleteByte);
        educationCriteria.andUserIdEqualTo(UserId);
        List<EducationEntity> list = super.selectByExample(educationExample);
        pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    /**
     * 新增学历信息
     *
     * @param record 学历
     * @return int 0，1：是否成功
     */
    @Override
    public int insert(EducationEntity record) {
        if (record != null) {
            return super.insert(record);
        } else {
            return -1;
        }
    }

    /**
     * 删除学历信息同时删除图片
     *
     * @param id       学历ID
     * @param operator 操作员ID
     * @return int 0，1：是否成功
     */
    @Override

    public int deleteById(String id, String operator) {
        if (id != null && operator != null) {
            //删除学历的同时删除图片
            //1.删除学历
            int flag1 = super.deleteById(id, operator);
            //2.删除图片
            pictureService.deletePictureById(id, operator);
            return flag1;

        } else {
            return -1;
        }

    }

    /**
     * 如果学历信息不为空则返回删除返回的结果，如果学历信息为空，则返回-1
     *
     * @param userId   用户ID
     * @param operator 操作员ID
     * @return int 0，1：是否成功
     */
    @Override
    public int deleteByPersonId(String userId, String operator) {
        if (userId != null && operator != null) {
            return educationDao.deleteEducationByUserId(userId, operator);
        } else {
            return -1;
        }


    }

    /**
     * 更新学历信息
     *
     * @param educationModel 学历实体
     * @return int 0，1：是否成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateById(EducationModel educationModel) {

        /**
         * 如果学历信息不为空则返回的结果，如果学历信息为空，则返回-1
         */
        int flag = 0;
        EducationEntity educationEntity = new EducationEntity();
        educationEntity.setId(educationModel.getId());
        educationEntity.setUserId(educationModel.getUserId());
        educationEntity.setCertificateName(educationModel.getCertificateName());
        educationEntity.setUniversityName(educationModel.getUniversityName());
        educationEntity.setReceiveTime(educationModel.getReceiveTime());
        educationEntity.setOperatorId(educationModel.getOperatorId());
        educationEntity.setOperator(educationModel.getOperator());
        educationEntity.setRemark(educationModel.getRemark());
        if (educationModel != null) {
            flag = educationDao.updateById(educationEntity);
            int flag2 = pictureService.deletePictureById(educationEntity.getId(), educationEntity.getOperator());

            //存图片
            if (!CollectionUtils.isEmpty(educationModel.getPictureUrls())) {
                List<String> pictureUrls = educationModel.getPictureUrls();
                for (String pictureUrl : pictureUrls) {
                    if(pictureUrl.contains(picServerUrl)){
                        String fastDFSUrl = pictureUrl.substring(picServerUrl.length(), pictureUrl.length());
                        flag = pictureService.addPicture(fastDFSUrl, educationEntity.getId());
                    }else{
                        flag = pictureService.addPicture(pictureUrl, educationEntity.getId());
                    }
                }
            }
        }
        return flag;
    }

    /**
     * @param pictureUrl 图片地址
     * @param keyId      学历ID
     * @return int 0，1：是否成功
     */
    @Override
    public int addPictureUrl(String pictureUrl, String keyId) {

        return pictureService.addPicture(pictureUrl, keyId);
    }

    /**
     * 查询提高班所有毕业生的学历信息-唐凌峰-2018年2月7日16:44:17
     *
     * @return List<PersonalEducationEntity>
     */
    @Override
    public List<PersonalEducationEntity> queryPersonalEducation() {
        List<PersonalEducationEntity> personalEducationEntityList = educationDao.queryPersonalEducation();
        if (personalEducationEntityList != null && personalEducationEntityList.size() > 0) {
            return personalEducationEntityList;
        }
        return null;
    }

    /**
     * 通过ID 查询毕业生学历信息-唐凌峰-2018年2月8日14:46:33
     *
     * @param persionId 个人ID
     * @return 学历实体返回集
     */
    @Override
    public List<PersonalEducationEntity> findEducationByPersonId(String persionId) {
        List<PersonalEducationEntity> personalEducationEntityList;
        personalEducationEntityList = educationDao.findEducationByPersonId(persionId);
        return personalEducationEntityList;

    }

    /**
     * 添加学历实体同时添加图片list
     *
     * @param EducationModelList 实体的list
     * @return int 0，1：是否成功
     */
    @Override
    public int addEducationModel(List<EducationModel> EducationModelList) {
        int companyCount = 0;
        int countPicture = 0;
        //唯一学历实体ID + 图片keyID
        String ID = BaseUuidUtils.base58Uuid();
        if (CollectionUtils.isEmpty(EducationModelList)) {
            logger.error("要添加的学历信息为空");
            return 0;
        } else {
            /*
            存实体
             */
            EducationEntity educationEntity = new EducationEntity();
            educationEntity.setId(ID);
            educationEntity.setUserId(EducationModelList.get(0).getUserId());
            educationEntity.setOperatorId(EducationModelList.get(0).getOperatorId());
            educationEntity.setCertificateName(EducationModelList.get(0).getCertificateName());
            educationEntity.setUniversityName(EducationModelList.get(0).getUniversityName());
            educationEntity.setReceiveTime(EducationModelList.get(0).getReceiveTime());
            educationEntity.setRemark(EducationModelList.get(0).getRemark());
        /*
        存图片
         */
        if (!CollectionUtils.isEmpty(EducationModelList.get(0).getPictureUrls())){
            List<String> pictureUrls = EducationModelList.get(0).getPictureUrls();
            for (String pictureUrl : pictureUrls) {
                //String fastdfsUrl = pictureUrl.substring(21,pictureUrl.length());
                pictureService.addPicture(pictureUrl, ID);
            }
        }
        return super.insert(educationEntity);
        }
    }

    /**
     * 根据学历ID查询相应的学历信息和图片urls
     *
     * @param id 学历ID
     * @return 学历信息实体
     */
    @Override
    public EducationModel queryEducationModel(String id) {
        EducationModel educationModel = new EducationModel();
        EducationEntity educationEntity = new EducationEntity();
        educationEntity = this.educationDao.findById(id);
        educationModel.setCertificateName(educationEntity.getCertificateName());
        educationModel.setUniversityName(educationEntity.getUniversityName());
        educationModel.setReceiveTime(educationEntity.getReceiveTime());
        educationModel.setRemark(educationEntity.getRemark());
        List<String> pictureUrls = pictureService.selectPictureById(id);
        educationModel.setPictureUrls(pictureUrls);
        return educationModel;
    }
}
