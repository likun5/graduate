package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.CompanySkillRelationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.CompanySkillRelationExample;
import org.springframework.stereotype.Repository;

/**
 *@author 徐玲博
 *create: 2017-11-26 16:14:54
 *DESCRIPTION:
 */
@Repository
public interface CompanySkillRelationDao extends BaseDao<CompanySkillRelationEntity,CompanySkillRelationExample> {
}
