package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.graduate.entity.ext.RoleApplicationModel;
import com.dmsdbj.itoo.graduate.entity.ext.UserRoleModel;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.ApplicationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.ApplicationExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 *@author : xulingbo 
 *create : 2018-01-04 15:54:54
 *DESCRIPTION :
 */
@Repository
public interface ApplicationDao extends BaseDao<ApplicationEntity,ApplicationExample> {
    /**
     * 用于顶栏显示
     * 根据userId查找服务-宋学孟-2018年1月18日
     *
     * @return //List<RoleApplicationModel>
     */
    List<RoleApplicationModel> queryUserServiceApplication(Map<String, Object> map);


    /**
     * 用portal的左侧栏显示
     * 根据userId查找所有的子模块和页面-宋学孟-2018年1月18日
     *
     * @return List<RoleApplicationModel>
     */
    List<RoleApplicationModel> queryServiceModuleApplication(Map<String, Object> map);


    /**
     * 用页面显示按钮
     * 根据userId和applicationID 查找页面中的按钮-宋学孟-2018年1月18日
     *
     * @return List<RoleApplicationModel>
     */
    List<RoleApplicationModel> queryModuleButtonApplication(Map<String, Object> map);

    /**
     * 根据userId查询UserEntity-宋学孟-2018年1月18日
     * @param userCode
     * @return
     */
    UserRoleModel selectUserInfoByCode(@Param("userCode") String userCode, @Param("userPwd") String userPwd);
}
