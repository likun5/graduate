package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.entity.ext.RegionCommercialModel;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.CommercialOppotunityExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.CommercialOppotunityDao;
import com.dmsdbj.itoo.graduate.service.CommercialOppotunityService;
import com.dmsdbj.itoo.graduate.entity.CommercialOppotunityEntity;

import java.util.List;

/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
@Service("commercialOppotunityService")
public class CommercialOppotunityServiceImpl extends BaseServiceImpl<CommercialOppotunityEntity, CommercialOppotunityExample> implements CommercialOppotunityService {

    //注入commercialOppotunityDao
    @Autowired
    private CommercialOppotunityDao commercialOppotunityDao;

    /**
     * 让BaseServiceImpl获取到Dao
     * @return BaseDao<CommercialOppotunityEntity, CommercialOppotunityExample>
     */
    @Override
    public  BaseDao<CommercialOppotunityEntity, CommercialOppotunityExample> getRealDao(){
        return this.commercialOppotunityDao;
    }

    /**
     * 通过区域id 查询地域商机
     * @param regionId 地域ID
     * @param pageNum  页码
     * @param pageSize 页量
     * @return  PageInfo<RegionCommercialModel>
     */
    @Override
    public PageInfo<RegionCommercialModel> queryRegionCommercialById(String regionId,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<RegionCommercialModel> regionCommercialModelList=null;
        regionCommercialModelList=commercialOppotunityDao.queryRegionCommercialById(regionId);
        if(regionCommercialModelList !=null && regionCommercialModelList.size()>0 ){
            return  new PageInfo<>(regionCommercialModelList);
        }

        return null;
    }

    /**
     * 方法重载-查询提高班毕业生所有毕业生的商机信息-唐凌峰-2018-1-29 15:37:19
     * @return List<RegionCommercialModel>
     */
    @Override
    public List<RegionCommercialModel> queryRegionCommercialById() {
        List<RegionCommercialModel> regionCommercialModels=commercialOppotunityDao.queryRegionCommercialById();
        if(regionCommercialModels !=null&& regionCommercialModels.size()>0){
            return regionCommercialModels;
        }
        return null;
    }


}
