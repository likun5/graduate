package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.graduate.entity.ext.TechnologyModel;
import com.dmsdbj.itoo.graduate.mybatisexample.SkillPointCriteria;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.SkillPointExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.SkillPointDao;
import com.dmsdbj.itoo.graduate.service.SkillPointService;
import com.dmsdbj.itoo.graduate.entity.SkillPointEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : 徐玲博
 * create : 2017-11-16 14:31:07
 * DESCRIPTION 技术点ServiceImpl
 */
@Service("skillPointService")
public class SkillPointServiceImpl extends BaseServiceImpl<SkillPointEntity, SkillPointExample> implements SkillPointService {


    //注入skillPointDao
    @Autowired
    private SkillPointDao skillPointDao;

    /**
     * 论坛接口-查询所有的技术-并根据技术方向分类-徐玲博-2017-12-26 15:55:23
     * 方案1、查询出技术方向，根据技术方向查询技术点实体list，技术方向也放在这个集合中
     * 方案2、查询出所有技术，根据pid，遍历技术到相应技术方向下；
     *
     * @return // List<Map<技术方向id, List < 技术实体 >>>
     */
    @Override
    public List<Map<String, List<SkillPointEntity>>> selectSkillPoint() {
        /*方案1*/
        List<Map<String, List<SkillPointEntity>>> mapList = new ArrayList<>();
        Map<String, List<SkillPointEntity>> map = new HashMap<>();
        SkillPointExample skillPointExample = new SkillPointExample();
        SkillPointCriteria skillPointCriteria = skillPointExample.createCriteria();
        skillPointCriteria.andPIdEqualTo("");//pid为空，查询出技术方向
        skillPointCriteria.andIsDeleteEqualTo((byte) 0);
//        skillPointExample.setOrderByClause("p_id desc");
        List<SkillPointEntity> parentSkillPointList = skillPointDao.selectByExample(skillPointExample);//查询出所有技术
        if (parentSkillPointList != null) {
            for (SkillPointEntity skillPointEntity : parentSkillPointList) {
                SkillPointExample skillPointExample1 = new SkillPointExample();
                SkillPointCriteria skillPointCriteria1 = skillPointExample1.createCriteria();
                String key = skillPointEntity.getId();//技术方向id
                skillPointCriteria1.andPIdEqualTo(key);
                skillPointCriteria1.andIsDeleteEqualTo((byte) 0);
                List<SkillPointEntity> skillPointList = skillPointDao.selectByExample(skillPointExample1);//查询出技术点
                skillPointList.add(skillPointEntity);//将技术方向也放入list集合中
                map.put(key, skillPointList);
            }
            mapList.add(map);
        }
        return mapList;

       /* 方案2*/
//        if(parentSkillPointList!=null){
//            for(int i=0;i<parentSkillPointList.size();i++){
//                List<SkillPointEntity> skillPointList=new ArrayList<>();
//                SkillPointEntity skillPointEntity=parentSkillPointList.get(i);//技术方向实体
//                String key=skillPointEntity.getPId();
//                if(!"".equals(key)) {
//                    for (int j = i+1; j <parentSkillPointList.size() ; j++) {
//                        SkillPointEntity skillPointEntity1=parentSkillPointList.get(j);//技术点实体
//                        if(skillPointEntity1.getPId().equals(key)){//如果技术点的pid等于技术方向的id
//                            skillPointList.add(skillPointEntity1);
//                        }
//                    }
//                }
//                skillPointList.add(skillPointEntity);//技术方向
//                map.put(key,skillPointList);
//            }
//            mapList.add(map);
//        }
//        return mapList;
    }

    /**
     * 查询所有技术（包括技术方向）-徐玲博-2018-1-13 22:52:15
     *
     * @return 技术list
     */
    @Override
    public List<TechnologyModel> selectAllTechnology() {
        List<TechnologyModel> technologyModelList = new ArrayList<>();
        List<SkillPointEntity> parentSkillPointList = skillPointDao.selectTechnicalDirection();//查询出所有技术方向

        //根据技术方向查询出所有的技术点
        if (parentSkillPointList != null) {
            for (SkillPointEntity skillPointEntity : parentSkillPointList) {
                String techPid=skillPointEntity.getId();//获取技术方向id
                String techName=skillPointEntity.getTechName();
                List<SkillPointEntity> skillPointList = skillPointDao.selectParentSkillByPid(techPid);//查询出技术点

                TechnologyModel technologyModel=new TechnologyModel();
                technologyModel.setId(techPid);//技术pid
                technologyModel.setTechName(techName);//获取技术方向
                technologyModel.setSkillPointEntityList(skillPointList);//获取技术点
                technologyModelList.add(technologyModel);
            }
        }
        return technologyModelList;
    }


    /**
     * 分页查询所有技术-徐玲博-2018-1-13 23:36:48
     *
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return  技术点list
     */
    public PageInfo<TechnologyModel> pageAllTechnology(int page, int pageSize){
        PageHelper.startPage(page,pageSize);
        List<TechnologyModel> technologyModelList=selectAllTechnology();
        return new PageInfo<>(technologyModelList);
    }


    /**
     * 让BaseServiceImpl获取到Dao
     *
     * @return BaseDao<SkillPointEntity,SkillPointExample>
     */
    @Override
    public BaseDao<SkillPointEntity, SkillPointExample> getRealDao() {
        return this.skillPointDao;
    }

    /**
     * 删除技术点或技术方向-徐玲博-2017-11-20 15:49:59
     *
     * @param id       技术点
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteSkillPoint(String id, String operator) {
        List<String> stringList = selectSkillByPId(id);

        int result;
        if (stringList != null) {
            stringList.add(id);//把父节点加入list中
            //循环删除技术点
            result = this.deleteByIds(stringList, operator);
            //删除技术方向
//            int deleteId=skillPointDao.deleteSkillPointById(Id,operatorName,operatorId);
//            result=deleteIds+deleteId;//删除父节点和子节点的个数
        } else {
            //删除技术点
            result = this.deleteById(id, operator);
        }
        return result;
    }

    /**
     * 查询父Id下的子信息-徐玲博-2017-11-20 15:49:14
     *
     * @param pId 技术点id
     * @return 子技术的实体
     */
    @Override
    public List<String> selectSkillByPId(String pId) {
        List<SkillPointEntity> skillPointEntityList = skillPointDao.selectParentSkillByPid(pId);

        List<String> stringList = new ArrayList<>();
        for (SkillPointEntity skillPointEntity : skillPointEntityList) {
            String id = skillPointEntity.getId();
            stringList.add(id);
        }
        return stringList;
    }

    /**
     * 查询技术点-或技术方向-徐玲博-2017-11-20 16:54:36
     *
     * @param id 用户Id
     * @return 技术列表
     */
    @Override
    public List<SkillPointEntity> selectSkillPointById(String id) {
        SkillPointExample skillPointExample = new SkillPointExample();
        SkillPointCriteria skillPointCriteria = skillPointExample.createCriteria();
        skillPointCriteria.andIdEqualTo(id);
        skillPointCriteria.andIsDeleteEqualTo((byte) 0);
        return this.selectByExample(skillPointExample);
    }

    /**
     * 查询技术方向-徐玲博-2017-11-21 14:42:51
     *
     * @param pid 技术方向
     * @return 技术方向列表
     */
    @Override
    public List<SkillPointEntity> selectParentSkillByPid(String pid) {
        SkillPointExample skillPointExample = new SkillPointExample();
        SkillPointCriteria skillPointCriteria = skillPointExample.createCriteria();
        skillPointCriteria
                .andPIdEqualTo(pid)
                .andIsDeleteEqualTo((byte) 0);
        return this.selectByExample(skillPointExample);
    }
    /**
     * 查询所有的技术方向实体-徐玲博-2018-1-13 19:15:10
     *
     * @return 技术方向list
     */
    @Override
    public List<SkillPointEntity> selectTechnicalDirection() {

        return skillPointDao.selectTechnicalDirection();
    }

    /**
     * 修改技术点或技术方向-徐玲博-2018-1-14 16:21:57
     *
     * @param skillPointEntity 技术实体
     * @return 受影响行
     */
    @Override
    public int updateSkillPoint(SkillPointEntity skillPointEntity) {
        int result;
        try {
            String id=skillPointEntity.getId();
            SkillPointExample skillPointExample=new SkillPointExample();
            SkillPointCriteria skillPointCriteria=skillPointExample.createCriteria();
            skillPointCriteria.andIdEqualTo(id);
            skillPointCriteria.andIsDeleteEqualTo((byte)0);
//            flag = this.updateById(pie);
//            result=this.updateByExample(skillPointEntity,skillPointExample);
            result=skillPointDao.updateById(skillPointEntity);
        } catch (Exception e) {
            throw new ItooRuntimeException(e);
        }
        return result;
    }

    /**
     * 分页查询技术方向-haoguiting-2018年3月10日
     *
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 技术方向list
     */
    public PageInfo<SkillPointEntity> PageSelectSkillDirection(int page,int pageSize){
        PageHelper.startPage(page,pageSize);
        List<SkillPointEntity> skillPointEntityList=skillPointDao.selectTechnicalDirection();
        return new PageInfo<>(skillPointEntityList);
    }


    /**
     * 分页查询根据技术方向查询技术点-haoguiting-2018年2月22日
     *
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 技术点list
     */
    @Override
    public PageInfo<SkillPointEntity> PageSelectSkillByDirectionId(String pid,int page,int pageSize){
        PageHelper.startPage(page,pageSize);
        List<SkillPointEntity> skillPointEntityList=skillPointDao.selectParentSkillByPid(pid);
        return new PageInfo<>(skillPointEntityList);
    }

    /**
     * 模糊查询技术点-haoguiting-2018年2月27日
     *
     * @param strLike  输入的技术点
     * @param pid 技术方向
     * @return 技术点list
     */
    @Override
    public List<SkillPointEntity> fuzzySkillInfoByName(String strLike,String pid){
        SkillPointExample example=new SkillPointExample();
        if (StringUtils.isEmpty(strLike)) {
            example.createCriteria()
                    .andPIdEqualTo(pid)
                    .andIsDeleteEqualTo((byte) 0);
        }else{
            example.createCriteria()
                    .andTechNameLike("%" + strLike + "%")
                    .andPIdEqualTo(pid)
                    .andIsDeleteEqualTo((byte) 0);
        }
        List<SkillPointEntity> list = skillPointDao.selectByExample(example);

        List<SkillPointEntity>  dictionaryEntities = new ArrayList<>();
        for (SkillPointEntity skillPointEntity : list) {
            SkillPointEntity info = new SkillPointEntity();
            info.setId(skillPointEntity.getId());
            info.setTechName(skillPointEntity.getTechName());
            info.setPId(skillPointEntity.getPId());
            info.setRemark(skillPointEntity.getRemark());
            dictionaryEntities.add(info);
        }
        return dictionaryEntities;
    }
}
