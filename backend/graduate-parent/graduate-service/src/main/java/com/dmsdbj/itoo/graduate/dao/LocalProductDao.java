package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.LocalProductEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.LocalProductExample;
import org.springframework.stereotype.Repository;

/**
 *@author sxm
 *create: 2017-11-16 14:31:07
 *DESCRIPTION:
 */
@Repository
public interface LocalProductDao extends BaseDao<LocalProductEntity,LocalProductExample> {
}
