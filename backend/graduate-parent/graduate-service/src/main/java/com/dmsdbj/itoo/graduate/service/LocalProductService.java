package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.LocalProductEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.LocalProductExample;
import java.util.List;


/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
public interface LocalProductService extends BaseService<LocalProductEntity, LocalProductExample>{

    /**
     *查询所有特产信息
     *@param
     *@return List<LocalProductEntity> 房间类型实体集合
     */
    List<LocalProductEntity> queryLocalProduct();

    /**
     *添加特产
     *@param
     *@return List<LocalProductEntity> 特产实体集合
     */
    boolean addLocalProduct(LocalProductEntity localProductEntity);

    /**
     *删除特产
     *@param
     *@return List<LocalProductEntity> 特产实体集合
     */
    boolean deleteLocalProduct(List<String> productIds);

    /**
     *更新特产
     *@param
     *@return List<LocalProductEntity> 特产实体集合
     */
    int updateLocalProduct(LocalProductEntity localProductEntity);
}
