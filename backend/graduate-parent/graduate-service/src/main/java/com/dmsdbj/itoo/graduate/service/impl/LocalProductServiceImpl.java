package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.mybatisexample.LocalProductCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.LocalProductExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.LocalProductDao;
import com.dmsdbj.itoo.graduate.service.LocalProductService;
import com.dmsdbj.itoo.graduate.entity.LocalProductEntity;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import com.dmsdbj.itoo.tool.tenancy.TenancyContext;
/**
 * @author sxm
 * create:2017-11-16 14:31:07
 * DESCRIPTION
 */
@Service("localProductService")
public class LocalProductServiceImpl extends BaseServiceImpl<LocalProductEntity, LocalProductExample> implements LocalProductService {


    //注入localProductDao
    @Autowired
    private LocalProductDao localProductDao;

    /**
     * 让BaseServiceImpl获取到Dao
     * @return BaseDao<LocalProductEntity, LocalProductExample>
     */
    @Override
    public  BaseDao<LocalProductEntity, LocalProductExample> getRealDao(){
        return this.localProductDao;
    }

    @Override
    public List<LocalProductEntity> queryLocalProduct(){

        LocalProductExample example = new LocalProductExample();
        LocalProductCriteria criteria = example.createCriteria();
        criteria.andIsDeleteEqualTo((byte) 0);
        return localProductDao.selectByExample(example);
    }


    /**
     * 添加特产信息 ---Added by liweizhong --2017年7月20日09:56:17
     *
     * @param localProductEntity 特产实体
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean addLocalProduct(LocalProductEntity localProductEntity) {
        boolean flag = false;
        int res = localProductDao.insert(localProductEntity);
        if (res == 1) {
            flag = true;
        }
        return flag;
    }

    /**
     * 删除特产信息 ---Added by liweizhong --2017年7月20日09:56:17
     *
     * @param productIds 特产实体
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteLocalProduct(List<String> productIds){
        boolean flag = false;
        String operator = TenancyContext.UserID.get();
        int res = localProductDao.deleteByIds(productIds,operator);
        if (res == productIds.size()) {
            flag = true;
        }
        return flag;
    }

    /**
     * 更新特产信息 ---Added by liweizhong --2017年7月20日09:56:17
     *
     * @param localProductEntity 特产实体
     * @return int
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateLocalProduct(LocalProductEntity localProductEntity){

        if (localProductEntity == null) {
            //logger.info("传入的房间实体为Null");
            return -10;//参数为空
        }
        if (localProductEntity.getId() == null || "".equals(localProductEntity.getId())) {
            //logger.info("传入的房间实体中的主键为null");
            return -10;//必要参数为空
        }
        LocalProductEntity localProductEntities = localProductDao.findById(localProductEntity.getId());
        if(localProductEntities !=null){
            localProductEntities.setRemark(localProductEntity.getRemark());
            localProductEntities.setName(localProductEntity.getName());
            localProductEntities.setOperator(localProductEntity.getOperator());
        }else {
            return -100;//查询没有该数据，则无法更新
        }
        return localProductDao.updateById(localProductEntity);
    }
}
