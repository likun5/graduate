package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.graduate.entity.ext.RoleApplicationModel;
import com.dmsdbj.itoo.graduate.entity.ext.UserRoleModel;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.ApplicationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.ApplicationExample;

import java.util.List;
import java.util.Map;


/**
 * @author : xulingbo
 * create :2018-01-04 15:54:54
 * DESCRIPTION ：
 */
public interface PortalManagementService extends BaseService<ApplicationEntity, ApplicationExample>{
    /**
     * 用于顶栏显示
     * 根据userId查找服务-宋学孟-2018年1月18日
     *
     * @return  //List<RoleApplicationModel>
     */
    List<RoleApplicationModel> queryUserServiceApplication(String userId);


    /**
     * 左侧栏显示
     * 根据userId查找所有的子模块和页面-宋学孟-2018年1月18日
     *
     * @return // List<Map<String, Object>>
     */
    List<Map<String, Object>> queryServiceModuleApplication(String userId, String applicationParentId);

    /**
     * 用页面按钮显示
     * 根据userId和applicationID查找页面中的按钮-宋学孟-2018年1月18日
     *
     * @return List<RoleApplicationModel>
     */
    List<RoleApplicationModel> queryModuleButtonApplication(String userId, String applicationId);

    /**
     * 登录根据userId查询UserEntity-宋学孟-2018年1月18日
     * @param userCode
     * @return
     */
    UserRoleModel selectUserInfoByCode(String userCode, String userPwd);

}
