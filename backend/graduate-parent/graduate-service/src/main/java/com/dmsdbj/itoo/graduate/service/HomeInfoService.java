package com.dmsdbj.itoo.graduate.service;

import com.dmsdbj.itoo.graduate.entity.ext.HomeInfoModel;
import com.dmsdbj.itoo.tool.base.service.BaseService;
import com.dmsdbj.itoo.graduate.entity.HomeInfoEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.HomeInfoExample;
import com.github.pagehelper.PageInfo;


/**
 * @author : 徐玲博
 * create :2017-11-16 14:31:07
 * DESCRIPTION 家庭信息
 */
public interface HomeInfoService extends BaseService<HomeInfoEntity, HomeInfoExample> {
    /**
     * 【毕业生接口】根据用户id删除家庭成员信息-徐玲博-2017-11-20 11:03:48
     *
     * @param userId   用户Id
     * @param operator 操作人
     * @return 受影响行数
     */
    int deleteHomeInfo(String userId, String operator);

    /**
     * 查询家庭信息列表-徐玲博-2017-11-20 11:04:24
     *
     * @param userId 当前登录用户ID
     * @return list列表
     */
    PageInfo<HomeInfoModel> selectHomePersonInfo(String userId, int page, int pageSize);

}
