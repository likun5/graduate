package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.graduate.entity.RegionInformationEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.RegionInformationExample;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import org.springframework.stereotype.Repository;

/**
 * Created by xueyu on 2018/3/29.
 */
@Repository
public interface RegionInformationDao extends BaseDao<RegionInformationEntity,RegionInformationExample> {
}
