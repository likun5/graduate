package com.dmsdbj.itoo.graduate.service.impl;


import com.dmsdbj.itoo.graduate.mybatisexample.PersonSkillRelationCriteria;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.PersonSkillRelationExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.PersonSkillRelationDao;
import com.dmsdbj.itoo.graduate.service.PersonSkillRelationService;
import com.dmsdbj.itoo.graduate.entity.PersonSkillRelationEntity;

import java.util.List;

/**
 * @author : 徐玲博
 * create :2017-11-26 16:14:54
 * DESCRIPTION 个人技术关系
 */
@Service("personSkillRelationService")
public class PersonSkillRelationServiceImpl extends BaseServiceImpl<PersonSkillRelationEntity, PersonSkillRelationExample> implements PersonSkillRelationService {


    //注入personSkillRelationDao
    @Autowired
    private PersonSkillRelationDao personSkillRelationDao;

    /**
     * 让BaseServiceImpl获取到Dao
     *
     * @return BaseDao<PersonSkillRelationEntity,PersonSkillRelationExample>
     */
    @Override
    public BaseDao<PersonSkillRelationEntity, PersonSkillRelationExample> getRealDao() {
        return this.personSkillRelationDao;
    }

    /**
     * 查询某人熟悉的所有技术-徐玲博-2017-12-4 10:58:00
     *
     * @param userId 人员id
     * @return 技术列表
     */
    @Override
    public List<PersonSkillRelationEntity> selectSkillByPerson(String userId) {
        PersonSkillRelationExample personSkillRelationExample = new PersonSkillRelationExample();
        PersonSkillRelationCriteria personSkillRelationCriteria= personSkillRelationExample.createCriteria();
        personSkillRelationCriteria.andUserIdEqualTo(userId).andIsDeleteEqualTo((byte)0);
        return personSkillRelationDao.selectByExample(personSkillRelationExample);
    }

    /**
     * 查询会某个技术的所有人-徐玲博-2017-12-4 10:58:19
     *
     * @param skillId 技术id
     * @return 人员列表
     */
    @Override
    public List<PersonSkillRelationEntity> selectPersonBySkill(String skillId) {
        PersonSkillRelationExample personSkillRelationExample = new PersonSkillRelationExample();
        PersonSkillRelationCriteria personSkillRelationCriteria= personSkillRelationExample.createCriteria();
        personSkillRelationCriteria.andSkillIdEqualTo(skillId).andIsDeleteEqualTo((byte)0);
        return personSkillRelationDao.selectByExample(personSkillRelationExample);
    }
    /**
     * 根据技术id删除关系表中毕业生-徐玲博-2017-12-5 23:26:38
     * @param skillId 技术id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deletePersonBySkillId(String skillId, String operator) {
        PersonSkillRelationExample personSkillRelationExample=new PersonSkillRelationExample();
        PersonSkillRelationCriteria personSkillRelationCriteria=personSkillRelationExample.createCriteria();
        personSkillRelationCriteria.andSkillIdEqualTo(skillId);
        return personSkillRelationDao.deleteByExample(personSkillRelationExample,operator);
    }
    /**
     * 根据人员id删除关系表中技术-徐玲博-2017-12-5 23:26:58
     * @param userId 用户id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deleteSkillByPersonId(String userId, String operator) {
        PersonSkillRelationExample personSkillRelationExample=new PersonSkillRelationExample();
        PersonSkillRelationCriteria personSkillRelationCriteria=personSkillRelationExample.createCriteria();
        personSkillRelationCriteria.andUserIdEqualTo(userId);
        return personSkillRelationDao.deleteByExample(personSkillRelationExample,operator);
    }

    /**
     * 根据人员id删除人员技能关系记录 -lishuang -2017-12-23 15:32:11
     * @param personId 人员id
     * @param operator 操作员
     * @return 是否删除成功
     */
    @Override
    public boolean deletePersonSkillRelationByPersonId(String personId,String operator){
        if(StringUtils.isAnyEmpty(personId,operator)){
            return  false;
        }
        PersonSkillRelationExample personSkillRelationExample = new PersonSkillRelationExample();
        personSkillRelationExample.createCriteria()
                .andUserIdEqualTo(personId);
        int affectNum = this.deleteByExample(personSkillRelationExample,operator);
        return affectNum > 0;
    }

}
