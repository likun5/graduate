package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.BrowseEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.BrowseExample;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 *@author haoguiting
 *create: 2017-11-26 16:14:55
 *DESCRIPTION: 浏览记录dao
 */
@Repository
public interface BrowseDao extends BaseDao<BrowseEntity,BrowseExample> {
    /**
     * 根据通知ID、是否浏览查询浏览人信息-haoguiting-2017年12月3日
     * @param map 浏览人id和通知id
     * @return 浏览记录实体
     */
    List<BrowseEntity> selectBrowseByNoticeId(Map<String, Object> map);

    /**
     * 更新浏览记录-haoguiting-2017年12月3日
     * @param map 通知id和浏览人id
     * @return 受影响行
     */
    int updateBrowseIsBrowse(Map<String, Object> map);

    /**
     * 根据浏览人ID查询未浏览记录-haoguiting-2017年12月3日
     * @param browsePersonId 浏览人id
     * @return 受影响行
     */
    int selectNoBrowseByBrowsePersonId(String browsePersonId);

    /**
     * 根据浏览人ID查询已浏览记录-haoguiting-2017年12月3日
     * @param browsePersonId 浏览人id
     * @return 受影响行
     */
   int selectBrowsedByBrowsePersonId(String browsePersonId);

    /**
     * 根据通知ID查询全部浏览人-haoguiting-2017-12-5 14:15:45
     * @param noticeId 通知id
     * @return 受影响行
     */
    List<BrowseEntity> selectAllBrowserByNoticeId(String noticeId );
}
