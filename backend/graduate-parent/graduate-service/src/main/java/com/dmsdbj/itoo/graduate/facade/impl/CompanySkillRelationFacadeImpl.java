package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.CompanySkillRelationEntity;
import com.dmsdbj.itoo.graduate.facade.CompanySkillRelationFacade;
import com.dmsdbj.itoo.graduate.service.CompanySkillRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *@author :徐玲博
 *create : 2017-11-26 16:14:54
 * DESCRIPTION 公司技术关系
 */
@Component("companySkillRelationFacade")
@Service
public class CompanySkillRelationFacadeImpl implements CompanySkillRelationFacade {

    //打印日志相关
	private static final Logger logger = LoggerFactory.getLogger(CompanySkillRelationFacadeImpl.class);

    @Autowired
    private CompanySkillRelationService companySkillRelationService;

    /**
     * 根据id查询CompanySkillRelation-徐玲博-2017-12-5 08:51:05
     * @param id 关系id
     * @return CompanySkillRelationEntity
     */
    @Override
    public CompanySkillRelationEntity findById(String id) {
        return companySkillRelationService.findById(id);
    }
    /**
     * 根据技术id查询所有使用的公司-徐玲博-2017-12-4 10:34:54
     *
     * @param skillId 技术id
     * @return 人员id -关系列表
     */
    @Override
    public List<CompanySkillRelationEntity> selectCompanyBySkill(String skillId) {
        return companySkillRelationService.selectCompanyBySkill(skillId);
    }
    /**
     * 根据公司id查询，包括所有的技术-徐玲博-2017-12-4 10:37:25
     *
     * @param companyId 公司id
     * @return 技术id -关系列表
     */
    @Override
    public List<CompanySkillRelationEntity> selectSkillByCompany(String companyId) {
        return companySkillRelationService.selectSkillByCompany(companyId);
    }
    /**
     * 添加公司下的技术-徐玲博-2017-12-4 10:40:08
     *
     * @param companySkillRelationEntity 公司技术关系实体
     * @return 受影响行
     */
    @Override
    public int addCompanySkillRelation(CompanySkillRelationEntity companySkillRelationEntity) {
        return companySkillRelationService.insert(companySkillRelationEntity);
    }
    /**
     * 修改公司下的技术-徐玲博-2017-12-4 10:42:39
     *
     * @param companySkillRelationEntity 公司技术关系实体
     * @return 受影响行
     */
    @Override
    public int updateCompanySkillRelation(CompanySkillRelationEntity companySkillRelationEntity) {
        return companySkillRelationService.updateById(companySkillRelationEntity);
    }
    /**
     * 删除某公司下技术-徐玲博-2017-12-4 10:41:41
     *
     * @param id       公司技术关系id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deleteCompanySkillRelation(String id, String operator) {
        return companySkillRelationService.deleteById(id,operator);
    }

    /**
     * 根据公司id删除关系表中技术-徐玲博-2017-12-5 22:10:55
     * @param companyId 公司id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deleteSkillByCompanyId(String companyId, String operator) {
        return companySkillRelationService.deleteSkillByCompanyId(companyId,operator);
    }
    /**
     *根据技术id删除关系表中公司信息-徐玲博-2017-12-5 23:20:31
     *
     * @param skillId 技术id
     * @param operator 操作人
     * @return 受影响行
     */
    @Override
    public int deleteCompanyBySkillId(String skillId, String operator) {
        return companySkillRelationService.deleteCompanyBySkillId(skillId,operator);
    }

}
