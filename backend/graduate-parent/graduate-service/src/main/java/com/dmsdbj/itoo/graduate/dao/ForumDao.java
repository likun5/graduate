package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.ForumEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.ForumExample;
import org.springframework.stereotype.Repository;

/**
 *@author ls
 *create: 2017-12-27 09:54:26
 *DESCRIPTION:
 */
@Repository
public interface ForumDao extends BaseDao<ForumEntity,ForumExample> {
}
