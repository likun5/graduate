package com.dmsdbj.itoo.graduate.service.impl;

import java.util.List;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dmsdbj.itoo.graduate.dao.NoticeCategoryDao;
import com.dmsdbj.itoo.graduate.entity.NoticeCategoryEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.NoticeCategoryExample;
import com.dmsdbj.itoo.graduate.service.NoticeCategoryService;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;

/**
 * @author hgt
 * create:2017-11-27 11:33:54
 * DESCRIPTION
 */
@Service("noticeCategoryService")
public class NoticeCategoryServiceImpl extends BaseServiceImpl<NoticeCategoryEntity, NoticeCategoryExample> implements NoticeCategoryService {


    //注入noticeCategoryDao
    @Autowired
    private NoticeCategoryDao noticeCategoryDao;

    /**
     * 让BaseServiceImpl获取到Dao
     * @return BaseDao<NoticeCategoryEntity, NoticeCategoryExample>
     */
    @Override
    public  BaseDao<NoticeCategoryEntity, NoticeCategoryExample> getRealDao(){
        return this.noticeCategoryDao;
    }


    /**
     * 添加公告分类信息-郝贵婷-2017年11月27日
     * @param noticeCatoryEntity 通知类别实体
     * @return 受影响行数
     */
    @Transactional(rollbackFor = Exception.class)
    public int addNoticeCategory(NoticeCategoryEntity noticeCatoryEntity){

        return this.insert(noticeCatoryEntity);
    }

    /**
     * 删除公告分类信息-郝贵婷-2017年11月27日
     * @param id  通知分类ID
     * @param operator 操作人
     * @return 受影响行数
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteNoticeCategory(String id, String operator){
        if (id != null && operator != null) {
            return super.deleteById(id, operator);
        } else {
            return -1;
        }
    }

    /**
     * 修改公告分类信息-郝贵婷-2017年11月27日
     * @param noticeCategoryEntity 通知分类实体
     * @return 受影响行
     */
    public int updateNoticeCategory(NoticeCategoryEntity noticeCategoryEntity){
        return noticeCategoryDao.updateNoticeCategory(noticeCategoryEntity);
    }


    /**
     * 查询全部通知分类-郝贵婷-2017年11月27日
     * @return 通知分类list列表
     */
    @Override()
    public List<NoticeCategoryEntity> selectNoticeCategoryInfo(){
        return noticeCategoryDao.selectNoticeCategoryInfo();
    }

    /**
     * @根分类ID查询通知分类-haoguiting-2017年11月27日
     * @param id 通知类别id
     * @return NoticeCategoryEntity通知类别实体
     */
    public  NoticeCategoryEntity findById(String id){
        //return  noticeCategoryDao.findById(id);
        return super.findById(id);
    }

    /**
     * 假分页-查询全部通知分类-郝贵婷-2018-01-11 14:15:45
     * @param page 页码
     * @param pageSize 每页显示条数
     * @return 类别list
     */
    @Override
    public PageInfo<NoticeCategoryEntity> PageSelectNoticeCategoryInfo(int page, int pageSize){
        PageHelper.startPage(page, pageSize);
        List<NoticeCategoryEntity> noticeCategoryEntityList=noticeCategoryDao.selectNoticeCategoryInfo();
        return new PageInfo<>(noticeCategoryEntityList);
    }


    /**
     * 根据主键批量删除类别-郝贵婷-2017年11月27日
     * @param id  通知分类ID
     * @param operator 操作人
     * @return 影响行数
     */
    @Override
//    @Transactional(rollbackFor = Exception.class)
    public int deleteNoticeCategorys(String id, String operator) {

        if (id != null && operator != null) {
            return super.deleteById(id, operator);
        } else {
            return -1;
        }

    }
}



