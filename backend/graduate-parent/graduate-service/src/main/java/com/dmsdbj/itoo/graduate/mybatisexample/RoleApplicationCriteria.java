package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class RoleApplicationCriteria extends GeneratedCriteria<RoleApplicationCriteria> implements Serializable{

    protected RoleApplicationCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String APPLICATION_ID = "applicationId";   
    private static final String ROLE_ID = "roleId";   

    public RoleApplicationCriteria andApplicationIdIsNull() {
        addCriterion("application_id is null");
        return this;
    }

    public RoleApplicationCriteria andApplicationIdIsNotNull() {
        addCriterion("application_id is not null");
        return this;
    }
    public RoleApplicationCriteria andApplicationIdEqualTo(String value) {
        addCriterion("application_id =", value, APPLICATION_ID);
        return this;
    }
    public RoleApplicationCriteria andApplicationIdNotEqualTo(String value) {
        addCriterion("application_id <>", value, APPLICATION_ID);
        return this;
    }    
    public RoleApplicationCriteria andApplicationIdGreaterThan(String value) {
        addCriterion("application_id >", value, APPLICATION_ID);
        return this;
    }    
    public RoleApplicationCriteria andApplicationIdGreaterThanOrEqualTo(String value) {
        addCriterion("application_id >=", value, APPLICATION_ID);
        return this;
    }    
    public RoleApplicationCriteria andApplicationIdLessThan(String value) {
        addCriterion("application_id <", value, APPLICATION_ID);
        return this;
    }     
    public RoleApplicationCriteria andApplicationIdLessThanOrEqualTo(String value) {
        addCriterion("application_id <=", value, APPLICATION_ID);
        return this;
    }
    public RoleApplicationCriteria andApplicationIdIn(List<String> values) {
        addCriterion("application_id in", values, APPLICATION_ID);
        return this;
    }
    public RoleApplicationCriteria andApplicationIdNotIn(List<String> values) {
        addCriterion("application_id not in", values, APPLICATION_ID);
        return this;
    }
    public RoleApplicationCriteria andApplicationIdBetween(String value1, String value2) {
        addCriterion("application_id between", value1, value2, APPLICATION_ID);
        return this;
    }
    public RoleApplicationCriteria andApplicationIdNotBetween(String value1, String value2) {
        addCriterion("application_id not between", value1, value2, APPLICATION_ID);
        return this;
    }
        public RoleApplicationCriteria andApplicationIdLike(String value) {
            addCriterion("application_id like", value, APPLICATION_ID);
            return this;
        }

        public RoleApplicationCriteria andApplicationIdNotLike(String value) {
            addCriterion("application_id not like", value, APPLICATION_ID);
            return this;
        }
    public RoleApplicationCriteria andRoleIdIsNull() {
        addCriterion("role_id is null");
        return this;
    }

    public RoleApplicationCriteria andRoleIdIsNotNull() {
        addCriterion("role_id is not null");
        return this;
    }
    public RoleApplicationCriteria andRoleIdEqualTo(String value) {
        addCriterion("role_id =", value, ROLE_ID);
        return this;
    }
    public RoleApplicationCriteria andRoleIdNotEqualTo(String value) {
        addCriterion("role_id <>", value, ROLE_ID);
        return this;
    }    
    public RoleApplicationCriteria andRoleIdGreaterThan(String value) {
        addCriterion("role_id >", value, ROLE_ID);
        return this;
    }    
    public RoleApplicationCriteria andRoleIdGreaterThanOrEqualTo(String value) {
        addCriterion("role_id >=", value, ROLE_ID);
        return this;
    }    
    public RoleApplicationCriteria andRoleIdLessThan(String value) {
        addCriterion("role_id <", value, ROLE_ID);
        return this;
    }     
    public RoleApplicationCriteria andRoleIdLessThanOrEqualTo(String value) {
        addCriterion("role_id <=", value, ROLE_ID);
        return this;
    }
    public RoleApplicationCriteria andRoleIdIn(List<String> values) {
        addCriterion("role_id in", values, ROLE_ID);
        return this;
    }
    public RoleApplicationCriteria andRoleIdNotIn(List<String> values) {
        addCriterion("role_id not in", values, ROLE_ID);
        return this;
    }
    public RoleApplicationCriteria andRoleIdBetween(String value1, String value2) {
        addCriterion("role_id between", value1, value2, ROLE_ID);
        return this;
    }
    public RoleApplicationCriteria andRoleIdNotBetween(String value1, String value2) {
        addCriterion("role_id not between", value1, value2, ROLE_ID);
        return this;
    }
        public RoleApplicationCriteria andRoleIdLike(String value) {
            addCriterion("role_id like", value, ROLE_ID);
            return this;
        }

        public RoleApplicationCriteria andRoleIdNotLike(String value) {
            addCriterion("role_id not like", value, ROLE_ID);
            return this;
        }
}

