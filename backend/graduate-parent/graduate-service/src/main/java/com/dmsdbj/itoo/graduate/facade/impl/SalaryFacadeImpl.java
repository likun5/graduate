package com.dmsdbj.itoo.graduate.facade.impl;


import com.dmsdbj.itoo.graduate.entity.SalaryEntity;
import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import com.dmsdbj.itoo.graduate.facade.SalaryFacade;
import com.dmsdbj.itoo.graduate.service.SalaryService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 *@author徐玲博
 *create: 2017-11-26 16:14:54
 * DESCRIPTION
 */
@Component("salaryFacade")
@Service
public class SalaryFacadeImpl implements SalaryFacade {


    //打印日志相关
    private static final Logger logger = LoggerFactory.getLogger(SalaryFacadeImpl.class);

    @Autowired
    SalaryService salaryService;

    /**
     * @author yyl
     * @return list
     * 根据薪资id查询
     */
    @Override
     public PageInfo<SalaryEntity> findAll(int page, int pageSize){
     //public List<SalaryEntity> findAll(  ){
       return  salaryService.findAll(  page, pageSize );
    }
    /**
     * 根据人员id查询Salary
     * @param personId
     * @return List<SalaryEntity>
     */
    @Override
    public List<SalaryEntity> selectByPersonId(String personId){
        return salaryService.selectByPersonId(personId);
    }


    /**
     * 根据年级查询最低薪资
     * @param  ：年级
     * @return 薪资列表
     */
    @Override
    public List<SalaryEntity>  selectLowestSalaryByGrade(String grade){
             return salaryService.selectLowestSalaryByGrade(grade);
    }
    /**
     * 根据年级查询最高薪资
     * @param  ：年级
     * @return 薪资列表
     */
    @Override
    public List<SalaryEntity>  selectHighestSalaryByGrade(String grade){
        return salaryService.selectHighestSalaryByGrade(grade);
    }

    /**
     * 根据年级查询平均薪资列表
     * @param  ：年度
     * @return 本年度平均工资和期数List
     */
    @Override
    public Double selectAverageSalaryByGrade(String grade){
        return salaryService.selectAverageSalaryByGrade(grade);
    }






    /**
     * 根据年级id查询本年最高薪资-于云丽-2018-01-14
     * @param
     * @return 薪资实体list
     */
    @Override
    public Double selectHighSalaryByGradeId(String gradeId){
        return salaryService.selectHighSalaryByGradeId(gradeId);
    }



    /**
     * 根据年级id查询本年最低薪资-于云丽-2018-01-14
     * @param
     * @return 薪资实体list
     */
    @Override
    public Double selectLowestSalaryByGradeId(String gradeId){
        return salaryService.selectLowestSalaryByGradeId(gradeId);
    }

    /**
     * 根据年级id查询本年平均薪资-于云丽-2018-01-14
     * @param
     * @return 薪资实体list
     */
    @Override
    public Double selectAverageSalaryByGradeId(String gradeId){
        return salaryService.selectAverageSalaryByGradeId(gradeId);
    }



    /**
     * 根据id查询Salary
     * @param id
     * @return SalaryEntity
     */
    @Override
    public SalaryEntity findById(String id) {
        return salaryService.findById(id);
    }
    /**
     *添加学历信息接口
     * @author yyl
     */
    @Override
    public int addSalaryEntity(SalaryEntity salaryEntity ){

        return salaryService.insert(salaryEntity);

    }
    /**
     *删除学历信息接口
     * @author yyl
     */
    @Override
    public int deleteSalaryEntity( String salaryId ,String operator){
        return  salaryService.deleteById(salaryId,operator);
    }

    /**
     *修改学历信息接口
     * @author yyl
     */
    @Override
    public int updateSalaryEntity( SalaryEntity salaryEntity){
        return  salaryService.updateById(salaryEntity);
    }

    /**
     *查询本年度全体平均工资接口
     * @author yyl
     */
    @Override
    public Double findAverageSalary(Date date) {
        return salaryService.selectAverageSalaryOfAll(date);
    }


    /**
     * 根据关系Id、年限查询薪资-时间列表(升序降序没有体现)
     *
     * @param pcRelation
     * @param date
     * @return 一条信息
     */
    @Override
    public List<SalaryEntity> selectByPcRelation(String pcRelation, Date date) {
        return salaryService.selectByPcRelation(pcRelation,date);
    }
    /**
     * 根据期数相应人员列表查询本期最高工资
     * @param grade
     * @param
     * @return 最高工资的记录
     */
    @Override
    public Map<String ,List<SalaryEntity>> selectByGrade(String grade ){
        return salaryService.selectByGrade(grade);
    }
    /**
     * 根据期数相应人员列表查询本期最高工资
     * @param name
     * @param
     * @return 最高工资的记录
     */
    @Override
    public  List<SalaryEntity>  selectByName(String name ){
        return salaryService.selectByName(name);
    }
    /**
     * 根据公司人员关系列表查询本期最高工资
     *
     * @param relationIds
     * @param date
     * @return 最高工资的记录（排序根据薪资升序排列如何实现？）
     */
    @Override
    public List selecthighestSalaryByRelatoinId(List<String> relationIds, Date date) {
        return salaryService.selecthighestSalaryByRelatoinId(relationIds,date);
    }

    /**
     * 根据期数查询本期最低工资
     *
     * @param relationIds
     * @param date
     * @return 平均最低的记录
     */
    @Override
    public List selectlowestSalaryByRelatoinId(List<String> relationIds, Date date) {
        return salaryService.selectlowestSalaryByRelatoinId(relationIds, date);
    }

    /**
     * 根据期数查询本期平均工资
     *
     * @param relationIds
     * @param date
     * @return 平均工资的记录
     */
    @Override
    public Double selectAverageSalaryByRelatoinId(List<String> relationIds, Date date) {
        return  salaryService.selectAverageSalaryByRelatoinId(relationIds, date);
    }

    /**
     * 整体本年度最高薪资统计
     *
     * @param date
     * @return 本年度全体中的工资和期数List
     */
    @Override
    public List selecthighestSalaryofAll(Date date) {
        return  salaryService.selecthighestSalaryofAll(date);
    }

    /**
     * 整体本年度最低薪资统计
     *
     * @param date
     * @return 本年度最低工资和期数List
     */
    @Override
    public List selectlowestSalaryofAll(Date date) {
        return  salaryService.selectlowestSalaryofAll(date);
    }

    /**
     *根据毕业生id查询薪资公司相关信息-haoguiting--2018年3月7日
     * @param userId
     * @return
     */
    @Override
    public PageInfo<CompanySalaryModel> PageSelectSalaryByUserId(String userId,int page, int pageSize){
        return salaryService.PageSelectSalaryByUserId(userId,page,pageSize);
    }
}
