package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.ForumEntity;
import com.dmsdbj.itoo.graduate.facade.ForumFacade;
import com.dmsdbj.itoo.graduate.service.ForumService;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *@authorls
 *create: 2017-12-27 09:54:26
 * DESCRIPTION
 */
@Component("forumFacade")
@Service
public class ForumFacadeImpl implements ForumFacade {

    //打印日志相关
	private static final Logger logger = LoggerFactory.getLogger(ForumFacadeImpl.class);

    @Autowired
    ForumService forumService;

    /**
     * 根据id查询Forum
     * @param id
     * @return ForumEntity
     */
    @Override
    public ForumEntity findById(String id) {
        return forumService.findById(id);
    }
    
}
