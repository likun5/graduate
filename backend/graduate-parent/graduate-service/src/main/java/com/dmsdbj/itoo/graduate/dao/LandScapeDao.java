package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import com.dmsdbj.itoo.graduate.entity.LandScapeEntity;
import com.dmsdbj.itoo.graduate.mybatisexample.LandScapeExample;
import org.springframework.stereotype.Repository;

/**
 *@author sxm
 *create: 2017-11-16 14:31:07
 *DESCRIPTION:
 */
@Repository
public interface LandScapeDao extends BaseDao<LandScapeEntity,LandScapeExample> {
}
