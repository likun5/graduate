package com.dmsdbj.itoo.graduate.facade.impl;

import com.dmsdbj.itoo.graduate.entity.EducationEntity;
import com.dmsdbj.itoo.graduate.entity.ext.EducationModel;
import com.dmsdbj.itoo.graduate.entity.ext.PersonalEducationEntity;
import com.dmsdbj.itoo.graduate.facade.EducationFacade;
import com.dmsdbj.itoo.graduate.service.EducationService;
import com.dmsdbj.itoo.tool.itooexception.ItooRuntimeException;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 *@authorsxm
 *create: 2017-11-16 14:31:07
 * DESCRIPTION
 */
@Component("educationFacade")
@Service
public class EducationFacadeImpl implements EducationFacade {

    //打印日志相关
	private static final Logger logger = LoggerFactory.getLogger(EducationFacadeImpl.class);

    @Autowired
    EducationService educationService;
    /**
     * 保存上传图片的路径（一张或多张）-宋学孟-2018年1月14日
     * @param pictureUrl  图片URLs
     * @param keyId 学历ID
     * @return 学历实体
     */
    @Override
    public int addPictureUrl(String pictureUrl, String keyId) {

        return educationService.addPictureUrl(pictureUrl, keyId);
    }

    /**
     * 查询提高班所有毕业生的学历信息
     * @return 学历实体
     */
    @Override
    public List<PersonalEducationEntity> queryPersonalEducation() {
        List<PersonalEducationEntity> personalEducationEntityList;
        try{
            personalEducationEntityList =educationService.queryPersonalEducation();
        }
        catch (Exception e){
            logger.error("查询失败！",e);
            throw new ItooRuntimeException(e);
        }
        return personalEducationEntityList;
    }

    /**
     * 通过个人ID 查询毕业生学历信息
     * @param persionId  用户ID
     * @return 学历实体
     */
    @Override
    public List<PersonalEducationEntity> findEducationByPersonId(String persionId) {
        List<PersonalEducationEntity> personalEducationEntityList;
        try{
            personalEducationEntityList=educationService.findEducationByPersonId(persionId);

        }catch (Exception e){
            logger.error("查询失败！");
            throw new ItooRuntimeException(e);
        }
        return personalEducationEntityList;
    }

    /**
     * 根据类型id查询Education
     * @param certificateType 学历类型
     * @return EducationEntity 学历实体
     */
    @Override
    public List<EducationEntity> findByCertificateType(String certificateType) {
        return educationService.findByCertificateType(certificateType);
    }

    /**
     * 根据ID查询学历信息
     * @param id 学历ID
     * @return  学历实体
     */
    @Override
    public EducationEntity findById(String id) {
        return educationService.findById(id);
    }

    /**
     * 分页个人所有学历信息
     * @param page 第几页
     * @param pageSize 每页显示数量
     * @return 带分页信息的学历ID
     */
    @Override
    public PageInfo<EducationEntity> findAll(int page, int pageSize) {
        return educationService.findAll( page,  pageSize);
    }

    /**
     * 分页查询个人所有学历信息
     * @param page 第几页
     * @param pageSize 每页显示数量
     * @param UserId 用户ID
     * @return 带分页信息的学历ID
     */
    @Override
    public PageInfo<EducationEntity> findPersonAll(int page, int pageSize, String UserId){
        return educationService.findPersonAll( page,  pageSize,UserId);
    }

    /**
     * 根据个人ID查询学历信息
     * @param personId 个人ID
     * @return 查询结果
     */
    @Override
    public List<EducationEntity> findByPersonId (String personId){
        return educationService.findByPersonId(personId);
    }

    /**
     * 根据姓名查询学历信息
     * @param name 姓名
     * @return 姓名信息实体
     */
    @Override
    public List<EducationEntity> findByName (String name){
         return educationService.findByName(name);
       // List<EducationEntity> list = new ArrayList<EducationEntity>();
        //return list;
    }

    /**
     * 增加学历信息
     * @param educationEntity 学历实体
     * @return int 0，1：是否成功
     */
    @Override
    public int addEducationEntity(EducationEntity educationEntity ){

        return educationService.insert(educationEntity);

    }

    /**
     *   删除学历实体
     * @param educationId 学历ID
     * @param operator 操作员ID
     * @return int 0，1：是否成功
     */
     @Override
    public int deleteEducationEntity( String educationId ,String operator){
           return  educationService.deleteById(educationId,operator);
    }

    /**
     * 更新学历信息
     * @param educationModel 学历
     * @return  int 0，1：是否成功
     */
    @Override
    public int updateEducationEntity(EducationModel educationModel){
        return  educationService.updateById(educationModel);
    }

    /**
     *  删除学历信息
     * @param userId 用户ID
     * @param operator 操作员ID
     * @return  int 0，1：是否成功
     */
    @Override
    public int deleteByPersonId(String userId,String operator){
        return  educationService.deleteByPersonId(userId,operator);
    }

    /**
     * 添加学历实体和图片list
     * @param EducationModelList 学历实体
     * @return  int 0，1：是否成功
     */
    @Override
    public int addEducationModel(List<EducationModel> EducationModelList){
        int count;
        try {
            count = educationService.addEducationModel(EducationModelList);
        } catch (Exception e) {
            logger.error("添加学历信息异常", e);
            throw new ItooRuntimeException(e);
        }
        return count;
    }

    /**
     * 根据学历ID查询相应的学历信息和图片urls
     * @param id 学历ID
     * @return  学历信息实体
     */
    @Override
    public  EducationModel queryEducationModel(String id){
        return  educationService.queryEducationModel(id);
    }

}
