package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class UserRoleCriteria extends GeneratedCriteria<UserRoleCriteria> implements Serializable{

    protected UserRoleCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String USER_ID = "userId";   
    private static final String ROLE_ID = "roleId";   

    public UserRoleCriteria andUserIdIsNull() {
        addCriterion("user_id is null");
        return this;
    }

    public UserRoleCriteria andUserIdIsNotNull() {
        addCriterion("user_id is not null");
        return this;
    }
    public UserRoleCriteria andUserIdEqualTo(String value) {
        addCriterion("user_id =", value, USER_ID);
        return this;
    }
    public UserRoleCriteria andUserIdNotEqualTo(String value) {
        addCriterion("user_id <>", value, USER_ID);
        return this;
    }    
    public UserRoleCriteria andUserIdGreaterThan(String value) {
        addCriterion("user_id >", value, USER_ID);
        return this;
    }    
    public UserRoleCriteria andUserIdGreaterThanOrEqualTo(String value) {
        addCriterion("user_id >=", value, USER_ID);
        return this;
    }    
    public UserRoleCriteria andUserIdLessThan(String value) {
        addCriterion("user_id <", value, USER_ID);
        return this;
    }     
    public UserRoleCriteria andUserIdLessThanOrEqualTo(String value) {
        addCriterion("user_id <=", value, USER_ID);
        return this;
    }
    public UserRoleCriteria andUserIdIn(List<String> values) {
        addCriterion("user_id in", values, USER_ID);
        return this;
    }
    public UserRoleCriteria andUserIdNotIn(List<String> values) {
        addCriterion("user_id not in", values, USER_ID);
        return this;
    }
    public UserRoleCriteria andUserIdBetween(String value1, String value2) {
        addCriterion("user_id between", value1, value2, USER_ID);
        return this;
    }
    public UserRoleCriteria andUserIdNotBetween(String value1, String value2) {
        addCriterion("user_id not between", value1, value2, USER_ID);
        return this;
    }
        public UserRoleCriteria andUserIdLike(String value) {
            addCriterion("user_id like", value, USER_ID);
            return this;
        }

        public UserRoleCriteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, USER_ID);
            return this;
        }
    public UserRoleCriteria andRoleIdIsNull() {
        addCriterion("role_id is null");
        return this;
    }

    public UserRoleCriteria andRoleIdIsNotNull() {
        addCriterion("role_id is not null");
        return this;
    }
    public UserRoleCriteria andRoleIdEqualTo(String value) {
        addCriterion("role_id =", value, ROLE_ID);
        return this;
    }
    public UserRoleCriteria andRoleIdNotEqualTo(String value) {
        addCriterion("role_id <>", value, ROLE_ID);
        return this;
    }    
    public UserRoleCriteria andRoleIdGreaterThan(String value) {
        addCriterion("role_id >", value, ROLE_ID);
        return this;
    }    
    public UserRoleCriteria andRoleIdGreaterThanOrEqualTo(String value) {
        addCriterion("role_id >=", value, ROLE_ID);
        return this;
    }    
    public UserRoleCriteria andRoleIdLessThan(String value) {
        addCriterion("role_id <", value, ROLE_ID);
        return this;
    }     
    public UserRoleCriteria andRoleIdLessThanOrEqualTo(String value) {
        addCriterion("role_id <=", value, ROLE_ID);
        return this;
    }
    public UserRoleCriteria andRoleIdIn(List<String> values) {
        addCriterion("role_id in", values, ROLE_ID);
        return this;
    }
    public UserRoleCriteria andRoleIdNotIn(List<String> values) {
        addCriterion("role_id not in", values, ROLE_ID);
        return this;
    }
    public UserRoleCriteria andRoleIdBetween(String value1, String value2) {
        addCriterion("role_id between", value1, value2, ROLE_ID);
        return this;
    }
    public UserRoleCriteria andRoleIdNotBetween(String value1, String value2) {
        addCriterion("role_id not between", value1, value2, ROLE_ID);
        return this;
    }
        public UserRoleCriteria andRoleIdLike(String value) {
            addCriterion("role_id like", value, ROLE_ID);
            return this;
        }

        public UserRoleCriteria andRoleIdNotLike(String value) {
            addCriterion("role_id not like", value, ROLE_ID);
            return this;
        }
}

