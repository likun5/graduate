package com.dmsdbj.itoo.graduate.dao;

import com.dmsdbj.itoo.graduate.entity.CompanyEntity;
import com.dmsdbj.itoo.graduate.entity.ext.CompanyModel;
import com.dmsdbj.itoo.graduate.mybatisexample.CompanyExample;
import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *@author sxm
 *create: 2017-11-16 14:31:07
 *DESCRIPTION:
 */
@Repository
public interface CompanyDao extends BaseDao<CompanyEntity,CompanyExample> {

    /**
     * 查询公司相关信息宋学孟--2017年11月25日
     * @param userId 学员Id
     * @return 公司实体list
     */
    List<CompanyModel> selectCompanyByUserId(@Param("userId") String userId);

    /**
     * 查询学员公司薪资列表信息-宋学孟-2017年12月4日
     * @param userId 学员Id
     * @return 公司薪资列表
     */
    @Deprecated
    List<CompanyModel> selectCompanyInfoByUserId(@Param("userId") String userId);
}
