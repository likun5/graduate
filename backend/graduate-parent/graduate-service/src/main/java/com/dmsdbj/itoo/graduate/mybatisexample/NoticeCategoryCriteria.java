package com.dmsdbj.itoo.graduate.mybatisexample;

import com.dmsdbj.itoo.tool.base.exampletool.Criterion;
import com.dmsdbj.itoo.tool.base.exampletool.GeneratedCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * 
 * @author :张欢-十二期
 * @version:V1.5
 * DESCRIPTION:解决引用包名大写改为小写
 * create:2017年11月4日
 * 
 */
public  class NoticeCategoryCriteria extends GeneratedCriteria<NoticeCategoryCriteria> implements Serializable{

    protected NoticeCategoryCriteria() {
        super();
        criteria = new ArrayList<>();
    }

    private static final String COLUMN_NAME = "columnName";   
    private static final String OPERATOR_ID = "operatorId";   
    private static final String TIMESTAMP_TIME = "timestampTime";   

    public NoticeCategoryCriteria andColumnNameIsNull() {
        addCriterion("column_name is null");
        return this;
    }

    public NoticeCategoryCriteria andColumnNameIsNotNull() {
        addCriterion("column_name is not null");
        return this;
    }
    public NoticeCategoryCriteria andColumnNameEqualTo(String value) {
        addCriterion("column_name =", value, COLUMN_NAME);
        return this;
    }
    public NoticeCategoryCriteria andColumnNameNotEqualTo(String value) {
        addCriterion("column_name <>", value, COLUMN_NAME);
        return this;
    }    
    public NoticeCategoryCriteria andColumnNameGreaterThan(String value) {
        addCriterion("column_name >", value, COLUMN_NAME);
        return this;
    }    
    public NoticeCategoryCriteria andColumnNameGreaterThanOrEqualTo(String value) {
        addCriterion("column_name >=", value, COLUMN_NAME);
        return this;
    }    
    public NoticeCategoryCriteria andColumnNameLessThan(String value) {
        addCriterion("column_name <", value, COLUMN_NAME);
        return this;
    }     
    public NoticeCategoryCriteria andColumnNameLessThanOrEqualTo(String value) {
        addCriterion("column_name <=", value, COLUMN_NAME);
        return this;
    }
    public NoticeCategoryCriteria andColumnNameIn(List<String> values) {
        addCriterion("column_name in", values, COLUMN_NAME);
        return this;
    }
    public NoticeCategoryCriteria andColumnNameNotIn(List<String> values) {
        addCriterion("column_name not in", values, COLUMN_NAME);
        return this;
    }
    public NoticeCategoryCriteria andColumnNameBetween(String value1, String value2) {
        addCriterion("column_name between", value1, value2, COLUMN_NAME);
        return this;
    }
    public NoticeCategoryCriteria andColumnNameNotBetween(String value1, String value2) {
        addCriterion("column_name not between", value1, value2, COLUMN_NAME);
        return this;
    }
        public NoticeCategoryCriteria andColumnNameLike(String value) {
            addCriterion("column_name like", value, COLUMN_NAME);
            return this;
        }

        public NoticeCategoryCriteria andColumnNameNotLike(String value) {
            addCriterion("column_name not like", value, COLUMN_NAME);
            return this;
        }
    public NoticeCategoryCriteria andOperatorIdIsNull() {
        addCriterion("operator_id is null");
        return this;
    }

    public NoticeCategoryCriteria andOperatorIdIsNotNull() {
        addCriterion("operator_id is not null");
        return this;
    }
    public NoticeCategoryCriteria andOperatorIdEqualTo(String value) {
        addCriterion("operator_id =", value, OPERATOR_ID);
        return this;
    }
    public NoticeCategoryCriteria andOperatorIdNotEqualTo(String value) {
        addCriterion("operator_id <>", value, OPERATOR_ID);
        return this;
    }    
    public NoticeCategoryCriteria andOperatorIdGreaterThan(String value) {
        addCriterion("operator_id >", value, OPERATOR_ID);
        return this;
    }    
    public NoticeCategoryCriteria andOperatorIdGreaterThanOrEqualTo(String value) {
        addCriterion("operator_id >=", value, OPERATOR_ID);
        return this;
    }    
    public NoticeCategoryCriteria andOperatorIdLessThan(String value) {
        addCriterion("operator_id <", value, OPERATOR_ID);
        return this;
    }     
    public NoticeCategoryCriteria andOperatorIdLessThanOrEqualTo(String value) {
        addCriterion("operator_id <=", value, OPERATOR_ID);
        return this;
    }
    public NoticeCategoryCriteria andOperatorIdIn(List<String> values) {
        addCriterion("operator_id in", values, OPERATOR_ID);
        return this;
    }
    public NoticeCategoryCriteria andOperatorIdNotIn(List<String> values) {
        addCriterion("operator_id not in", values, OPERATOR_ID);
        return this;
    }
    public NoticeCategoryCriteria andOperatorIdBetween(String value1, String value2) {
        addCriterion("operator_id between", value1, value2, OPERATOR_ID);
        return this;
    }
    public NoticeCategoryCriteria andOperatorIdNotBetween(String value1, String value2) {
        addCriterion("operator_id not between", value1, value2, OPERATOR_ID);
        return this;
    }
        public NoticeCategoryCriteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, OPERATOR_ID);
            return this;
        }

        public NoticeCategoryCriteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, OPERATOR_ID);
            return this;
        }
    public NoticeCategoryCriteria andTimestampTimeIsNull() {
        addCriterion("timestamp_time is null");
        return this;
    }

    public NoticeCategoryCriteria andTimestampTimeIsNotNull() {
        addCriterion("timestamp_time is not null");
        return this;
    }
    public NoticeCategoryCriteria andTimestampTimeEqualTo(String value) {
        addCriterion("timestamp_time =", value, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCategoryCriteria andTimestampTimeNotEqualTo(String value) {
        addCriterion("timestamp_time <>", value, TIMESTAMP_TIME);
        return this;
    }    
    public NoticeCategoryCriteria andTimestampTimeGreaterThan(String value) {
        addCriterion("timestamp_time >", value, TIMESTAMP_TIME);
        return this;
    }    
    public NoticeCategoryCriteria andTimestampTimeGreaterThanOrEqualTo(String value) {
        addCriterion("timestamp_time >=", value, TIMESTAMP_TIME);
        return this;
    }    
    public NoticeCategoryCriteria andTimestampTimeLessThan(String value) {
        addCriterion("timestamp_time <", value, TIMESTAMP_TIME);
        return this;
    }     
    public NoticeCategoryCriteria andTimestampTimeLessThanOrEqualTo(String value) {
        addCriterion("timestamp_time <=", value, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCategoryCriteria andTimestampTimeIn(List<String> values) {
        addCriterion("timestamp_time in", values, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCategoryCriteria andTimestampTimeNotIn(List<String> values) {
        addCriterion("timestamp_time not in", values, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCategoryCriteria andTimestampTimeBetween(String value1, String value2) {
        addCriterion("timestamp_time between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
    public NoticeCategoryCriteria andTimestampTimeNotBetween(String value1, String value2) {
        addCriterion("timestamp_time not between", value1, value2, TIMESTAMP_TIME);
        return this;
    }
}

