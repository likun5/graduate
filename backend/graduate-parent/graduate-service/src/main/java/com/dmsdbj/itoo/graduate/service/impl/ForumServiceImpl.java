package com.dmsdbj.itoo.graduate.service.impl;

import com.dmsdbj.itoo.tool.base.dao.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dmsdbj.itoo.graduate.mybatisexample.ForumExample;
import com.dmsdbj.itoo.tool.base.service.impl.BaseServiceImpl;
import com.dmsdbj.itoo.graduate.dao.ForumDao;
import com.dmsdbj.itoo.graduate.service.ForumService;
import com.dmsdbj.itoo.graduate.entity.ForumEntity;

/**
 * @author ls
 * create:2017-12-27 09:54:26
 * DESCRIPTION
 */
@Service("forumService")
public class ForumServiceImpl extends BaseServiceImpl<ForumEntity, ForumExample> implements ForumService {


    //??forumDao
	@Autowired
    private ForumDao forumDao;

    /**
     * ?BaseServiceImpl???Dao
     * @return BaseDao<ForumEntity, ForumExample>
     */
    @Override
    public  BaseDao<ForumEntity, ForumExample> getRealDao(){
        return this.forumDao;
    }
}
