package testUtil;

import com.dmsdbj.itoo.graduate.facade.*;
import org.junit.Before;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CommonFacade {
	public BeanFactory factory;
    public CommercialOppotunityFacade commercialOppotunityFacade;
    public CompanyFacade companyFacade;
    public DictionaryFacade dictionaryFacade;
    public EducationFacade educationFacade;
    public HomeInfoFacade homeInfoFacade;
    public LandScapeFacade landScapeFacade;
    public LocalProductFacade localProductFacade;
    public PersonalInfoFacade personalInfoFacade;
    public PictureFacade pictureFacade;
    public SkillPointFacade skillPointFacade;
	public BrowseFacade browseFacade;
	public CompanySkillRelationFacade companySkillRelationFacade;
	public NoticeFacade noticeFacade;
	public NoticeCategoryFacade noticeCategoryFacade;
	public PersonCompanyRelationFacade personCompanyRelationFacade;
	public PersonSkillRelationFacade personSkillRelationFacade;
	public SalaryFacade salaryFacade;
	public AdministrativeRegionFacade administrativeRegionFacade;
	public PortalManagementFacade portalManagementFacade;
	public SalaryEduCertFacade salaryEduCertFacade;

	@Before
	public void setUp() {
		factory = new ClassPathXmlApplicationContext("classpath:spring/spring.xml");
		commercialOppotunityFacade = (CommercialOppotunityFacade) factory.getBean("commercialOppotunityFacade");
		companyFacade = (CompanyFacade) factory.getBean("companyFacade");
		dictionaryFacade = (DictionaryFacade) factory.getBean("dictionaryFacade");
		educationFacade = (EducationFacade) factory.getBean("educationFacade");
		homeInfoFacade = (HomeInfoFacade) factory.getBean("homeInfoFacade");
		landScapeFacade = (LandScapeFacade) factory.getBean("landScapeFacade");
		localProductFacade = (LocalProductFacade) factory.getBean("localProductFacade");
		personalInfoFacade = (PersonalInfoFacade) factory.getBean("personalInfoFacade");
		pictureFacade = (PictureFacade) factory.getBean("pictureFacade");
		skillPointFacade = (SkillPointFacade) factory.getBean("skillPointFacade");
		browseFacade=(BrowseFacade) factory.getBean("browseFacade");
		companySkillRelationFacade=(CompanySkillRelationFacade) factory.getBean("companySkillRelationFacade");
		noticeCategoryFacade=(NoticeCategoryFacade) factory.getBean("noticeCategoryFacade");
		noticeFacade=(NoticeFacade) factory.getBean("noticeFacade");
		personCompanyRelationFacade=(PersonCompanyRelationFacade) factory.getBean("personCompanyRelationFacade");
		personSkillRelationFacade=(PersonSkillRelationFacade) factory.getBean("personSkillRelationFacade");
		salaryFacade = (SalaryFacade) factory.getBean("salaryFacade");
		administrativeRegionFacade = (AdministrativeRegionFacade) factory.getBean("administrativeRegionFacade");
		portalManagementFacade=(PortalManagementFacade) factory.getBean("portalManagementFacade");
		salaryEduCertFacade =(SalaryEduCertFacade) factory.getBean("salaryEduCertFacade");
	}
}
