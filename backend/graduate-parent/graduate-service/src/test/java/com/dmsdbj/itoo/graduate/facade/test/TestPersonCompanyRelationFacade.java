package com.dmsdbj.itoo.graduate.facade.test;

import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.PersonCompanyRelationEntity;

import java.util.List;


public class TestPersonCompanyRelationFacade extends CommonFacade {

    /**
	 *@author sxm
	 *create:2017-11-26 16:14:54
     *DESCRIPTION:分页查询测试
     */
    @Test
    public  void findById(){

        PersonCompanyRelationEntity personCompanyRelationEntity = personCompanyRelationFacade.findById("0044fbbe28c254673bc37a");
        if (personCompanyRelationEntity!=null){
        	assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     * 测试：根据个人id查询个人公司关系id的List -lishuang-2017-12-16 17:10:53
     */
    @Test
    public void selectPersonCompanyRelationIdsByPersonId(){
        String personId = "0044fbbe28c254673bc37a";
        List<String> ralatioIds = personCompanyRelationFacade.selectPersonCompanyRelationIdsByPersonId(personId);
        if(ralatioIds!=null && ralatioIds.size()>0){
            assert true:"正确";
        }else{
            assert false:"null";
        }
    }

    @Test
    public void selectPersonCompanyRelationIdsByGrade(){
        String grade = "10";
        List<String> ralatioIds = personCompanyRelationFacade.selectPersonCompanyRelationIdsByGrade(grade);
        if(ralatioIds!=null && ralatioIds.size()>0){
            assert true:"正确";
        }else{
            assert false:"null";
        }
    }
}
