package com.dmsdbj.itoo.graduate.facade.test;

import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.PictureEntity;


public class TestPictureFacade extends CommonFacade {

    /**
	 *@author sxm
	 *create:2017-11-16 14:31:07
     *DESCRIPTION:分页查询测试
     */
    @Test
    public  void findById(){

        PictureEntity pictureEntity = pictureFacade.findById("0044fbbe28c254673bc37a");
        if (pictureEntity!=null){
        	assert true:"正确";
        }else {
            assert false:"null";
        }
    }
    @Test
    public void testUploadPersonPicture() {
        String fileUrl = "C:/Users/玲博/Pictures/001.jpg";//图片地址
        String fileExtName = "jpg"; //图片类型

        String jpg = pictureFacade.uploadPersonPicture(fileUrl, fileExtName);
//        String conff = "classpath:spring/fast_client.conf";
//        String conf = conff.replace("classpath:", URLDecoder.decode(this.getClass().getResource("/").getPath(), "UTF-8"));
//        assert !"".equals(conf) : "上传成功！";
        System.out.println("==================================="+jpg);
    }

    /**
     * 根据与图片表有关表的id，删除图片-徐玲博-2018-2-9 16:16:23
     */
    @Test
    public  void deletePicture(){
        String keyId="";
        String operator="";
        pictureFacade.deletePictureById(keyId,operator);
    }
    @Test
    public  void selcbyid(){
        String keyId="KHNvKSBY8iNLq2EHZjhgNf";

        System.out.println(pictureFacade.selectPictureById(keyId));
    }
}
