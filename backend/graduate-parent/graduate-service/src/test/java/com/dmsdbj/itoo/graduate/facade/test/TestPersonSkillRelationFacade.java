package com.dmsdbj.itoo.graduate.facade.test;

import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.PersonSkillRelationEntity;


public class TestPersonSkillRelationFacade extends CommonFacade {

    /**
	 *@author sxm
	 *create:2017-11-26 16:14:54
     *DESCRIPTION:分页查询测试
     */
    @Test
    public  void findById(){

        PersonSkillRelationEntity personSkillRelationEntity = personSkillRelationFacade.findById("0044fbbe28c254673bc37a");
        if (personSkillRelationEntity!=null){
        	assert true:"正确";
        }else {
            assert false:"null";
        }
    }


}
