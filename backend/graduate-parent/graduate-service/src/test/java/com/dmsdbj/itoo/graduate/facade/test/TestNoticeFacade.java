package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.graduate.entity.BrowseEntity;
import com.dmsdbj.itoo.graduate.entity.ext.NoticeBrowseModel;
import com.dmsdbj.itoo.graduate.entity.ext.NoticeNoticeCategoryModel;
import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.NoticeEntity;

import java.util.ArrayList;
import java.util.List;


public class TestNoticeFacade extends CommonFacade {

    /**
	 *@author hgt
	 *create:2017-11-26 16:14:54
     *DESCRIPTION:根据ID查询通知
     */
    @Test
    public  void findNoticeNoticeCategoryModelById(){

        NoticeNoticeCategoryModel noticeNoticeCategoryModel = noticeFacade.findNoticeNoticeCategoryModelById("29z9RqE7RsLQYVR8EXCiYU");
        if (noticeNoticeCategoryModel!=null){
        	assert true:"正确";
        	System.out.println(noticeNoticeCategoryModel.getId());
            System.out.println(noticeNoticeCategoryModel);
        }else {
            assert false:"null";
        }
    }

    /**
     *@author hgt
     *create:2017-11-26 16:14:54
     *DESCRIPTION:删除通知信息-郝贵婷-2017年11月27
     */
    @Test
    public  void deleteNotice(){
        int resultCount;
        resultCount = noticeFacade.deleteNotice("VawnYnw6ATzqPjV87K4biS","haoguiting");
        if (resultCount!=0){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }


    /**
     *@author hgt
     *create:2017-11-26 16:14:54
     *DESCRIPTION:更新通知信息-郝贵婷-2017年11月27
     */
    @Test
    public  void updateNotice(){
        NoticeEntity notice = new NoticeEntity();
        notice.setId("VawnYnw6ATzqPjV87K4biS");
        notice.setTitle("测试更新-2018年1月9日");
        notice.setOperator("hgt");
        int resultCount;
        resultCount= noticeFacade.updateNotice(notice);
        if (resultCount!=0){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     *@author hgt
     *create:2017-11-26 16:14:54
     *DESCRIPTION:添加通知信息-郝贵婷-2017年12月6日
     */
    @Test
    public  void addNotice(){

        NoticeBrowseModel noticeBrowseModel = new NoticeBrowseModel();
//        noticeBrowseModel.setId(UUID.randomUUID().toString().substring(0,16));
        noticeBrowseModel.setTitle("测试添加1.9");
        noticeBrowseModel.setColumnId("1144fbbe28c254673bd37a");

        List<BrowseEntity> browserPerson = new ArrayList<>();
        BrowseEntity browser = new BrowseEntity();
        browser.setBrowsePersonId("19191919191");
        browser.setIsBrowse("0");
        browserPerson.add(browser);

        browser = new BrowseEntity();
        browser.setBrowsePersonId("91919191919");
        browser.setIsBrowse("0");
        browserPerson.add(browser);

        noticeBrowseModel.setBrowsePersons(browserPerson);
        int result = noticeFacade.addNotice(noticeBrowseModel);
        if (result>0){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     *@author
     *create:2017-11-26 16:14:54
     *DESCRIPTION:根据分类ID查询通知-郝贵婷-2017年12月2日
     */
    @Test
    public void selectNoticeNoticeCategoryByColumnId(){
        List<NoticeNoticeCategoryModel> noticeNoticeCategoryModelList;
        noticeNoticeCategoryModelList = noticeFacade.selectNoticeNoticeCategoryByColumnId("1144fbbe28c254673bd37a");
        if (noticeNoticeCategoryModelList!=null){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }


    /**
     *@author hgt
     *create:2017-11-26 16:14:54
     *DESCRIPTION:查询全部通知信息-郝贵婷-2017年11月27日
     */
    @Test
    public  void selectNoticeNoticeCategory(){
        List<NoticeNoticeCategoryModel> noticeNoticeCategoryModel;
        noticeNoticeCategoryModel = noticeFacade.selectNoticeNoticeCategory();
        if (noticeNoticeCategoryModel!=null){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     * 根据通知分类统计通知数量-郝贵婷-2018年1月9日
     * @param
     * @return
     */
    @Test
    public void  selectNoticeCountByColumnId(){
        int resultCount;
        resultCount = noticeFacade.selectNoticeCountByColumnId("1144fbbe28c254673bd37a");
        if (resultCount!=0){
            assert true:"正确";
            System.out.print(resultCount);
        }else {
            assert false:"null";
        }
    }
}
