package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.graduate.entity.ext.CompanyModel;
import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import org.junit.Test;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import testUtil.CommonFacade;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class TestCompanyFacade extends CommonFacade {

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");



    /**
     * 添加公司相关信息-2017年11月25日
     */
    @Test
    public void addCompany() throws ParseException {
        List<CompanyModel> companyModelList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        CompanyModel companyModel = new CompanyModel();
        //companyModel.setCompanyId("005");
        companyModel.setCompanyName("水立方");
        companyModel.setCompanyAddress("成都");
        companyModel.setUserId("004");
        companyModel.setEntryTime(sdf.parse("2016-05-04"));
        companyModel.setQuitTime(sdf.parse("2018-05-04"));
        companyModelList.add(companyModel);
        int count = companyFacade.addCompany(companyModelList);
        if (count > 0) {
            assert true ;
        } else {
            assert false : "查询公司相关信息失败";
        }
    }

    /**
     * 删除公司-2017年11月25日
     */
    @Test
    public void deleteCompany() {
        String userId = "001";
        String companyId = "4ZcxguardwQwwdBYdpXQB5";
        String operatorId = "sxm";

        int count = companyFacade.deleteCompany(companyId, userId, operatorId);
        if (count > 0) {
            assert true ;
        } else {
            assert false : "查询公司相关信息失败";
        }
    }

    /**
     * 修改公司相关信息-2017年11月25日
     */
    @Test
    public void updateCompany() throws ParseException {
        CompanyModel companyModel = new CompanyModel();
        companyModel.setCompanyAddress("成都1");
        companyModel.setCompanyName("水立方1");
        companyModel.setCompanyId("4ZcxguardwQwwdBYdpXQB5");
        companyModel.setUserId("004");
        companyModel.setEntryTime(sdf.parse("2016-05-04"));
        companyModel.setQuitTime(sdf.parse("2019-05-04"));

        int count = companyFacade.updateCompany(companyModel);
        if (count > 0) {
            assert true ;
        } else {
            assert false : "查询公司相关信息失败";
        }
    }

    /**
     * 查询公司相关信息-2017年11月25日
     */
    @Test
    public void selectCompanyByUserId() {
        String userId = "001";
        CompanyModel companyModel = companyFacade.selectCompanyByUserId(userId);
        List<CompanyModel> companyModelList = new ArrayList<>();
        companyModelList.add(companyModel);
        if (!StringUtils.isEmpty(companyModel.getCompanyName())) {
            assert true ;
        } else {
            assert false : "查询公司相关信息失败";
        }
    }

    /**
     * 查询学员工作经历和薪资-宋学孟-2017年12月4日
     */
    @Test
    public void selectCompanySalaryByUserId() {

        String userId = "001";
        List<CompanySalaryModel> companySalaryModelList = companyFacade.selectCompanySalaryByUserId(userId);
        if (!CollectionUtils.isEmpty(companySalaryModelList)) {
            assert true;
        } else {
            assert false;
        }
    }


}
