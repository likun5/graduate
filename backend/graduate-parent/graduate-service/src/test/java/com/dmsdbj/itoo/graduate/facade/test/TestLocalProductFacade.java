package com.dmsdbj.itoo.graduate.facade.test;

import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.LocalProductEntity;
import java.util.ArrayList;
import java.util.List;

public class TestLocalProductFacade extends CommonFacade {

    /**
	 *@author sxm
	 *create:2017-11-16 14:31:07
     *DESCRIPTION:分页查询测试
     */
    @Test
    public  void findById(){

        LocalProductEntity localProductEntity = localProductFacade.findById("0044fbbe28c254673bc37a");
        if (localProductEntity!=null){
        	assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    @Test
    public void queryLocalProduct(){
        List<LocalProductEntity> localProductEntity = localProductFacade.queryLocalProduct();
        System.out.println(localProductEntity);
    }

    @Test
    public void addLocalProduct(){

        LocalProductEntity localProductEntity = new  LocalProductEntity();
        localProductEntity.setId("0044fbbe28c254673bc37d");
        localProductEntity.setUserId("0044fbbe28c254673bc37a");
        localProductEntity.setName("焖子");
        localProductEntity.setRemark("王大雷故乡特产");

        System.out.println("添加场所信息Right：" + localProductFacade.addLocalProduct(localProductEntity));
    }

    @Test
    public void testDeleteRoom_Right() {
        List <String> list = new ArrayList<>();
        String productId = "0044fbbe28c254673bc37a";
        list.add(productId);
        System.out.println("删除场所信息Right" + localProductFacade.deleteLocalProduct(list));
    }

    @Test
    public void testUpdateBuild_Right() {
        LocalProductEntity localProductEntity = new LocalProductEntity();
        localProductEntity.setId("0044fbbe28c254673bc37a");
        localProductEntity.setUserId("0044fbbe28c254673bc37a");
        localProductEntity.setDescription("孔唯妍");
        localProductEntity.setName("王大雷");
        localProductEntity.setRemark("我叫王大雷");
        System.out.println("修特产所信息Right" + localProductFacade.updateLocalProduct(localProductEntity));
    }

}
