package com.dmsdbj.itoo.graduate.facade.test;

import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.AdministrativeRegionEntity;


public class TestAdministrativeRegionFacade extends CommonFacade {

    /**
	 *@author : 李爽
	 *create : 2017-12-05 10:55:09
     *DESCRIPTION :分页查询测试
     */
    @Test
    public  void findById(){

        AdministrativeRegionEntity administrativeRegionEntity = administrativeRegionFacade.findById("0044fbbe28c254673bc37a");
        if (administrativeRegionEntity!=null){
        	assert true:"正确";
        }else {
            assert false:"null";
        }
    }


}
