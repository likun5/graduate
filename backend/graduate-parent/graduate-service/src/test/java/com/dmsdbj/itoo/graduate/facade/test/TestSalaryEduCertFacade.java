package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.graduate.entity.ext.PeriodSalaryModel;
import org.junit.Test;
import org.springframework.util.CollectionUtils;
import testUtil.CommonFacade;

import java.util.List;

public class TestSalaryEduCertFacade extends CommonFacade {

    /**
     * 根据期数ID查询每年薪资统计信息 郑晓东 2018年1月20日
     */
    @Test
    public void selectAMMSalaryByPeriodId(){
        List<PeriodSalaryModel> list = salaryEduCertFacade.selectAMMSalaryByPeriodId("0044f1b128c234673bc32a");
        for (PeriodSalaryModel entity:list){
            System.out.print(entity.getMaxSalary()+";"+entity.getMinSalary()+";"+entity.getAveSalary()+";"+entity.getSalaryYear());
            System.out.println();
        }
        assert !CollectionUtils.isEmpty(list):"此内容显示证明未查询到数据";
    }

    /**
     * 查询每期今年薪资统计信息 郑晓东 2018年1月20日
     */
    @Test
    public void selectAMMSalaryInThisYear(){
        List<PeriodSalaryModel> list = salaryEduCertFacade.selectAMMSalaryInThisYear();
        for (PeriodSalaryModel entity:list){
            System.out.print(entity.getMaxSalary()+";"+entity.getMinSalary()+";"+entity.getAveSalary()+";"+entity.getSalaryYear());
            System.out.println();
        }
        assert !CollectionUtils.isEmpty(list):"此内容显示证明未查询到数据";
    }

    /**
     * 查询每期毕业时薪资统计信息 郑晓东 2018年1月20日
     */
    @Test
    public void selectAMMSalaryInGradeYear(){
        List<PeriodSalaryModel> list = salaryEduCertFacade.selectAMMSalaryInGradeYear();
        for (PeriodSalaryModel entity:list){
            System.out.print(entity.getMaxSalary()+";"+entity.getMinSalary()+";"+entity.getAveSalary()+";"+entity.getSalaryYear());
            System.out.println();
        }
        assert !CollectionUtils.isEmpty(list):"此内容显示证明未查询到数据";
    }
}
