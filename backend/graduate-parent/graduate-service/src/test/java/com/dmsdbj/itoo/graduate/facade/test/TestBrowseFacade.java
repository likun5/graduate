package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.PeriodPersonModel;
import com.dmsdbj.itoo.graduate.entity.ext.PersonInfoModel;
import com.dmsdbj.itoo.graduate.tool.DictionaryTypeCode;
import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.BrowseEntity;

import java.util.List;


public class TestBrowseFacade extends CommonFacade {

    /**
	 *@author hgt
	 *create:2017-11-26 16:14:54
     *DESCRIPTION:分页查询测试
     */
    @Test
    public  void findById(){

        BrowseEntity browseEntity = browseFacade.findById("0044fbbe28c254673bc37a");
        if (browseEntity!=null){
        	assert true:"正确";
        }else {
            //assert false:"null";
        }
    }


    /**
     *@author hgt
     *create:2017-12-2 17:21:23
     *DESCRIPTION:根据通知ID、是否浏览查询浏览人信息
     */
    @Test
    public  void selectBrowseByNoticeId(){
        List<PersonInfoModel> personalInfoEntityList;
        personalInfoEntityList = browseFacade.selectBrowseByNoticeId("0044fbbe28c254673bc37a","1");
        if (personalInfoEntityList!=null){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }


    /**
     *@author 更新浏览记录-郝贵婷-2017年12月3日
     *create:2017-11-26 16:14:54
     *DESCRIPTION: 将未浏览记录更新为浏览记录
     */
    @Test
    public  void updateBrowseIsBrowse(){
        int resultCount;
        resultCount = browseFacade.updateBrowseIsBrowse("VawnYnw6ATzqPjV87K4biS","A1csJpwUpKb2xP6M4e9WLJ");
        if (resultCount!=0){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     *@author 根据浏览人ID查询未浏览记录-郝贵婷-2017年12月3日
     *create:2017-11-26 16:14:54
     *DESCRIPTION:
     */
    @Test
    public  void selectNoBrowseByBrowsePersonId(){
        int resultCount;
        resultCount = browseFacade.selectNoBrowseByBrowsePersonId("A1csJpwUpKb2xP6M4e9WLJ");
        if (resultCount!=0){
            assert true:"正确";
            System.out.println(resultCount);
        }else {
            assert false:"null";
        }
    }

    /**
     *@author 根据浏览人ID查询已浏览记录-郝贵婷-2017年12月3日
     *create:2017-11-26 16:14:54
     *DESCRIPTION:
     */
    @Test
    public  void selectBrowsedByBrowsePersonId(){
        int resultCount;
        resultCount = browseFacade.selectBrowsedByBrowsePersonId("A1csJpwUpKb2xP6M4e9WLJ");
        if (resultCount!=0){
            assert true:"正确";
            System.out.println(resultCount);
        }else {
            assert false:"null";
        }
    }

    /**
     * 查询所有期数及对应期数人员 郝贵婷 -2018年1月14日
     * @return
     */
    @Test
    public  void findPeriodPersonModels(){
        List<PeriodPersonModel> periodPersonModelList;
        periodPersonModelList = browseFacade.findPeriodPersonModels();
        if (periodPersonModelList!=null){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     *@author 根据浏览人ID查询已浏览记录-郝贵婷-2017年12月3日
     *create:2017-11-26 16:14:54
     *DESCRIPTION:
     */
    @Test
    public  void selectAllBrowserByNoticeId(){
        List<PersonalInfoEntity> personalInfoEntityList;
        personalInfoEntityList = browseFacade.selectAllBrowserByNoticeId("0044fbbe28c254673bc37a");
        if (personalInfoEntityList!=null){
            assert true:"正确";
            //System.out.println(personalInfoEntityList);
        }else {
            assert false:"null";
        }
    }

    /**
     * 查询全部期数（构建树形结构）--hgt-2018-1-30
     */
    @Test
    public void selectAllGrade(){
        List<DictionaryEntity> dictionaryEntityList=browseFacade.selectAllGrade(DictionaryTypeCode.GRADE);
        if(dictionaryEntityList!=null){
            assert true:"正确";
        }else{
            assert true:"null";
        }
    }
}
