package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.graduate.entity.ext.TechnologyModel;
import com.dmsdbj.itoo.tool.uuid.BaseUuidUtils;
import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.SkillPointEntity;

import java.util.List;
import java.util.Map;


public class TestSkillPointFacade extends CommonFacade {

    /**
     * @author 徐玲博
     * create: 2017-11-21 21:41:19
     * DESCRIPTION: 分页查询测试
     */
    @Test
    public void findById() {

        SkillPointEntity skillPointEntity = skillPointFacade.findById("0044fbbe28c254673bc37a");
        if (skillPointEntity != null) {
            assert true : "正确";
        } else {
            assert false : "null";
        }
    }

    /**
     * 添加技术点或技术方向-徐玲博-2017-11-20 15:13:47
     */
    @Test
    public void addSkillPoint() {
        SkillPointEntity skillPointEntity = new SkillPointEntity();
        skillPointEntity.setOperatorId("123321hao");
        skillPointEntity.setOperator("haoguiting");
        skillPointEntity.setId(BaseUuidUtils.base58Uuid());
        skillPointEntity.setPId("VNnvmb3G4DRQqEcvqs91B2");
        skillPointEntity.setTechName("测试添加-hgt");//技术点名称
//        skillPointEntity.setUserId("SdHAYGfnaGYJa68TenWzPm");
        skillPointEntity.setRemark("");
        if (skillPointFacade.addSkillPoint(skillPointEntity) > 0) {
            assert true : "正确";
        } else {
            assert false : "null";
        }

    }

    /**
     * 修改技术点或技术方向-徐玲博-2017-11-20 16:54:02
     */
    @Test
    public void updateSkillPoint() {
        SkillPointEntity skillPointEntity = new SkillPointEntity();
//        skillPointEntity.setCompanyId("0044fbbe28c254673bc37a");
        skillPointEntity.setOperatorId("123haohao");
        skillPointEntity.setOperator("123haohao");
        skillPointEntity.setId("123321Z4c6v8nLtj3cs123");
        skillPointEntity.setPId("VNnvmb3G4DRQqEcvqs91B2");
        skillPointEntity.setTechName("test技术点-修改3");//技术点名称
//        skillPointEntity.setUserId("SdHAYGfnaGYJa68TenWzPm");
        skillPointEntity.setRemark("技术点-修改测试-hgt");
        if (skillPointFacade.updateSkillPoint(skillPointEntity) > 0) {
            assert true : "正确";
        } else {
            assert false : "null";
        }

    }

    /**
     * 查询技术点-或技术方向-徐玲博-2017-11-20 16:54:36
     */
    @Test
    public void selectSkillPointById() {
        String Id = "7BM7mPgawFgDZtisifi91a";
        if (skillPointFacade.selectSkillPointById(Id) != null) {
            assert true : "正确";
        } else {
            assert false : "null";
        }
    }

    /**
     * 查询技术方向-徐玲博-2017-11-21 20:36:52
     * rq
     */
    @Test
    public void selectParentSkillByPid() {
        String p_id = "7BM7mPgawFgDZtisifi91a";
        if (skillPointFacade.selectParentSkillByPid(p_id) != null) {
            assert true : "正确";
        } else {
            assert false : "断言失败";
        }
    }

    @Test
    public void selectSkillPoint() {
        List<Map<String, List<SkillPointEntity>>> mapArrayList =skillPointFacade.selectSkillPoint();
        if(mapArrayList!=null){
            assert true : "正确";
        } else {
            assert false : "断言失败";
        }

        System.out.println(mapArrayList);
    }

    /**
     * 查询所有的技术方向实体-徐玲博-2018-1-13 19:15:10
     */
    @Test
    public void selectTechnicalDirection(){
        List<SkillPointEntity> skillPointEntityList=skillPointFacade.selectTechnicalDirection();
        assert !skillPointEntityList.isEmpty() :"查询成功";
        System.out.println(skillPointEntityList);
    }

    /**
     * [接口]查询所有的技术点（包括技术方向）-徐玲博-2018-1-13 23:11:00
     */
    @Test
    public void selectAllTechnology(){
        List<TechnologyModel> technologyModelList=skillPointFacade.selectAllTechnology();
        assert !technologyModelList.isEmpty() :"查询技术成功";
        System.out.println(technologyModelList);
    }
}
