package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.graduate.entity.HomeInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.HomeInfoModel;
import com.dmsdbj.itoo.tool.uuid.BaseUuidUtils;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import testUtil.CommonFacade;

import java.text.ParseException;
import java.text.SimpleDateFormat;


public class TestHomeInfoFacade extends CommonFacade {
    /**
     * 添加家庭成员-徐玲博-2017-11-20 11:03:07
     * @throws ParseException 解析异常
     */
    @Test
    public void addHomeInfo() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        HomeInfoEntity hie = new HomeInfoEntity();
        hie.setEmail("无");
        hie.setFamilyMemName("小花");
        hie.setOperatorId("SdHAYGfnaGYJa68TenWz");
        hie.setOperator("徐玲博");
        hie.setPhone("18723511236");
        hie.setQq("7894652336");
        hie.setRelationshipId("关系");//与当前用户关系
        hie.setUserId("SdHAYGfnaGYJa68TenWzPm");
        hie.setWechat("_weixinhao23");
        hie.setWork("白富美");
        hie.setId(BaseUuidUtils.base58Uuid());
        hie.setRemark("修改家庭成员信息-徐玲博测试3");
        if (homeInfoFacade.addHomeInfo(hie) > 0) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }

    }

    /**
     * 查询家庭信息列表-徐玲博-2017-11-20 11:04:24
     */
    @Test
    public void selecetHomeInfo() {
        String userId = "SdHAYGfnaGYJa68TenWzPm";
        int page=1;
        int pageSize=2;
        PageInfo<HomeInfoModel> homeInfoModelPageInfo=homeInfoFacade.selectHomePersonInfo(userId,page,pageSize);
        if ( homeInfoModelPageInfo!= null) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }
        System.out.println(homeInfoModelPageInfo);
    }

    /**
     * 修改家庭成员-徐玲博-2017-11-20 11:06:56
     * @throws ParseException 解析异常
     */
    @Test
    public void updateHomeInfo() throws ParseException {
        HomeInfoEntity hie = new HomeInfoEntity();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        hie.setEmail("无");
        hie.setFamilyMemName("徐爸");
        hie.setOperatorId("SdHAYGfnaGYJa68TenWz");
        hie.setOperator("徐玲博");
        hie.setPhone("18746511236");
        hie.setQq("7894651236");
        hie.setRelationshipId("父女");//与当前用户关系
        hie.setTimestampTime(sdf.parse("2017-11-20"));
        hie.setUserId("SdHAYGfnaGYJa68TenWzPm");
        hie.setWechat("_weixinhao");
        hie.setWork("高富帅");
        hie.setId("98N61uRR2zN2Zf2SXVKRVW");
        hie.setRemark("修改家庭成员信息-徐玲博测试2");
        if (homeInfoFacade.updateHomeInfo(hie) > 0) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }
    }

    /**
     * 删除家庭成员信息-徐玲博-2017-11-20 11:03:48
     */
    @Test
    public void deleteHomeInfo() {
        String Id="98N61uRR2zN2Zf2SXVKRVW";
        String operator="xlb";
        if(homeInfoFacade.deleteHomeInfo(Id,operator)>0){
            assert true : "正确";
        } else {
            assert false : "失败";
        }
    }

}
