package com.dmsdbj.itoo.graduate.facade.test;

import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.NoticeCategoryEntity;

import java.util.List;


public class TestNoticeCategoryFacade extends CommonFacade {

    /**
	 *@author hgt
	 *create:2017-11-26 16:14:54
     *DESCRIPTION:根分类ID查询通知分类-郝贵婷-2017年11月27日
     */
    @Test
    public  void findById(){

        NoticeCategoryEntity noticeCategoryEntity = noticeCategoryFacade.findById("2233fb11fbc254673bd37a");
        if (noticeCategoryEntity!=null){
        	assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     *@author hgt
     *create:2017-11-26 16:14:54
     *DESCRIPTION:查询通知分类信息-郝贵婷-2017年11月27日
     */
    @Test
    public  void selectNoticeCategoryInfo(){
        List<NoticeCategoryEntity> noticeCategoryEntityList;
        noticeCategoryEntityList= noticeCategoryFacade.selectNoticeCategoryInfo();
        if (noticeCategoryEntityList!=null){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     *@author hgt
     *create:2017-11-26 16:14:54
     *DESCRIPTION:添加通知分类信息-郝贵婷-2017年11月27日
     */
    @Test
    public  void addNoticeCategory(){
        NoticeCategoryEntity noticeCategory = new NoticeCategoryEntity();
        noticeCategory.setColumnName("11测试添加-人工智能");
        noticeCategory.setOperator("hgt");
        int resultCount;
        resultCount = noticeCategoryFacade.addNoticeCategory(noticeCategory);
        if (resultCount!=0){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     *@author hgt
     *create:2017-11-26 16:14:54
     *DESCRIPTION:修改公告分类是信息-郝贵婷-2017年11月27
     */
    @Test
    public  void updateNoticeCategory(){
        //NoticeCategoryEntity noticeCategory = noticeCategoryFacade.findById("0044fbbe28c254673bc37a");
        NoticeCategoryEntity noticeCategory = new NoticeCategoryEntity();
        noticeCategory.setId("0044fbbe28c254673bc37a");
        noticeCategory.setColumnName("测试-人工智能");
        noticeCategory.setOperator("hgt");
        int resultCount;
        resultCount = noticeCategoryFacade.updateNoticeCategory(noticeCategory);
        if (resultCount!=0){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }



    /**
     *@author hgt
     *create:2017-11-26 16:14:54
     *DESCRIPTION:删除通知分类信息-郝贵婷-2017年11月27
     */
    @Test
    public  void deleteNoticeCategory(){
        int resultCount;
        resultCount= noticeCategoryFacade.deleteNoticeCategory("2QNK47Fxzobz2gTv5tBEtn","haoguiting");
        if (resultCount!=0){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }
}

