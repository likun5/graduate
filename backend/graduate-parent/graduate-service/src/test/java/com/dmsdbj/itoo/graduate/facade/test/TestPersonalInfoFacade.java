package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.graduate.entity.ext.*;
import com.dmsdbj.itoo.tool.uuid.BaseUuidUtils;
import com.github.pagehelper.PageInfo;
import net.sf.json.JSONObject;
import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TestPersonalInfoFacade extends CommonFacade {
    @Test
    public void findByUserId() {
        String id = "YtH67q6ykRmfH5F6Re2Fb";
        PersonalInfoEntity personalInfoEntity = personalInfoFacade.findByUserId(id);
        System.out.println(personalInfoEntity + "\n");

//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//        String bir=sdf.format(personalInfoEntity.getBirthday());
//        System.out.println(bir+"\n");
//
//        Date data = java.sql.Date.valueOf(bir);
//        System.out.println(data+"\n");

    }

    /**
     * 毕业生-根据毕业生用户Id-添加个人信息-徐玲博-2017-11-19 11:28:07
     *
     * @throws ParseException 解析异常
     */
    @Test
    public void addPersonInfo() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        PersonalInfoEntity pie = new PersonalInfoEntity();
        pie.setAddress("河北省廊坊市广阳区爱民西道100号");
//        pie.setBirthday("2017-11-11");
        pie.setEmail("dmsd_hzw@163.com");
        pie.setEmergName("米老师");
        pie.setEmergPhone("18888888888");
        pie.setEmergRelation("师生");
//        pie.setEnterCollegeTime(sdf.parse("2017-11-12"));
        pie.setEnglishName("Fiona");
//        pie.setEnterDmtTime(sdf.parse("2017-11-13"));
        pie.setGrade("10");
//        pie.setGraduateCollegeTime(sdf.parse("2017-11-14"));
//        pie.setGraduateDmtTime(sdf.parse("2017-11-15"));
        pie.setLoginId("171119888");
        pie.setName("小博");
        pie.setNativePlace("河北省廊坊市广阳区爱民西道100号");
        pie.setOperatorId("");
        pie.setPhone("18766666666");
        pie.setProfessionalField("活泼开朗，热情大方，美丽动人，人见人爱，花见花开");
        pie.setSex("女");
        pie.setQq("123456789");
        pie.setWechat("_123");
        pie.setId(BaseUuidUtils.base58Uuid());
        pie.setRemark("addPersonInfoTest-单元测试-xlb-2017-11-19");
        if (personalInfoFacade.addPersonInfo(pie) > 0) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }
    }


    /**
     * 根据登录ID查询用户信息-徐玲博-2017-11-19 11:32:48
     */
    @Test
    public void selectPersonByLoginId() {
        String loginId = "171119666";

        if (personalInfoFacade.selectPersonByLoginId(loginId) != null) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }

    }

    /**
     * 毕业生-根据毕业生 用户Id-修改个人信息-徐玲博-2017-11-19 11:28:07
     *
     * @throws ParseException 解析异常
     */
    @Test
    public void updatePersonInfo() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        PersonInfoModel pie = new PersonInfoModel();
        pie.setAddress("河北省廊坊市广阳区爱民西道100号");
//        pie.setBirthday(sdf.parse("2017-10-11"));
        pie.setEmail("dmsd_hzw@163.com");
        pie.setEmergName("米老师");
        pie.setEmergPhone("18888888888");
        pie.setEmergRelation("师生");
//        pie.setEnterCollegeTime(sdf.parse("2017-10-12"));
        pie.setEnglishName("Fiona");
//        pie.setEnterDmtTime(sdf.parse("2017-10-13"));
        pie.setGrade("10");
//        pie.setGraduateCollegeTime(sdf.parse("2017-10-14"));
//        pie.setGraduateDmtTime(sdf.parse("2017-10-15"));
        pie.setLoginId("171119888");
        pie.setName("xlb");
        pie.setNativePlace("河北省廊坊市广阳区爱民西道100号");
        pie.setOperatorId("");
        pie.setPhone("18766666661");
        pie.setProfessionalField("玉面小飞龙");
        pie.setSex("女");
        pie.setQq("123456789");
        pie.setWechat("_456");
        pie.setId("1234567898");
        pie.setRemark("UpdatePersonInfo-单元测试-xlb-2017-11-19");
        pie.setPictureUrl("http://192.168.22.64/group1/M00/00/01/wKgWQFp5UIKAEmj5AAAmoQw-pac549.jpg");
        if (personalInfoFacade.updatePersonInfo(pie) > 0) {
            assert true : "正确";
        } else {
            assert false : "错误";
        }
    }

    /**
     * 毕业生-根据用户Id-删除个人信息-徐玲博-2017-11-19 11:43:50
     */
    @Test
    public void deletePersonInfo() {
        String Id = "QMm7ArfxYWQrdH9r5ERv2m";
        String operator = "徐玲博";
        if (personalInfoFacade.deletePersonInfo(Id, operator)) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }
    }

    /**
     * 地域-根据毕业生姓名查询个人信息-徐玲博-2017-11-30 20:07:18
     */
    @Test
    public void selectPersonByName() {
        String name = "小玲";
        if (personalInfoFacade.selectPersonByName(name) != null) {
            assert true : "正确";
        } else {
            assert false : "失败";

        }
    }

    /**
     * 通知-查询毕业生信息-徐玲博-2017-12-6 11:55:36
     *
     * @return 毕业生信息列表
     */
    @Test
    public void selectPersonInfo() {
        List<PersonGradeModel> personGradeModelList = personalInfoFacade.selectPersonInfo();
        System.out.println(personGradeModelList + "\n");
        if (personGradeModelList != null) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }
    }


    @Test
    public void selectPersonInfoByRegionGradeNameSex() {
        PersonInfoParamsModel personInfoParamsModel = new PersonInfoParamsModel();
        personInfoParamsModel.setCityId("");
        personInfoParamsModel.setCountryId("43302"); //区县id
        personInfoParamsModel.setName("");
        personInfoParamsModel.setProvinceId("");
        personInfoParamsModel.setRegionId("0044fbbe28c2546731c32a"); //地域类型id
        personInfoParamsModel.setSex("女");
        personInfoParamsModel.setTermId("0044fbb121c234673bc32a"); //期数id

        PageInfo<PersonInfoModel> pageInfo = personalInfoFacade.selectPersonInfoByRegionGradeNameSex(personInfoParamsModel, 1, 5);
        if (pageInfo != null) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }

    }

    /**
     * 组合查询+模糊查询+分页查询 毕业生信息查询-徐玲博-2
     */
    @Test
    public void selectPersonByCombination() {
        String name = "";
        String grade = "";
        String company = "";
        String education = "";
        String salaryRange = "";
        int page = 1;
        int pageSize = 10;

        PageInfo<PersonManageModel> personManageModelPageInfo = personalInfoFacade.selectPersonByCombination(name, company, grade, education, salaryRange, page, pageSize);
        if (personManageModelPageInfo != null) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }

        System.out.println(personManageModelPageInfo);
    }

    /**
     * 循环产生uuid-徐玲博-2018-1-9 19:46:54
     */
    @Test
    public void generateUUID() {
        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String uuid = BaseUuidUtils.base58Uuid();
            stringList.add(uuid + "\n");

        }
        System.out.println(stringList);
    }

    /**
     * 生成随机数-徐玲博-2018-1-9 19:48:38
     */
    @Test
    public void generateRandom() {
        Random random = new Random(16);
        for (int i = 0; i < 10; i++) {
            System.out.println(random.nextInt() + "\n");
        }
    }

    /**
     * 测试接口方法 查询所有期及对应人员信息 personalInfoFacade.findPeriodPersonModels  郑晓东
     */
    @Test
    public void findPeriodPersonModels() {
//        List<PeriodPersonModel> list = personalInfoFacade.findPeriodPersonModels();
        List<PeriodPersonModel> periodPersonModelList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            PeriodPersonModel periodPersonModel = new PeriodPersonModel();
            periodPersonModel.setPeriodId("periodId" + i);
            periodPersonModel.setPeriodName("PeriodName" + i);
            for (int j = 0; j < 2; j++) {
                PersonalInfoEntity personalInfoEntity = new PersonalInfoEntity();
                personalInfoEntity.setId("personid" + i + j);
                personalInfoEntity.setName("personname" + i + j);
                periodPersonModel.getPersonalInfoList().add(personalInfoEntity);
            }
            periodPersonModelList.add(periodPersonModel);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("123", periodPersonModelList);
        System.out.println(jsonObject);
//        assert !CollectionUtils.isEmpty(list):"此信息出现证明方法相同，但无数据返回！";
    }

    public static void main(String[] args) {
        List<PeriodPersonModel> periodPersonModelList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            PeriodPersonModel periodPersonModel = new PeriodPersonModel();
            periodPersonModel.setPeriodId("periodId" + i);
            periodPersonModel.setPeriodName("PeriodName" + i);
            List<PersonalInfoEntity> list = new ArrayList<>();
            for (int j = 0; j < 2; j++) {
                PersonalInfoEntity personalInfoEntity = new PersonalInfoEntity();
                personalInfoEntity.setId("personid" + i + j);
                personalInfoEntity.setName("personname" + i + j);
                list.add(personalInfoEntity);
            }
            periodPersonModel.setPersonalInfoList(list);
            periodPersonModelList.add(periodPersonModel);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", periodPersonModelList);
        System.out.println(jsonObject);
    }

    @Test
    public void covertToDate() {
        String stringDate = "Thu Oct 16 07:13:48 GMT 2014";
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM ddHH:mm:ss 'GMT' yyyy", Locale.US);
//        Date date =sdf.parse(stringDate);
//     System.out.println(date.toString());
        sdf = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
//        System.out.println(sdf.format(date));
    }

    @Test
    public void selectPersonRegin() {
        String name = null;
        String sex = null;
        String grade = null;
        String regionName = null;
        String dictionaryAddressId=null;
        List<PersonReginModel> personReginModelList = personalInfoFacade.selectPersonRegion(name, sex, regionName, grade,dictionaryAddressId);
        if (personReginModelList != null) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }
        System.out.println("\n"+"地域组合查询" + personReginModelList + "\n");
    }

    /**
     * 测试插入用户（注册）-袁甜梦-2018年3月11日16:30:41
     */
    @Test
    public void testInsertUser(){
        UserModel userModel =new UserModel();
        userModel.setUserRealName("徐凌波");
        userModel.setUserCode("1");
        userModel.setPassword("1");
       boolean bool= personalInfoFacade.insertUser(userModel);
        if (bool) {
            assert true : "正确";
        } else {
            assert false : "失败";
        }



    }
}

