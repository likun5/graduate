package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.graduate.entity.ext.RegionCommercialModel;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.CommercialOppotunityEntity;

import java.util.List;


public class TestCommercialOppotunityFacade extends CommonFacade {

    /**
	 *@author sxm
	 *create:2017-11-16 14:31:07
     *DESCRIPTION:分页查询测试
     */
    @Test
    public  void findById(){

        CommercialOppotunityEntity commercialOppotunityEntity = commercialOppotunityFacade.findById("0044fbbe28c254673bc37a");
        if (commercialOppotunityEntity!=null){
        	assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    @Test
    public void  queryRegionCommercialById(){
        int pageNum=1;
        int pageSize=20;
        String regionId="55";

        PageInfo<RegionCommercialModel> regionCommercialModelPageInfo = commercialOppotunityFacade.queryRegionCommercialById(regionId,pageNum,pageSize);
        assert regionCommercialModelPageInfo == null || true :"查询成功！";

    }

    @Test
    public void  queryAllComercial(){
        List<RegionCommercialModel> regionCommercialModelList=commercialOppotunityFacade.queryRegionCommercialById();
        assert regionCommercialModelList == null || regionCommercialModelList.size() <= 0 || true :"查询成功！";
    }

}
