package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
import com.dmsdbj.itoo.graduate.entity.ext.DictionaryTreeModel;
import org.junit.Test;
import testUtil.CommonFacade;

import java.util.List;


public class TestDictionaryFacade extends CommonFacade {

    /**
	 *@author sxm
	 *create:2017-11-16 14:31:07
     *DESCRIPTION:分页查询测试
     */
    @Test
    public  void findById(){

        DictionaryEntity dictionaryEntity = dictionaryFacade.findById("0044fbbe28c254673bc37a");
        if (dictionaryEntity!=null){
        	assert true:"正确";
        }else {
            assert false:"null";
        }
    }
    @Test
    public  void findTree(){

        List<DictionaryTreeModel> dictionaryEntity = dictionaryFacade.selectAllDictionaryTypeByTree();
        if (dictionaryEntity!=null){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }
    @Test
    public  void fuzzyDictionaryInfoByName(){

        List<DictionaryEntity> dictionaryEntity = dictionaryFacade.fuzzyDictionaryInfoByName("姐","关系");
        if (dictionaryEntity!=null){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     *  根据typeCode查询字典信息-袁甜梦-2018年3月5日21:03:38
     */
    @Test
    public void testSelectDictionaryByTypeCode(){
        List<DictionaryEntity> dictionaryEntity=dictionaryFacade.selectDitNameByTypeCode("GRADE");
        if (dictionaryEntity!=null){
            for (DictionaryEntity list:dictionaryEntity
                 ) {
                System.out.println(list.getCreateTime());

            }
            System.out.println(

            );
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

    /**
     * 单条添加字典表信息-袁甜梦-2018年3月6日10:10:24
     */
    @Test
    public void testAddDictionaray(){
        DictionaryEntity dictionaryEntity=new DictionaryEntity();
        dictionaryEntity.setTypeCode("GRADE");
        dictionaryEntity.setDictTypeName("期数");
        dictionaryEntity.setOperatorId("002");
        dictionaryEntity.setOperator("郝贵婷");
        dictionaryEntity.setDictName("17期");
        dictionaryEntity.setRemark("测试2018年3月6日");
        int flag=dictionaryFacade.addDictionary(dictionaryEntity);
        if (flag>0){
            System.out.println(flag);
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

}
