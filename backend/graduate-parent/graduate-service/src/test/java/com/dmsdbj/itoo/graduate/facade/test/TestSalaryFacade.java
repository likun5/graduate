package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.SalaryEntity;


public class TestSalaryFacade extends CommonFacade {

    /**
	 *@author sxm
	 *create:2017-11-26 16:14:54
     *DESCRIPTION:分页查询测试
     */
    @Test
    public  void findById(){

        SalaryEntity salaryEntity = salaryFacade.findById("0044fbbe28c254673bc37a");
        if (salaryEntity!=null){
        	assert true:"正确";
        }else {
            assert false:"null";
        }
    }
    @Test
    public  void findByd(){

        PageInfo<CompanySalaryModel>  list = salaryFacade.PageSelectSalaryByUserId("2ZB5fnyJEEax4aGiPKH57z",1,5);
        if (list!=null){
            assert true:"正确";
        }else {
            assert false:"null";
        }
    }

}
