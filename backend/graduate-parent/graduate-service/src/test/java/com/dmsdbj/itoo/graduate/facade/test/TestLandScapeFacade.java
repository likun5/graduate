package com.dmsdbj.itoo.graduate.facade.test;

import com.dmsdbj.itoo.tool.uuid.BaseUuidUtils;
import org.junit.Test;
import org.springframework.util.CollectionUtils;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.LandScapeEntity;

import java.util.ArrayList;
import java.util.List;


public class TestLandScapeFacade extends CommonFacade {

    /**
     * 根据风景id查询风景-徐玲博-2018-2-1 15:00:12
     */
    @Test
    public void findById() {
        String id = "881Swnj8gXJd4Wjw8aVmRq";
        LandScapeEntity landScapeEntity = landScapeFacade.findById(id);
        if (landScapeEntity != null) {
            assert true : "正确";
        } else {
            assert false : "null";
        }

        System.out.println(landScapeEntity);
    }

    /**
     * 查询风景-徐玲博-2018-2-1 15:01:00
     */
    @Test
    public void queryLandScape() {
        String id="YtH67q6ykRmfH5F6Re2Fb";
        List<LandScapeEntity> landScapeEntity = landScapeFacade.queryLandScape(id);
        System.out.println(landScapeEntity);
        if (CollectionUtils.isEmpty(landScapeEntity)) {
            assert false : "null";
        } else {
            assert true : "正确";
        }
    }

    /**
     * 更新风景-徐玲博-2018-2-1 15:06:43
     */
    @Test
    public void updateLandSpace() {

        String userId = "YbtBeNBMf78kQEji9nwEQP";
        String id="881Swnj8gXJd4Wjw8aVmRq";
        LandScapeEntity landScapeEntity = new LandScapeEntity();
        landScapeEntity.setId(id);
        landScapeEntity.setUserId(userId);
        landScapeEntity.setDescription("明清朝的政治中心");
        landScapeEntity.setName("北京故宫博物院");
        landScapeEntity.setRemark("徐玲博-2018-2-1-测试-勿动");
        System.out.println("修改景区信息Right" + landScapeFacade.updateLandSpace(landScapeEntity));
    }

    /**
     * 添加风景-徐玲博-2018-2-1 17:49:05
     */
    @Test
    public void addLandScape(){
        LandScapeEntity landScapeEntity=new LandScapeEntity();
        landScapeEntity.setId(BaseUuidUtils.base58Uuid());
        landScapeEntity.setUserId("YbtBeNBMf78kQEji9nwEQP");//当前登录人id
        landScapeEntity.setName("前门");
        landScapeEntity.setDescription("正阳门，俗称前门、前门楼子、大前门，原名丽正门");
        landScapeEntity.setRemark("徐玲博-测试数据-勿动");
        landScapeEntity.setOperator("徐玲博");
        int flag=landScapeFacade.addLandScape(landScapeEntity);
        if(flag>0){
            assert true:"成功";
        }else {
            assert false:"为空";
        }
    }

    /**
     * 删除风景-徐玲博-2018-2-1 17:54:02
     */
    @Test
    public void deleteLandScape(){


        List<LandScapeEntity> landScapeEntityList=new ArrayList<>();
        int flag=landScapeFacade.deleteLandSpace(landScapeEntityList);
        if(flag>0){
            assert  true:"正确！";
        }else {
            assert false:"没有删除成功！";
        }
    }

    /**
     * 根据ids批量删除-徐玲博-2018-2-8 11:17:24
     */
    @Test
    public void deleteLand(){
        List<String> ids=new ArrayList<>();
        String id="FZZXUmgefCE9MLf1vzWtdV";//删除前门
        ids.add(id);
        String operator="徐玲博删除的";
        int flag=landScapeFacade.deleteLand(ids,operator);
        System.out.println(flag);
    }
}
