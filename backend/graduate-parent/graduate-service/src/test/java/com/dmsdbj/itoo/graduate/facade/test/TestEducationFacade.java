package com.dmsdbj.itoo.graduate.facade.test;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.dmsdbj.itoo.graduate.entity.ext.PersonalEducationEntity;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import testUtil.CommonFacade;
import com.dmsdbj.itoo.graduate.entity.EducationEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class TestEducationFacade extends CommonFacade {

    /**
	 *@author sxm
	 *create:2017-11-16 14:31:07
     *DESCRIPTION:分页查询测试
     */
    @Test
    public  void findById(){

        EducationEntity educationEntity = educationFacade.findById("0044fbbe28c254673bc37a");
        if (educationEntity!=null){
        	assert true:"正确";
        }else {
            assert false:"null";
        }

    }

    @Test
    public void queryEducaitonInfo(){
        List<PersonalEducationEntity> personalEducationEntityList=educationFacade.queryPersonalEducation();
        assert !CollectionUtils.isNotEmpty(personalEducationEntityList) || true :"查询成功！";
    }
    SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd");
@Test
public void insertEducaitonInfo(){
    SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd");
    EducationEntity educationEntity = new  EducationEntity();
    educationEntity.setCertificateNo("xyxy");
    educationEntity.setCertificateType(111);
    educationEntity.setRemark("xyxy");
    educationEntity.setCertificateName("xyxy");
    try {
        educationEntity.setReceiveTime(sdf.parse("2018-02-25"));
    } catch (ParseException e) {
        e.printStackTrace();
    }
    int result = educationFacade.addEducationEntity(educationEntity);
    assert result <= 0 || true :"查询成功！";
}

@Test
    public void queryfindPersonAll(){
    PageInfo<EducationEntity> result=educationFacade.findPersonAll(1,5,"054");
    System.out.println(result);
}



}
