package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.ForumEntity;

/**
 *@author : ls
 *create: 2017-12-27 09:54:26.
 */
public interface ForumFacade {
      /**
      * @author kongwy
      * @param id
      * @return ForumEntity
      */
     ForumEntity findById(String id);
}
