package com.dmsdbj.itoo.graduate.facade;
import java.util.List;
import com.dmsdbj.itoo.graduate.entity.NoticeCategoryEntity;
import com.github.pagehelper.PageInfo;

/**
 *@author haoguiting
 *create: 2017-11-27 11:24:11
 */
public interface NoticeCategoryFacade {
    /**
     * 根分类ID查询通知分类-haoguiting-2017年11月27日
     * @param id 通知类别id
     * @return NoticeCategoryEntity通知类别实体
     */
    NoticeCategoryEntity findById(String id);

    /**
     * 查询全部通知分类-haoguiting-2017年11月27日
     * @return 通知分类list列表
     */
    List<NoticeCategoryEntity> selectNoticeCategoryInfo();

    /**
     * 添加公告分类信息-haoguiting-2017年11月27日
     * @param  noticeCategory 通知类别实体
     * @return 受影响行
     */
    int addNoticeCategory(NoticeCategoryEntity noticeCategory);

    /**
     * 修改通知分类是信息-haoguiting-2017年11月27
     * @param noticeCategory 通知分类实体
     * @return 受影响行
     */
    int updateNoticeCategory(NoticeCategoryEntity noticeCategory);


    /**
     * 删除通知分类信息-haoguiting-2017年11月27
     * @param noticeCatagoryID  通知分类ID
     * @param operator 操作人
     * @return 影响行数
     */
    int deleteNoticeCategory(String noticeCatagoryID,String operator);

    /**
     * 真分页-查询全部通知分类-haoguiting-2018-01-11 14:15:45
     * @param page 页码
     * @param pageSize 每页显示条数
     * @return 类别list
     */
    PageInfo<NoticeCategoryEntity> PageSelectNoticeCategoryInfo(int page, int pageSize);

    /**
     * 根据主键批量删除类别-haoguiting-2017年11月27
     * @param noticeCatagoryID  通知分类ID
     * @param operator 操作人
     * @return 影响行数
     */
    int deleteNoticeCategorys(String noticeCatagoryID,String operator);
}
