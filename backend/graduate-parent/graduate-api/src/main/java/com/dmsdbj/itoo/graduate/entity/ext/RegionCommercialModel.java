package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;

import java.io.Serializable;

/**
 * Created by tlf on 2018/1/11.
 * 地域商机综合实体！
 */
public class RegionCommercialModel extends BaseEntity implements Serializable{
    private String commercialName;
    private String commercialDes;
    private String regionName;
    private String persionName;
    private String persionPhone;
    private String commercialLat;
    private String commercialLng;

    public String getCommercialName() {
        return commercialName;
    }

    public void setCommercialName(String commercialName) {
        this.commercialName = commercialName;
    }

    public String getCommercialDes() {
        return commercialDes;
    }

    public void setCommercialDes(String commercialDes) {
        this.commercialDes = commercialDes;
    }

    public String getPersionName() {
        return persionName;
    }

    public String getPersionPhone() {
        return persionPhone;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setPersionName(String persionName) {
        this.persionName = persionName;
    }

    public void setPersionPhone(String persionPhone) {
        this.persionPhone = persionPhone;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public void setCommercialLat(String commercialLat) {
        this.commercialLat = commercialLat;
    }

    public String getCommercialLat() {
        return commercialLat;
    }

    public void setCommercialLng(String commercialLng) {
        this.commercialLng = commercialLng;
    }

    public String getCommercialLng() {
        return commercialLng;
    }
}

