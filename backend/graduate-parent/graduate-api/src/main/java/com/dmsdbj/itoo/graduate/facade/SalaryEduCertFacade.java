package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.ext.PeriodSalaryModel;

import java.util.List;

/**
 * 薪资学历统计Facade接口
 * 郑晓东
 * 2018年1月19日18点29分
 */
public interface SalaryEduCertFacade {

    /**
     * 查询每期每年平均、最高、最低工资
     * @param periodId 期数ID
     * @return 期数薪资统计数据
     */
    List<PeriodSalaryModel> selectAMMSalaryByPeriodId(String periodId);

    /**
     * 查询今年每期平均、最高、最低薪资统计 郑晓东 2018年1月20日16点38分
     * @return 薪资统计集合
     */
    List<PeriodSalaryModel> selectAMMSalaryInThisYear();

    /**
     * 查询每期毕业时平均、最高、最低薪资统计  郑晓东 2018年1月20日
     * @return 薪资统计集合
     */
    List<PeriodSalaryModel> selectAMMSalaryInGradeYear();
}
