package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;

import java.io.Serializable;

/**
 * Created by bobo on 2018/1/31.
 */
public class PersonReginModel extends PersonalInfoEntity implements Serializable{
    private String regionInfoId;//地域表id
    private String poi;//poi_id
    private String lng;//经度
    private String lat;//纬度
    private String regionAddress;//地域地址
    private String dictAddressType;//地域类型名称
    private String companyRegionId;//公司地域id
    private String companyAddress;//公司地址
    private String companyName;//公司名称
    private String gradeName;//期数名称

    public String getDictAddressType() {
        return dictAddressType;
    }

    public void setDictAddressType(String dictAddressType) {
        this.dictAddressType = dictAddressType;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getRegionInfoId() {
        return regionInfoId;
    }

    public void setRegionInfoId(String regionInfoId) {
        this.regionInfoId = regionInfoId;
    }

    public String getPoi() {
        return poi;
    }

    public void setPoi(String poi) {
        this.poi = poi;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getRegionAddress() {
        return regionAddress;
    }

    public void setRegionAddress(String regionAddress) {
        this.regionAddress = regionAddress;
    }

    public String getCompanyRegionId() {
        return companyRegionId;
    }

    public void setCompanyRegionId(String companyRegionId) {
        this.companyRegionId = companyRegionId;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
