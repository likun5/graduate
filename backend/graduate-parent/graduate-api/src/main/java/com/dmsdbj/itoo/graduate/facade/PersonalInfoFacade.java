package com.dmsdbj.itoo.graduate.facade;
import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.*;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author : 徐玲博
 * create :  2017-11-21 21:38:02
 */
public interface PersonalInfoFacade {
    /**
     * 根据id 查询个人信息-徐玲博-2017-11-21 21:45:18
     *
     * @param id 个人id
     * @return 个人信息
     */
    PersonalInfoEntity findByUserId(String id);

    /**
     * 根据id 查询个人信息-包括图像-徐玲博-2018-2-6 15:35:22
     * @param id 用户id
     * @return 用户信息
     */
    PersonInfoModel selectByPersonId(String id);

    /**
     * 毕业生-根据毕业生用户Id-添加个人信息-徐玲博-2017-11-19 11:28:07
     *
     * @param personalInfoEntity 个人信息实体
     * @return 受影响行
     */
    int addPersonInfo(PersonalInfoEntity personalInfoEntity);

    /**
     * 根据登录ID查询用户信息-徐玲博-2017-11-19 11:32:48
     *
     * @param loginId 登录ID
     * @return 个人信息实体
     */
    PersonalInfoEntity selectPersonByLoginId(String loginId);

    /**
     * 毕业生-根据毕业生 用户Id-修改个人信息-徐玲博-2017-11-19 11:28:07
     *
     * @param personInfoModel 用户实体
     * @return 受影响行
     */
    int updatePersonInfo(PersonInfoModel personInfoModel);

    /**
     * 毕业生-根据用户Id-删除个人信息-徐玲博-2017-11-19 11:43:50
     * 删除该用户，所有相关的信息，例如家庭、公司、薪资、学历等都将被删除
     *
     * @param personId 用户ID
     * @param operator 操作人
     * @return 是否删除成功
     */
    boolean deletePersonInfo(String personId, String operator);

    /**
     * 根据毕业生姓名查询个人信息-徐玲博-2017-11-30 17:14:23
     *
     * @param name 毕业生姓名
     * @return list列表
     */
    List<PersonalInfoEntity> selectPersonByName(String name);


    /**
     * 通知-查询毕业生信息-徐玲博-2017-12-6 11:55:36
     *
     * @return 毕业生信息列表
     */
    List<PersonGradeModel> selectPersonInfo();

    /**
     * 根据期数查询毕业生 -李爽-2017-12-7 21:50:28
     *
     * @param grade 期数
     * @return 毕业生list
     */
    List<PersonalInfoEntity> selectPersonByGrade(String grade);

    /**
     * 模糊查询（姓名or性别or期数） - 李爽-2017-12-7 21:50:24
     *
     * @param queryParam 模糊查询条件
     * @return 毕业生list
     */
    List<PersonalInfoEntity> selectPersonByNameSexGrade(String queryParam);

    /**
     * 组合查询+模糊查询+分页查询 毕业生信息查询-徐玲博-2018-1-2 11:13:16
     *
     * @param name        毕业生姓名
     * @param company     最新公司信息
     * @param grade       期数
     * @param education   学历
     * @param salaryRange 薪资范围
     * @param page        页码
     * @param pageSize    页大小
     * @return 毕业生信息list
     */
    PageInfo<PersonManageModel> selectPersonByCombination(String name, String company, String grade, String education, String salaryRange, int page, int pageSize);

    /**
     * 分页查询-根据地域类型、地域、期数、姓名、性别，查询学员信息-李爽-2018-1-13 17:27:42
     * @return 学员信息
     */
    PageInfo<PersonInfoModel> selectPersonInfoByRegionGradeNameSex(PersonInfoParamsModel personInfoParamsModel, int pageNum, int pageSize);

    /**
     * 查询所有期数及对应期数人员 郑晓东 2018年1月14日18点30分
     * @return
     */
    List<PeriodPersonModel> findPeriodPersonModels();

    /**
     * 数据中心-添加在校生或毕业生信息列表到到Personal表中--徐玲博-2018-1-15 15:02:09
     * @param personalInfoEntityList 个人信息实体
     * @return 受影响行
     */
    int addListToPersonal(List<PersonGradeModel> personalInfoEntityList);

    /**
     * 批量添加人员信息-徐玲博-2018-1-25 15:13:49
     *
     * @param personalInfoEntityList 人员信息表
     * @return 受影响行
     */
    int insertPersonInfoList(List<PersonalInfoEntity> personalInfoEntityList);

    /**
     * 定时同步数据中心-徐玲博-2018-1-15 14:11:28
     *
     * @return 受影响行
     */
    int synchronizeDataWarehouse();

    /**
     * 地域接口-组合查询人员地域信息--徐玲博-2018-1-31 16:50:01
     * @param name 个人名称
     * @param sex 性别
     * @param regionName 地域 省市县
     * @param grade 期数
     * @param dictionaryAddressId 地域类型
     * @return 个人地域列表
     */
    List<PersonReginModel> selectPersonRegion( String name, String sex, String regionName,String grade,String dictionaryAddressId);

    //-------------------------注册------------------------------------------------------------------

    /**
     *添加用户（注册）-袁甜梦-2018年3月11日11:09:32
     * @param userModel
     * @return boolean
     */
    boolean insertUser(UserModel userModel);

    /**
     *修改密码-袁甜梦-2018年3月20日08:55:00
     * @param userId  用户id
     * @param userCode 用户code
     * @param password 密码
     * @return  是否修改成功
     */
    boolean updateUserPassword(String userId,String userCode,String password);

    /**
     * 个人重置密码-袁甜梦-2018年3月20日14:02:13
     * @param userCode 用户编码
     * @return 是否更新成功
     */
    boolean resetPassword(String userCode);
}
