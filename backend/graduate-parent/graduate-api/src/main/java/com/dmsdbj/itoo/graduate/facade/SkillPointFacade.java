package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.SkillPointEntity;
import com.dmsdbj.itoo.graduate.entity.ext.TechnologyModel;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * @author : 徐玲博
 * create : 22017-11-21 21:38:27
 */
public interface SkillPointFacade {
    /**
     * 根据id查询技术点或技术方向
     *
     * @param id 技术Id
     * @return SkillPointEntity 技术点（方向）实体
     */
    SkillPointEntity findById(String id);

    /**
     * 添加技术点或技术方向-徐玲博-2017-11-20 15:13:47
     *
     * @param skillPointEntity 技术实体
     * @return 受影响行
     */
    int addSkillPoint(SkillPointEntity skillPointEntity);

    /**
     * 修改技术点或技术方向-徐玲博-2017-11-20 16:54:02
     *
     * @param skillPointEntity 技术点实体
     * @return 受影响行
     */
    int updateSkillPoint(SkillPointEntity skillPointEntity);

    /**
     * 删除技术点或技术方向-徐玲博-2017-11-20 16:54:17
     *
     * @param id       技术点ID
     * @param operator 操作人
     * @return 受影响行
     */
    int deleteSkillPoint(String id, String operator);

    /**
     * 查询技术点-或技术方向-徐玲博-2017-11-20 16:54:36
     *
     * @param id 用户Id
     * @return 技术列表
     */
    List<SkillPointEntity> selectSkillPointById(String id);

    /**
     * 查询技术方向-徐玲博-2017-11-21 14:42:51
     *
     * @param pid 技术方向
     * @return 技术方向列表
     */
    List<SkillPointEntity> selectParentSkillByPid(String pid);

    /**
     * 论坛接口-查询所有的技术-并根据技术方向分类-徐玲博-2017-12-26 15:55:23
     *
     * @return 技术列表
     */
    List<Map<String, List<SkillPointEntity>>> selectSkillPoint();

    /**
     * 查询所有技术（包括技术方向）-徐玲博-2018-1-13 22:52:15
     *
     * @return 技术list
     */
    List<TechnologyModel> selectAllTechnology();

    /**
     * 分页查询所有技术-徐玲博-2018-1-13 23:45:51
     *
     * @return 技术点list
     */
    PageInfo<TechnologyModel> pageAllTechnology(int page,int pageSize);

    /**
     * 查询所有的技术方向实体-徐玲博-2018-1-13 19:15:10
     *
     * @return 技术方向list
     */
    List<SkillPointEntity> selectTechnicalDirection();


    /**
     * 分页查询技术方向-haoguiting-2018年3月10日
     *
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 技术点list
     */
    PageInfo<SkillPointEntity> PageSelectSkillDirection(int page,int pageSize);

    /**
     * 分页查询根据技术方向查询技术点-haoguiting-2018年2月22日
     *
     * @param pid 技术方向
     * @param page 第几页
     * @param pageSize 每页显示的数量
     * @return 技术点list
     */
    PageInfo<SkillPointEntity> PageSelectSkillByDirectionId(String pid,int page,int pageSize);


    /**
     * 模糊查询技术点-haoguiting-2018年2月27日
     *
     * @return 技术点list
     */
    List<SkillPointEntity> fuzzySkillInfoByName(String strLike,String pid);
}