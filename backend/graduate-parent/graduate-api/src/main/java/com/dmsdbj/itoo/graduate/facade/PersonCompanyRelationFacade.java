package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.PersonCompanyRelationEntity;

import java.util.List;

/**
 *@author 徐玲博
 *create: 2017-11-26 16:14:54.
 */
public interface PersonCompanyRelationFacade {
      /**
      * @author kongwy
      * @param id
      * @return PersonCompanyRelationRelationEntity
      */
     PersonCompanyRelationEntity findById(String id);

    /**
     * 根据个人id查询个人公司关系id的List -lishuang-2017-12-16 17:10:53
     * @param personId 用户id
     * @return 关系id列表
     */
    List<String> selectPersonCompanyRelationIdsByPersonId(String personId);

    /**
     * 根据期数查询本期所有人的与公司的关系id的List -lishaugn-2017-12-16 17:10:40
     * @param grade 期数
     * @return 关系id列表
     */
    List<String> selectPersonCompanyRelationIdsByGrade(String grade);

}
