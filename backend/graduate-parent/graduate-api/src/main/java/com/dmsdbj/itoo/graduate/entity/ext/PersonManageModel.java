package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;

import java.io.Serializable;

/**
 * Created by bobo on 2018/1/9.
 */
public class PersonManageModel extends PersonalInfoEntity implements Serializable {
    private String userId;//毕业生Id
    private String companyName;//公司名
    private String salary;//薪资
    private String gradeName;//期数，例如10期
    private String certificateName;//学历

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }
}
