package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.CommercialOppotunityEntity;
import com.dmsdbj.itoo.graduate.entity.ext.RegionCommercialModel;
import com.github.pagehelper.PageInfo;
import java.util.List;


/**
 *@author sxm
 *create: 2017-11-15 15:47:40.
 */
public interface CommercialOppotunityFacade {
      /**
      * @author kongwy
      * @param id
      * @return CommercialOppotunityEntity
      */
     CommercialOppotunityEntity findById(String id);

    /**
     * 通过地域ID 查询地域商机--唐凌峰-2018-1-11 09:48:42
     * @param regionId 地域ID
     * @param pageNum  页码
     * @param pageSize 页量
     * @return   PageInfo<RegionCommercialModel>
     */
    PageInfo<RegionCommercialModel> queryRegionCommercialById(String regionId, int pageNum, int pageSize);


    /**
     * 方法重载-查询提高班毕业生所有商机信息-唐凌峰。
     * @return List<RegionCommercialModel>
     */
   List<RegionCommercialModel>queryRegionCommercialById();

}
