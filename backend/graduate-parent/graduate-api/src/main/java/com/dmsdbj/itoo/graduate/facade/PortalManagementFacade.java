package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.ApplicationEntity;
import com.dmsdbj.itoo.graduate.entity.ext.RoleApplicationModel;
import com.dmsdbj.itoo.graduate.entity.ext.UserRoleModel;

import java.util.List;
import java.util.Map;

/**
 *@author xulingbo
 *create: 2018-01-04 15:54:54.
 */
public interface PortalManagementFacade {
      /**
      * @author : xulingbo
      * @param : id
      * @return ApplicationEntity
      */
     ApplicationEntity findById(String id);

    /**
     * 用于顶栏显示
     * 根据userId查找服务-宋学孟-2018年1月18日
     *
     * @return List<RoleApplicationModel>
     */
    List<RoleApplicationModel> queryUserServiceApplication(String userId);

    /**
     * 左侧栏显示-根据userId查找所有的子模块和页面-宋学孟-2018年1月18日
     *
     * @return //List<Map<String, Object>>
     */
    List<Map<String, Object>> queryServiceModuleApplication(String userId, String applicationParentId);

    /**
     * 用portal的左侧栏显示
     * 根据userId查找所有的子模块和页面-宋学孟-2018年1月18日
     *
     */
    List<RoleApplicationModel> queryModuleButtonApplication(String userId, String applicationId);

    /**
     * 登录根据userId查询UserEntity-宋学孟-2018年1月18日
     * @param userCode
     * @return
     */
    UserRoleModel selectUserInfoByCode(String userCode, String userPwd);

}
