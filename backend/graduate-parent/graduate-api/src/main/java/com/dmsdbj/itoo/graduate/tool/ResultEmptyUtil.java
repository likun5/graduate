package com.dmsdbj.itoo.graduate.tool;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * 判断是否为空
 * Created by lishuang on 2017/12/18.
 */
public class ResultEmptyUtil {

     public static <T> T getReturnValue(T str){
        if(ObjectUtils.isEmpty(str)){
            return null;
        }
         return str;
    }


    public static <T> List<T> getReturnValue(List<T> list){
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }


}
