package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.CompanySkillRelationEntity;

import java.util.List;

/**
 * @author : 徐玲博
 * create : 2017-11-26 16:14:54.
 */
public interface CompanySkillRelationFacade {
    /**
     * @param id id
     * @return CompanySkillRelationEntity
     * @author 徐玲博
     */
    CompanySkillRelationEntity findById(String id);

    /**
     * 根据技术id查询所有使用的公司-徐玲博-2017-12-4 10:34:54
     *
     * @param skillId 技术id
     * @return 人员id -关系列表
     */
    List<CompanySkillRelationEntity> selectCompanyBySkill(String skillId);

    /**
     * 根据公司id查询，包括所有的技术-徐玲博-2017-12-4 10:37:25
     *
     * @param companyId 公司id
     * @return 技术id -关系列表
     */
    List<CompanySkillRelationEntity> selectSkillByCompany(String companyId);

    /**
     * 添加公司下的技术-徐玲博-2017-12-4 10:40:08
     *
     * @param companySkillRelationEntity 公司技术关系实体
     * @return 受影响行
     */
    int addCompanySkillRelation(CompanySkillRelationEntity companySkillRelationEntity);

    /**
     * 修改公司下的技术-徐玲博-2017-12-4 10:42:39
     *
     * @param companySkillRelationEntity 公司技术关系实体
     * @return 受影响行
     */
    int updateCompanySkillRelation(CompanySkillRelationEntity companySkillRelationEntity);

    /**
     * 删除某公司下技术-徐玲博-2017-12-4 10:41:41
     *
     * @param id       公司技术关系id
     * @param operator 操作人
     * @return 受影响行
     */
    int deleteCompanySkillRelation(String id, String operator);

    /**
     * 根据公司id删除关系表中技术-徐玲博-2017-12-5 22:10:55
     * @param companyId 公司id
     * @param operator 操作人
     * @return 受影响行
     */
    int deleteSkillByCompanyId(String companyId,String operator);

    /**
     *根据技术id删除关系表中公司信息-徐玲博-2017-12-5 23:20:31
     *
     * @param skillId 技术id
     * @param operator 操作人
     * @return 受影响行
     */
    int deleteCompanyBySkillId(String skillId,String operator);
}
