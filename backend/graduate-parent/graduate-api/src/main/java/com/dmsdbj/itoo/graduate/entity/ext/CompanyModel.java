package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.graduate.entity.CompanyEntity;
import com.dmsdbj.itoo.graduate.entity.SkillPointEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by sxm on 2017/11/23.
 */
public class CompanyModel extends CompanyEntity implements Serializable {

    private String userId;
    private String companyId;
    private String personCompanyId;
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
    private Date entryTime;
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
    private Date quitTime;
    private String funnyThing;
    private String position;
    private int isDeparture;
    private List<String> skillIds; //保存公司时，选择技术点的id
    private String recommendResource;
    private String recruitmentInfo;
    private String techName;
    private List<String> pictureUrls;
    private List<SkillPointEntity> skillPointEntities;  //查询公司时，查询相关的技术点显示（和skillIds可以合并，待优化）

    //地图信息点
    private String poiId;

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    //地图经度
    private String lng;
    //地图纬度
    private String lat;

    public String getPoiId() {
        return poiId;
    }

    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }



    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getRegionAddress() {
        return regionAddress;
    }

    public void setRegionAddress(String regionAddress) {
        this.regionAddress = regionAddress;
    }

    public String getDictionaryAddressId() {
        return dictionaryAddressId;
    }

    public void setDictionaryAddressId(String dictionaryAddressId) {
        this.dictionaryAddressId = dictionaryAddressId;
    }

    //地域信息内容
    private String regionAddress;
    //地域信息内容
    private String dictionaryAddressId;

    public List<SkillPointEntity> getSkillPointEntities() {
        return skillPointEntities;
    }

    public void setSkillPointEntities(List<SkillPointEntity> skillPointEntities) {
        this.skillPointEntities = skillPointEntities;
    }

    public List<String> getPictureUrls() {
        return pictureUrls;
    }

    public void setPictureUrls(List<String> pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    public List<String> getSkillIds() {
        return skillIds;
    }

    public void setSkillIds(List<String> skillIds) {
        this.skillIds = skillIds;
    }

    public String getRecruitmentInfo() {
        return recruitmentInfo;
    }

    public void setRecruitmentInfo(String recruitmentInfo) {
        this.recruitmentInfo = recruitmentInfo;
    }

    public String getTechName() {
        return techName;
    }

    public void setTechName(String techName) {
        this.techName = techName;
    }



    public String getRecommendResource() {
        return recommendResource;
    }

    public void setRecommendResource(String recommendResource) {
        this.recommendResource = recommendResource;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getPersonCompanyId() {
        return personCompanyId;
    }

    public void setPersonCompanyId(String personCompanyId) {
        this.personCompanyId = personCompanyId;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public Date getQuitTime() {
        return quitTime;
    }

    public void setQuitTime(Date quitTime) {
        this.quitTime = quitTime;
    }

    public String getFunnyThing() {
        return funnyThing;
    }

    public void setFunnyThing(String funnyThing) {
        this.funnyThing = funnyThing;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getIsDeparture() {
        return isDeparture;
    }

    public void setIsDeparture(int isDeparture) {
        this.isDeparture = isDeparture;
    }
}
