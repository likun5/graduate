package com.dmsdbj.itoo.graduate.facade;


import com.dmsdbj.itoo.graduate.entity.SalaryEntity;
import com.dmsdbj.itoo.graduate.entity.ext.CompanySalaryModel;
import com.github.pagehelper.PageInfo;

import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 *@author 徐玲博
 *create: 2017-11-26 16:14:54.
 */
public interface SalaryFacade {


    /**
     * @author yyl
     * @return list
     * 根据薪资id查询
     */
       PageInfo<SalaryEntity> findAll(int page, int pageSize);
    //public List<SalaryEntity> findAll( );
    /**
     * @author yyl
     * @param id
     * @return SalaryEntity
     * 根据薪资id查询
     */
    SalaryEntity findById(String id);
    /**
     * 根据年级查询最低薪资
     * @param  ：年级
     * @return 薪资列表
     */
     List<SalaryEntity>  selectLowestSalaryByGrade(String grade);
    /**
     * 根据年级查询最高薪资
     * @param  ：年级
     * @return 薪资列表
     */
      List<SalaryEntity>  selectHighestSalaryByGrade(String grade);

    /**
     * 根据年级查询平均薪资列表
     * @param  ：年度
     * @return 本年度平均工资和期数List
     */
      Double selectAverageSalaryByGrade(String grade);



    /**
     * 根据年级id查询本年最高薪资-于云丽-2018-01-14
     * @param
     * @return 薪资实体list
     */
    Double selectHighSalaryByGradeId(String gradeId);



    /**
     * 根据年级id查询本年最低薪资-于云丽-2018-01-14
     * @param
     * @return 薪资实体list
     */
    Double selectLowestSalaryByGradeId(String gradeId);


    /**
     * 根据年级id查询本年平均薪资-于云丽-2018-01-14
     * @param
     * @return 薪资实体list
     */
    Double selectAverageSalaryByGradeId(String gradeId);

    /**
     * @author yyl
     * @return list
     * 根据人员id查询
     */
    List<SalaryEntity> selectByPersonId(String personId);
    /**
     * @author yyl
     * @return list
     * 根据期数查询
     */

    Map<String ,List<SalaryEntity>> selectByGrade(String grade );
    /**
     * @author yyl
     * @return SalaryEntity
     * 根据薪资id查询
     */
     List<SalaryEntity>  selectByName(String name);
    //List<SalaryEntity> selectByName(String name);
    /**
     *添加薪资信息接口
     *@author yyl
     */
    int addSalaryEntity(SalaryEntity salaryEntity );

    /**
     *删除薪资信息
     *@author yyl
     */
    int deleteSalaryEntity( String salaryEntityId ,String operator);
    /**
     *修改薪资信息
     *@author yyl
     */
    int updateSalaryEntity( SalaryEntity salaryEntity);

    /**
     * 根据关系Id、年限查询薪资-时间列表(升序降序没有体现)
     * @param :Name
     * @param date
     * @return 一条信息
     */
    List<SalaryEntity> selectByPcRelation(String pcRelation, Date date);

    /**
     * 根据期数相应人员列表查询本期最高工资
     *
     * @param :relationIds
     * @param date
     * @return 最高工资的记录（排序根据薪资升序排列如何实现？）
     */
    List selecthighestSalaryByRelatoinId(List<String> relationIds, Date date) ;

    /**
     * 根据期数查询本期最低工资
     *
     * @param relationIds
     * @param date
     * @return 平均最低的记录
     */
    List selectlowestSalaryByRelatoinId(List<String> relationIds, Date date) ;

    /**
     * 根据期数查询本期平均工资
     *
     * @param relationIds
     * @param date
     * @return 平均工资的记录
     */
    Double selectAverageSalaryByRelatoinId(List<String> relationIds, Date date);

    /**
     * 整体本年度最高薪资统计
     *
     * @param date
     * @return 本年度全体中的工资最高值列表
     */
    List selecthighestSalaryofAll(Date date);


    /**
     * 整体本年度最低薪资统计
     * @param date
     * @return 本年度最低工资和期数List
     */
    List selectlowestSalaryofAll(Date date) ;

    /**
     *查询全员平均薪资信息
     *@author yyl
     */
    Double findAverageSalary(Date date);

    /**
     *根据毕业生id查询薪资公司相关信息-haoguiting--2018年3月7日
     * @param userId
     * @return
     */
    PageInfo<CompanySalaryModel> PageSelectSalaryByUserId(String userId,int page, int pageSize);
}
