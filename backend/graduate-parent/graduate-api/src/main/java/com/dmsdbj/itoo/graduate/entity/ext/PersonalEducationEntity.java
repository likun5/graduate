package com.dmsdbj.itoo.graduate.entity.ext;

import java.io.Serializable;

/** 统计提高班毕业生的学历
 * Created by tlf on 2018/2/7.
 */
public class PersonalEducationEntity implements Serializable{
    private String grade;
    private String enter_college_time;
    private String  enter_dmt_time;

    private  String emerg_name;
    private  String emerg_phone;
    private  String certificate_no;
    private  String certificate_type;

    private  String university_name;
    private  String certificate_name;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public void setEnter_college_time(String enter_college_time) {
        this.enter_college_time = enter_college_time;
    }

    public String getEnter_college_time() {
        return enter_college_time;
    }

    public String getCertificate_name() {
        return certificate_name;
    }

    public String getCertificate_no() {
        return certificate_no;
    }

    public String getCertificate_type() {
        return certificate_type;
    }

    public String getEmerg_name() {
        return emerg_name;
    }

    public String getEmerg_phone() {
        return emerg_phone;
    }

    public String getEnter_dmt_time() {
        return enter_dmt_time;
    }

    public void setCertificate_no(String certificate_no) {
        this.certificate_no = certificate_no;
    }

    public void setEnter_dmt_time(String enter_dmt_time) {
        this.enter_dmt_time = enter_dmt_time;
    }

    public String getUniversity_name() {
        return university_name;
    }

    public void setCertificate_name(String certificate_name) {
        this.certificate_name = certificate_name;
    }

    public void setCertificate_type(String certificate_type) {
        this.certificate_type = certificate_type;
    }

    public void setEmerg_name(String emerg_name) {
        this.emerg_name = emerg_name;
    }

    public void setEmerg_phone(String emerg_phone) {
        this.emerg_phone = emerg_phone;
    }

    public void setUniversity_name(String university_name) {
        this.university_name = university_name;
    }
}
