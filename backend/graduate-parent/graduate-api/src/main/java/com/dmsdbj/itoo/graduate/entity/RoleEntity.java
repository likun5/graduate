package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class RoleEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 角色名称
	 */
	private String roleName;
	/**
	 *
	 * 角色描述
	 */
	private String roleDescribe;

	/**
	 *
	 * @return String 角色名称
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * 
	 * @param roleName 角色名称
	 */
    public void setRoleName(String roleName) {
		this.roleName = (roleName== null ? null : roleName.trim());
	}
	/**
	 *
	 * @return String 角色描述
	 */
	public String getRoleDescribe() {
		return roleDescribe;
	}

	/**
	 * 
	 * @param roleDescribe 角色描述
	 */
    public void setRoleDescribe(String roleDescribe) {
		this.roleDescribe = (roleDescribe== null ? null : roleDescribe.trim());
	}
	
	@Override
    public String toString() {
		return "RoleEntity{"+
		"roleName='"+roleName+'\''+
		"roleDescribe='"+roleDescribe+'\''+
		'}';
	}
}
