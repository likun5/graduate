package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class PersonCompanyRelationEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 用户id
	 */
	private String userId;
	/**
	 *
	 * 公司id
	 */
	private String companyId;
	/**
	 *
	 * 入职时间
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date entryTime;
	/**
	 *
	 * 离职时间
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date quitTime;
	/**
	 *
	 * 趣事
	 */
	private String funnyThing;
	/**
	 *
	 * 职位
	 */
	private String position;
	/**
	 *
	 * 招聘信息
	 */
	private String recruitmentInfo;
	/**
	 *
	 * 是否离职（0/1 未离职/已离职）
	 */
	private Integer isDeparture;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return String 用户id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * 
	 * @param userId 用户id
	 */
    public void setUserId(String userId) {
		this.userId = (userId== null ? null : userId.trim());
	}
	/**
	 *
	 * @return String 公司id
	 */
	public String getCompanyId() {
		return companyId;
	}

	/**
	 * 
	 * @param companyId 公司id
	 */
    public void setCompanyId(String companyId) {
		this.companyId = (companyId== null ? null : companyId.trim());
	}
	/**
	 *
	 * @return Date 入职时间
	 */
	public Date getEntryTime() {
		return entryTime;
	}

	/**
	 * 
	 * @param entryTime 入职时间
	 */
    public void setEntryTime(Date entryTime) {
		this.entryTime = entryTime;
	}
	/**
	 *
	 * @return Date 离职时间
	 */
	public Date getQuitTime() {
		return quitTime;
	}

	/**
	 * 
	 * @param quitTime 离职时间
	 */
    public void setQuitTime(Date quitTime) {
		this.quitTime = quitTime;
	}
	/**
	 *
	 * @return String 趣事
	 */
	public String getFunnyThing() {
		return funnyThing;
	}

	/**
	 * 
	 * @param funnyThing 趣事
	 */
    public void setFunnyThing(String funnyThing) {
		this.funnyThing = (funnyThing== null ? null : funnyThing.trim());
	}
	/**
	 *
	 * @return String 职位
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * 
	 * @param position 职位
	 */
    public void setPosition(String position) {
		this.position = (position== null ? null : position.trim());
	}
	/**
	 *
	 * @return String 招聘信息
	 */
	public String getRecruitmentInfo() {
		return recruitmentInfo;
	}

	/**
	 * 
	 * @param recruitmentInfo 招聘信息
	 */
    public void setRecruitmentInfo(String recruitmentInfo) {
		this.recruitmentInfo = (recruitmentInfo== null ? null : recruitmentInfo.trim());
	}
	/**
	 *
	 * @return Integer 是否离职（0/1 未离职/已离职）
	 */
	public Integer getIsDeparture() {
		return isDeparture;
	}

	/**
	 * 
	 * @param isDeparture 是否离职（0/1 未离职/已离职）
	 */
    public void setIsDeparture(Integer isDeparture) {
		this.isDeparture = isDeparture;
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "PersonCompanyRelationEntity{"+
		"userId='"+userId+'\''+
		"companyId='"+companyId+'\''+
		"entryTime='"+entryTime+'\''+
		"quitTime='"+quitTime+'\''+
		"funnyThing='"+funnyThing+'\''+
		"position='"+position+'\''+
		"recruitmentInfo='"+recruitmentInfo+'\''+
		"isDeparture='"+isDeparture+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
