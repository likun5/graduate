package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class SkillPointEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 父id
	 */
	private String pId;
	/**
	 *
	 * 技术名称
	 */
	private String techName;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;
	/**
	 *
	 * 公司id
	 */
	private String companyId;

	/**
	 *
	 * @return String 父id
	 */
	public String getPId() {
		return pId;
	}

	/**
	 * 
	 * @param pId 父id
	 */
    public void setPId(String pId) {
		this.pId = (pId== null ? null : pId.trim());
	}
	/**
	 *
	 * @return String 技术名称
	 */
	public String getTechName() {
		return techName;
	}

	/**
	 * 
	 * @param techName 技术名称
	 */
    public void setTechName(String techName) {
		this.techName = (techName== null ? null : techName.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	/**
	 *
	 * @return String 公司id
	 */
	public String getCompanyId() {
		return companyId;
	}

	/**
	 * 
	 * @param companyId 公司id
	 */
    public void setCompanyId(String companyId) {
		this.companyId = (companyId== null ? null : companyId.trim());
	}
	
	@Override
    public String toString() {
		return "SkillPointEntity{"+
		"pId='"+pId+'\''+
		"techName='"+techName+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		"companyId='"+companyId+'\''+
		'}';
	}
}
