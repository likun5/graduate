package com.dmsdbj.itoo.graduate.entity.ext;

/**
 *  期数薪资关系（每期）实体
 *  郑晓东 2018年1月15日
 */
public class PeriodSalaryModel {

    //期数ID
    private String periodId;
    //期数名称
    private String periodName;
    //年份
    private String salaryYear;
    //平均薪资
    private Double aveSalary;
    //最高薪资
    private Double maxSalary;
    //最低薪资
    private Double minSalary;
    //统计总值
    private Double totalSalary;

    public String getPeriodId() {
        return periodId;
    }

    public void setPeriodId(String periodId) {
        this.periodId = periodId;
    }

    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName;
    }

    public String getSalaryYear() {
        return salaryYear;
    }

    public void setSalaryYear(String salaryYear) {
        this.salaryYear = salaryYear;
    }

    public Double getAveSalary() {
        return aveSalary;
    }

    public void setAveSalary(Double aveSalary) {
        this.aveSalary = aveSalary;
    }

    public Double getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(Double maxSalary) {
        this.maxSalary = maxSalary;
    }

    public Double getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Double minSalary) {
        this.minSalary = minSalary;
    }

    public Double getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(Double totalSalary) {
        this.totalSalary = totalSalary;
    }
}
