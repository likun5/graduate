package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class ApplicationEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 资源名称
	 */
	private String applicationName;
	/**
	 *
	 * 类型
	 */
	private String type;
	/**
	 *
	 * 直接父类资源id
	 */
	private String applicationParentId;
	/**
	 *
	 * 资源URL
	 */
	private String applicationUrl;
	/**
	 *
	 * 资源标识
	 */
	private String permission;
	/**
	 *
	 * permission_name
	 */
	private String permissionName;
	/**
	 *
	 * 是否是管理模块
	 */
	private String isManagedModule;
	/**
	 *
	 * description
	 */
	private String description;
	/**
	 *
	 * 排序id
	 */
	private String sortId;
	/**
	 *
	 * 资源图标
	 */
	private String applicationIcon;
	/**
	 *
	 * 所有的父类资源id
	 */
	private String applicationParentIds;
	/**
	 *
	 * 状态（是否可用）
	 */
	private String isState;

	/**
	 *
	 * @return String 资源名称
	 */
	public String getApplicationName() {
		return applicationName;
	}

	/**
	 * 
	 * @param applicationName 资源名称
	 */
    public void setApplicationName(String applicationName) {
		this.applicationName = (applicationName== null ? null : applicationName.trim());
	}
	/**
	 *
	 * @return String 类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * 
	 * @param type 类型
	 */
    public void setType(String type) {
		this.type = (type== null ? null : type.trim());
	}
	/**
	 *
	 * @return String 直接父类资源id
	 */
	public String getApplicationParentId() {
		return applicationParentId;
	}

	/**
	 * 
	 * @param applicationParentId 直接父类资源id
	 */
    public void setApplicationParentId(String applicationParentId) {
		this.applicationParentId = (applicationParentId== null ? null : applicationParentId.trim());
	}
	/**
	 *
	 * @return String 资源URL
	 */
	public String getApplicationUrl() {
		return applicationUrl;
	}

	/**
	 * 
	 * @param applicationUrl 资源URL
	 */
    public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = (applicationUrl== null ? null : applicationUrl.trim());
	}
	/**
	 *
	 * @return String 资源标识
	 */
	public String getPermission() {
		return permission;
	}

	/**
	 * 
	 * @param permission 资源标识
	 */
    public void setPermission(String permission) {
		this.permission = (permission== null ? null : permission.trim());
	}
	/**
	 *
	 * @return String permission_name
	 */
	public String getPermissionName() {
		return permissionName;
	}

	/**
	 * 
	 * @param permissionName permission_name
	 */
    public void setPermissionName(String permissionName) {
		this.permissionName = (permissionName== null ? null : permissionName.trim());
	}
	/**
	 *
	 * @return String 是否是管理模块
	 */
	public String getIsManagedModule() {
		return isManagedModule;
	}

	/**
	 * 
	 * @param isManagedModule 是否是管理模块
	 */
    public void setIsManagedModule(String isManagedModule) {
		this.isManagedModule = (isManagedModule== null ? null : isManagedModule.trim());
	}
	/**
	 *
	 * @return String description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description description
	 */
    public void setDescription(String description) {
		this.description = (description== null ? null : description.trim());
	}
	/**
	 *
	 * @return String 排序id
	 */
	public String getSortId() {
		return sortId;
	}

	/**
	 * 
	 * @param sortId 排序id
	 */
    public void setSortId(String sortId) {
		this.sortId = (sortId== null ? null : sortId.trim());
	}
	/**
	 *
	 * @return String 资源图标
	 */
	public String getApplicationIcon() {
		return applicationIcon;
	}

	/**
	 * 
	 * @param applicationIcon 资源图标
	 */
    public void setApplicationIcon(String applicationIcon) {
		this.applicationIcon = (applicationIcon== null ? null : applicationIcon.trim());
	}
	/**
	 *
	 * @return String 所有的父类资源id
	 */
	public String getApplicationParentIds() {
		return applicationParentIds;
	}

	/**
	 * 
	 * @param applicationParentIds 所有的父类资源id
	 */
    public void setApplicationParentIds(String applicationParentIds) {
		this.applicationParentIds = (applicationParentIds== null ? null : applicationParentIds.trim());
	}
	/**
	 *
	 * @return String 状态（是否可用）
	 */
	public String getIsState() {
		return isState;
	}

	/**
	 * 
	 * @param isState 状态（是否可用）
	 */
    public void setIsState(String isState) {
		this.isState = (isState== null ? null : isState.trim());
	}
	
	@Override
    public String toString() {
		return "ApplicationEntity{"+
		"applicationName='"+applicationName+'\''+
		"type='"+type+'\''+
		"applicationParentId='"+applicationParentId+'\''+
		"applicationUrl='"+applicationUrl+'\''+
		"permission='"+permission+'\''+
		"permissionName='"+permissionName+'\''+
		"isManagedModule='"+isManagedModule+'\''+
		"description='"+description+'\''+
		"sortId='"+sortId+'\''+
		"applicationIcon='"+applicationIcon+'\''+
		"applicationParentIds='"+applicationParentIds+'\''+
		"isState='"+isState+'\''+
		'}';
	}
}
