package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class ForumEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * skill_id
	 */
	private String skillId;
	/**
	 *
	 * 标题
	 */
	private String title;
	/**
	 *
	 * 发布内容
	 */
	private String content;
	/**
	 *
	 * 操作用户id
	 */
	private String operatorid;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return String skill_id
	 */
	public String getSkillId() {
		return skillId;
	}

	/**
	 * 
	 * @param skillId skill_id
	 */
    public void setSkillId(String skillId) {
		this.skillId = (skillId== null ? null : skillId.trim());
	}
	/**
	 *
	 * @return String 标题
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title 标题
	 */
    public void setTitle(String title) {
		this.title = (title== null ? null : title.trim());
	}
	/**
	 *
	 * @return String 发布内容
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 
	 * @param content 发布内容
	 */
    public void setContent(String content) {
		this.content = (content== null ? null : content.trim());
	}
	/**
	 *
	 * @return String 操作用户id
	 */
	public String getOperatorid() {
		return operatorid;
	}

	/**
	 * 
	 * @param operatorid 操作用户id
	 */
    public void setOperatorid(String operatorid) {
		this.operatorid = (operatorid== null ? null : operatorid.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "ForumEntity{"+
		"skillId='"+skillId+'\''+
		"title='"+title+'\''+
		"content='"+content+'\''+
		"operatorid='"+operatorid+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
