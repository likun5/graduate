package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.AdministrativeRegionEntity;

import java.util.List;

/**
 *@author 李爽
 *create: 2017-12-05 09:39:56.
 */
public interface AdministrativeRegionFacade {
      /**
      * @author 李爽
      * @param id
      * @return AdministrativeRegionEntity
      */
     AdministrativeRegionEntity findById(String id);

    /**
     * 查询所有的省份 -李爽-2017-12-5 15:00:26
     * @return
     */
    List<AdministrativeRegionEntity> selectProvince();

    /**
     * 根据id查询子行政区，比如根据某一省/市 id，查询其下是市/区县 -李爽-2017-12-5 15:02:36
     * @param id
     * @return
     */
    List<AdministrativeRegionEntity> selectSubRegionById(String id);



}
