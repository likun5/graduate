package com.dmsdbj.itoo.graduate.tool;

/**
 * Created by lishuang on 2017/12/6.
 */
public class DictionaryTypeCode {
    public static final String GRADE = "GRADE"; /*期数*/
    public static final String ADDRESS_TYPE = "ADDRESS_TYPE"; //地域条件-地址类型

    public static final String RELEATION="RELEATION"; //关系类型
}
