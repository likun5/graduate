package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.graduate.entity.HomeInfoEntity;

import java.io.Serializable;

/**
 * Created by bobo on 2018/1/29.
 */
public class HomeInfoModel extends HomeInfoEntity implements Serializable {
    /**
     * 关系名称
     */
    private String relationName;

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }
}
