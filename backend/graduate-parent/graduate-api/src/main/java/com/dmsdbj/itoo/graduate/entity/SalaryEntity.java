package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-17 14:40:30.
 */

public class SalaryEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 个人公司关系id
	 */
	private String pcRelationId;
	/**
	 *
	 * 工资变动时间
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date salaryChangeTime;
	/**
	 *
	 * 工资
	 */
	private Double salary;
	/**
	 *
	 * 一年几薪
	 */
	private BigDecimal timesPerYear;
	/**
	 *
	 * 职位
	 */
	private String possession;
	/**
	 *
	 * 年薪
	 */
	private String annualSalary;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return String 个人公司关系id
	 */
	public String getPcRelationId() {
		return pcRelationId;
	}

	/**
	 * 
	 * @param pcRelationId 个人公司关系id
	 */
    public void setPcRelationId(String pcRelationId) {
		this.pcRelationId = (pcRelationId== null ? null : pcRelationId.trim());
	}
	/**
	 *
	 * @return Date 工资变动时间
	 */
	public Date getSalaryChangeTime() {
		return salaryChangeTime;
	}

	/**
	 * 
	 * @param salaryChangeTime 工资变动时间
	 */
    public void setSalaryChangeTime(Date salaryChangeTime) {
		this.salaryChangeTime = salaryChangeTime;
	}
	/**
	 *
	 * @return Double 工资
	 */
	public Double getSalary() {
		return salary;
	}

	/**
	 * 
	 * @param salary 工资
	 */
    public void setSalary(Double salary) {
		this.salary = salary;
	}
	/**
	 *
	 * @return BigDecimal 一年几薪
	 */
	public BigDecimal getTimesPerYear() {
		return timesPerYear;
	}

	/**
	 * 
	 * @param timesPerYear 一年几薪
	 */
    public void setTimesPerYear(BigDecimal timesPerYear) {
		this.timesPerYear = timesPerYear;
	}
	/**
	 *
	 * @return String 职位
	 */
	public String getPossession() {
		return possession;
	}

	/**
	 * 
	 * @param possession 职位
	 */
    public void setPossession(String possession) {
		this.possession = (possession== null ? null : possession.trim());
	}
	/**
	 *
	 * @return String 年薪
	 */
	public String getAnnualSalary() {
		return annualSalary;
	}

	/**
	 * 
	 * @param annualSalary 年薪
	 */
    public void setAnnualSalary(String annualSalary) {
		this.annualSalary = (annualSalary== null ? null : annualSalary.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "SalaryEntity{"+
		"pcRelationId='"+pcRelationId+'\''+
		"salaryChangeTime='"+salaryChangeTime+'\''+
		"salary='"+salary+'\''+
		"timesPerYear='"+timesPerYear+'\''+
		"possession='"+possession+'\''+
		"annualSalary='"+annualSalary+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
