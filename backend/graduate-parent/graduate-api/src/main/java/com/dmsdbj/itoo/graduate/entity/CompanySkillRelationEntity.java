package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class CompanySkillRelationEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 技术id
	 */
	private String skillId;
	/**
	 *
	 * 公司id
	 */
	private String companyId;
	/**
	 *
	 * 推荐资料
	 */
	private String recommendResource;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return String 技术id
	 */
	public String getSkillId() {
		return skillId;
	}

	/**
	 * 
	 * @param skillId 技术id
	 */
    public void setSkillId(String skillId) {
		this.skillId = (skillId== null ? null : skillId.trim());
	}
	/**
	 *
	 * @return String 公司id
	 */
	public String getCompanyId() {
		return companyId;
	}

	/**
	 * 
	 * @param companyId 公司id
	 */
    public void setCompanyId(String companyId) {
		this.companyId = (companyId== null ? null : companyId.trim());
	}
	/**
	 *
	 * @return String 推荐资料
	 */
	public String getRecommendResource() {
		return recommendResource;
	}

	/**
	 * 
	 * @param recommendResource 推荐资料
	 */
    public void setRecommendResource(String recommendResource) {
		this.recommendResource = (recommendResource== null ? null : recommendResource.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "CompanySkillRelationEntity{"+
		"skillId='"+skillId+'\''+
		"companyId='"+companyId+'\''+
		"recommendResource='"+recommendResource+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
