package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
import com.dmsdbj.itoo.graduate.entity.ext.DictionaryTreeModel;
import com.github.pagehelper.PageInfo;
import java.util.List;

/**
 * @author 李爽
 * create: 2017-11-15 15:47:40.
 */
public interface DictionaryFacade {
    /**
     * 测试方法
     * @param id
     * @return DictionaryEntity字典实体
     * @author 李爽
     */
    DictionaryEntity findById(String id);

    /**
     * 根据字典id查询字典名词 - yyl-2018-01-15 01:48:00
     *
     * @param id 字典id
     * @return Strin字典名字
     */
    String findDictionaryNameById(String id);

    /**
     * 根据字典名称查询字典id - 李爽-2017-11-29 19:25:09
     *
     * @param dictionaryName 字典名称
     * @return 字典id
     */
    String findDictionaryIdByName(String dictionaryName);

    /**
     *根据名字模糊查询字典信息
     * @param strLike 模糊条件
     * @param dictionaryTypeName 字典类型名字
     * @return
     */
    List<DictionaryEntity> fuzzyDictionaryInfoByName(String strLike,String dictionaryTypeName);
    /**
     * 添加字典信息 -李爽-2017-11-29 18:53:59
     *
     * @param dictionaryEntity 字典实体
     * @return 添加是否成功
     */
    int addDictionary(DictionaryEntity dictionaryEntity);

    /**
     * 批量添加字典信息 -李爽-2017-12-2 19:18:12
     *
     * @param list 字典集合
     * @return 添加是否成功
     */
    int addDictionary(List<DictionaryEntity> list);

    /**
     * 修改字典信息 -李爽-2017-11-29 18:54:39
     *
     * @param dictionaryEntity 字典实体
     * @return 修改是否成功
     */
    boolean updateDictionary(DictionaryEntity dictionaryEntity);

    /**
     * 根据字典id删除字典信息 -李爽-2017-11-29 18:55:40
     *
     * @param id 字典id
     * @return 删除是否成功
     */
    boolean deleteDictionary(String id);

    /**
     * 批量删除字典信息 -李爽-2017-11-29 18:56:32
     *
     * @param ids id集合
     * @return 删除是否成功
     */
    boolean deleteDictionary(List<String> ids);


    /**
     * 根据字典类型查询字典信息 -李爽-2017-12-6 14:27:55
     *
     * @param typeCode 类型code
     * @return 字典信息list
     */
    List<DictionaryEntity> selectDictionaryByTypeCode(String typeCode);

    /**
     * 查询所有的字典类型 -李爽-2017-12-6 14:27:51
     *
     * @return 字典类型list
     */
    List<DictionaryEntity> selectAllDictionaryType();

    /**
     * 分页查询字典类型-hgt-2018年3月10日
     * @param page 页数
     * @param pageSize 每页大小
     * @return 字典类型list
     */
    PageInfo<DictionaryEntity> PageSelectDictionaryType(int page,int pageSize);

    /**
     * 查询字典类型以树形返回-杜娟-2018-2-24 12:16:55
     * @return 返回字典类型
     */
    List<DictionaryTreeModel> selectAllDictionaryTypeByTree();

    /**
     * 根据typecode查询字典内容-杜娟-2018-2-24 18:14:45
     * @param typeCode 类型code
     * @return 字典类型内容
     */
    List<DictionaryEntity> selectDitNameByTypeCode(String typeCode);
    //TODO:分页查询字典信息 -李爽-2017-11-29 18:57:32


}
