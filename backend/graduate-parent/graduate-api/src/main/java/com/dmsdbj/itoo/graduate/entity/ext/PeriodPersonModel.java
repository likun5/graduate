package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;

import java.io.Serializable;
import java.util.List;

/**
 * 期数与用户的关联关系 郑晓东 2018年1月14日18点28分
 */
public class PeriodPersonModel implements Serializable {

    //期数id
    private String periodId;
    //期数名称
    private String periodName;
    //学生信息
    private List<PersonalInfoEntity> personalInfoList;

    public String getPeriodId() {
        return periodId;
    }

    public void setPeriodId(String periodId) {
        this.periodId = periodId;
    }

    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName;
    }

    public List<PersonalInfoEntity> getPersonalInfoList() {
        return personalInfoList;
    }

    public void setPersonalInfoList(List<PersonalInfoEntity> personalInfoList) {
        this.personalInfoList = personalInfoList;
    }
}
