package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class LandScapeEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 用户id
	 */
	private String userId;
	/**
	 *
	 * 景区名称
	 */
	private String name;
	/**
	 *
	 * 描述
	 */
	private String description;
	/**
	 *
	 * 行政区id
	 */
	private String regionId;
	/**
	 *
	 * 详细地址
	 */
	private String address;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return String 用户id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * 
	 * @param userId 用户id
	 */
    public void setUserId(String userId) {
		this.userId = (userId== null ? null : userId.trim());
	}
	/**
	 *
	 * @return String 景区名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name 景区名称
	 */
    public void setName(String name) {
		this.name = (name== null ? null : name.trim());
	}
	/**
	 *
	 * @return String 描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description 描述
	 */
    public void setDescription(String description) {
		this.description = (description== null ? null : description.trim());
	}
	/**
	 *
	 * @return String 行政区id
	 */
	public String getRegionId() {
		return regionId;
	}

	/**
	 * 
	 * @param regionId 行政区id
	 */
    public void setRegionId(String regionId) {
		this.regionId = (regionId== null ? null : regionId.trim());
	}
	/**
	 *
	 * @return String 详细地址
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * 
	 * @param address 详细地址
	 */
    public void setAddress(String address) {
		this.address = (address== null ? null : address.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "LandScapeEntity{"+
		"userId='"+userId+'\''+
		"name='"+name+'\''+
		"description='"+description+'\''+
		"regionId='"+regionId+'\''+
		"address='"+address+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
