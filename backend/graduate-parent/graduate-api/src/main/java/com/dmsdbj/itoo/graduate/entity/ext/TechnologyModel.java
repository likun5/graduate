package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.graduate.entity.SkillPointEntity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bobo on 2018/1/13.
 */
public class TechnologyModel extends SkillPointEntity implements Serializable{

    private List<SkillPointEntity> skillPointEntityList;//技术点

    public List<SkillPointEntity> getSkillPointEntityList() {
        return skillPointEntityList;
    }

    public void setSkillPointEntityList(List<SkillPointEntity> skillPointEntityList) {
        this.skillPointEntityList = skillPointEntityList;
    }
}
