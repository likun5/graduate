package com.dmsdbj.itoo.graduate.entity.ext;

import java.io.Serializable;

/**
 * Created by Asin on 2018-02-24.
 */
public class DictionaryTreeModel implements Serializable {
    private String id;
    private String name;
    private String pId;
    private String nId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getnId() {
        return nId;
    }

    public void setnId(String nId) {
        this.nId = nId;
    }



}
