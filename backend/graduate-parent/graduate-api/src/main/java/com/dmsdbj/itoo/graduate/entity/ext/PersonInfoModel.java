package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;

import java.io.Serializable;

/**
 *
 * Created by lishuang on 2018/1/6.
 */
public class PersonInfoModel extends PersonalInfoEntity implements Serializable {

    private String companyRegionId;
    private String companyAddress;
    private String pictureUrl;
    private String emergRelationName;
    private String gradeName;

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getCompanyRegionId() {
        return companyRegionId;
    }

    public void setCompanyRegionId(String companyRegionId) {
        this.companyRegionId = companyRegionId;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getEmergRelationName() {
        return emergRelationName;
    }

    public void setEmergRelationName(String emergRelationName) {
        this.emergRelationName = emergRelationName;
    }
}
