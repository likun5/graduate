package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.PersonSkillRelationEntity;


import java.util.List;

/**
 * @author : 徐玲博
 * create : 2017-11-26 16:14:54.
 */
public interface PersonSkillRelationFacade {
    /**
     * @param id id
     * @return PersonSkillRelationEntity
     * @author 徐玲博
     */
    PersonSkillRelationEntity findById(String id);

    /**
     * 查询某人熟悉的所有技术-徐玲博-2017-12-4 10:58:00
     *
     * @param userId 人员id
     * @return 技术列表
     */
    List<PersonSkillRelationEntity> selectSkillByPerson(String userId);

    /**
     * 查询会某个技术的所有人-徐玲博-2017-12-4 10:58:19
     *
     * @param skillId 技术id
     * @return 人员列表
     */
    List<PersonSkillRelationEntity> selectPersonBySkill(String skillId);

    /**
     * 添加个人和技术的关系-徐玲博-2017-12-4 11:03:39
     *
     * @param personSkillRelationEntity 个人技术关系实体
     * @return 受影响行
     */
    int addPersonSkillRelation(PersonSkillRelationEntity personSkillRelationEntity);

    /**
     * 修改个人和技术的关系-徐玲博-2017-12-4 11:03:39
     *
     * @param personSkillRelationEntity 个人技术关系实体
     * @return 受影响行
     */
    int updatePersonSkillRelation(PersonSkillRelationEntity personSkillRelationEntity);

    /**
     * 删除个人和技术的关系-徐玲博-2017-12-4 11:03:39
     *
     * @param id 用户id
     * @param operator 操作人
     * @return 受影响行
     */
    int deletePersonSkillRelation(String id,String operator);

    /**
     * 根据技术id删除关系表中毕业生-徐玲博-2017-12-5 23:26:38
     * @param skillId 技术id
     * @param operator 操作人
     * @return 受影响行
     */
    int deletePersonBySkillId(String skillId,String operator);

    /**
     * 根据人员id删除关系表中技术-徐玲博-2017-12-5 23:26:58
     * @param userId 用户id
     * @param operator 操作人
     * @return 受影响行
     */
    int deleteSkillByPersonId(String userId,String operator);

}
