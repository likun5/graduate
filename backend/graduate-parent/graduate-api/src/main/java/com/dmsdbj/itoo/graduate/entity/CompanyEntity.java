package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class CompanyEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 公司名称
	 */
	private String companyName;
	/**
	 *
	 * 公司电话
	 */
	private String companyTel;
	/**
	 *
	 * 公司行政区id
	 */
	private String regionId;
	/**
	 *
	 * 地址
	 */
	private String companyAddress;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return String 公司名称
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * 
	 * @param companyName 公司名称
	 */
    public void setCompanyName(String companyName) {
		this.companyName = (companyName== null ? null : companyName.trim());
	}
	/**
	 *
	 * @return String 公司电话
	 */
	public String getCompanyTel() {
		return companyTel;
	}

	/**
	 * 
	 * @param companyTel 公司电话
	 */
    public void setCompanyTel(String companyTel) {
		this.companyTel = (companyTel== null ? null : companyTel.trim());
	}
	/**
	 *
	 * @return String 公司行政区id
	 */
	public String getRegionId() {
		return regionId;
	}

	/**
	 * 
	 * @param regionId 公司行政区id
	 */
    public void setRegionId(String regionId) {
		this.regionId = (regionId== null ? null : regionId.trim());
	}
	/**
	 *
	 * @return String 地址
	 */
	public String getCompanyAddress() {
		return companyAddress;
	}

	/**
	 * 
	 * @param companyAddress 地址
	 */
    public void setCompanyAddress(String companyAddress) {
		this.companyAddress = (companyAddress== null ? null : companyAddress.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "CompanyEntity{"+
		"companyName='"+companyName+'\''+
		"companyTel='"+companyTel+'\''+
		"regionId='"+regionId+'\''+
		"companyAddress='"+companyAddress+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
