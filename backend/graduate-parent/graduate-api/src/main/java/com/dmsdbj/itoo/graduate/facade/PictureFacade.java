package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.PictureEntity;

import java.util.List;

/**
 *@author 徐玲博
 *create: 2017-11-26 16:14:54.
 */
public interface PictureFacade {
      /**
      * @author kongwy
      * @param id
      * @return PictureEntity
      */
     PictureEntity findById(String id);

    /**
     * 个人上传图片-徐玲博-2018-1-13 11:24:27
     * @param fileUrl 图片的全路径 例如‘E:\\cat.jpg’
     * @param fileExtName 图片扩展名 例如‘jpg、png’等
     * @return 图片在fastDFS的路径
     */
    String uploadPersonPicture(String fileUrl,String fileExtName);

    /**
     * 保存上传图片的路径（一张）-宋学孟-2018年1月14日
     * @param pictureUrl 图片url
     * @param keyId 关联主键id
     * @return 影响行数
     */
    int addPicture(String pictureUrl, String keyId);

    /**
     * 保存上传图片的路径（一张或多张）-宋学孟-2018年1月14日
     * @param pictureEntityList 图片实体list
     * @return 影响行数
     */
    int addPictureUrl(List<PictureEntity> pictureEntityList);

    /**
     * 根据相应的id，查询对应的实体（包含url）-宋学孟-2018年1月14日
     *
     * @param keyId 关联主键id
     * @return 图片url列表
     */
    List<String> selectPictureById(String keyId);

    /**
     * 根据与图片表有关表的id，删除图片-徐玲博-2018-2-9 16:16:23
     * @param keyId 例如个人表的userid
     * @param operator 操作人
     * @return 受影行
     */
    int deletePictureById(String keyId,String operator);
}
