package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jplus.hyberbin.excel.annotation.ExcelModelConfig;
import org.jplus.hyberbin.excel.annotation.Lang;
import org.jplus.hyberbin.excel.annotation.input.InputDicConfig;
import java.io.Serializable;
import java.util.Date;
/**
 * Created by bobo on 2018/1/15.
 */
@ExcelModelConfig
public class StudentEntity  extends BaseEntity implements Serializable{

    /** 户口所在地*/
    private String accountAddress;
    /**学号*/
    @Lang(value = "学号")
    private String code;
    /** 邮箱*/
    @Lang(value = "邮箱")
    private String email;
    /**入学时间*/
    @Lang(value = "入学时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date entranceDate;
    /**身份证号*/
    @Lang(value = "身份证号")
    private String identityCardId;
    /**姓名*/
    @Lang(value = "姓名")
    private String name;
    /**民族*/
    @Lang(value = "民族")
    private String nation;
    /**籍贯*/
    private String nativePlacenativePlace;
    /**照片*/
    @Lang(value = "照片")
    private String pictrue;
    /**政治面貌*/
    private String politicalStatus;
    /**性别*/
    @Lang(value = "性别")
    private String sex;
    /**学生状态（休学、退学）*/
    private String status;
    /**电话号码*/
    @Lang(value="电话")
    private String telNum;
    /**毕业院校*/
    private String graduatedSchool;
    /**是否可以毕业(0/1 否/是)*/
    private Integer isGraduate;
    /**无法毕业理由*/
    private String noGraduateReason;
    /**生源地*/
    private String originalPlace;
    /**曾用名*/
    private String usedName;
    /**行政班id*/
    @InputDicConfig(dicCode = "classesId")//Excel导入的配置
    @Lang(value = "班级")
    private String classesId;
    /**专业id*/
    @InputDicConfig(dicCode = "professionId")//Excel导入的配置
    @Lang(value = "专业")
    private String professionId;
    /**是否报到（0/1 否/是）*/
    private Integer isRegistry;
    /**缴费号*/
    @Lang(value="缴费号")
    private String payNumber;
    /**宿舍id*/
    @InputDicConfig(dicCode = "roomId")//Excel导入的配置
    @Lang(value = "宿舍")
    private String roomId;
    /**加入团 党的时间*/
    private String joinPartyDate;

    /**
     *
     * 微信openid
     */
    private String openId;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    /**
     *
     * 年级id（从字典表获取年级信息）
     */
    private String gradeId;

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getJoinPartyDate() {
        return joinPartyDate;
    }

    public void setJoinPartyDate(String joinPartyDate) {
        this.joinPartyDate = joinPartyDate;
    }

    /**
     * @return String
     */
    public String getAccountAddress() {
        return accountAddress;
    }

    /**
     * @param accountAddress
     */
    public void setAccountAddress(String accountAddress) {
        this.accountAddress = (accountAddress == null ? null : accountAddress.trim());
    }

    /**
     * @return String
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = (code == null ? null : code.trim());
    }

    /**
     * @return String
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = (email == null ? null : email.trim());
    }

    /**
     * @return Date
     */
    public Date getEntranceDate() {
        return entranceDate;
    }

    /**
     * @param entranceDate
     */
    public void setEntranceDate(Date entranceDate) {
        this.entranceDate = entranceDate;
    }

    /**
     * @return String
     */
    public String getIdentityCardId() {
        return identityCardId;
    }

    /**
     * @param identityCardId
     */
    public void setIdentityCardId(String identityCardId) {
        this.identityCardId = (identityCardId == null ? null : identityCardId.trim());
    }

    /**
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = (name == null ? null : name.trim());
    }

    /**
     * @return String
     */
    public String getNation() {
        return nation;
    }

    /**
     * @param nation
     */
    public void setNation(String nation) {
        this.nation = (nation == null ? null : nation.trim());
    }

    /**
     * @return String
     */
    public String getNativePlacenativePlace() {
        return nativePlacenativePlace;
    }

    /**
     * @param nativePlacenativePlace
     */
    public void setNativePlacenativePlace(String nativePlacenativePlace) {
        this.nativePlacenativePlace = (nativePlacenativePlace == null ? null : nativePlacenativePlace.trim());
    }

    /**
     * @return String
     */
    public String getPictrue() {
        return pictrue;
    }

    /**
     * @param pictrue
     */
    public void setPictrue(String pictrue) {
        this.pictrue = (pictrue == null ? null : pictrue.trim());
    }

    /**
     * @return String
     */
    public String getPoliticalStatus() {
        return politicalStatus;
    }

    /**
     * @param politicalStatus
     */
    public void setPoliticalStatus(String politicalStatus) {
        this.politicalStatus = (politicalStatus == null ? null : politicalStatus.trim());
    }

    /**
     * @return String
     */
    public String getSex() {
        return sex;
    }

    /**
     * @param sex
     */
    public void setSex(String sex) {
        this.sex = (sex == null ? null : sex.trim());
    }

    /**
     * @return String
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = (status == null ? null : status.trim());
    }

    /**
     * @return String
     */
    public String getTelNum() {
        return telNum;
    }

    /**
     * @param telNum
     */
    public void setTelNum(String telNum) {
        this.telNum = (telNum == null ? null : telNum.trim());
    }

    /**
     * @return String
     */
    public String getGraduatedSchool() {
        return graduatedSchool;
    }

    /**
     * @param graduatedSchool
     */
    public void setGraduatedSchool(String graduatedSchool) {
        this.graduatedSchool = (graduatedSchool == null ? null : graduatedSchool.trim());
    }

    /**
     * @return Integer
     */
    public Integer getIsGraduate() {
        return isGraduate;
    }

    /**
     * @param isGraduate
     */
    public void setIsGraduate(Integer isGraduate) {
        this.isGraduate = isGraduate;
    }

    /**
     * @return String
     */
    public String getNoGraduateReason() {
        return noGraduateReason;
    }

    /**
     * @param noGraduateReason
     */
    public void setNoGraduateReason(String noGraduateReason) {
        this.noGraduateReason = (noGraduateReason == null ? null : noGraduateReason.trim());
    }

    /**
     * @return String
     */
    public String getOriginalPlace() {
        return originalPlace;
    }

    /**
     * @param originalPlace
     */
    public void setOriginalPlace(String originalPlace) {
        this.originalPlace = (originalPlace == null ? null : originalPlace.trim());
    }

    /**
     * @return String
     */
    public String getUsedName() {
        return usedName;
    }

    /**
     * @param usedName
     */
    public void setUsedName(String usedName) {
        this.usedName = (usedName == null ? null : usedName.trim());
    }

    /**
     * @return String
     */
    public String getClassesId() {
        return classesId;
    }

    /**
     * @param classesId
     */
    public void setClassesId(String classesId) {
        this.classesId = (classesId == null ? null : classesId.trim());
    }

    /**
     * @return String
     */
    public String getProfessionId() {
        return professionId;
    }

    /**
     * @param professionId
     */
    public void setProfessionId(String professionId) {
        this.professionId = (professionId == null ? null : professionId.trim());
    }

    /**
     * @return Integer
     */
    public Integer getIsRegistry() {
        return isRegistry;
    }

    /**
     * @param isRegistry
     */
    public void setIsRegistry(Integer isRegistry) {
        this.isRegistry = isRegistry;
    }

    /**
     * @return String
     */
    public String getPayNumber() {
        return payNumber;
    }

    /**
     * @param payNumber
     */
    public void setPayNumber(String payNumber) {
        this.payNumber = (payNumber == null ? null : payNumber.trim());
    }

    /**
     * @return String
     */
    public String getRoomId() {
        return roomId;
    }

    /**
     * @param roomId
     */
    public void setRoomId(String roomId) {
        this.roomId = (roomId == null ? null : roomId.trim());
    }

    @Override
    public String toString() {
        return "StudentEntity{" +
                "accountAddress='" + accountAddress + '\'' +
                "code='" + code + '\'' +
                "email='" + email + '\'' +
                "entranceDate='" + entranceDate + '\'' +
                "identityCardId='" + identityCardId + '\'' +
                "name='" + name + '\'' +
                "nation='" + nation + '\'' +
                "nativePlacenativePlace='" + nativePlacenativePlace + '\'' +
                "pictrue='" + pictrue + '\'' +
                "politicalStatus='" + politicalStatus + '\'' +
                "sex='" + sex + '\'' +
                "status='" + status + '\'' +
                "telNum='" + telNum + '\'' +
                "graduatedSchool='" + graduatedSchool + '\'' +
                "isGraduate='" + isGraduate + '\'' +
                "noGraduateReason='" + noGraduateReason + '\'' +
                "originalPlace='" + originalPlace + '\'' +
                "usedName='" + usedName + '\'' +
                "classesId='" + classesId + '\'' +
                "professionId='" + professionId + '\'' +
                "isRegistry='" + isRegistry + '\'' +
                "payNumber='" + payNumber + '\'' +
                "roomId='" + roomId + '\'' +
                '}';
    }
}
