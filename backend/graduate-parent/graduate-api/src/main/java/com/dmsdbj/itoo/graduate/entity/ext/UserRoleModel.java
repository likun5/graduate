package com.dmsdbj.itoo.graduate.entity.ext;

import java.io.Serializable;

/**
 * Created by sxmty on 2018/1/18.
 */
public class UserRoleModel implements Serializable {

    String userCode;
    String userId;
    String password;
    String userRealName;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserRealName() {
        return userRealName;
    }

    public void setUserRealName(String userRealName) {
        this.userRealName = userRealName;
    }
}
