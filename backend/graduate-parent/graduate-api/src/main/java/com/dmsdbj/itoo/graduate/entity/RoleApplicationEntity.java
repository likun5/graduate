package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class RoleApplicationEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 资源id
	 */
	private String applicationId;
	/**
	 *
	 * 角色id
	 */
	private String roleId;

	/**
	 *
	 * @return String 资源id
	 */
	public String getApplicationId() {
		return applicationId;
	}

	/**
	 * 
	 * @param applicationId 资源id
	 */
    public void setApplicationId(String applicationId) {
		this.applicationId = (applicationId== null ? null : applicationId.trim());
	}
	/**
	 *
	 * @return String 角色id
	 */
	public String getRoleId() {
		return roleId;
	}

	/**
	 * 
	 * @param roleId 角色id
	 */
    public void setRoleId(String roleId) {
		this.roleId = (roleId== null ? null : roleId.trim());
	}
	
	@Override
    public String toString() {
		return "RoleApplicationEntity{"+
		"applicationId='"+applicationId+'\''+
		"roleId='"+roleId+'\''+
		'}';
	}
}
