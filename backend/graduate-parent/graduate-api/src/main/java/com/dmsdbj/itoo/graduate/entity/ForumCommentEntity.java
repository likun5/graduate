package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class ForumCommentEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 论坛id
	 */
	private String forumId;
	/**
	 *
	 * 评论内容
	 */
	private String content;
	/**
	 *
	 * 操作人id
	 */
	private String operatorid;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return String 论坛id
	 */
	public String getForumId() {
		return forumId;
	}

	/**
	 * 
	 * @param forumId 论坛id
	 */
    public void setForumId(String forumId) {
		this.forumId = (forumId== null ? null : forumId.trim());
	}
	/**
	 *
	 * @return String 评论内容
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 
	 * @param content 评论内容
	 */
    public void setContent(String content) {
		this.content = (content== null ? null : content.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorid() {
		return operatorid;
	}

	/**
	 * 
	 * @param operatorid 操作人id
	 */
    public void setOperatorid(String operatorid) {
		this.operatorid = (operatorid== null ? null : operatorid.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "ForumCommentEntity{"+
		"forumId='"+forumId+'\''+
		"content='"+content+'\''+
		"operatorid='"+operatorid+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
