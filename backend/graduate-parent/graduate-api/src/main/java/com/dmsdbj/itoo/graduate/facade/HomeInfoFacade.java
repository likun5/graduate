package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.HomeInfoEntity;
import com.dmsdbj.itoo.graduate.entity.ext.HomeInfoModel;
import com.github.pagehelper.PageInfo;

/**
 * @author : 徐玲博
 * create : 2017-11-21 21:38:10
 */
public interface HomeInfoFacade {
    /**
     * @param id 主键id
     * @return HomeInfoEntity 家庭实体
     * @author kongwy
     */
    HomeInfoEntity findById(String id);

    /**
     * 添加家庭成员-徐玲博-2017-11-20 11:03:07
     *
     * @param homeInfoEntity 家庭成员信息实体
     * @return 受影响行
     */
    int addHomeInfo(HomeInfoEntity homeInfoEntity);

    /**
     * 删除家庭成员信息-徐玲博-2017-11-20 11:03:48
     *
     * @param id 家庭成员id
     * @param operator  操作人
     * @return 受影响行数
     */
    int deleteHomeInfo(String id, String operator );

    /**
     * 查询家庭信息列表-徐玲博-2017-11-20 11:04:24
     * @param userId 当前登录的用户id
     * @param page 页数
     * @param pageSize 每页大小
     * @return  家庭信息列表
     */
    PageInfo<HomeInfoModel> selectHomePersonInfo(String userId, int page, int pageSize);

    /**
     * 修改家庭成员-徐玲博-2017-11-20 11:06:56
     * @param homeInfoEntity 家庭成员实体
     * @return 受影响行
     */
    int updateHomeInfo(HomeInfoEntity homeInfoEntity);
}
