package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class BrowseEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 浏览人id
	 */
	private String browsePersonId;
	/**
	 *
	 * 通知id
	 */
	private String noticeId;
	/**
	 *
	 * 是否浏览（0/1 未浏览/已浏览）
	 */
	private String isBrowse;
	/**
	 *
	 * 浏览时间
	 */
	private String browseTime;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return String 浏览人id
	 */
	public String getBrowsePersonId() {
		return browsePersonId;
	}

	/**
	 * 
	 * @param browsePersonId 浏览人id
	 */
    public void setBrowsePersonId(String browsePersonId) {
		this.browsePersonId = (browsePersonId== null ? null : browsePersonId.trim());
	}
	/**
	 *
	 * @return String 通知id
	 */
	public String getNoticeId() {
		return noticeId;
	}

	/**
	 * 
	 * @param noticeId 通知id
	 */
    public void setNoticeId(String noticeId) {
		this.noticeId = (noticeId== null ? null : noticeId.trim());
	}
	/**
	 *
	 * @return String 是否浏览（0/1 未浏览/已浏览）
	 */
	public String getIsBrowse() {
		return isBrowse;
	}

	/**
	 * 
	 * @param isBrowse 是否浏览（0/1 未浏览/已浏览）
	 */
    public void setIsBrowse(String isBrowse) {
		this.isBrowse = (isBrowse== null ? null : isBrowse.trim());
	}
	/**
	 *
	 * @return String 浏览时间
	 */
	public String getBrowseTime() {
		return browseTime;
	}

	/**
	 * 
	 * @param browseTime 浏览时间
	 */
    public void setBrowseTime(String browseTime) {
		this.browseTime = (browseTime== null ? null : browseTime.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "BrowseEntity{"+
		"browsePersonId='"+browsePersonId+'\''+
		"noticeId='"+noticeId+'\''+
		"isBrowse='"+isBrowse+'\''+
		"browseTime='"+browseTime+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
