package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.graduate.entity.NoticeEntity;

import java.io.Serializable;

/**
 * @author hgt
 * create: 2017-12-06 15:14:54.
 */

public class NoticeNoticeCategoryModel extends NoticeEntity implements Serializable {
    private String columnName;

    /**
     * @return String
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * @param columnName 分类名称
     */
    public void setColumnName(String columnName) {
        this.columnName = (columnName== null ? null : columnName.trim());
    }

    @Override
    public String toString(){
        return "NoticeNoticeCategoryModel{columnName="+columnName +",super="+super.toString()+"}";
    }
}
