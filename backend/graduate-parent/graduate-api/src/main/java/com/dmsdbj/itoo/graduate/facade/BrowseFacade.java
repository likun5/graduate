package com.dmsdbj.itoo.graduate.facade;

        import com.dmsdbj.itoo.graduate.entity.BrowseEntity;
        import com.dmsdbj.itoo.graduate.entity.DictionaryEntity;
        import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;
        import com.dmsdbj.itoo.graduate.entity.ext.PeriodPersonModel;
        import com.dmsdbj.itoo.graduate.entity.ext.PersonGradeModel;
        import com.dmsdbj.itoo.graduate.entity.ext.PersonInfoModel;
        import com.github.pagehelper.PageInfo;

        import java.util.List;

/**
 *@author haoguiting
 *create: 2017-11-26 16:14:54.
 */
public interface BrowseFacade {
    /**
     * @author kongwy
     * @param id 浏览记录Id
     * @return BrowseEntity
     */
    BrowseEntity findById(String id);

    /**
     * 更新浏览记录-haoguiting-2017年12月3日
     * @param noticeId 通知id
     * @param browsePersonId  浏览人id
     * @return 受影响行
     */
    int updateBrowseIsBrowse(String noticeId, String browsePersonId);

    /**
     * 根据通知ID、是否浏览查询浏览人信息-haoguiting-2017年12月3日
     * @param noticeId 通知id
     * @param isBrowse 是否浏览
     * @return 浏览人list列表
     */
    List<PersonInfoModel> selectBrowseByNoticeId(String noticeId, String isBrowse);

    /**
     * 根据浏览人ID查询未浏览记录-haoguiting-2017年12月3日
     * @param browsePersonId 浏览人id
     * @return 受影响行
     */
    int selectNoBrowseByBrowsePersonId(String browsePersonId);

    /**
     * 根据浏览人ID查询已浏览记录-haoguiting-2017年12月3日
     * @param browsePersonId 浏览人id
     * @return 受影响行
     */
    int selectBrowsedByBrowsePersonId(String browsePersonId);

    /**
     * 分页-根据通知ID、是否浏览查询浏览人信息-haoguiting-2018年01月11日
     * @param noticeId  通知Id
     * @param isBrowse  是否浏览
     * @param page 页码
     * @param  pageSize 页大小
     * @return 浏览人list
     */
    PageInfo<PersonalInfoEntity> PageSelectBrowseByNoticeId(String noticeId, String isBrowse ,int page, int pageSize);

    /**
     * 查询所有期数及对应期数人员 haoguiting -2018年1月14日
     * @return 期数及人员list列表
     */
    List<PeriodPersonModel> findPeriodPersonModels();

    /**
     * 根据通知ID查询全部浏览人-haoguiting-2017-12-5 14:15:45 selectAllBrowserByNoticeId
     * @param noticeId 通知id
     * @return 受影响行
     */
    List<PersonalInfoEntity> selectAllBrowserByNoticeId(String noticeId);

    /**
     * 查询全部接受人员（树形结构）-haoguiting-2018-1-30
     * @return 接受人list
     */
    List<PersonGradeModel> selectAllBrowsersIncludeGradeName();

    /**
     * 查询全部期数（构建树形结构）--haoguiting-2018-1-30
     * @param typeCode  字典类别
     * @return 期数list
     */
    List<DictionaryEntity> selectAllGrade(String typeCode);
}
