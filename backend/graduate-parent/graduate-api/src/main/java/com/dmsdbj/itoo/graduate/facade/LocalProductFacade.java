package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.LocalProductEntity;
import java.util.List;

/**
 *@author sxm
 *create: 2017-11-15 15:47:40.
 */
public interface LocalProductFacade {
      /**
      * @author kongwy
      * @param id
      * @return LocalProductEntity
      */
     LocalProductEntity findById(String id);


    /**
     *查询所有特产信息
     *@param
     *@return List<LocalProductEntity> 特产实体集合
     */
    List<LocalProductEntity> queryLocalProduct();


    /**
     *添加特产
     *@param
     *@return List<LocalProductEntity> 特产实体集合
     */
    boolean addLocalProduct(LocalProductEntity localProductEntity);

    /**
     *删除特产
     *@param
     *@return productIds 特产集合
     */
    boolean deleteLocalProduct(List<String> productIds);

    /**
     *更新特产
     *@param
     *@return List<LocalProductEntity> 特产实体集合
     */
    int updateLocalProduct(LocalProductEntity localProductEntity);
}
