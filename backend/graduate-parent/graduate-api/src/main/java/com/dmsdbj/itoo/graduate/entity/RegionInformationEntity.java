package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class RegionInformationEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 信息点id
	 */
	private String poiId;
	/**
	 *
	 * 经度
	 */
	private String lng;
	/**
	 *
	 * 纬度
	 */
	private String lat;
	/**
	 *
	 * 地域信息内容
	 */
	private String regionAddress;
	/**
	 *
	 * 地域类型id
	 */
	private String dictionaryAddressId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return String 信息点id
	 */
	public String getPoiId() {
		return poiId;
	}

	/**
	 * 
	 * @param poiId 信息点id
	 */
    public void setPoiId(String poiId) {
		this.poiId = (poiId== null ? null : poiId.trim());
	}
	/**
	 *
	 * @return String 经度
	 */
	public String getLng() {
		return lng;
	}

	/**
	 * 
	 * @param lng 经度
	 */
    public void setLng(String lng) {
		this.lng = (lng== null ? null : lng.trim());
	}
	/**
	 *
	 * @return String 纬度
	 */
	public String getLat() {
		return lat;
	}

	/**
	 * 
	 * @param lat 纬度
	 */
    public void setLat(String lat) {
		this.lat = (lat== null ? null : lat.trim());
	}
	/**
	 *
	 * @return String 地域信息内容
	 */
	public String getRegionAddress() {
		return regionAddress;
	}

	/**
	 * 
	 * @param regionAddress 地域信息内容
	 */
    public void setRegionAddress(String regionAddress) {
		this.regionAddress = (regionAddress== null ? null : regionAddress.trim());
	}
	/**
	 *
	 * @return String 地域类型id
	 */
	public String getDictionaryAddressId() {
		return dictionaryAddressId;
	}

	/**
	 * 
	 * @param dictionaryAddressId 地域类型id
	 */
    public void setDictionaryAddressId(String dictionaryAddressId) {
		this.dictionaryAddressId = (dictionaryAddressId== null ? null : dictionaryAddressId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "RegionInformationEntity{"+
		"poiId='"+poiId+'\''+
		"lng='"+lng+'\''+
		"lat='"+lat+'\''+
		"regionAddress='"+regionAddress+'\''+
		"dictionaryAddressId='"+dictionaryAddressId+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
