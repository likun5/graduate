package com.dmsdbj.itoo.graduate.entity.ext;
import com.dmsdbj.itoo.graduate.entity.BrowseEntity;
import com.dmsdbj.itoo.graduate.entity.NoticeEntity;
import java.io.Serializable;
import java.util.List;

public class NoticeBrowseModel extends NoticeEntity implements Serializable {
    /**
     *
     * 浏览人id
     */
    private List<BrowseEntity> browsePersons;
    /**
     *
     * @return String
     */
    public List<BrowseEntity> getBrowsePersons() {
        return browsePersons;
    }

    /**
     *
     * @param browsePersons 浏览人
     */
    public void setBrowsePersons(List<BrowseEntity> browsePersons) {
//        this.browsePersonIds = (browsePersonId== null ? null : browsePersonId.trim());
        this.browsePersons = browsePersons;
    }
}
