package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class AdministrativeRegionEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 父id
	 */
	private Long pid;
	/**
	 *
	 * 简称
	 */
	private String shortname;
	/**
	 *
	 * 名称
	 */
	private String name;
	/**
	 *
	 * 全称
	 */
	private String mergerName;
	/**
	 *
	 * 层级 1 2 3省市区县
	 */
	private Integer level;
	/**
	 *
	 * 拼音
	 */
	private String pinyin;
	/**
	 *
	 * 长途区号
	 */
	private String code;
	/**
	 *
	 * 邮编
	 */
	private String zipCode;
	/**
	 *
	 * 首字母
	 */
	private String initial;
	/**
	 *
	 * 经度
	 */
	private String lng;
	/**
	 *
	 * 纬度
	 */
	private String lat;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return Long 父id
	 */
	public Long getPid() {
		return pid;
	}

	/**
	 * 
	 * @param pid 父id
	 */
    public void setPid(Long pid) {
		this.pid = pid;
	}
	/**
	 *
	 * @return String 简称
	 */
	public String getShortname() {
		return shortname;
	}

	/**
	 * 
	 * @param shortname 简称
	 */
    public void setShortname(String shortname) {
		this.shortname = (shortname== null ? null : shortname.trim());
	}
	/**
	 *
	 * @return String 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name 名称
	 */
    public void setName(String name) {
		this.name = (name== null ? null : name.trim());
	}
	/**
	 *
	 * @return String 全称
	 */
	public String getMergerName() {
		return mergerName;
	}

	/**
	 * 
	 * @param mergerName 全称
	 */
    public void setMergerName(String mergerName) {
		this.mergerName = (mergerName== null ? null : mergerName.trim());
	}
	/**
	 *
	 * @return Integer 层级 1 2 3省市区县
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * 
	 * @param level 层级 1 2 3省市区县
	 */
    public void setLevel(Integer level) {
		this.level = level;
	}
	/**
	 *
	 * @return String 拼音
	 */
	public String getPinyin() {
		return pinyin;
	}

	/**
	 * 
	 * @param pinyin 拼音
	 */
    public void setPinyin(String pinyin) {
		this.pinyin = (pinyin== null ? null : pinyin.trim());
	}
	/**
	 *
	 * @return String 长途区号
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 
	 * @param code 长途区号
	 */
    public void setCode(String code) {
		this.code = (code== null ? null : code.trim());
	}
	/**
	 *
	 * @return String 邮编
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * 
	 * @param zipCode 邮编
	 */
    public void setZipCode(String zipCode) {
		this.zipCode = (zipCode== null ? null : zipCode.trim());
	}
	/**
	 *
	 * @return String 首字母
	 */
	public String getInitial() {
		return initial;
	}

	/**
	 * 
	 * @param initial 首字母
	 */
    public void setInitial(String initial) {
		this.initial = (initial== null ? null : initial.trim());
	}
	/**
	 *
	 * @return String 经度
	 */
	public String getLng() {
		return lng;
	}

	/**
	 * 
	 * @param lng 经度
	 */
    public void setLng(String lng) {
		this.lng = (lng== null ? null : lng.trim());
	}
	/**
	 *
	 * @return String 纬度
	 */
	public String getLat() {
		return lat;
	}

	/**
	 * 
	 * @param lat 纬度
	 */
    public void setLat(String lat) {
		this.lat = (lat== null ? null : lat.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "AdministrativeRegionEntity{"+
		"pid='"+pid+'\''+
		"shortname='"+shortname+'\''+
		"name='"+name+'\''+
		"mergerName='"+mergerName+'\''+
		"level='"+level+'\''+
		"pinyin='"+pinyin+'\''+
		"code='"+code+'\''+
		"zipCode='"+zipCode+'\''+
		"initial='"+initial+'\''+
		"lng='"+lng+'\''+
		"lat='"+lat+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
