package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class PictureEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 与图片表有关系的表id，例如userId
	 */
	private String keyId;
	/**
	 *
	 * 图片类型id，从字典表查个人、特产、公司等
	 */
	private String pictureTypeId;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;
	/**
	 *
	 * 图片url
	 */
	private String pictureUrl;

	/**
	 *
	 * @return String 与图片表有关系的表id，例如userId
	 */
	public String getKeyId() {
		return keyId;
	}

	/**
	 * 
	 * @param keyId 与图片表有关系的表id，例如userId
	 */
    public void setKeyId(String keyId) {
		this.keyId = (keyId== null ? null : keyId.trim());
	}
	/**
	 *
	 * @return String 图片类型id，从字典表查个人、特产、公司等
	 */
	public String getPictureTypeId() {
		return pictureTypeId;
	}

	/**
	 * 
	 * @param pictureTypeId 图片类型id，从字典表查个人、特产、公司等
	 */
    public void setPictureTypeId(String pictureTypeId) {
		this.pictureTypeId = (pictureTypeId== null ? null : pictureTypeId.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	/**
	 *
	 * @return String 图片url
	 */
	public String getPictureUrl() {
		return pictureUrl;
	}

	/**
	 * 
	 * @param pictureUrl 图片url
	 */
    public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = (pictureUrl== null ? null : pictureUrl.trim());
	}
	
	@Override
    public String toString() {
		return "PictureEntity{"+
		"keyId='"+keyId+'\''+
		"pictureTypeId='"+pictureTypeId+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		"pictureUrl='"+pictureUrl+'\''+
		'}';
	}
}
