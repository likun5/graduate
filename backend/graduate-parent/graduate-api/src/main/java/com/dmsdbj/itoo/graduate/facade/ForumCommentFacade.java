package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.ForumCommentEntity;

/**
 *@author ls
 *create: 2017-12-27 09:54:26.
 */
public interface ForumCommentFacade {
      /**
      * @author kongwy
      * @param id
      * @return ForumCommentEntity
      */
     ForumCommentEntity findById(String id);
}
