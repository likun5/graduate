package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class UserRoleEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 用户ID
	 */
	private String userId;
	/**
	 *
	 * 角色ID
	 */
	private String roleId;

	/**
	 *
	 * @return String 用户ID
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * 
	 * @param userId 用户ID
	 */
    public void setUserId(String userId) {
		this.userId = (userId== null ? null : userId.trim());
	}
	/**
	 *
	 * @return String 角色ID
	 */
	public String getRoleId() {
		return roleId;
	}

	/**
	 * 
	 * @param roleId 角色ID
	 */
    public void setRoleId(String roleId) {
		this.roleId = (roleId== null ? null : roleId.trim());
	}
	
	@Override
    public String toString() {
		return "UserRoleEntity{"+
		"userId='"+userId+'\''+
		"roleId='"+roleId+'\''+
		'}';
	}
}
