package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class NoticeEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 通知标题
	 */
	private String title;
	/**
	 *
	 * 发布人id
	 */
	private String publisherId;
	/**
	 *
	 * 栏目id
	 */
	private String columnId;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;
	/**
	 *
	 * 通知内容
	 */
	private String noticeContent;

	/**
	 *
	 * @return String 通知标题
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param title 通知标题
	 */
    public void setTitle(String title) {
		this.title = (title== null ? null : title.trim());
	}
	/**
	 *
	 * @return String 发布人id
	 */
	public String getPublisherId() {
		return publisherId;
	}

	/**
	 * 
	 * @param publisherId 发布人id
	 */
    public void setPublisherId(String publisherId) {
		this.publisherId = (publisherId== null ? null : publisherId.trim());
	}
	/**
	 *
	 * @return String 栏目id
	 */
	public String getColumnId() {
		return columnId;
	}

	/**
	 * 
	 * @param columnId 栏目id
	 */
    public void setColumnId(String columnId) {
		this.columnId = (columnId== null ? null : columnId.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	/**
	 *
	 * @return String 通知内容
	 */
	public String getNoticeContent() {
		return noticeContent;
	}

	/**
	 * 
	 * @param noticeContent 通知内容
	 */
    public void setNoticeContent(String noticeContent) {
		this.noticeContent = (noticeContent== null ? null : noticeContent.trim());
	}
	
	@Override
    public String toString() {
		return "NoticeEntity{"+
		"title='"+title+'\''+
		"publisherId='"+publisherId+'\''+
		"columnId='"+columnId+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		"noticeContent='"+noticeContent+'\''+
		'}';
	}
}
