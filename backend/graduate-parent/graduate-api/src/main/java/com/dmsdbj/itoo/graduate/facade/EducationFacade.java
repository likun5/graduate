package com.dmsdbj.itoo.graduate.facade;

import com.dmsdbj.itoo.graduate.entity.EducationEntity;
import com.dmsdbj.itoo.graduate.entity.ext.EducationModel;
import com.dmsdbj.itoo.graduate.entity.ext.PersonalEducationEntity;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 *@author 徐玲博
 *create: 2017-11-26 16:14:54.
 */
public interface EducationFacade {
    /**
     * 根据ID查找学历
     * @param id 学历ID
     * @return 学历实体
     */
     EducationEntity findById(String id);

    /**
     * 分页查找所有学历
     * @param page 第几页
     * @param pageSize 每页显示数量
     * @return 带分页的学历信息
     */
      PageInfo<EducationEntity> findAll(int page, int pageSize);

    /**
     * 分页查找某人学历
     * @param page 第几页
     * @param pageSize  每页显示数量
     * @param UserId 用户ID
     * @return 带分页的学历信息
     */
      PageInfo<EducationEntity> findPersonAll(int page, int pageSize, String UserId);

    /**
     * 根据ID查找学历信息
     * @param personId 用户ID
     * @return  学历实体list
     */
    List<EducationEntity> findByPersonId (String personId);

    /**
     * 根据名字查找学历信息
     * @param name 用户姓名
     * @return  学历实体list
     */
    List<EducationEntity> findByName (String name);

    /**
     * 添加学历
     * @param educationEntity 学历实体
     * @return int 0,1：是否成功
     */
     int addEducationEntity(EducationEntity educationEntity );

    /**
     * 删除学历
     * @param educationId 学历ID
     * @param operator 操作员ID
     * @return int 0,1：是否成功
     */
     int deleteEducationEntity( String educationId ,String operator);

    /**
     * 更新学历
     * @param educationModel 学历
     * @returnint 0,1：是否成功
     */
     int updateEducationEntity( EducationModel educationModel);

    /**
     * 删除学历信息
     * @param userId 用户ID
     * @param operator 操作员ID
     * @return  int 0,1：是否成功
     */
      int deleteByPersonId(String userId,String operator);

    /**
     * 根据学历类型查询学历
     * @param certificateType  学历类型
     * @return 学历实体
     */
      List<EducationEntity> findByCertificateType(String certificateType);
    /**
     * 保存上传图片的路径（一张）-于云丽-2018年1月22日
     * @param pictureUrl 图片url
     * @param keyId 学历ID
     * @return 学历实体
     */
    int addPictureUrl(String pictureUrl, String keyId);

    /**
     * 查询提高班所有毕业生的学历信息-唐凌峰-2018年2月7日16:48:38
     * @return List<PersonalEducationEntity>学历实体
     */
    List<PersonalEducationEntity> queryPersonalEducation();


    /**
     * 通过iD查询毕业生学历信息-唐凌峰-2018-2-8 14:49:29
     * @param PersonId 个人ID
     * @return List<PersonalEducationEntity> 学历实体
     */
    List<PersonalEducationEntity>findEducationByPersonId(String PersonId);

    /**
     * 增加学历信息和图片urls
     * @param EducationModelList 学历list
     * @return int 0,1：是否成功
     */
    int addEducationModel(List<EducationModel> EducationModelList);

    /**
     * 根据学历ID查询相应的学历信息和图片urls
     * @param id 学历ID
     * @return  学历信息实体
     */
    EducationModel queryEducationModel(String id);
}

