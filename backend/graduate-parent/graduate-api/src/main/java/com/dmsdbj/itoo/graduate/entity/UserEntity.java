package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class UserEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 密码
	 */
	private String password;
	/**
	 *
	 * 用户（教师和学生）编码
	 */
	private String userCode;
	/**
	 *
	 * 用户（教师和学生）姓名
	 */
	private String userRealName;
	/**
	 *
	 * 邮箱
	 */
	private String email;
	/**
	 *
	 * 电话号码
	 */
	private String telNum;
	/**
	 *
	 * 学校编号
	 */
	private String schoolNo;

	/**
	 *
	 * @return String 密码
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param password 密码
	 */
    public void setPassword(String password) {
		this.password = (password== null ? null : password.trim());
	}
	/**
	 *
	 * @return String 用户（教师和学生）编码
	 */
	public String getUserCode() {
		return userCode;
	}

	/**
	 * 
	 * @param userCode 用户（教师和学生）编码
	 */
    public void setUserCode(String userCode) {
		this.userCode = (userCode== null ? null : userCode.trim());
	}
	/**
	 *
	 * @return String 用户（教师和学生）姓名
	 */
	public String getUserRealName() {
		return userRealName;
	}

	/**
	 * 
	 * @param userRealName 用户（教师和学生）姓名
	 */
    public void setUserRealName(String userRealName) {
		this.userRealName = (userRealName== null ? null : userRealName.trim());
	}
	/**
	 *
	 * @return String 邮箱
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email 邮箱
	 */
    public void setEmail(String email) {
		this.email = (email== null ? null : email.trim());
	}
	/**
	 *
	 * @return String 电话号码
	 */
	public String getTelNum() {
		return telNum;
	}

	/**
	 * 
	 * @param telNum 电话号码
	 */
    public void setTelNum(String telNum) {
		this.telNum = (telNum== null ? null : telNum.trim());
	}
	/**
	 *
	 * @return String 学校编号
	 */
	public String getSchoolNo() {
		return schoolNo;
	}

	/**
	 * 
	 * @param schoolNo 学校编号
	 */
    public void setSchoolNo(String schoolNo) {
		this.schoolNo = (schoolNo== null ? null : schoolNo.trim());
	}
	
	@Override
    public String toString() {
		return "UserEntity{"+
		"password='"+password+'\''+
		"userCode='"+userCode+'\''+
		"userRealName='"+userRealName+'\''+
		"email='"+email+'\''+
		"telNum='"+telNum+'\''+
		"schoolNo='"+schoolNo+'\''+
		'}';
	}
}
