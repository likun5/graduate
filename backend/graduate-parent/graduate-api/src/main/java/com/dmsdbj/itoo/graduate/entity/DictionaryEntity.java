package com.dmsdbj.itoo.graduate.entity;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
/**
 * @author: 冯静姣 
 * create: 2018-03-14 21:14:03.
 */

public class DictionaryEntity extends BaseEntity implements Serializable{
 	
	/**
	 *
	 * 名称
	 */
	private String dictName;
	/**
	 *
	 * 字典类型code
	 */
	private String typeCode;
	/**
	 *
	 * 字典类型名称
	 */
	private String dictTypeName;
	/**
	 *
	 * 操作人id
	 */
	private String operatorId;
	/**
	 *
	 * 时间戳
	 */
	@JsonFormat(
        pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8"
    )
	private Date timestampTime;

	/**
	 *
	 * @return String 名称
	 */
	public String getDictName() {
		return dictName;
	}

	/**
	 * 
	 * @param dictName 名称
	 */
    public void setDictName(String dictName) {
		this.dictName = (dictName== null ? null : dictName.trim());
	}
	/**
	 *
	 * @return String 字典类型code
	 */
	public String getTypeCode() {
		return typeCode;
	}

	/**
	 * 
	 * @param typeCode 字典类型code
	 */
    public void setTypeCode(String typeCode) {
		this.typeCode = (typeCode== null ? null : typeCode.trim());
	}
	/**
	 *
	 * @return String 字典类型名称
	 */
	public String getDictTypeName() {
		return dictTypeName;
	}

	/**
	 * 
	 * @param dictTypeName 字典类型名称
	 */
    public void setDictTypeName(String dictTypeName) {
		this.dictTypeName = (dictTypeName== null ? null : dictTypeName.trim());
	}
	/**
	 *
	 * @return String 操作人id
	 */
	public String getOperatorId() {
		return operatorId;
	}

	/**
	 * 
	 * @param operatorId 操作人id
	 */
    public void setOperatorId(String operatorId) {
		this.operatorId = (operatorId== null ? null : operatorId.trim());
	}
	/**
	 *
	 * @return Date 时间戳
	 */
	public Date getTimestampTime() {
		return timestampTime;
	}

	/**
	 * 
	 * @param timestampTime 时间戳
	 */
    public void setTimestampTime(Date timestampTime) {
		this.timestampTime = timestampTime;
	}
	
	@Override
    public String toString() {
		return "DictionaryEntity{"+
		"dictName='"+dictName+'\''+
		"typeCode='"+typeCode+'\''+
		"dictTypeName='"+dictTypeName+'\''+
		"operatorId='"+operatorId+'\''+
		"timestampTime='"+timestampTime+'\''+
		'}';
	}
}
