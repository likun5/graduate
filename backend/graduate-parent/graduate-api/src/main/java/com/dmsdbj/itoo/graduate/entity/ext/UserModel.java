package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.graduate.entity.PersonalInfoEntity;

import java.io.Serializable;

/**
 @author 袁甜梦
 DESCRIPTION 注册model
 create 2018-03-11
*/

public class UserModel implements Serializable {
    private String id;
    private String userCode;
    private String password;
    private  String userRealName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserRealName() {
        return userRealName;
    }

    public void setUserRealName(String userRealName) {
        this.userRealName = userRealName;
    }
}
