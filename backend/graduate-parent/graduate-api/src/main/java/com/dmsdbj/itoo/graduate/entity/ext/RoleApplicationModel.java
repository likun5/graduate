package com.dmsdbj.itoo.graduate.entity.ext;

import com.dmsdbj.itoo.tool.base.entity.BaseEntity;

import java.io.Serializable;

/**
 * Created by bobo on 2018/1/4.
 */
public class RoleApplicationModel extends BaseEntity implements Serializable {

    //资源名称
    private String applicationName;
    //直接父类资源id
    private String applicationParentId;
    //所有的父类资源id
    private String applicationParentIds;
    //资源URL
    private String applicationUrl;
    //description
    private String description;
    //状态（是否可用）
    private String isState;
    //资源标识
    private String permission;
    //permission_name
    private String permissionName;
    //类型
    private String type;
    //是否是管理模块
    private String isManagedModule;
    //排序id
    private String sortId;
    //资源图标
    private String applicationIcon;


    //角色名称
    private String roleName;
    //角色描述
    private String roleDescribe;

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationParentId() {
        return applicationParentId;
    }

    public void setApplicationParentId(String applicationParentId) {
        this.applicationParentId = applicationParentId;
    }

    public String getApplicationParentIds() {
        return applicationParentIds;
    }

    public void setApplicationParentIds(String applicationParentIds) {
        this.applicationParentIds = applicationParentIds;
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsState() {
        return isState;
    }

    public void setIsState(String isState) {
        this.isState = isState;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsManagedModule() {
        return isManagedModule;
    }

    public void setIsManagedModule(String isManagedModule) {
        this.isManagedModule = isManagedModule;
    }

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public String getApplicationIcon() {
        return applicationIcon;
    }

    public void setApplicationIcon(String applicationIcon) {
        this.applicationIcon = applicationIcon;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDescribe() {
        return roleDescribe;
    }

    public void setRoleDescribe(String roleDescribe) {
        this.roleDescribe = roleDescribe;
    }
}
